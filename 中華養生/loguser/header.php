<?php
if (!isset($_SESSION)) {session_start();}
    //-----已login-----;
    if(isset($_SESSION['MM_UID'])){header("Location:../index.php");
	   exit;}
    //---- 會員指向及解密----;
    if (isset($_GET['ak'])) {$_SESSION['PrevUrl'] = urldecode($_GET['ak']);}
   $dir_s= "../";
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data['titles'];?></title>
    <meta charset="utf-8">
    <meta name="keywords" content="<?php echo $data['keyw'];?>"> 
    <meta name="description" content="<?php echo $data['desc'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../css_fu/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css_fu/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="../css_fu/style1.css" >
    <link rel="stylesheet" type="text/css" href="../css_fu/style-desktop.css" >
    <link rel="shortcut icon" href="../img/la-16x16.png"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js_fu/jquery.min.js"></script>
    <script src="../js_fu/bootstrap.min.js"></script>
    <!-- jQuery 往上面 -->
    <script src="../js_fu/jquery.easing.1.3.js"></script>
    <!-- jQuery 表單驗證 -->
    <script src="../js_fu/jquery.validate.js"></script>
</head>
<body>
<div id="wrap"><!--最外層包裝 //-->
  <div id="header-nav"><!--頁首 bar//--> 
     <div class="container">
       <div class="row">
       </div>
     </div>
  </div>