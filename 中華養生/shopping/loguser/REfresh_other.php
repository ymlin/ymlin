<?php @require_once("../conn/wp-c.php");?>
<?php header("Content-Type:text/html; charset=utf-8");?> 
<?php
    if (!isset($_SESSION)) { session_start();}
/*---- send DB ----*/
function sqldb($SQLdata){
           global $wp_c;
           $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());
}
/*---- uservip DB ID ----*/
function sqldb_vpno(){
           global $wp_c;
    
           $sql_vp = "SELECT Max(m_id) as max_id3 FROM uservip";
           $vps = mysqli_query($wp_c, $sql_vp) or die(mysqli_error());
           $row_vp = mysqli_fetch_assoc($vps);
           $maxid3 = $row_vp['max_id3'] + 1;
             return $maxid3;
}
/*---- uservip USER ID DB----*/
function sql_uids($mname, $mmail, $mpass){
           global $wp_c;
           
             $mpass_md5= md5($mpass);
           $sql_vp = "SELECT m_id FROM uservip WHERE (m_name = '{$mname}' AND m_username = '{$mmail}' AND m_passwd = '{$mpass_md5}')";
           $vps = mysqli_query($wp_c, $sql_vp) or die(mysqli_error());
           $row_vp = mysqli_fetch_assoc($vps);
           $mid = $row_vp['m_id'];
             return $mid;
}
/* ---- uservip DB----*/
function sql_mvp_login($sqlna, $m_mail, $m_pass){
         global $wp_c;
           
           $sql_vp = "SELECT ". $sqlna ." AS dbname FROM uservip WHERE (m_passwd= '{$m_pass}' AND m_username = '{$m_mail}')";
           $vps = mysqli_query($wp_c, $sql_vp) or die(mysqli_error());
           $row_vp = mysqli_fetch_assoc($vps);
           $datana = $row_vp['dbname'];
         return $datana;
}
/*---- uservip USER inser FB data DB----*/
function sql_us_reg_fb($mmail, $mpass, $mname){
           global $wp_c;
           header("Content-Type:text/html; charset=utf-8");
    
            $sql_vp = "SELECT Max(m_id) as max_id3 FROM uservip";
            $vps = mysqli_query($wp_c, $sql_vp) or die(mysqli_error());
            $row_vp = mysqli_fetch_assoc($vps);
            $maxid2 = $row_vp['max_id3'] + 1;
    
           /*-- 寫入會員 --*/
		    $Ovip = "bgl" . $maxid2;
           //--指定正確的時區;
              date_default_timezone_set('Asia/Taipei');
			$m_joinDate= date("Y-m-d");
			$m_level= "member";
            $m_mess1 = '我願意接受來自團購網的各項優惠與團購訊息';
            $m_mess2 = '我已經閱讀並同意團購網聲明的使用者條款';

            //*-- 寫入 user --*/
              $mpass_md5= md5($mpass);
            $insertSQL = ("INSERT INTO uservip (m_NO, m_name, m_username, m_passwd, m_fbid, m_fbmail, m_level, m_Message1, m_Message2, m_joinDate) VALUES ('{$Ovip}', '{$mname}', '{$mmail}', '{$mpass_md5}', '{$mpass}', '{$mmail}', '{$m_level}', '{$m_mess1}', '{$m_mess2}', '{$m_joinDate}')");
            $Result1 = mysqli_query($wp_c, $insertSQL) or die(mysqli_error());
             
           return $maxid2;
}
/*---- uservip USER load FB data DB----*/
function sql_us_login_fb($mmail, $mpass, $mname){
           global $wp_c;
    
            $sql_vp = "SELECT m_id FROM uservip WHERE (m_fbmail = '{$mmail}' AND m_fbid = '{$mpass}')";
            $vps = mysqli_query($wp_c, $sql_vp) or die(mysqli_error());
            $row_vp = mysqli_fetch_assoc($vps);
            $mid = $row_vp['m_id'];
          
         return $mid;
}
/* ---- ucenter uservip DB----*/
function use_uc_mvip($m_name, $m_oldpass, $isuid, $outno){
         include_once("../config.inc.php");
         include_once("../uc_client/client.php");
    
           /*-- ucenter data -- $isuid=2:使用用戶 E-mail登錄*/
          list($uid, $username, $password, $email) = uc_user_login($m_name, $m_oldpass, $isuid);
          if($outno == 1){ return $uid;}
          elseif($outno == 2){ return $email;}
          elseif($outno == 3){ return $password;}
          elseif($outno == 4){ return $username;}
}
/*---- UCenter regedit DB----*/
function use_uc_reg($m_mail, $m_pass, $m_name){
         global $wp_c;
         include_once("fundb_uc.php");
         header("Content-Type:text/html; charset=utf-8");
    
            /*-- ucenter data --*/
          $uid = uc_user_register($m_name, $m_pass, $m_mail);
          return $uid;
}
/*---- NEW Facebook user get subsidy DB----*/
function use_subsky($use_id){
         global $wp_c;
           $sql_sub = "SELECT subda_id FROM subsidy_data WHERE subda_id = '1'";
           $qusub = mysqli_query($wp_c, $sql_sub) or die(mysqli_error());
           $row_sub = mysqli_fetch_assoc($qusub );
           $subdaid = $row_sub['subda_id'];//新會員E-Coupon;
    
            //-- use to subsidy--;
            $sub_pr = '50';
              //--指定正確的時區;
              date_default_timezone_set('Asia/Taipei');
	        $sub_da = date("Y-m-d");
            //*-- 寫入 E-Coupon --*/
            $SQLdata = "INSERT INTO subsidy (subda_id, sub_price, sub_date, m_id)VALUES('{$subdaid}', '{$sub_pr}', '{$sub_da}', '{$use_id}')";
            $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());
    
          return $use_id;
}
//----------------------------------------------------------------------------------/
/*---- regedit inster data ----*/
if ((isset($_GET["fu"])) && ($_GET["fu"] !== '')) {
              $m_mail = base64_decode($_GET["fu"]);//mail;
              $m_name = base64_decode($_GET["n"]);//user name;
              $m_pass = base64_decode($_GET["d"]);

              $midl = sql_us_login_fb($m_mail, $m_pass, $m_name);//user login;
    
              if($midl == ''){
                 $m_no = sql_us_reg_fb($m_mail, $m_pass, $m_name);//user regedit;
                 $mids = sql_us_login_fb($m_mail, $m_pass);//user login;
                    use_subsky($mids);//e-conpen;
                    $_SESSION['MM_UID'] = $mids;
                  
                 header("Location:../index.php");
                 exit;
              }else{
                 $_SESSION['MM_UID'] = $midl;
                 
                 header("Location:../index.php");
                 exit;
              }//NOT have not uservip or usevip;

}//fb user login;
//*---- other web site user login ----*/;
if ((isset($_GET["u"])) && ($_GET["u"] !== "")) {
              $m_mail = base64_decode($_GET["u"]);//mail;
              $m_pass = base64_decode($_GET["p"]);
               $mpass_md5 = md5($m_pass);
              $m_name = sql_mvp_login("m_name", $m_mail, $m_pass);//user name;

              //----- user id產生 ----;
              $use_id = sqldb_vpno();
            
              /*-- 寫入會員 --*/
		      $Ovip = "bgl" . $use_id;
              //--指定正確的時區;
                date_default_timezone_set('Asia/Taipei');
			  $m_joinDate= date("Y-m-d");
			  $m_level= "member";
              $m_mess1 = '我願意接受來自團購網的各項優惠與團購訊息';
              $m_mess2 = '我已經閱讀並同意團購網聲明的使用者條款';

               //*-- 寫入 user --*/
               $insertSQL = ("INSERT INTO uservip (m_NO, m_name, m_username, m_passwd, m_level, m_Message1, m_Message2, m_joinDate) VALUES ('{$Ovip}', '{$m_name}', '{$m_mail}', '{$mpass_md5}', '{$m_level}', '{$m_mess1}', '{$m_mess2}', '{$m_joinDate}')");
               sqldb($insertSQL);
            
              //-- UCenter synlogin --;
                $_SESSION['MM_UID'] = sql_uids($m_name, $m_mail, $m_pass);
    
               /*----- remeber user ----*/  
                  setcookie("name", $m_name, time()+1800*1*1); 
		          setcookie("username", $m_uname, time()+1800*1*1); 
                    //設定使用者名稱的 Cookie <分(秒)*時*天>值1800=30;
		          setcookie("password", $m_pass, time()+1800*1*1); //設定密碼的 Cookie 值;
} //----- data insert END------;
header("Location:../index.php");
exit;
?>