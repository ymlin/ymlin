<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>顯示所有記錄</title>
  </head>
  <body>
    <h1 align="CENTER">零組件報價表</h1>
    <?php
      require_once("dbtools.inc.php");
      $link = create_connection();
      $sql = "SELECT * FROM users ";
      $result = execute_sql($link, $sql);
			
      echo "<table border='1' align='center'><tr align='center'>";
      for ($i = 0; $i < mysqli_num_fields($result); $i++)
        echo "<td>" . mysqli_fetch_field_direct($result, $i)->name. "</td>";
      echo "</tr>";
	
      while ($row = mysqli_fetch_assoc($result))
      {
       echo "<tr>";		
		echo "<td>" . $row["id"] . "</td>";	   
        echo "<td>" . $row["account"] . "</td>";
        echo "<td>" . $row["password"] . "</td>";
        echo "<td>" . $row["name"] . "</td>";
        echo "<td>" . $row["telephone"] . "</td>";	
        echo "<td>" . $row["cellphone"] . "</td>";
        echo "<td>" . $row["Address"] . "</td>";	
        echo "<td>" . $row["admin"] . "</td>";													
        echo "</tr>";							
      }

      echo "</table>" ;
      mysqli_free_result($result);
      mysqli_close($link);
    ?>
  </body>
</html>