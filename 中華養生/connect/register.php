<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>註冊</title>
			<?php include '../Article/index_head.php';?>
		
	</style>
	</head>
	<body>
	<?php include '../Article/nav.php';?>
	<?php include '../Article/header_top.php';?>
		<?php include '../Article/header.php';?>
<div class="container">

<form class='form-horizontal' action="addmember.php" method="post" name="myForm">
    <fieldset>
        <div class="login_bar"><h3>註冊會員</h3></div>
	<div class="col-md-12">
	<div class="content_bar">
        <div class="form-group">
            <label class="col-md-2 control-label" for='account'>帳號</label>
            <div class="controls col-md-6">
                <input id='account' name='account' type='text' size="30" placeholder='請輸入帳號' />
                <p class='help-block'>*請輸入您的e-mail，這將會成為您登入的帳號</p>
            </div>
        </div>
                
        <div class="form-group">
            <label class="col-md-2 control-label" for='password'>密碼</label>
            <div class="controls col-md-6">
                <input id='password' name="password" type="password"  placeholder='請輸入密碼' />
                <p class='help-block'>*請輸入6碼以上的英文或數字</p>
            </div>
        </div>
		<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">確認密碼</label>
          <div class="controls col-md-6">
            <input name="re_password" type="password"  placeholder="***請再次輸入您的密碼" class="input-xlarge"/>
            <p class="help-block">*請再次輸入您的密碼</p>
			</div>
			</div>
			<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">姓名</label>
          <div class="controls col-md-6">
            <input type="text" name="name"  placeholder="*請再次輸入您的姓名" class="input-xlarge"/>
            <p class="help-block">請輸入您的中文或英文姓名</p>
			</div>
			</div>
			<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">手機</label>
          <div class="controls col-md-6">
            <input type="text" name="cellphone"  placeholder="請輸入您的聯絡手機" class="input-xlarge"/>
            <p class="help-block">*請輸入您的聯絡手機號碼</p>
			</div>
			</div>
		
    
    <div class=" form-actions  col-md-offset-2" >
      
	  　<button type="reset" class='btn btn-primary'>重填</button>　
	  <button type='submit' class='btn btn-primary' >送出</button>
    </div>
</form>

	</div> 
</div>	
 </div>
 <?php include 'footer.php';?>
 </body>


<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>