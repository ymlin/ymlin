<?php
	require_once("dbtools.inc.php"); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['remove'])
	{
		$sql = "DELETE FROM `articles` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>文章管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include 'link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px;" >
	<?php include 'admin_top.php';?>
	
	<div class="container">  
		<table  width="1200" border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="17" id="color">
				<form align="center" action = "admin_article_edit.php" align="center">
					<h2 align="center">文章管理</h2>
					<div align="center">
						<button class='btn btn-primary'>新增文章</button>
					</div>
				</form>
			</td> 
			</tr>  

			<tr>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>id</strong></div></td> 
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>標題</strong></div></td> 
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>作者</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>關鍵字</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>標籤</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>文章</strong></div></td>  
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>圖片</strong></div></td>
			<td id="color2" colspan="4"><div align="center" style="white-space:nowrap;"><strong>類型</strong></div></td>  
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>週點擊率</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>點擊率</strong></div></td>  
			<td id="color2" colspan="2"><div align="center" style="white-space:nowrap;"><strong>關聯商品管理</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>最後編輯</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>管理</strong></div></td>  			
			</tr>  
			
			<?php
				$sql = "SELECT * FROM `articles`";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				while($result = $sth->fetchobject())
				{
					echo "<tr>";
					
				?>
					<form action = "admin_article_update.php"  method = "GET">
				<?php
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->id . "</td>";	
						echo "<td align=\"center\" style=\"white-space:nowrap;\" >";
						echo mb_substr($result->title,0,5,"UTF-8");
						if(mb_strlen($result->title,"UTF-8") > 5) echo "...";
						echo "</td>";	
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->editor . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->keyword . "</td>";	
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->tag . "</td>";
						if(!($_GET['preview'] == $result->id))
						{
				?>
							<td align="center">
								<button formaction="admin_article.php"  class='btn btn-primary' type = "submit" name = "preview" value= "<?php echo  $result->id ?>">預覽</button>  
							</td>
				<?php
						}
						else
						{
				?>
							<td align="center" style="white-space:nowrap;">
							<button formaction="admin_article.php"  class='btn btn-primary' type = "submit" name = "preview" value= "">收回</button>
							<br>
							<div >
				<?php			
							echo $result->comment . "</div></td>";
						}
				?>
						<td align="center"><img src="mickey/<?php echo $result->photo_name; ?>" alt="" height="42" width="42" /></td>
				<?php
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->user_type . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->religion . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->season . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->buddha . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->seven_click_rate . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->total_click_rate . "</td>";
						echo "<td align=\"center\">";
						
						$sql2 = "SELECT * FROM `relative_product`" 
								. " WHERE `article_id` = ?";
						$sth2 = $db->prepare($sql2);
						$sth2->execute(array($result->id));
						
						$count = 1;
						while($result2 = $sth2->fetchobject())
						{
							$sql3 = "SELECT * FROM `products`" 
								. " WHERE `id` = ?";
							$sth3 = $db->prepare($sql3);
							$sth3->execute(array($result2->product_id));
							$result3 = $sth3->fetchobject();
							echo "<div style=\"white-space:nowrap;\">" . $count . ".";
							echo mb_substr($result3->name,0,3,"UTF-8");
							if(mb_strlen($result3->name,"UTF-8") > 3) echo "...";
							echo "</td>";
							$count++;
						}
						
						echo "</td><td style=\"white-space:nowrap;\">";
						
						if($_GET['addproduct'])
						{
				?>		
							<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "addprod" value= "" >新增</button> 
				<?php			
						}
						else if($_GET['removeproduct'])
						{
				?>		
							<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "rmprod" value= "" >刪除</button> 
				<?php			
						}
						else
						{
				?>			
							<div style="white-space:nowrap;">
								<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "addproduct" value="1" >新增</button>  
								<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "removeproduct" value="1" onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
							</div>
				<?php		
						}
				?>
						</td>
				<?php	
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->datetime . "</td>";
				?>	
						<td align="center" style="white-space:nowrap;">
							<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
							<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
						</td>  
					</form>
				<?php
				}
			?>
		</table>  
	</div>  
</body>  
</html>