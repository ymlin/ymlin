<?php
	require_once("dbtools.inc.php"); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($password == decryptIt($result->password))
	{
	}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['update'])
	{
		if(!$_GET['account'] || !$_GET['password'])
		{
?>
			<script type='text/javascript' charset="UTF-8">
			alert('帳號或密碼不可為空');
			</script>
<?php		
		}
		else
		{
			$sql = "UPDATE `users` SET account = ?, password = ?, name = ?, user_type = ? " 
				. " WHERE `id` = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['account'], encryptIt($_GET['password']), $_GET['name'], $_GET['type']=="personal"?"個人":"公司",$_GET['update']));
		}
	}
?>

<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>會員資料</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include 'link.php';?> 
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include 'admin_top.php';?>
	<div class="container">
		<form class='form-horizontal' action="admin_info.php" method="GET" name="myForm">
			<div class="login_bar"><h3>會員資料</h3></div>
				<?php
					$sql = "SELECT * FROM `users`" 
							. " WHERE `account` = ?";
					$sth = $db->prepare($sql);
					$sth->execute(array($account));
					
					$result = $sth->fetchobject();
					$depassword = decryptIt($result->password);
				?>
				<div class="col-md-12">
					<div class="content_bar">
						<div class="form-group">
							<label class="col-md-2 control-label" for='account'>帳號</label>
							<div class="controls col-md-4">
								<input id='account' name='account' type='text' size="30" value="<?php echo "{$result->account}";?>" />
								<p class='help-block'>*您的e-mail，這將會成為您登入的帳號</p>
							</div>
						</div>
								
						<div class="form-group">
							<label class="col-md-2 control-label" for='password'>密碼</label>
							<div class="controls col-md-3">
								<input id='password' name="password" type="text" value="<?php echo "{$depassword}";?>" />
								<p class='help-block'>*建議8碼以上的英文或數字</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for='name'>稱呼</label>
							<div class="controls col-md-4">
								<input id='name' name='name' type='text' size="30" value="<?php echo "{$result->name}";?>" />
								<p class='help-block'>*您的姓名或暱稱</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for='type'>類型</label>
							<div class="controls col-md-3">
								<select name="type">
									<option value="personal" <?php if($result->user_type=="個人") echo "selected";?>>個人</option>
									<option value="company" <?php if($result->user_type=="公司") echo "selected";?>>公司</option>
								</select>
								<p class='help-block'>*帳號類型</p>
							<div class="controls col-md-3">
						</div>
						
						<div class=" form-actions  col-md-offset-2" >　
							<button type="submit" name = "update" value="<?php echo "{$result->id}";?>" class="btn btn-primary" >儲存變更</button>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
</body>  
</html>  