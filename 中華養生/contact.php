﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body  style="font-family:Microsoft JhengHei;background: #f5f5f5;">

    <!-- Navigation -->
		<?php include 'header.php';?>
	

    <!-- Page Content -->
	  <div class="container"style="margin-bottom:60px;">
		<div class="row">
		 <div class="col-md-12 ">
		<div class="" style="line-height: 30px;font-size:35px;font-weight:600;letter-spacing:2px;margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
			聯絡我們
		</div>
		</div>
		</div>
		<div class="row">
			 <div class="col-md-12 ">
				   <div class="" style="font-size:20px;line-height: 35px;font-weight:600;letter-spacing:2px;margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
						
							<p class="MsoNormal">
								&nbsp;
							</p>
							<p class="MsoNormal">
								如果您對於中華養生有任何問題如想要各種合作、新聞稿發送等以下需求。<span></span>
							</p>
							<p class="MsoNormal">
								*業務相關合作或有任何養生問題<span></span>
							</p>
							<p class="MsoNormal">
								*您有健康養生的產品<span>(</span>葷食就不好意思了<span>)</span>，想要合作<span></span>
							</p>
							<p class="MsoNormal">
								*您有交換資訊的需求<span></span>
							</p>
							<p class="MsoNormal">
								*想要轉發我們的文章<span></span>
							</p>
							<p class="MsoNormal">
								*其他我們列不出來的需求<span></span>
							</p>
							<p class="MsoNormal">
								歡迎用下面的電子郵件跟我們聯絡。<span></span>
							</p>	
							<p class="MsoNormal">
								<a href="mailto:begood.cht@gmail.com">begood.cht@gmail.com</a>
							</p>
													
						
						
					</div>
				</div>
			 
			</div>
		</div>
	 
   	
	<?php include 'footer.php';?>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
