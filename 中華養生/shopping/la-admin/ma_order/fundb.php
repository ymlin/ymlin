<?php @require_once("../../conn/wp-c.php");?>
<?php
header("Content-Type:text/html; charset=UTF-8");
if (!isset($_SESSION)) {session_start();}
/*---- send DB ----*/;
function sqldb($SQLdata){
           global $wp_c;
           $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());         
}
/*---- product_t DB ----*/;
function sql_order_oth($SQLdata){
           global $wp_c;
    
           $sql_pro = "SELECT * FROM oders_other WHERE od_ot_id = '{$SQLdata}'";
           $Result1 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row = mysqli_fetch_assoc($Result1);
           $prices2= $row['pro_ot_tPrice2'];
             return $prices2;
}
/*---- order_address DB ----*/;
function sql_order_addres($SQLdata){
           global $wp_c;
    
           $sql_pro = "SELECT m_id FROM oders_address WHERE od_id = '{$SQLdata}'";
           $Result1 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row = mysqli_fetch_assoc($Result1);
            $mids= $row['m_id'];
             return $mids;
}
/*---- order DB ----*/;
function sql_orders($db_na, $SQLdata){
           global $wp_c;
    
           $sql_pro = "SELECT " . $db_na . " as ord_name FROM oders WHERE od_id = '{$SQLdata}'";
           $Result1 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row = mysqli_fetch_assoc($Result1);
            $ordname = $row['ord_name'];
             return $ordname;
}
/*---- uservip DB ----*/;
function sql_vip($db_na, $SQLdata){
           global $wp_c;
    
           $sqlna= "m_" . $db_na;
           $sql_VP = "SELECT " . $sqlna . " as mvp_name FROM uservip WHERE m_id = '{$SQLdata}'";
           $us_VP = mysqli_query($wp_c, $sql_VP) or die(mysqli_error());
           $rows_v = mysqli_fetch_assoc($us_VP);
             return $usevp= $rows_v['mvp_name'];
}
/*----------------------------------------------------------------------------*/
//---- Edit order ALL -----;
if ((isset($_POST['ed_od_send'])) && ($_POST['ed_od_send'] == "edodr")){
              $ord_id= $_POST['od_d'];
    
             //--訂購商品 取得陣列Data--;
	       for($s=0; $s < count($_POST['od_ot_id']);$s++){
              $odr_th_id = $_POST['od_ot_id'][$s];   
              $ord_th_que = $_POST['ord_th_que'][$s];
              $or_price2 = sql_order_oth($odr_th_id);
              $or_sum = $ord_th_que * $or_price2;
               
              $SQLdata3 = ("UPDATE oders_other SET od_ot_que = '{$ord_th_que}', od_ot_sum = '{$or_sum}' WHERE od_ot_id = '{$odr_th_id}'");
              sqldb($SQLdata3);
           }

             header("Location: ord_edit.php?od_d=$ord_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit order shi ALL -----;
if ((isset($_POST['ed_shi'])) && ($_POST['ed_shi'] == "edshi")){
               $ord_id= $_POST['od_d'];
               $ord_shi_fr = $_POST['shi_no'];
               
              $SQLdata3 = ("UPDATE oders SET od_shi_no = '{$ord_shi_fr}' WHERE od_id = '{$ord_id}'");
              sqldb($SQLdata3);
             header("Location: ord_edit.php?od_d=$ord_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit order invocie ALL -----;
if ((isset($_POST['ed_invo'])) && ($_POST['ed_invo'] == "edinvo")){
               $ord_id= $_POST['od_d'];
               $ord_inv_no = $_POST['invo_no'];
               
              $SQLdata3 = ("UPDATE oders SET od_invoice_no = '{$ord_inv_no}' WHERE od_id = '{$ord_id}'");
              sqldb($SQLdata3);
             header("Location: ord_edit.php?od_d=$ord_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Del order ALL -----;
if ((isset($_POST['de_od_send'])) && ($_POST['de_od_send'] == "orddel")){
           //----- Data 第一筆空值-----;
		   if(empty($_POST['d_ordd'][0])){
			  header("Location: index.php");
	          exit;}
           //-----取得陣列Data-----;
	       for($s=0;$s < count($_POST['d_ordd']);$s++){
              $ord_id= $_POST['d_ordd'][$s];
                
		      $SQLdata = "DELETE FROM oders WHERE od_id = '{$ord_id}'";
		      sqldb($SQLdata);
              $SQLdata2 = "DELETE FROM oders_address WHERE od_id = '{$ord_id}'";
		      sqldb($SQLdata2);
              $SQLdata3 = "DELETE FROM oders_other WHERE od_id = '{$ord_id}'";
		      sqldb($SQLdata3);
              $SQLdata4 = "DELETE FROM oders_other_2 WHERE od_id = '{$ord_id}'";
		      sqldb($SQLdata4);
	        }          
              header("Location:index.php");
	          exit;
}
/*----------------------------------------------------------------------------*/
//---- Message user order shipping data -----;
if ((isset($_POST['ed_mess'])) && ($_POST['ed_mess'] == "edmessage")){

              $ord_id = $_POST['od_d'];
              $ord_no = sql_orders('od_no', $ord_id);
              $mid = sql_order_addres($ord_id);
              $usmail = sql_vip('username', $mid);
              $us_name = sql_vip('name', $mid);
    
             //-- 帶入訂單資料 --;
              $od = $ord_id;
                @require_once("funorder_mail.php");
              $ordershow = $_SESSION['order_mail'];
                unset($_SESSION['order_mail']);
    
             //*-- Mail Send User --*/
                $sCharset = 'utf-8';
                $from_na = '樂蔬活LaSulife';//from mang;
                $from_name ="=?UTF-8?B?".base64_encode($from_na)."?=";
                $sMailFrom = $from_name."<service@lasulife.com>"; //寄件者名稱和信箱
		        $sHeaders = "MIME-Version: 1.0\r\n" .
                            "Content-type: text/html; charset=".$sCharset."\r\n" .
                            "From:".$sMailFrom."\r\n";   
                $sSubject = "=?UTF-8?B?".base64_encode("樂蔬活已出貨通知")."?=";
                $sMessage =<<<"mailsb"
                     <p>親愛的{$us_name} 您好</p>
                     <p>您日前於LaSu愛團購訂購的產品訂單資料如下 :</p>
                     <table border="1" style="border: 1px solid black; border-collapse: collapse;padding: 10px;">
                          <tr>
                              <th style="padding: 10px;">品名</th>
                              <th style="padding: 10px;">單價</th>
                              <th style="padding: 10px;">訂購數量</th>
                              <th style="padding: 10px;">小計</th>
                          </tr>
                          {$ordershow}
                     </table><br>
                     <p>我們今天已經出貨，出貨單號：{$ord_no}</p>
                     <p>您可上會員中心確認您的出貨資料</p>
                     <br>
                     <p>如有任何問題請來信  <a href="mailto:#">service@lasulife.com</a></p>
				     <font size="+1"><p>樂蔬活全體同仁　敬上</p></font>
mailsb;
		        @mail($usmail, $sSubject, $sMessage, $sHeaders);//user;
		        @mail("scott@tptc.tw", $sSubject, $sMessage, $sHeaders);//manag;
		        //@mail("sakaitw@yahoo.com.tw", $sSubject, $sMessage, $sHeaders);//manag;
              header("Location: ord_edit.php?od_d=$ord_id");
	          exit;
}
/*----------------------------------------------------------------------------*/
//---- edit 出貨 state data -----;
if ((isset($_POST['ed_state'])) && ($_POST['ed_state'] == "shpstate")){
              $ord_id= $_POST['od_d'];

            if((isset($_POST['sta_time']))&&($_POST['sta_time']!== '')){
              $ed_sta_ti = $_POST['sta_time'];
              $SQLdata7 = ("UPDATE oders SET od_state_time='{$ed_sta_ti}' WHERE od_id = '{$ord_id}'");
              sqldb($SQLdata7);
            }
            if((isset($_POST['sta_name']))&&($_POST['sta_name']!== '0')){
              $ed_sta_na = $_POST['sta_name'];  
              $SQLdata7 = ("UPDATE oders SET od_state='{$ed_sta_na}' WHERE od_id = '{$ord_id}'");
              sqldb($SQLdata7);
            }
             header("Location:ord_edit.php?od_d=$ord_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
header("Location:../index.php");
exit;
?>