<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM `articles` WHERE id = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($id));
	
	$result = $sth->fetchobject();
	$newshare = $result->share+1;
	
	$sql = "UPDATE `articles` SET share=? WHERE id = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($newshare, $id));
?>

		
				