﻿<!DOCTYPE HTML>
<html>
	<head>
		<title>jQuery Infinite Scroll</title>
		<meta charset='utf-8'>
		<link href='http://fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css' />
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />		
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		
		<div class="container">
			
			<div id="wrapper">
				<?php
					require_once('php/conn.php');
					$mysqli = new mysqli($host, $username, $password, $db);
					mysqli_query($mysqli,"SET CHARACTER SET utf8");
		mysqli_query($mysqli, "SET collation_connection = 'utf8_unicode_ci'");
					$datas = $mysqli->query("SELECT * FROM edits where category='傳承文化' LIMIT 0,1");
					
					while($data = $datas->fetch_object()){
						?>
					<h1><p align="center"> <p><?php echo $data->title ?></p></p></h1>
						<div class="container">
											
											<div class="row">   
												<div class="col-md-12">
													<div class="latestpic"> 
														<a href="../Article/showpage.php?id=<?php echo $data->id ?>"  ><img src="article_pic/<?php echo  $data->photo_name ?>"  class="img-responsive"  class="img-responsive" alt="Cinque Terre"  /></a>
													</div>
												</div>
											</div>
										</div>	
					<?php
											?> 
			<p>
		文/ <?php echo $data->editor?> on <?php echo $data->datetime?> in <?php echo $data->category?>
			</p>

<?php
						echo '<p>'.$data->comment.'</p>';

					}
					$datas->close();
				?>
				<a class="url" data-href="php/infiniteScroll.php?page=1"></a>			
			</div>
		</div>
		<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
		<script type="text/javascript" src="js/infiniteScroll.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#wrapper").infiniteScroll({
					loading: '<div class="loading"><img src="loading.gif"> Loading...</div>'
				});
			});
		</script>
	</body>
</html>