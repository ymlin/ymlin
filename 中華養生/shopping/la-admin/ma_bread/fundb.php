<?php @require_once("../../conn/wp-c.php");?>
<?php
    if (!isset($_SESSION)) {session_start();}
//---- send DB ----;
function sqldb($SQLdata){
           global $wp_c;
           $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());         
    }
//---- 編號產生 DB ----;
function sqldbno($SQLdata){
           global $wp_c;
           $sql_pro = "SELECT Max(sec_ID) as max_id3 FROM brand_sec";
           $Result1 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row_pro = mysqli_fetch_assoc($Result1);
           $maxid3 = $row_pro['max_id3'] + 1;
           return $maxid3;
    }
/*----------------------------------------------------------------------------*/
//---- creat bread -----;
/*----------------------------------------------------------------------------*/
if (isset($_POST['cr_br_send']) ){
            //-----Data 第一筆空值-----;
		    if(empty($_POST['cr_bre_name'][0])){
				header("Location:index.php");
	            exit;}
              
            //-----取得陣列Data-----;
	        for($s=0; $s < count($_POST['cr_bre_name']); $s++){
              //-----Data 空值-----;
		      if(empty($_POST['cr_bre_name'][$s])){
				  header("Location:index.php");
	              exit;} 

              $cr_name= $_POST['cr_bre_name'][$s];
              $cr_text= $_POST['cr_bre_text'][$s];
    
             $SQLdata = ("INSERT INTO brand_sec (sec_Name, sec_text) VALUES ('{$cr_name}', '{$cr_text}')");
             sqldb($SQLdata);
            }
            header ("Location:index.php");
            exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit product -----;
if (isset($_POST['ed_br_send']) ){
              $bre_id= $_POST['br_d'];
              $ed_na= $_POST['sec_Name'];
              $ed_tx= $_POST['sec_text'];
              
    	     $SQLdata = ("UPDATE brand_sec SET sec_Name = '{$ed_na}', sec_text = '{$ed_tx}' WHERE sec_ID = '{$bre_id}'");
             sqldb($SQLdata);
	         
             header("Location:bre_edit.php?br_d=$bre_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Del product -----;
if (isset($_POST['de_br_send']) ){
            //----- Data 第一筆空值-----;
		    if(empty($_POST['d_bred'][0])){
				header("Location:index.php");
	            exit;}
             //-----取得陣列Data-----;
              $num= count($_POST['d_bred']);
	        for($s=0;$s<$num;$s++){
                $bre_id= $_POST['d_bred'][$s];
		        $SQLdata = "DELETE FROM brand_sec WHERE sec_ID  = '{$bre_id}'";
		        sqldb($SQLdata);			
	        } 
	         
             header("Location:index.php");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- creat bread category -----;
/*----------------------------------------------------------------------------*/
if (isset($_POST['cr_ca_send']) ){
            //-----Data 第一筆空值-----;
		    if(empty($_POST['cr_cat_name'][0])){
				header("Location:cat_view.php");
	            exit;}
              
            //-----取得陣列Data-----;
	        for($s=0; $s < count($_POST['cr_cat_name']); $s++){
              //-----Data 空值-----;
		      if(empty($_POST['cr_cat_name'][$s])){
				  header("Location:cat_view.php");
	              exit;} 

              $cr_brid= $_POST['cr_br_id'][$s];
              $cr_shid= $_POST['od_shid'][$s];
              $cr_name= $_POST['cr_cat_name'][$s];
              $cr_img= $_POST['cr_cat_img'][$s];
              $cr_pri= $_POST['cr_cat_pri'][$s];
              $cr_text= $_POST['cr_cat_text'][$s];
    
             $SQLdata = ("INSERT INTO brand_category (sec_ID, od_sh_id, brcate_name, brcate_img, brcate_shipping, brcate_text) VALUES ('{$cr_brid}','{$cr_shid}','{$cr_name}','{$cr_img}','{$cr_pri}', '{$cr_text}')");
             sqldb($SQLdata);
            }
            header ("Location:cat_view.php");
            exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit bread category -----;
if (isset($_POST['ed_ca_send']) ){
              $cat_id= $_POST['ca_d'];
              $ed_brid= $_POST['ed_br_id'];
              $ed_shid= $_POST['ed_shid'];
              $ed_name= $_POST['edcat_name'];
              $ed_img= $_POST['edcat_img'];
              $ed_pri= $_POST['edcat_pri'];
              $ed_text= $_POST['edcat_text'];
              
    	     $SQLdata = ("UPDATE brand_category SET sec_ID = '{$ed_brid}',od_sh_id = '{$ed_shid}',brcate_name = '{$ed_name}',brcate_img = '{$ed_img}',brcate_shipping = '{$ed_pri}', brcate_text = '{$ed_text}' WHERE brcate_id = '{$cat_id}'");
             sqldb($SQLdata);
	         
             header("Location:cat_edit.php?ca_d=$cat_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Del product category -----;
if (isset($_POST['de_ca_send']) ){
            //----- Data 第一筆空值-----;
		    if(empty($_POST['d_catd'][0])){
				header("Location:index.php");
	            exit;}
             //-----取得陣列Data-----;
              $num= count($_POST['d_catd']);
	        for($s=0;$s<$num;$s++){
                $cat_id= $_POST['d_catd'][$s];
		        $SQLdata = "DELETE FROM brand_category WHERE brcate_id = '{$cat_id}'";
		        sqldb($SQLdata);			
	        }         
             header("Location:cat_view.php");
	         exit;
}
header ("Location:../index.php");
exit;
?>