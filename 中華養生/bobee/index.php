<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta name="keywords" content=""> 
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="http://begoodlive.com/shopping/css/bootstrap.min.css" >
		<link rel="stylesheet" type="text/css" href="http://begoodlive.com/shopping/css/bootstrap.css" >
		<!-- <link rel="stylesheet" type="text/css" href="css/style.css" >-->
		<link rel="stylesheet" type="text/css" href="http://begoodlive.com/shopping/css/style1.css" >
		<link rel="stylesheet" type="text/css" href="http://begoodlive.com/shopping/css/style-desktop.css" >
		<link rel="shortcut icon" href="http://begoodlive.com/shopping/img/favicon.ico"/>
	  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	  <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="http://begoodlive.com/shopping/js/jquery.min.js"></script>
		<script src="http://begoodlive.com/shopping/js/bootstrap.min.js"></script>
		<script src="http://begoodlive.com/shopping/js/jquery.easing.1.3.js"></script>
		<!-- jQuery google -->
		<script src="http://begoodlive.com/shopping/js/jquery_js.js"></script>
		<!-- jQuery Touth -->
		<script src="http://begoodlive.com/shopping/js/swipe.js"></script>
		<script src="http://begoodlive.com/shopping/js/jquery.SuperSlide2.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
		
		<title>登入</title>
		<?php include 'index_head.php';?>
	</head>
	<body>
		<div >		  
			<nav id="myNavbar-2" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
				
			</nav>
		</div>
		
		
		<div id="header-wrapper"><!--頁首 //--> 
		 <!--Logo-->
		 <div class="container">
			   <div class="row column">
				<header id="header">
				  <div class="mobile">
					 <div id="logo_mob" class="col-md-12">
					   <a href="http://begoodlive.com/content.php"><img alt="" src="http://begoodlive.com/shopping/img/logo_color3.png" class="img-polaroid"></a>
					 </div>
				  </div>
				  <div class="desktop">
					<div id="logo">
					   <a href="http://begoodlive.com/content.php"><img alt="" src="http://begoodlive.com/shopping/img/logo_color3.png" class="img-polaroid"></a>
					 </div>
					 <!-- Only desktop -->
					 
				  </div>
				</header>
			   </div>
			 </div>
		</div>
		<br>
		<div class="container">
			<form class='form-horizontal' action="check_pwd.php" method="post" name="myForm">
				<div class="login_bar"><h3>會員登入</h3></div>
				<div class="col-md-12">
					<div class="content_bar">
						<div class="form-group">
							<label class="col-md-2 control-label" for='account'>帳號</label>
							<div class="controls col-md-3">
								<input id='account' name='account' type='text' size="30" placeholder='請輸入帳號' />
								<p class='help-block'>*請輸入您的e-mail帳號</p>
							</div>
							<div class=" col-md-2" >
								<a href="register.php">未加入會員?</a>　
							</div>
						</div>
								
						<div class="form-group">
							<label class="col-md-2 control-label" for='password'>密碼</label>
							<div class="controls col-md-2">
								<input id='password' name='password' type='password' value='' placeholder='請輸入密碼' />
								<p class='help-block'>*請輸入密碼</p>
							</div>
							<div class=" col-md-2" >
							</div>
						</div>
							
						
						<div class=" form-actions col-md-offset-2" >
						  　<button type='submit' class='btn btn-primary'>登入</button>
						  　<button type="reset" class='btn btn-primary'>重填</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>