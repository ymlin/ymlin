<hr><!-- Footer-->
<div id="footer-wrapper" class="footer">
 <div class="row ">
   <div class="container text-center spn1">
     <div class="col-md-12 column">
        <img alt="" src="./img/logo_color2.png" class="img-polaroid img2">
     </div>
   </div>
   <div class="col-md-12 fonts text-center spn1"><?php nav_menu_footer(0);?></div>
   <div class="col-md-12 fonts text-center"><?php echo $data['foot'];?></div>
   </div>
</div>
<!-- 上層 menu-->
<script src="js/scrollfix-home.js"></script>
<script type="text/javascript">
        $('#banner-wrapper').scrollFix();
</script>
<div class="mobile">
   <div class="container">
     <div class="row">
        <?php @require_once('functions/EDcart.php');?>
        <?php @require_once("functions/Getcart.php");?>
        <link rel="stylesheet" href="./css/mobile-footer.css" >
        <script src="./js/mobile-footer.js"></script>
        <div id="toggle_m_nav" class="mob_fot" href="#">
           <div id="m_nav_menu" class="m_nav m_nav_hline">
              <div class="m_nav_ham" id="m_ham_1"></div>
              <div class="m_nav_ham" id="m_ham_2"></div>
              <div class="m_nav_ham" id="m_ham_3"></div>
           </div>
        </div>
        <div class="col-md-12 nav_font2">
          <div id="m_nav_container" class="m_nav">
             <ul id="m_nav_list" class="m_nav">   
               <?php usevip(0, $uservip);?>
               <li><a href="./orders.php">結帳</a></li>
             </ul>
          </div>
        </div>
     </div>
   </div>
   <div class="fot_m">
      <div class="row nav_m">
        <div class="col-md-12 text-center">
           <?php nav_menu_footer2(0,$cart->get_contents());?>
        </div>
      </div>
   </div>
</div>
<div id="gott"></div>
</body>
</html>