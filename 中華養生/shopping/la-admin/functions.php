<?php
/*----------------------------------------------------------------------------*/
/*---- product 管理 ----*/
function t_bar1($msg){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">商品管理<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
						  <li>
							 <a href="{$msg}ma_product/pro_add.php">商品新增</a>
						  </li>
						  <li>
							 <a href="{$msg}ma_product/index.php">商品檢示</a>
						  </li>
                          <li role="presentation" class="divider"></li>
                          <li>
							 <a href="{$msg}ma_product/type_view.php">商品規格檢示</a>
						  </li>
                          <li>
							 <a href="{$msg}ma_product/ord_shipping.php">運費檢示</a>
						  </li>
						</ul>
				     </li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- Order 管理 ----*/
function t_bar2($msg){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">訂單管理<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
						  <li>
							 <a href="{$msg}ma_order/index.php">訂單檢示</a>
						  </li>
						</ul>
				     </li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- bread 管理 ----*/
function t_bar3($msg){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">商品類別品牌<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
                          <li>
							 <a href="{$msg}ma_bread/bre_add.php">類別新增</a>
						  </li>
						  <li>
							 <a href="{$msg}ma_bread/index.php">類別檢示</a>
						  </li>
                          <li role="presentation" class="divider"></li>
						  <li>
							 <a href="{$msg}ma_bread/cat_view.php">廠商檢示</a>
						  </li>
						</ul>
				     </li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- placard 管理 ----*/
function t_bar4($msg){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">首頁廣告<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
                          <li>
							 <a href="{$msg}ma_advertise/adv_add.php">大圖新增</a>
						  </li>
						  <li>
							 <a href="{$msg}ma_advertise/index.php">大圖檢示</a>
						  </li>
                          <li role="presentation" class="divider"></li>
                          <li>
							 <a href="{$msg}ma_advertise_txt/adv_add.php">公告新增</a>
						  </li>
						  <li>
							 <a href="{$msg}ma_advertise_txt/index.php">公告檢示</a>
						  </li>
						</ul>
				     </li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- Member ----*/
function t_bar6($msg){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">會員管理<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
						 <li><a href="{$msg}ma_member/index.php">會員檢示</a></li>
						 <li><a href="{$msg}ma_member/mem_ec_add.php">E-Coupon新增</a></li>
						</ul>
				     </li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- META----*/
function t_bar7($msg){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">頁面Meta<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
						  <li>
							 <a href="{$msg}ma_meta/index.php">首頁檢示</a>
						  </li>
						  <li>
							 <a href="{$msg}ma_meta/pro_view.php">商品檢示</a>
						  </li>
						</ul>
				     </li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- statistics----*/
function t_bar8($msg, $mandb = '-1'){
         global $wp_c;
            switch($mandb){
              case 1:
                $mandb = '1';
              break;
              case 2:
                $mandb = '2';
              break;
              case 3:
                $mandb = '3';
              break;
              case 4:
                $mandb = '-1';
              break;
            }
             $sql_man = "SELECT * FROM uservip WHERE m_id = '{$mandb}'";
             $ult1 = mysqli_query($wp_c, $sql_man) or die(mysqli_error());
             $man_nums = mysqli_num_rows($ult1);
    
        if($man_nums >= 1){
        echo <<<"s1"
                     <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="mread">統計資訊<strong class="caret"></strong></div></a>
				       <ul class="dropdown-menu">
						  <li>
							 <a href="{$msg}ma_statistics/index.php">檢示</a>
						  </li>
						</ul>
				     </li> 
s1;
        }
}
?>