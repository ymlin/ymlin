<?php @require_once("../conn/wp-c.php");?>
<?php @require_once('EDcart.php');?>
<?php
if (!isset($_SESSION)) {session_start();}
//----clear session ----;
unset($_SESSION['sub_id']);//價金;
unset($_SESSION['sn_id']);//便利代號;
unset($_SESSION['st_name']);//便利店名;
/*---- uservip DB ----*/
function sql_vip($SQLdata, $db_na){
         global $wp_c;
    
           $sqlna= "m_" . $db_na;
           $sql_VP = "SELECT " . $sqlna . " as mvp_name FROM uservip WHERE m_id = '{$SQLdata}'";
           $us_VP = mysqli_query($wp_c, $sql_VP) or die(mysqli_error());
           $rows_v = mysqli_fetch_assoc($us_VP);
         return $usevp= $rows_v['mvp_name'];
}
//---- product_t DB ----;
function sql_product_ot($SQLdata, $db_na){
         global $wp_c;
    
           $sqlna= "pro_t" . $db_na;
           $sql_pro_ot = "SELECT " . $sqlna ." AS prodname FROM product_tw WHERE pro_tID = '{$SQLdata}'";
           $ult1 = mysqli_query($wp_c, $sql_pro_ot) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
         return $dbdata= $row['prodname'];
}
/*---- 加購DB odres chk----*/
function odr_oth(){
         global $wp_c;

           for($b=0; $b < count($_SESSION['cr_o_proid']); $b++){
               $proid= $_SESSION['cr_o_proid'][$b];
               $oth_que = $_SESSION['cr_o_que'][$b];
               
             //---- 加訂數量是否為空值 --;
             if($oth_que > 0){
               $sql_pro = "SELECT * FROM product_tw WHERE pro_tID = '{$proid}'";
               $ult2 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
               $row2 = mysqli_fetch_assoc($ult2);

                $oth_na = $row2['pro_tName'];
                $oth_pay = $row2['pro_tPrice2'];
                $oth_tot = number_format($oth_que * $oth_pay);
              echo <<<"oth1"
                   <tr>
                     <td>{$oth_na}</td>
                     <td>單價 : NT$ {$oth_pay}元</td>
                     <td>數量 : {$oth_que}件</td>
                     <td>小計NT$ : {$oth_tot}元</td>
                   </tr>
oth1;
              }//-- 空值 END;
           }
 }
/*---- GET subsidy view----*/
function sql_subsidy($sub_id){
         global $wp_c;
    
           $sql_uses = "SELECT * FROM subsidy LEFT JOIN subsidy_data ON subsidy.subda_id = subsidy_data.subda_id WHERE subsidy.sub_id = '{$sub_id}'";
           $uses = mysqli_query($wp_c, $sql_uses) or die(mysqli_error());
           $row_s = mysqli_fetch_assoc($uses);
           $num_dv = mysqli_num_rows($uses);
    
         if($num_dv > 0){
             $subid = $row_s['sub_id'];
             $subna = $row_s['subda_name'];
             $subprice = $row_s['sub_price'];

           echo <<<"sub"
                <td>{$subna}</td>
                <td></td>
                <td></td>
                <td>NT$ : ({$subprice})元</td>
sub;
          }
}
/*---- get subsidy DB----*/
function sql_subsidy_db($dbname, $sub_id){
         global $wp_c;
    
           $sql_uses = "SELECT ". $dbname ." AS sqlname FROM subsidy WHERE sub_id = '{$sub_id}'";
           $uses = mysqli_query($wp_c, $sql_uses) or die(mysqli_error());
           $row_s = mysqli_fetch_assoc($uses);
    
             $sub_na = $row_s['sqlname'];
         return $sub_na;
}
/*---- shipping 運費合計運算 ----*/
function odr_shipping($subtotal, $proid){
         global $wp_c;
    
           $sql_shp = "SELECT pro_fre_price FROM product_freight WHERE pro_tID = '{$proid}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
            //品牌運費上限查詢
           $sql_cat = "SELECT brand_category.brcate_shipping FROM product_tw LEFT JOIN brand_category ON product_tw.brcate_id = brand_category.brcate_id WHERE product_tw.pro_tID = '{$proid}'";
           $cat1 = mysqli_query($wp_c, $sql_cat) or die(mysqli_error());
           $row1 = mysqli_fetch_assoc($cat1);
            $cat_shi = $row1['brcate_shipping'];
            //運費上限比較
         if($subtotal < $cat_shi){
             $total_shp = $row['pro_fre_price'];
         }else{
             $total_shp = 0;
         }
        return $total_shp;
}
/*---- 品牌運費運算 ----*/
function sql_shopping_2($na, $prid){
         global $wp_c;
           $dbna= 'brand_category.brcate_'. $na;
           $sql_br_sec = "SELECT ".$dbna." AS dbname FROM product_tw LEFT JOIN brand_category ON product_tw.brcate_id = brand_category.brcate_id WHERE product_tw.pro_tID ='{$prid}'";
           $br_sec = mysqli_query($wp_c, $sql_br_sec) or die(mysqli_error());
           $row_br_sec = mysqli_fetch_assoc($br_sec);         
        return  $row_br_sec['dbname'];
}
/*----------------------------------------------------------------------------*/
/*---- ALL Del Cart ----*/
if (isset($_POST['cr_od_Del']) && ($_POST['cr_od_Del'] == "cr_od_Del")){
         /*---- 執行購物車動作 --*/
          $cart =& $_SESSION['edCart'];
          if(!is_object($cart))$cart = new edCart();
          $cart->empty_cart();
          $cart->empty_brdid();
          header("Location:../index.php");
          exit();
}
/*----------------------------------------------------------------------------*/
/*---- Updata or 合並 and Del Cart ----*/
if (isset($_POST['cr_od_Up']) && ($_POST['cr_od_Up'] == "cr_od_Up")){
         /*---- 執行購物車動作 --*/
          $cart =& $_SESSION['edCart']; 
          if(!is_object($cart))$cart = new edCart();
                $pro_id3 = 0;
                $ord_id3 = 0;
                $type_id3 = 0;
                $pro_id = 0;
                $type_id = 0;
          for($sNO=0; $sNO<$_POST['itemcount']; $sNO++){
              $pro_id=$_POST['pro_id'][$sNO];
              $type_id=$_POST['pro_ty2'][$sNO];
            if(($pro_id == $pro_id3)&&($type_id == $type_id3)){
              //合並商品;
              $qut_id3 = $qut_id3+$_POST['odr_qu'][$sNO];
		      $cart->edit_item($ord_id3,$qut_id3,$type_id3);
              $cart->del_item($_POST['itemid'][$sNO]);
            }else{
              //修改數量; 
              $ord_id3= $_POST['itemid'][$sNO];
              $pro_id3= $_POST['pro_id'][$sNO];
              $type_id3= $_POST['pro_ty2'][$sNO];
              $qut_id3= $_POST['odr_qu'][$sNO];
              $cart->edit_item($ord_id3,$qut_id3,$type_id3);
            }
	      }
          header("Location:../orders.php");
          exit();
}
/*----------------------------------------------------------------------------*/
/*---- Creat Oders product ----*/
if (isset($_POST['cr_od_send']) && ($_POST['cr_od_send'] == "cr_od_send")){
          /*---- 執行購物車動作 --*/
          $cart =& $_SESSION['edCart']; 
          if(!is_object($cart))$cart = new edCart();
          //$cart->deliverfee = 0; //請設定購物車的運費
          //$cart->sub_price = 0; //seting E-Coupon
          //foreach($cart->get_contents() as $item) {echo $item['id'].'<br>';}//商品內容
         /*---- sub_price 折扣 --*/
                  $sub_pri = '0';
                  $subid = 0;
                  unset($_SESSION['sub_id']);
              if(isset($_POST['sub_id'])){$subid = $_POST['sub_id'];}
              if($subid >= 1){
                  $_SESSION['sub_id'] = $subid;
                  $cart->sub_price = sql_subsidy_db('sub_price', $_SESSION['sub_id']);
                   //seting E-Coupon
                  //for($startNO=0;$startNO < $_POST['itemcount'];$startNO++){
                      //$cart_itemid = $_POST['itemid'][$startNO];
                      //$cart_price = $_POST['qty'][$startNO];
		            //$cart->edit_itemecoupon($cart_itemid, $cart_price, 2);
	              //}
                  $cart->_update_total();
              }else{ $_SESSION['sub_id'] = 0;}

         /*---- shipping 每件商品運費 --*/
             $outmod = 0;
          //if(isset($_POST['out_mod'])&&($_POST['out_mod'] !== FALSE)){
             //$outmod = $_POST['out_mod'];
            //$_SESSION['cr_o_out'] = $outmod;//運費id;
             //foreach($cart->get_contents() as $item) {   
               //是否商品運費
               //$shopping_price = odr_shipping($item['subtotal'], $item['proid'], $outmod);
               //seting shopping
               //$cart->edit_itemshopping($item['id'], $shopping_price);
             //}
          
             /*---- 品牌 運費 --*/
             $prudno=count($cart->get_contents());
             $cart->empty_brdid();//clear bread cart清理;
            for($as=0; $as<$prudno; $as++){
              $od=$cart->items[$as];
              //for($ad=0; $ad<count($cart->items[$as]); $ad++){
                $pro_id = $cart->itemproid[$od];
                $brid_no = sql_shopping_2('id',$pro_id);
                $pro_num = $cart->itemqtys[$od];
                $pro_pay = $cart->itemprices[$od];
                 date_default_timezone_set('Asia/Taipei');
                $shno = date("mdHis");
                $cart->add_brdid($shno, $brid_no, $pro_id, $pro_pay, $pro_num);
                //no,brid_num,product_id,produc_price,product_num;
              //}
            }
                $shopping_sub =0;
            foreach($cart->get_bread() as $brem) {
               //echo $brem['brid'] .'='. $brem['total'];
               //echo '</br>';
              //是否商品運費
              $shopping_price = odr_shipping($brem['total'], $brem['proid']);
              $shopping_sub = $shopping_sub + $shopping_price;
            }
              //請設定購物車的運費
              $cart->deliverfee = $shopping_sub; 
              $cart->_update_total(); 
          //}
}
header("Location:../orders_use.php");
exit();
?>