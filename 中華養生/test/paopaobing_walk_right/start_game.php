﻿<!DOCTYPE html><html><head>
<body bgcolor=\"#000000\" >

	<iframe style="position:absolute; left:0px; top:0px;" id="xframe" name = "xframe" src="game.php" width="1360" height="768" frameborder="0"></iframe>
	<iframe style="position:absolute; left:0px; top:0px;" id="yframe" name = "yframe" src="game.php" width="1360" height="768" frameborder="0"></iframe>
	
	<script type="text/javascript">
	function getHighIndex () {
		selector = "*";

		var elements = document.querySelectorAll(selector) ||
					   oXmlDom.documentElement.selectNodes(selector),
			i = 0,
			e, s,
			max = elements.length,
			found = [];

		for (; i < max; i += 1) {
			e = elements[i].style.zIndex;
			s = elements[i].style.position;
			if (e && s !== "static") {
			  found.push(parseInt(e, 10));
			}
		}

		return found.length ? Math.max.apply(null, found) : 0;
	}
	
	function x_reload()
	{
		document.getElementById("xframe").style.display = "none";
		parent.xframe.location.reload();
		document.getElementById("xframe").style.display = "block";
		setTimeout(y_reload, 5000); 
	}
	
	function y_reload()
	{
		document.getElementById("yframe").style.display = "none";
		parent.yframe.location.reload();
		document.getElementById("yframe").style.display = "block";
		setTimeout(x_reload, 5000); 
	}
	</script>
	
</body>
</head>

</html>