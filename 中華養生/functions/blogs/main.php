 <?php
  require_once("dbtools.inc.php"); 
      
      //取得登入者帳號及名稱
      session_start();
	  if (isset($_SESSION["login_account"]))
	  {
        $login_account = $_SESSION["login_account"];
  
	  ?>
<!doctype html>
<html>
  <head>
    <title>會員管理</title>
	
    <meta charset="utf-8"><?php include 'index_head.php';?>
  </head>
  <body>
  	<?php include 'nav.php';?>
	<?php include 'header_top.php';?>
	
		<?php include 'modify.php';?>
   	<?php include 'footer.php';?>
  </body>
   <?php
	  }
	else
	 header("location:index.php");
 ?>
</html>