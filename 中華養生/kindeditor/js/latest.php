<?php
//裁切字串
function cut_content($a,$b){
    $a = strip_tags($a); //去除HTML標籤
    $sub_content = mb_substr($a, 0, $b, 'UTF-8'); //擷取子字串
    echo $sub_content;  //顯示處理後的摘要文字
    //顯示 "......"
    if (strlen($a) > strlen($sub_content)) echo '...<a href="https://www.facebook.com/" class="color=""" >Learn More>></a>';
}

//以上程式已經包裝起來,您可存放在例如:function.php網頁
//往後只要使用include("function.php");
//加上 cut_content($a,$b);即可,不需每次撰寫.
//$a代表欲裁切內容.
//$b代表欲裁切字數(字元數)
?>
							
							<div style="
			padding: 20px 30px 5px 10px;
			box-shadow: 2px 2px 2px 4px rgba(128,128,128,0.1);
			margin: 0 0 10% 0;">
								
								<div style="margin: 1em 2em 2em 1em">
											<h2>最新文章</h2>
										</div>
											
											<?php
      require_once("dbtools.inc.php");
	  
     
      $sql = "SELECT * FROM edits ORDER BY datetime DESC";
       $result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	  $i=0;
	   date_default_timezone_set("Asia/Taipei");
			
	while ($row = mysqli_fetch_assoc($result)){
	if($i<3)
      {$date=$row['datetime'];
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time); 
						$i=$i+1;
	  //cut_content( $row['title'],0);
	  ?>
											<!-- Excerpt -->
												<div style="border-bottom: solid 1px #dbdbdb;margin: 0 0 2.75em 0;
					padding: 0 0 2em 0;">
												
												<article class="boxes excerpt"> 
											
												<div class="col-sm-12 ">
													<a href="showpage.php?id=<?php echo $row['id'] ?>" class="image left"><img src="images/pic04.jpg" alt="" /></a>
													<div>
														<header>
															<div style="margin: 0 0 1em 0;"> <?php echo  $row['editor'] ?>|<?php echo  $CheckDay ?>| <?php echo  $row['category'] ?></div>
															<h3><a href="showpage.php?id=<?php echo $row['id'] ?>"><?php echo  $row['title'] ?></a></h3>
														</header>
														<p>Phasellus quam turpis, feugiat sit amet ornare in, hendrerit in lectus </p>
															
													</div>
													</div>
													
												</article>
												
												</div>
	 <?php
  }
  }
  ?>
											<!-- Excerpt -->
												

									</div>
									
									