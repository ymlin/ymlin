<?php @require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php @require_once("../../functions/fundb.php");?>
<?php @require_once("../../functions.php");?>
<?php if (isset($_GET['pr_d'])){ $proids=$_GET['pr_d'];}?><!-- la-amin fun -->
<?php if(isset($_GET["pg"])){$_SESSION['pg2'] = $_GET["pg"];}?>
<?php @require_once("../functions.php");?>
<?php @require_once("functions_pro.php");?>
<?php @require_once("../header_all.php");?>
<!--online edit-->
<link rel="stylesheet" href="../kindeditor/themes/default/themes/default/default.css" />
<link rel="stylesheet" href="../kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="../kindeditor/plugins/code/prettify.js"></script>
<script>
    KindEditor.ready(function(K) {
        var editor1=K.create('textarea[name="pro_exch"]',{//name=form中textarea的name属性
            cssPath : '../kindeditor/plugins/code/prettify.css',
            uploadJson : '../kindeditor/php/upload_json.php',
            fileManagerJson : '../kindeditor/php/file_manager_json.php',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
            }
        });
        prettyPrint();
    });
</script>
  <!-- feature -->
  <div id="features-wrapper">
     <div class="container"><!--內容區 -->
        <article class="row clearfix mem_list"><!-- 對齊中間 -->
          <div class="col-lg-12 column">
           <div class="col-md-12 top_banner_cont">商品修改</div>
           <div class="col-md-12 hline"> 
              <div class="col-sm-7 inner">
                 <form class="form-horizontal" action="fundb.php" method="post" name="Form_edit" id="Form_edit">
                  <!-- Select Basic -->
                  <div class="form-group">
                     <label class="col-sm-3 text-center font5" for="brand">分類</label>
                     <div class="col-sm-5">
                        <select id="ed_br_id" name="ed_br_id" class="input25">
                           <option selected="selected">請選擇</option>
                           <option class="divider"></option>
                           <?php proedit2($proids);?><!-- 品牌比較-->
                        </select>
                     </div>
                  </div>
                  <!-- Select Basic -->
                  <div class="form-group">
                     <label class="col-sm-3 text-center font5" for="brand">品牌</label>
                     <div class="col-sm-5">
                        <select id="ed_ca_id" name="ed_ca_id" class="input25">
                           <option selected="selected">請選擇</option>
                           <option class="divider"></option>
                           <?php proedit4($proids);?><!-- 分類比較-->
                        </select>
                     </div>
                  </div>
                     <?php proedit($proids, 1);?><!-- 商品名稱-->
                     <?php proedit($proids, 2);?><!-- 商品原價-->
                     <?php proedit($proids, 3);?><!-- 商品特價-->
                     <?php proedit($proids, 5);?><!-- 商品數量-->
                     <?php //proedit($proids, 7);?><!-- 商品運費-->
                     <?php proedit($proids, 6);?><!-- 商品照片-->
                     <?php proedit($proids, 8);?><!-- 商品首頁-->
                     <?php proedit3($proids, 1);?><!-- 商品簡易說明 -->
                     <?php proedit3($proids, 2);?><!-- 商品注意事項 -->
                     <?php proedit5($proids);?><!-- 商品說明快編器 -->
                     <input id="pr_d" name="pr_d" value="<?php echo $proids;?>"  type="hidden"><!-- 商品id -->
                     <div class="col-sm-offset-1 spn1">
                        <button id="ed_pr_send" name="ed_pr_send"  value="ed_pro" class="btn">確定送出</button>
                        <button class="btn button1" type="button" title="回上頁" onclick="location.href='index.php?page=<?php echo $_SESSION['pg2'];?>'">回上頁</button>
                     </div>
                 </form>
              </div>
              <div class="col-sm-5">
                   <div class="title2">商 品 照 片</div>
			          <?php proeditimg($proids);?><!-- 商品照片圖-->
                      <a href="<?php echo proeditview($proids);?>" target="_blank"><div class="font3">商品預覽>>GO</div></a><!-- 商品照片圖-->
                   <!-- 圖片上傳 -->
                      <a href="ftp://lasulife.com/group/mall/p1" target="_blank"><h4>圖片上傳檔案>>GO</h4><hr></a>
		      </div>
              <div class="col-sm-4 hline2 inner">
                   <div class="title2">商品運費</div>
                   <form action="fundb.php" method="post" name="Form_fre" id="Form_fre">
			          <?php product_fr_view($proids);?><!-- 商品照片圖-->
                      <!-- 商品id -->
                      <input id="pr_d" name="pr_d" value="<?php echo $proids;?>"  type="hidden">
                      <br>
                      <div class="col-md-offset-1 spn1">
                         <button id="fre_del" name="fre_del" value="fredel" class="btn btn-xs">確定刪除</button>
                         <button class="btn btn-xs button1" type="button" title="回上頁" onclick="location.href='pro_freight.php?pr_d=<?php echo $proids;?>'">新增</button>
                      </div>
                   </form>
		      </div>
              <div class="col-sm-4 hline2 inner">
                   <div class="title2">商品規格</div>
                   <form action="fundb.php" method="post" name="Form_typ" id="Form_typ">
			          <?php pro_type_view($proids);?><!-- 商品照片圖-->
                      <div class="col-md-offset-1 spn1">
                         <button class="btn btn-xs button1" type="button" title="回上頁" onclick="location.href='type_add.php?pr_d=<?php echo $proids;?>'">新增</button>
                      </div>
                   </form>
		      </div>
              <div class="col-sm-4 hline2 inner"><!-- E-Coupon分類-->
                 <div class="title2">E-Coupon分類</div>
                 <div class="col-sm-12">
                   <form action="fundb.php" method="GET" name="Form_sub">       
                   <div class="col-sm-6">
                      <select name="subda_id" class="input15">
                        <option value="#" selected="true"> 請選擇E-Coupon </option>
                        <?php ecoupon_pro_edit($proids);?>
                      </select>
                   </div>
                   <div class="col-sm-6 spn1">
                     <!-- id hidden -->
                     <input name="pr_d" value="<?php echo $proids;?>" type="hidden">
                     <button id="ed_sub" name="ed_sub" value="ed_sub" class="btn btn-xs">確定送出</button>
                   </div>
                   </form>
                 </div> 
		      </div>
              <!-- <div class="col-sm-4 hline2 inner">
                   <div class="title2">商品Meta</div>
                      <div class="col-md-offset-1 spn1">
                         <button class="btn btn-xs button1" type="button" title="回上頁" onclick="location.href='meta_edit.php?pr_d=<?php echo $proids;?>'">異動</button>
                      </div>
		      </div>-->
           </div>
         </div>
        </article>
     </div><!--內容 結尾-->
   </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?>
<?php end_sql($end_sqls);?><!-- END sql -->