<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data['titles'];?></title>
    <meta charset="utf-8">
    <meta name="keywords" content="<?php print $data['keyw'];?>"> 
    <meta name="description" content="<?php echo $data['desc'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
    <!-- <link rel="stylesheet" type="text/css" href="css/style.css" >-->
    <link rel="stylesheet" type="text/css" href="css/style1.css" >
    <link rel="stylesheet" type="text/css" href="css/style-desktop.css" >
    <link rel="shortcut icon" href="./img/favicon.ico"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- jQuery google -->
    <script src="js/jquery_js.js"></script>
    <!-- jQuery Touth -->
    <script src="js/swipe.js"></script>
    <script src="js/jquery.SuperSlide2.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
    <!--facebook view-->
    <?php home_fb2();?>
</head>
<body>
<div id="wrap"><!--最外層包裝 //-->
  <div id="header-nav"><!--頁首 //--> 
     <div class="container">
       <div class="row">
        <div class="col-lg-12 top_bar2 desktop">
           <ul class="read6">
             <?php nav_menu_sider(0,$uservip);?> 
           </ul>
           <ul class="read5">
             <?php usevip(0, $uservip);?>
           </ul>
        </div>
       </div>
     </div>
  </div>
  <div id="header-wrapper"><!--頁首 //--> 
     <!--Logo-->
     <div class="container">
       <div class="row column">
        <header id="header">
          <div class="mobile">
             <div id="logo_mob" class="col-md-12">
               <a href="<?php info_url('./');?>"><img alt="" src="./img/logo_color2.png" class="img-polaroid"/></a>
             </div>
          </div>
          <div class="desktop">
		    <div id="logo">
               <a href="<?php info_url('./');?>"><img alt="" src="./img/logo_color3.png" class="img-polaroid" /></a>
             </div>
             <!-- Only desktop -->
             <nav id="nav">
                <?php nav_menu_cart(0,$cart->total,count($cart->get_contents()));?>
             </nav>
          </div>
        </header>
       </div>
     </div>
  </div>
  <div class="mobile home_list"><!--search-->
     <div class="col-sm-12 text-center">
        <div class="row fonts">
          <?php home_search2();?>
          <div class="col-sm-12 table-responsive" >
            <table class="table">
               <tr><?php navmenu_mobile(0);?></tr> 
            </table>
          </div>
        </div>                        
     </div>
  </div>
  <div id="banner-wrapper" class="headnav" style="z-index:9999;">
    <div class="h_nav">
     <div class="container">
	    <div class="row clearfix">
		   <div class="col-lg-12 column hea_bar">
              <div class="desktop">
                 <ul class="read6">
                   <?php navmenu_view(0);?><!--  GET NAV MENU // -->
                 </ul>
              </div>
		   </div>
	    </div>
      </div><!--頁首 END-->
     </div>
  </div>