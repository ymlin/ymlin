<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 
?>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>
	
	<style>
	html{
	 -webkit-tap-highlight-color:rgba(0,0,0,0);
	}
	</style>

	<body style = 'background-color: transparent;'>
	<div  class="col-md-4" style="width:100%; background-color: transparent; min-height: 700px;" >

		<?php
			$sql = "SELECT prefer FROM `users` WHERE id = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['userid']));
			$result = $sth->fetchobject();
			$prefers = explode(" ", $result->prefer);
			$user_type = $result->user_type;
			
			$prefer_rel = explode(",", $prefers[0]);
			$qry_prefer = "( top = '1' or (( religion = '不限' ";
			for($i = 0 ; $i < count($prefer_rel) ; $i++ ) $qry_prefer=$qry_prefer." or religion = '".$prefer_rel[$i]."' ";
			$qry_prefer = $qry_prefer.") and ";
			
			$prefer_bud = explode(",", $prefers[1]);
			$qry_prefer = $qry_prefer."( buddha = '無' ";
			for($i = 0 ; $i < count($prefer_bud) ; $i++ ) $qry_prefer=$qry_prefer." or buddha = '".$prefer_bud[$i]."' ";
			$qry_prefer = $qry_prefer.") and ";
			
			$prefer_sea = explode(",", $prefers[1]);
			$qry_prefer = $qry_prefer."( season = '無' ";
			for($i = 0 ; $i < count($prefer_sea) ; $i++ ) $qry_prefer=$qry_prefer." or season = '".$prefer_sea[$i]."' ";
			$qry_prefer = $qry_prefer."))) ";
			
			
		
			if($_GET['category'] == 'byby') 		$sql = "SELECT * FROM `articles` WHERE keyword = '拜拜大小事' and (user_type='通用' || user_type='?') and ";
			else if($_GET['category'] == 'god') 	$sql = "SELECT * FROM `articles` WHERE keyword = '仙佛故事' and (user_type='通用' || user_type='?') and ";
			else if($_GET['category'] == 'custom') 	$sql = "SELECT * FROM `articles` WHERE keyword = '節氣習俗' and (user_type='通用' || user_type='?') and ";
			else 									$sql = "SELECT * FROM `articles` WHERE (user_type='通用' || user_type='?') and ";
			$sth = $db->prepare($sql.$qry_prefer."ORDER BY top DESC,datetime DESC");
			$sth->execute(array($user_type));
			
			while($result = $sth->fetchobject())
			{
			
				if($result->video != "")
				{
					$pos = strpos($result->video, 'youtu.be');
					$image = "http://img.youtube.com/vi/".substr($result->video, $pos+9)."/maxresdefault.jpg";
				}
				else
				{
					$image = "../mickey/".$result->photo_name;
				}
				
				$datetime = $result->datetime;
		?>
		<div style="background-color:#ffffff; width:80%; margin: 0px auto; padding: 30px 30px 30px 30px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1);">
			<div style="width:90%; margin: 0px auto; font-weight:600;line-height: 120%;font-size:150%; text-decoration: none;color:black">
			<?php
				echo $result->title;
			?>
			</div>
			<br>

			<div style=" width:90%; margin: 0px auto;">
				<div style="display:inline-block; width:80%; margin:0px 0px 5%; color:gray;font-size: 110%; position:relative; top:-0.2em;">
				<?php
					echo date("Y-m-d", strtotime($result->datetime)) . " | " . $result->keyword;
				?>
				</div>
				<div style="display:inline-block; width:18%; height:1.4em; margin: 0px auto; background:url('../mickey/share.png') no-repeat; background-size: 100% 90%; background-position: center 0.2em;" onclick="alert('share <?php echo "bobee.begoodlive.com"." ".$result->id; ?>')" ></div>
			</div>
			
			<?php
				if($result->video != "")
				{
			?>
			<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: cover; background-position: center center;" onclick="alert('video <?php echo ($result->video!="")?($result->video):"";?>')" >
				<div style="width:100%; height:100%; margin: 0px auto; background:url('../mickey/play.png') no-repeat; background-size: cover; background-position: center center;"></div>
			</div>
			<?php
				}
				else
				{
			?>
			<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: contain; background-position: center center;" >
			</div>
			<?php
				}
			?>
			<br>
			<div style="width:90%;margin: 0px auto;color:gray;font-weight:400;line-height: 120%;font-size: 120%;">
			<?php
				echo mb_substr(strip_tags($result->comment),0,50,"UTF-8");
				if(mb_strlen(strip_tags($result->comment),"UTF-8") > 50) echo "...";
			?>
			</div>
			<br>
			<div style="width:90%; margin: 0px auto;">
				<a href="mobile_article.php?id=<?php echo $result->id; ?>">
				<div style="
						color: black;
						background-color: white;
						border-style: solid;
						border-color: black;
						border-width: 1px;
						border-radius: 3px;
						text-align: center;
						height: 120%;
						font-size: 150%;
						display:inline-block;">
				 閱讀更多 ➔</div>
				</a>
			</div>
		</div>
		<br>
		<div style="width:40%; margin: 0px auto; ">
			<div style="width: 15vw;margin: 0 auto;box-shadow: 0px 0px 1px 2px rgba(0,0,0,0.1);color: #ffffff;font-size: 2vw;text-align: center;">
				<?php
					echo date("Y/m/d H:m", strtotime($datetime));
				?>
			</div>
		</div>
		<br>
		<?php			
			}
		?>
	</div>	
	</body>
</html>
		
				