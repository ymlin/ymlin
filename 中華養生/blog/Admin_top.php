﻿<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
	 <?php
  require_once("dbtools.inc.php"); 
      
      //取得登入者帳號及名稱
      session_start();
	  if (isset($_SESSION["login_account"]))
	  {
        $login_account = $_SESSION["login_account"];
  
	  ?>
	<head>
      <meta charset="utf-8" />
		 
      <title>Responsive tables</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, minimal-ui">
      <meta name="format-detection" content="telephone=no">
      <meta name="description" content="An awesome solution for responsive tables with complex data.">
		  <link rel="shortcut icon" href="./images/favicon.png"/>
      <!-- Font Awesome -->
      <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

		  <!-- Latest compiled and minified Bootstrap CSS -->
      <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

     
		<style>
    #wrap{min-height: 100%;
      height: auto;
      /* negative indent footer by its heigth*/
      margin: 0 auto -60px;
      /* pad bottom by footer height*/
      padding: 0 0 60px;}
	  </style>
	</head>

   <body data-spy="scroll" data-target="#navbar" data-offset="50">
      <div style="margin-bottom:90px">
	  <div id="navbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <span class="navbar-brand"><span class="fa fa-table"></span><a href='index.php'>Home</a></span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
			 <li><a href="Admin.php">管理員專區</a></li>
           <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">講師文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit.php" tabindex="-1">全部講師文章</a></li>					
                        <li><a href="edit.php" tabindex="-1">新增講師文章</a></li>
						  
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">學員文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_student.php" tabindex="-1">全部學員文章</a></li>
						 <li><a href="edit_student.php" tabindex="-1">新增學員文章</a></li>
                       
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">講師影音<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_film.php" tabindex="-1">全部影音</a></li>
						  <li><a href="edit_film.php" tabindex="-1">新增影音</a></li>                       
                     </ul>
                   </li>
				    <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">書院活動<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_htz.php" tabindex="-1">全部書院活動</a></li>
						  <li><a href="edit_htz.php" tabindex="-1">新增書院活動</a></li>                       
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">關鍵字<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_keyword.php" tabindex="-1">全部關鍵字</a></li>
                         <li><a href="edit_keyword.php" tabindex="-1">新增關鍵字</a></li>
                     </ul>
                   </li>
              <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">帳號<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_mem.php" tabindex="-1">全部帳號</a></li>
                       
                     </ul>
                   </li>
		<li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">圖片<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="add_picture.php" tabindex="-1">新增圖片</a></li>
                       
                     </ul>
                   </li>
              <li><a href="logout.php">登出</a></li>
            </ul>
          </div>
        </div>
    </div>
   </div>
  
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

      <!-- Latest compiled and minified Bootstrap JavaScript -->
      <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

      <script src="js/rwd-table.min.js?v=5.0.3"></script>
   
		
	</body>
	 <?php
	  }
	else
	 header("location:index.php");
 ?>
</html>
