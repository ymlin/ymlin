﻿<!DOCTYPE HTML>
<!--
	Prologue by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>黃庭書院</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		
		<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/bootstrap-theme.min.css" rel="stylesheet" >
		
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	  <style>  
	
.home_list .hline{padding:0.5em;border:1px solid #E1E1E1; border-radius:6px;}
.home_list .hline2{padding:0.5em;border:1px solid #E1E1E1; border-radius:6px;margin-right:45px;margin-left:45px}
 .home_list .font{font-size: 1.2em;letter-spacing:0.2em;text-align:left;}
 .home_list .font2{letter-spacing:0.2em;text-align:center;}
 </style>  

	</head>
	<body style="">

		<!-- Header -->
	
		<!-- Main -->
			<div id="main">

				<!-- Intro -->
					<section id="top" class="one dark cover" >
					
	<?php include 'header_top.php';?>
		<?php include 'header.php';?>
						<div class="container" >	
					
							<div class="row">
						
								<div class="col-md-8" ">

									<!-- Article list -->
									
									
										<?php include 'latest.php';?>
					
									</div>
									<!-- Spotlight -->
									<div class="col-md-4" >
							<?php include 'sidebar.php';?>
							</div>	
							</div>		
							
					</div>	
					</section>

				<!-- Portfolio -->
					<section id="portfolio" class="two">
					<div class="container">	
							<div class="row"style="margin-top:60px">		
					  <div class="col-sm-12 home_list">
							
				<?php
//裁切字串
function cut_contents($a,$b){
    $a = strip_tags($a); //去除HTML標籤
    $sub_contents = mb_substr($a, 0, $b, 'UTF-8'); //擷取子字串
    echo $sub_contents;  //顯示處理後的摘要文字
    //顯示 "......"
   
}

//以上程式已經包裝起來,您可存放在例如:function.php網頁
//往後只要使用include("function.php");
//加上 cut_content($a,$b);即可,不需每次撰寫.
//$a代表欲裁切內容.
//$b代表欲裁切字數(字元數)
?>
							
			  <?php
      require_once("dbtools.inc.php");
	  
     
      $sql3 = "SELECT * FROM categorys where id<19 and id>12";
       $result3 = mysqli_query($wp_c,$sql3) or die(mysqli_error('error'));
	 
	   date_default_timezone_set("Asia/Taipei");
	   
			?>
			<div class="row">
			<?php	
			$aa=0;
	while ($row3 = mysqli_fetch_assoc($result3)){
	$date=$row3['datetime'];
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time);
 if(strtotime($date)<strtotime($datetime)){		
		?>
		
										
											<?php include 'article.php';?>
										<?php
					 $aa ++; if($aa%3==0){echo '</div><div class="row">';}
					 }
					 
					 }
					 ?>	
									
									   </div>		
								  </div>		   
									</div>
									</div>
					</section>

				<!-- About Me -->
					<section id="about" class="three">
						<div class="container">	
						
							 <div class=""style="margin-top:60px">
						<div style="margin-left:10px ;font-size:170%; font-weight: 900;">
						<a href="http://www.htz.org.tw/blog/Acategory.php?id=19">講師影音</a>
						<hr>
							</div>
							</div>
							<div class="row"style="margin-top:30px">		
					  <div class="col-md-8">
						 
	  
							
			  <?php
      require_once("dbtools.inc.php");
	  
 $sql="SELECT * FROM edit_film order by datetime DESC";
       $result= mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	 
	 
	   date_default_timezone_set("Asia/Taipei");
			?>
			<div class="row">
		
			<?php	
			$aa=0;
			$taa=0;
	while ( $row20 = mysqli_fetch_assoc($result) and $taa<6){
	$date=$row20['datetime'];
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time);
 if(strtotime($date)<strtotime($datetime)){	
	   $taa++;
		?>
		
		<div class="col-md-4">
										
											<section class="box feature last">
									<div  style="margin:0.5em 0 0.5em 0;">

									<a href="showpage_film.php?id=<?php echo $row20['id'] ?>" class="image featured">	<img src="mickey/<?php echo  $row20["photo_name"] ?>" alt="" class="img-polaroid"/></a>
									
									</div>
									<div style="margin:0.5em 0 0.5em 0;">
										
									<div style="margin:2.0em 0 0 1em;font-size: 18px;height:50px;"><a href="showpage_film.php?id=<?php echo $row20['id'] ?>"><?php cut_content( $row20['title'],28);  ?></a> </div>
							<div style="margin-left:20px;color:#d3d3d3;">
									<p>on <?php echo $CheckDay?></p>
									</div>
									</div>
								</section>
									</div>
										<?php
					 $aa ++; if($aa%3==0){echo '</div><div class="row">';}
					 
					 
					 }
					 }
					 ?>	
									
									   </div>	 
								  </div>	 <?php include 'fb.php';?>	
								
 								
									</div>
									</div>
								
									
									
									
					</section>
<section id="about" class="three">
						<div class="container">	
							 <div class=""style="margin-top:60px">
						<div style="margin-left:10px ;font-size:170%; font-weight: 900;">
						<a href="http://www.htz.org.tw/blog/Acategory.php?id=20">書院活動</a>
						<hr>
							</div>
							</div>
							<div class="row"style="margin-top:30px">		
					  <div class="col-sm-12 home_list">
							
	  <div class="col-md-8">	
						
			  <?php
      require_once("dbtools.inc.php");
	  
 $sql="SELECT * FROM edit_htz order by datetime DESC";
       $result= mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	 
	 
	   date_default_timezone_set("Asia/Taipei");
			?>
			<div class="row">
			<?php	
			$aa=0;
			$taa=0;
	while ( $row20 = mysqli_fetch_assoc($result) and $taa<4){
	$date=$row20['datetime'];
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time);
 if(strtotime($date)<strtotime($datetime)){	
	$taa++;
		?>
		<?php
	   date_default_timezone_set("Asia/Taipei");
			
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($row20['datetime']);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time); 


 
?>
		<div class="col-md-6">
										
											<section class="box feature last">
									<div class="col-md-7" style="">

									<a href="showpage_htz.php?id=<?php echo $row20['id'] ?>" class="image featured">	<img src="mickey/<?php echo  $row20["photo_name"] ?>" alt="" class="img-polaroid"/></a>
									
									</div>
									<div cass="col-md-5" style="margin:0.5em 0 0.5em 0;">
										
									<div style="margin:2.0em 0 1em 1em;font-size: 18px;height:30px;"><a href="showpage_htz.php?id=<?php echo $row20['id'] ?>"><?php cut_content( $row20['title'],28);  ?></a> </div>
							<div style="margin-left:20px;color:#696969;">
									<p>on <?php echo $CheckDay?></p>
									</div>
									</div>
								</section>
									</div>
										<?php
					 $aa ++; if($aa%4==0){echo '</div><div class="row">';}
					 
					 
					 }
					 
					 }?>	
									
									   </div>		
								  </div>
							 <div class="col-md-4">		 

				<?php include 'htz_sidebar.php';?>
							
							 
							</div>
								  	</div>
									</div>
									</div>
									</div>
									
					</section>
				<!-- Contact -->
					<section id="contact" class="four">
					
			
						<div class="container">
		 <div class=""style="margin-top:60px">
						<div style="margin-left:10px ;font-size:170%; font-weight: 900;">
						<a href="http://www.htz.org.tw/blog/Acategory.php?id=21">學員心得</a>
						<hr>
							</div>
							</div>
							<div class="row"style="margin-top:25px">		
					  <div class="col-sm-12 home_list">
							
				
							
			  <?php
      require_once("dbtools.inc.php");
	  
     
   
	 
	   date_default_timezone_set("Asia/Taipei");
			?>
			<div class="row">
			<?php	
			$aa=0;
			$test=0;
			 require_once("dbtools.inc.php");
			 $sql="SELECT * FROM edit_student order by datetime DESC";
       $result= mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	 
	  $i=0;
	   date_default_timezone_set("Asia/Taipei");
			
	while ($row9 = mysqli_fetch_assoc($result)and $test<3){
				$date=$row9['datetime'];
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time);
 if(strtotime($date)<strtotime($datetime)){	
	$test++;
		?>
			
		
										
											<div class="col-md-4"style="">
										
											<section class="box feature last">
									<div  style="margin:0.5em 0 0.5em 0;">

									<a href="showpage_student.php?id=<?php echo $row9['id'] ?>" class="image featured">	<img src="mickey/<?php echo  $row9["photo_name"] ?>" alt="" style="border-radius:12px;border-radius:12px;height:240px"></a>
									
									</div>
									<div style="margin:0.5em 0 0.5em 0;margin-left:15px">
										
										<div style="margin:2.0em 0 0em 0;font-size: 19px;height:55px;"><a href="showpage_student.php?id=<?php echo $row9['id'] ?>"><?php cut_content( $row9['title'],28);  ?></a> </div>
								<div style="color:#d3d3d3;">
										<p>on <?php echo $CheckDay?></p>
										</div>
										<div style="font-size: 16px;">
									<p><?php cut_contents( $row9['comment'],85);  $sub_contents = mb_substr($row9['comment'], 0, 85, 'UTF-8');if(strlen($row9['comment']) > strlen($sub_contents)) echo '   &nbsp;&nbsp;&nbsp;......	<a href="showpage_student.php?id=' . $row9['id'] .' ">閱讀更多</a>';?> </p>
											
											
											</div>
										
											</div>
								</section>
									</div>
										<?php
					 $aa ++; if($aa%3==0){echo '</div><div class="row">';}
					 }
					 }
					 ?>	
									
									   </div>		
								  </div>		   
									</div>
									</div>

					</section>
						
						<?php include 'footer.php';?>

			
	</div>
		<!-- Footer -->
			

		<!-- Scripts -->
		
	</body>
</html>