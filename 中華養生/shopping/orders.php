<?php @require_once("conn/wp-c.php");?>
<?php @require_once('functions/EDcart.php');?>
<?php @require_once("functions/fundb.php");?>
<?php $dir_s1="1";?>
<?php @require_once("loguser/use_chk.php");?><!-- END -->
<?php $cart = & $_SESSION['edCart'];//shopping cart; 
if(!is_object($cart)) $cart = new edCart();
$cart->deliverfee = 0; //seting 運費清除;
foreach($cart->get_contents() as $item){$cart->edit_itemshopping($item['id'], 0);}
$cart->sub_price = 0; //seting E-Coupon
?>
<?php if($cart->itemcount == 0){
    echo '<script language="javascript">window.location.replace("index.php");</script>';
    header("Location:index.php");
    exit();
}//Empty cart;
?>
<?php @require_once("functions.php");?>
<?php @require_once("header.php");?>
<?php $urls = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?><!--Get url-->
<script type="Text/JavaScript" src="./js/jquery.min.js"></script>
<script type="text/javascript">
   $(function(){
      $("#cr_od_send").click(function(){
      var name = $("#pro_ty2").val();
      if( name==''||name==0){
         alert('提醒: 商品的規格請選取，並點選~更新購物車~!');
         return false;
       }
       });
   });
</script><!-- pass END -->
   <div class="container"><!--內容區 -->
      <article class="ord_list"><!-- 對齊中間 -->
        <div class="row clearfix">
           <div class="col-lg-12 column">
              <form class="form-horizontal" action="./functions/funorder_p.php" method="post" name="commentForm" id="commentForm">
                 <div class="col-md-12 hline">
                    <!---- setp 1 ---->
                    <div class="col-md-12">
                       <div class="col-sm-6 font3 spn2">Setp 1 <small>請確認購買數量與金額</small></div>
                       <div class="col-sm-12 table-responsive">
                       <table class="table table-hover">
                            <tbody>
                              <?php $prsubd='';
                                  if($cart->itemcount > 0) {
	                              foreach($cart->get_contents() as $item) {
                               ?>
                               <tr>
                                  <td class="con"><a href="./functions/addtocart.php?A=Remove&amp;ordno=<?php echo $item['id'];?>" class="delcart"><span class="cart_remove font-fa">X</span></a>
                                     <input name="itemid[]" type="hidden" id="itemid[]" value="<?php echo $item['id'];?>" />
                                     <input name="pro_id[]" id="pro_id[]" type="hidden" value="<?php echo $item['proid'];?>">
                                  </td>
                                  <td><div class="desktop"><img src="<?php echo $item['pic'];?>"><?php echo $item['id'];?></div></td>
                                  <td class="con"><?php echo $item['info'];?></td>
                                  <td class="con"><?php port_pro_type2($item['proid'],$item['type']);?>
                                  </td>
                                  <td class="con font5">單價:<br>NT$ <?php echo number_format($item['price']);?> 元</td>
                                  <td class="con font5">數量 : <input name="odr_qu[]" id="odr_qu[]" value="<?php echo $item['qty'];?>" type="text" size="2"/></td>
                                  <td class="con font5">NT$ <?php echo number_format($item['subtotal']);?></td>
                               </tr>
                                  <?php $prsubd .=subsidy_pro($Useid, $item['proid']);?><!-- subsidy -->
                                <?php }}?>
                            </tbody>
                       </table>
                       </div>
                       <div class="col-sm-12 text-right spn1">
	    	             <div class="cart_total">
                            小計:NT$<?php echo number_format($cart->total);?>
                            <button name="cr_od_Up" value="cr_od_Up" title="更新數量" class="btn button1">更新數量</button>
                            <button name="cr_od_Del" value="cr_od_Del" title="清空購物車" class="btn">清空購物車</button></div>
                       </div>
                       <!-- 加購商品 -->
                       <div class="col-sm-12 font4 spn1">加購商品</div>
                       <div class="col-sm-12 table-responsive">
                          <table class="table">
                              <tr><?php other_pro_oder($pro_d, $cat_d, $urls);?></tr> 
                          </table>
                       </div>
                    </div>
                 </div>
                 <!---- setp 2 ---->
                 <div class="col-md-12 hline">
                    <!-- E-Coupon -->
                    <div class="col-sm-12 spn1">
                        <?php subsidy_view_odr($prsubd);?>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <!-- Button -->
                     <div class="text-right">
                        <button class="btn button1" type="button" title="繼續選購" onclick="location.href='index.php'">繼續選購</button>
                        <input name="itemcount" type="hidden" id="itemcount" value="<?php echo $cart->itemcount;?>" />
                        <button id="cr_od_send" name="cr_od_send" value="cr_od_send" title="確認訂單" class="btn">確認訂單</button>
                     </div>
                  </div>
              </form>
           </div>
         </div>
      </article>
   </div><!--內容 結尾-->
 </div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!--END sql-->