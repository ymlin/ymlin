<?php
	class TemplateStruct {
		private $header = "header.php";
		private $footer = "footer.php";
		private $content;
		function setContent($p)
		{
			$this->content = $p;
		}
		
		function output()
		{
			include $this->header;
			echo $this->content;
			include $this->footer;
		}
	}
?>
		
				