<?php
/*
由 webforce cart v.1.2 修改 (http://webforce.co.nz/cart)
---------------------------------------------------------
織夢平台2005 - 分享是成長的開始
http://www.e-dreamer.idv.tw
*/
class edCart {
	var $total = 0;
	var $deliverfee = 0; //修改，運費
	var $sub_price = 0; //修改，E-Coupon
	var $grandtotal2 = 0; //加上了運費扣除電子票後的總合費用
	var $grandtotal3 = 0; //加上了運費扣除電子票及運費的總合費用
	var $itemcount = 0;
	var $items = array();
	var $itemprices = array();
	var $itemqtys = array();
	var $iteminfo = array();
	var $itempic = array();
	var $itemfreg = array();

	function cart() {} // 宣告函數

	function get_contents(){ // 取得購物車內容
		$items = array();
		foreach($this->items as $tmp_item){
		    $item = FALSE;
			$item['id'] = $tmp_item;
			$item['proid'] = $this->itemproid[$tmp_item];
			$item['qty'] = $this->itemqtys[$tmp_item];
			$item['price'] = $this->itemprices[$tmp_item];
			$item['info'] = $this->iteminfo[$tmp_item];
			$item['type'] = $this->itemtypes[$tmp_item];
			$item['pic'] = $this->itempic[$tmp_item];
			$item['freight'] = $this->itemfreg[$tmp_item];
			$item['subtotal'] = $item['qty'] * $item['price'];
            $item['freg_total'] = $item['freight']+$item['subtotal'];
			$items[] = $item;
		}
		return $items;
	} 


	function add_item($itemid,$proid=FALSE,$qty=1,$price=FALSE,$info=FALSE,$types=FALSE,$pic =FALSE,$freg =FALSE){ // 新增至購物車no,product=id,number,price,info,type,img,freight;
		//if(!$price){
			//$price = ed_get_price($itemid,$qty);
		//}
		//if(!$info){
			//$info = ed_get_info($itemid);
		//}
		  if(isset($this->itemqtys[$itemid]) && $this->itemqtys[$itemid] > 0){ 
			$this->itemqtys[$itemid] = $qty + $this->itemqtys[$itemid];
			$this->_update_total();
		  } else {
			$this->items[]=$itemid;
			$this->itemproid[$itemid]=$proid;
			$this->itemqtys[$itemid] = $qty;
			$this->itemprices[$itemid] = $price;
			$this->iteminfo[$itemid] = $info;
			$this->itemtypes[$itemid] = $types;			
			$this->itempic[$itemid] = $pic;
            $this->itemfreg[$itemid] = $freg;
		  }
		    $this->_update_total();

	} 

	function edit_item($itemid,$qty,$types){ // 更新購物車數量,商品類別
		if($qty < 1) {
			$this->del_item($itemid);
		} else {
			$this->itemqtys[$itemid] = $qty;
			$this->itemtypes[$itemid] = $types;
		}
		$this->_update_total();
	}
    
    function edit_itemecoupon($itemid, $sqldb, $listt){ // 更新折扣
        if($listt == 1){
		   $this->itemshoping[$itemid] = $sqldb;
        }else{
           $this->itemsubprice[$itemid] = $sqldb;
        }
		$this->_update_total();
	}
    
	function edit_itemshopping($itemid, $sqldb){ // 更新購物車每件商品運費
		$this->itemfreg[$itemid] = $sqldb;
		$this->_update_total();
	}
    
	function del_item($itemid){ // 移除購物車
		$ti = array();
		$this->itemqtys[$itemid] = 0;
		foreach($this->items as $item){
			if($item != $itemid){
				$ti[] = $item;
			}
		}
		$this->items = $ti;
		$this->_update_total();
	} 


	function empty_cart(){ // 清空購物車
		$this->total = 0;
		$this->itemcount = 0;
		$this->items = array();
		$this->itemprices = array();
		$this->itemqtys = array();
		$this->itemdescs = array();
	} 


	function _update_total(){ // 更新購物車的內容
		$this->itemcount = 0;
		$this->total = 0;
		if(sizeof($this->items > 0)){
			foreach($this->items as $item) {
				$this->total = $this->total + ($this->itemprices[$item] * $this->itemqtys[$item]);
				//$this->total = $this->total + $this->itemfreg[$item];//商品單品運費;
				$this->itemcount++;
			}
		}
		$this->grandtotal2 = $this->total - $this->sub_price; //不含運費
		$this->grandtotal3 = $this->total + $this->deliverfee - $this->sub_price; //計算最後總計
	} 
/*--------------------------------------------------------------------------------*/
    var $brids = array();
	var $bridbrid = array();
	var $bridproid = array();
	var $bridprices = array();
	var $bridnum = array();
	var $bridsub = array();
    
    function get_bread(){ // 取得購物車內容
		$brids = array();
		foreach($this->brids as $tmp_brid){
		    $item = FALSE;
			$item['id'] = $tmp_brid;
			$item['brid'] = $this->bridbrid[$tmp_brid];
            $item['proid'] = $this->bridproid[$tmp_brid];
			$item['total'] = $this->bridsub[$tmp_brid];
			$items[] = $item;
		}
		return $items;
	} 
    
	function add_brdid($shno, $brid, $brproid, $brprice, $brnum){ //新增品牌
		  if(isset($this->bridbrid[$brid]) && $this->bridbrid[$brid] == $brid){
              $dsub = $this->bridsub[$brid];
              $this->bridsub[$brid] = $dsub + ($brprice * $brnum);      
		  } else {
			$this->brids[]= $brid;
			$this->bridbrid[$brid]= $brid;
			$this->bridproid[$brid]= $brproid;
			$this->bridprices[$brid] = $brprice;
			$this->bridnum[$brid] = $brnum;
			$this->bridsub[$brid] = $brprice * $brnum;
		  }
	}
	function empty_brdid(){ // 清空購物車
		    $this->brids = array();
            $this->bridbrid = array();
            $this->bridproid = array();
			$this->bridprices = array();
			$this->bridnum = array();
			$this->bridsub = array();
	}
	function del_brdid($shno){ // 移除購物車
		$ti = array();
		$this->bridsub[$shno] = 0;
		foreach($this->brids as $item){
			if($item != $shno){
				$ti[] = $item;
			}
		}
		$this->brids = $ti;
	} 
}
?>