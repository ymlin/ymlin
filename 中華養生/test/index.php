﻿
    <?php @require_once("header.php");?>
        <!-- Loader -->
    	<div class="loader">
    		<div class="loader-img"></div>
    	</div>
		
		<!-- Top menu -->
		<nav>
			<a class="scroll-link" href="#top-content">Game</a>
			<a class="scroll-link" href="#what-we-do">Concept</a>
			<a class="scroll-link" href="#process">Process</a>
			<a class="scroll-link" href="#portfolio">pros & cons</a>
			<a class="scroll-link" href="#testimonials">Tool</a>
			<a class="scroll-link" href="#team">About</a>
			
			<div class="hide-menu">
				<a href=""><i class="fa fa-bars"></i></a>
			</div>
		</nav>
		<div class="show-menu">
			<a href=""><i class="fa fa-bars"></i></a>
		</div>
		
        <!-- Top content -->
        <div class="top-content">
            <div class="top-content-text wow fadeInUp">
            	<div class="divider-2"><span></span></div>
            	<h1><a href="">This is Monopoly</a></h1>
            	<div class="divider-2"><span></span></div>
            	<p>This is  Monopoly. A game connect web and mobile . you can use it with your friends whereever they are . Enjoy!</p>
            	<div class="top-content-bottom-link">
            		<a class="big-link-1" href="roomlist.php">Start &nbsp game &nbsp !</a>
            	</div>
            </div>
        </div>
        
        <!-- What we do -->
        <div class="what-we-do-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 what-we-do section-description wow fadeIn">
	                    <h2>What we do</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                    <p>
	                    	Combine &nbsp web design &nbsp and &nbsp mobile app &nbsp. &nbsp Like &nbsp Nintendo's Wii
						</p>
							<p>
							Let &nbsp your friends &nbsp have &nbsp a &nbsp new &nbsp try &nbsp.&nbsp Just &nbsp enjoy!
	                    </p>
	                </div>
	            </div>
	            <div class="row">
                	<div class="col-sm-4 what-we-do-box wow fadeInUp">
	                	<div class="what-we-do-box-icon"><i class="fa fa-pencil"></i></div>
	                    <h3>Web design</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                    
					</div>
                    <div class="col-sm-4 what-we-do-box wow fadeInDown">
	                	<div class="what-we-do-box-icon"><i class="fa fa-gears"></i></div>
	                    <h3>Mobile App</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                    </div>
                    <div class="col-sm-4 what-we-do-box wow fadeInUp">
	                	<div class="what-we-do-box-icon"><i class="fa fa-twitter"></i></div>
	                    <h3>Social media</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                    </div>
	            </div>
	        </div>
        </div>
        
        <!-- Our process -->
        <div class="block-1-container process-container section-container section-container-gray">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 block-1 section-description wow fadeIn">
	                	<h2>Our process</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                    <p>
	                    	Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum 
	                    	est notare quam littera gothica, quam nunc putamus parum claram lorem.
	                    </p>
	                </div>
	            </div>
	            <div class="row">
	            	<div class="col-sm-6 block-1-left wow fadeInLeft">
	            		<div class="slider-1-box slider-1-our-process">
	            			<img class="slider-1-img-active" src="assets/img/process/2.jpg" alt="" data-at2x="assets/img/process/2.jpg">
	                    	<img src="assets/img/process/1.jpg" alt="" data-at2x="assets/img/process/1.jpg">
	                    	<img src="assets/img/process/3.jpg" alt="" data-at2x="assets/img/process/3.jpg">
	                    	<div class="slider-1-nav"></div>
	            		</div>
	            	</div>
	            	<div class="col-sm-6 block-1-right wow fadeInUp">
	            		<h3>First step</h3>
	            		<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
	            		<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
	            		<h3>Second step</h3>
	            		<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram lorem ullamcorper suscipit lobortis nisl ut aliquip.</p>
	            	</div>
	            </div>
	        </div>
        </div>
        
        <!-- Clients -->
       
        
        <!-- Counters -->
        <div class="counters-container section-container section-container-full-bg">
        	<div class="container">
        		<div class="row">
	            	<div class="col-sm-3 counter-box wow fadeInUp">
                    	<h4>NCTU</h4>
                    	<p>Proud of  Unniversity</p>
	            	</div>
	            	<div class="col-sm-3 counter-box wow fadeInDown">
                    	<h4>CS</h4>
                    	<p>Department of Enlightenment </p>
	            	</div>
	            	<div class="col-sm-3 counter-box wow fadeInUp">
                    	<h4>X</h4>
                    	<p>Lines of code</p>
	            	</div>
	            	<div class="col-sm-3 counter-box wow fadeInDown">
                    	<h4>Graduated</h4>
                    	<p>Project of my team</p>
	            	</div>
	            </div>
        	</div>
        </div>
        
        <!-- Portfolio -->
        <div class="portfolio-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 portfolio section-description wow fadeIn">
	                	<h2>The pros and cons </h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                    <p>
	                    	 <div style="font-size:28px">★甘苦談小分享★</div>
							 </p>
							abore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
	                    </p>
	                </div>
	            </div>
	            <div class="row">
	            	<div class="col-sm-12 portfolio-filters wow fadeInUp">
	            		<a href="#" class="filter-all active">All</a> / 
	            		<a href="#" class="filter-design">Design</a> / 
	            		<a href="#" class="filter-development">Development</a> / 
	            		<a href="#" class="filter-branding">Branding</a>
	            	</div>
	            </div>
	            <div class="row">
	            	<div class="col-sm-12 portfolio-masonry">
		                <div class="portfolio-box design">
		                	<img src="assets/img/portfolio/1.jpg" alt="" data-at2x="assets/img/portfolio/1.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Lorem website</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box development">
		                	<img src="assets/img/portfolio/2.jpg" alt="" data-at2x="assets/img/portfolio/2.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Sit amet logo</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box branding">
		                	<img src="assets/img/portfolio/3.jpg" alt="" data-at2x="assets/img/portfolio/3.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Ipsum social</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box development">
		                	<img src="assets/img/portfolio/4.jpg" alt="" data-at2x="assets/img/portfolio/4.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Etiam processus dynamicus</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box design">
		                	<img src="assets/img/portfolio/5.jpg" alt="" data-at2x="assets/img/portfolio/5.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Processus dynamicus logo</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box development">
		                	<img src="assets/img/portfolio/6.jpg" alt="" data-at2x="assets/img/portfolio/6.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Mirum est notare</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box design">
		                	<img src="assets/img/portfolio/7.jpg" alt="" data-at2x="assets/img/portfolio/7.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">March notare site</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box development">
		                	<img src="assets/img/portfolio/8.jpg" alt="" data-at2x="assets/img/portfolio/8.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Sit amet logo</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box branding">
		                	<img src="assets/img/portfolio/9.jpg" alt="" data-at2x="assets/img/portfolio/9.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Lorem website</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box design">
		                	<img src="assets/img/portfolio/10.jpg" alt="" data-at2x="assets/img/portfolio/10.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Mirum est notare</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box development">
		                	<img src="assets/img/portfolio/11.jpg" alt="" data-at2x="assets/img/portfolio/11.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Etiam processus social</a></p>
		                		</div>
		                	</div>
		                </div>
		                <div class="portfolio-box branding">
		                	<img src="assets/img/portfolio/12.jpg" alt="" data-at2x="assets/img/portfolio/12.jpg">
		                	<div class="portfolio-box-text-container">
		                		<div class="portfolio-box-text">
		                			<p><a href="#">Sit website logo</a></p>
		                		</div>
		                	</div>
		                </div>
	                </div>
	            </div>
	        </div>
        </div>

        <!-- Testimonials -->
        <div class="testimonials-container section-container section-container-gray">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 testimonials section-description wow fadeInUp">
	                    <h2>Tool</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                    <div class="testimonial-active"></div>
	                    <div class="testimonial-single" style="font-size:35px">
	                    	
							<p>
	                    		 html5  &nbsp &nbsp  Bootstrap  
	                    		<br>
	                    		<a href=""> Framework for 	developing<br>
							 responsive, mobile-first web sites.</a>
	                    	</p>
	                    	<div class="testimonial-single-image">
	                    		<img src="assets/img/testimonials/1.jpg" alt="" data-at2x="assets/img/testimonials/1.jpg">
	                    	</div>
	                    </div>
	                    <div class="testimonial-single">
	                    	<p>
	                    		 Mysql &nbsp &nbsp p hp 
								<br>
	                    		<a href=""> &nbsp  &nbsp Standard language for accessing databases.
									<br>
									Server scripting language
								
								</a>
	                    	</p>
						
	                    	<div class="testimonial-single-image">
	                    		<img src="assets/img/testimonials/2.jpg" alt="" data-at2x="assets/img/testimonials/2.jpg">
	                    	</div>
	                    </div>
	                    <div class="testimonial-single">
	                    	<p>
	                    		  Javascript &nbsp &nbsp  CSS  
								 <br>
	                    		<a href=""> Programming language of HTML and the Web.
									<br>
									Stylesheet language 
								</a>
	                    	</p>
	                    	<div class="testimonial-single-image">
	                    		<img src="assets/img/testimonials/3.jpg" alt="" data-at2x="assets/img/testimonials/3.jpg">
	                    	</div>
	                    </div>
	                    <div class="testimonial-single">
	                    	<p>
	                    		Java
								<br>
	                    		<a href="">Mobile App design </a>
	                    	</p>
	                    	<div class="testimonial-single-image">
	                    		<img src="assets/img/testimonials/4.jpg" alt="" data-at2x="assets/img/testimonials/4.jpg">
	                    	</div>
	                    </div>
	                </div>
	            </div>
	        </div>
        </div>
        
        <!-- Our motto -->
        <div class="our-motto-container section-container section-container-full-bg">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 our-motto section-description wow fadeInLeftBig">
	                    <p>
	                    	"Success is not the key to happiness. Happiness is the key to success. 
	                    	If you love what you are doing, you will be successful."
	                    </p>
	                    <div class="our-motto-author">Wish  &nbsp our  &nbsp team  &nbsp success</div>
	                </div>
	            </div>
	        </div>
        </div>
        
        <!-- The team -->
        <div class="team-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 team section-description wow fadeIn">
	                    <h2>The team</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                    <p>
	                    	Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut 
	                    	aliquip ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud.
	                    </p>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-3 team-box wow fadeInUp">
		                <div class="team-photo">
		                	<img src="assets/img/about/1.jpg" alt="" data-at2x="assets/img/about/1.jpg">
		                </div>
	                    <h3>John Doe</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
	                    <div class="team-social">
		                	<a href="#"><i class="fa fa-facebook"></i></a>
		                	<a href="#"><i class="fa fa-dribbble"></i></a>
		                    <a href="#"><i class="fa fa-twitter"></i></a>
		                </div>
	                </div>
	                <div class="col-sm-3 team-box wow fadeInDown">
		                <div class="team-photo">
		                	<img src="assets/img/about/2.jpg" alt="" data-at2x="assets/img/about/2.jpg">
		                </div>
	                    <h3>Tim Brown</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
	                    <div class="team-social">
		                	<a href="#"><i class="fa fa-facebook"></i></a>
		                	<a href="#"><i class="fa fa-dribbble"></i></a>
		                    <a href="#"><i class="fa fa-twitter"></i></a>
		                </div>
	                </div>
	                <div class="col-sm-3 team-box wow fadeInUp">
		                <div class="team-photo">
		                	<img src="assets/img/about/4.jpg" alt="" data-at2x="assets/img/about/4.jpg">
		                </div>
	                    <h3>Sarah Red</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
	                    <div class="team-social">
		                	<a href="#"><i class="fa fa-facebook"></i></a>
		                	<a href="#"><i class="fa fa-dribbble"></i></a>
		                    <a href="#"><i class="fa fa-twitter"></i></a>
		                </div>
	                </div>
					 <div class="col-sm-3 team-box wow fadeInUp">
		                <div class="team-photo">
		                	<img src="assets/img/about/3.jpg" alt="" data-at2x="assets/img/about/3.jpg">
		                </div>
	                    <h3>Mickey Lin</h3>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
	                    <div class="team-social">
		                	<a href="#"><i class="fa fa-facebook"></i></a>
		                	<a href="#"><i class="fa fa-dribbble"></i></a>
		                    <a href="#"><i class="fa fa-twitter"></i></a>
		                </div>
	                </div>
	            </div>
	        </div>
        </div>
        
       
        
        <!-- Scroll to top -->
        <div class="section-container section-container-gray">
	        <div class="container">
		        <div class="row">
		        	<div class="col-sm-12">
		        		<div class="scroll-to-top">
		        			<a class="scroll-link" href="#top-content"><i class="fa fa-chevron-up"></i></a>
		        		</div>
		        	</div>
		        </div>
	        </div>
        </div>
 <?php @require_once("footer.php");?>             
      
        

       