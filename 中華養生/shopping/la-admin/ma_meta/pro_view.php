<?php @require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php @require_once("../../functions/fundb.php");?>
<?php @require_once("../../functions.php");?>
<?php @require_once("../functions.php");?>
<?php @require_once("functions_mt.php");?>
<?php @require_once("../header_all.php");?>
  <!-- feature -->
  <div id="features-wrapper">
    <div class="container"><!--內容區 -->
       <article class="row clearfix"><!-- 對齊中間 -->
          <div class="col-lg-12 mem_list">
             <div class="col-md-12 top_banner_cont">Mate</div>
             <form class="form-horizontal" action="fundb.php" method="post" name="Form_edit" id="Form_edit">  
             <div class="col-md-12 hline">
                <div class="col-sm-12 font3">商品 Mate Keyword</div>     
                   <div class="col-sm-12">
                      <label class="col-sm-2 text-right font3" for="keyname">名稱</label>
                      <div class="col-sm-9">
                         <input class="input35" type="text" name="keyname" placeholder="keyname">
                      </div>
                   </div>
                   <div class="col-sm-9 col-sm-offset-2 spn1">
                      <button id="del_key_send" name="del_key_send" value="del_mt" class="btn button1">確定刪除</button>
                      <button id="pro_mt_send" name="pro_mt_send" value="pro_mt" class="btn">確定新增</button>
                   </div>

             </div>
             <div class="col-md-12 hline">
                <div class="col-sm-12 font3">Mate Keyword 檢示</div>
                   <table class="table table-bordered">
				     <thead>
                        <tr style="background-color:#e8e6e5">
                          <th width="5%">#</th>
                          <th width="5%">編號</th>
                          <th>名稱</th>
					    </tr>
                     </thead>
                     <tbody>
                        <?php sql_pkey_view();?><!--keyword檢示-->
                     </tbody> 
                   </table> 
             </div>
             </form>
          </div>             
       </article>      
    </div><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?>
<?php end_sql($end_sqls);?><!--END sql-->