<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['create'])
	{
		$sql = "INSERT INTO goodday (id, month, day, good, bad, goodtime) VALUES ('', ?, ?, ?, ?, ?)";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['month'], $_GET['day'], $_GET['good'], $_GET['bad'], $_GET['goodtime']));
	}
	else if($_GET['update'])
	{
		$sql = "UPDATE goodday SET month=?, day=?, good=?, bad=?, goodtime=? WHERE id = ? ";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['month'], $_GET['day'], $_GET['good'], $_GET['bad'], $_GET['goodtime'], $_GET['update']));
	}
	else if($_GET['remove'])
	{
		$sql = "DELETE FROM `goodday` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>宜/忌 管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px;" >
	<?php include __DIR__.'/../admin_top.php';?>
	
	<div style="width:80%; margin: 0px auto;">  
		<table  border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="6" id="color">
				<h2 align="center">宜/忌 管理</h2>
			</td> 
			</tr>  

			<tr> 
			<td id="color2" colspan="2"><div align="center"><strong>日期</strong></div></td>
			<td id="color2"><div align="center"><strong>宜</strong></div></td>
			<td id="color2"><div align="center"><strong>忌</strong></div></td>
			<td id="color2"><div align="center"><strong>吉時</strong></div></td>
			<td id="color2"><div align="center"><strong>管理</strong></div></td>  			
			</tr>  
			
			<tr>
				<form action = "admin_goodday.php"  method = "GET">
					<td align="center" colspan="2">
						<div align="center">
							<select name="month">
								<?php
									for($i = 1 ; $i <= 12 ; $i++)
										echo "<option value=\"{$i}\">{$i}</option>";
								?>
							</select>
							<select name="day">
								<?php
									for($i = 1 ; $i <= 31 ; $i++)
										echo "<option value=\"{$i}\">{$i}</option>";
								?>
							</select>
						</div>
					</td> 
					<td align="center">
						<input  name="good"  type="text" size="50" value="">
					</td>
					<td align="center">
						<input  name="bad"  type="text" size="50" value="">
					</td>
					<td align="center">
						<input  name="goodtime"  type="text" size="10" value="">
					</td>
					<td align="center">
						<button  class='btn btn-primary' type = "submit" name = "create" value= "1">新增</button>  
					</td> 
				</form>
			</tr>
			
			<?php
				$sql = "SELECT * FROM `goodday` ORDER BY month,day";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				while($result = $sth->fetchobject())
				{
					echo "<tr>";
					
				?>
					<form action = "admin_goodday.php"  method = "GET">
				<?php
						if($_GET['edit'] == $result->id)
						{
				?>
							<td align="center" colspan="2">
								<select name="month">
									<?php
										for($i = 1 ; $i <= 12 ; $i++)
										{
											if($i == $result->month)
												echo "<option value=\"{$i}\" selected>{$i}</option>";
											else
												echo "<option value=\"{$i}\">{$i}</option>";
										}
									?>
								</select>
								<select name="day">
									<?php
										for($i = 1 ; $i <= 31 ; $i++)
										{
											if($i == $result->day)
												echo "<option value=\"{$i}\" selected>{$i}</option>";
											else
												echo "<option value=\"{$i}\">{$i}</option>";
										}
									?>
								</select>
							</td>
							<td align="center">
								<input  name="good"  type="text" size="50" value="<?php echo $result->good;?>">
							</td>
							<td align="center">
								<input  name="bad"  type="text" size="50" value="<?php echo $result->bad;?>">
							</td>
							<td align="center">
								<input  name="goodtime"  type="text" size="10" value="<?php echo $result->goodtime;?>">
							</td>
				<?php
						}
						else
						{
							echo "<td align=\"center\"  colspan=\"2\">" . $result->month . "月 ";
							echo $result->day . "日" . "</td>";
							echo "<td align=\"center\" >" . $result->good . "</td>";
							echo "<td align=\"center\" >" . $result->bad . "</td>";
							echo "<td align=\"center\" >" . $result->goodtime . "</td>";
						}
						
						if($_GET['edit'] == $result->id)
						{
				?>
							<td align="center" style="white-space:nowrap;">
								<button  class='btn btn-primary' type = "submit" name = "update" value= "<?php echo  $result->id ?>">確認修改</button>  
							</td>
				<?php			
						}
						else
						{
				?>
							<td align="center" >
								<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
								<button  class='btn btn-primary' formaction="admin_goodday.php" type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
							</td>
				<?php
						}
				?>
					</form>
				<?php
				}
			?>
		</table>  
	</div>  
</body>  
</html>