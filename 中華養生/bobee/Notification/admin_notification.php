<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['create'])
	{
		$currenttime = date("Y-m-d H:i:s");
		$sql = "INSERT INTO notification (id, comment, shortcomm, religion, buddha, season, month, day, datetime) VALUES ('', ?, ?, ?, ?, ?, ?, ?, ?)";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['comment'], $_GET['shortcomm'], $_GET['religion'], $_GET['buddha'], $_GET['season'], $_GET['month'], $_GET['day'], $currenttime));
	}
	else if($_GET['update'])
	{
		$currenttime = date("Y-m-d H:i:s");
		$sql = "UPDATE notification SET comment=?, shortcomm=?, religion=?, buddha=?, season=?, month=?, day=?, datetime=? WHERE id = ? ";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['comment'], $_GET['shortcomm'], $_GET['religion'], $_GET['buddha'], $_GET['season'], $_GET['month'], $_GET['day'], $currenttime, $_GET['update']));
	}
	else if($_GET['remove'])
	{
		$sql = "DELETE FROM `notification` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
	
	if($_GET['addart'])
	{
		$sql = "INSERT INTO `notification_relative_component` (id, notification_id, component_id, article_product) " 
			. " VALUES('', ?, ?, 0) ";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['addart'], $_GET['component_id']));
	}
	else if($_GET['rmart'])
	{
		$sql = "DELETE FROM `notification_relative_component`" 
			. " WHERE notification_id = ? and component_id = ? and article_product = 0";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['rmart'], $_GET['component_id']));
	}
	
	if($_GET['addprod'])
	{
		$sql = "INSERT INTO `notification_relative_component` (id, notification_id, component_id, article_product) " 
			. " VALUES('', ?, ?, 1) ";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['addprod'], $_GET['component_id']));
	}
	else if($_GET['rmprod'])
	{
		$sql = "DELETE FROM `notification_relative_component`" 
			. " WHERE notification_id = ? and component_id = ? and article_product = 1";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['rmprod'], $_GET['component_id']));
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>提醒管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px;" >
	<?php include __DIR__.'/../admin_top.php';?>
	
	<div style="width:80%; margin: 0px auto;">  
		<table  border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="12" id="color">
				<h2 align="center">提醒管理</h2>
			</td> 
			</tr>  

			<tr>
			<td id="color2"><div align="center"><strong>id</strong></div></td> 
			<td id="color2"><div align="center"><strong>內容</strong></div></td>
			<td id="color2"><div align="center"><strong>短標題</strong></div></td>
			<td id="color2"><div align="center"><strong>時間</strong></div></td>
			<td id="color2" colspan="2"><div align="center" ><strong>分類</strong></div></td>
			<td id="color2"><div align="center"><strong>最後編輯</strong></div></td>
			<td id="color2" colspan="2"><div align="center"><strong>關聯文章管理</strong></div></td>
			<td id="color2" colspan="2"><div align="center"><strong>關聯產品管理</strong></div></td>
			<td id="color2"><div align="center"><strong>管理</strong></div></td>  			
			</tr>  
			
			<tr>
				<form action = "admin_notification.php"  method = "GET">
					<td></td>
					<td align="center">
						<input  name="comment"  type="text" size="40" value="">
					</td> 
					
					<td align="center">
						<input  name="shortcomm"  type="text" size="8" value="">
					</td> 
					
					<td align="center" colspan="2">
						<div align="center">
							<select name="month">
								<?php
									for($i = 0 ; $i <= 12 ; $i++)
										echo "<option value=\"{$i}\">{$i}</option>";
								?>
							</select>
							<select name="day">
								<?php
									for($i = 1 ; $i <= 31 ; $i++)
										echo "<option value=\"{$i}\">{$i}</option>";
								?>
							</select>
						</div>
					</td> 
					<td align="center">
						<div align="center">
							<select name="religion">
								<?php
									$sql = "SELECT * FROM `religion`";
									$sth = $db->prepare($sql);
									$sth->execute();
									while($result = $sth->fetchobject())
									{
										echo "<option value=\"{$result->name}\">{$result->name}</option>";
									}
								?>
							</select>
							<select name="buddha">
								<?php
									$sql = "SELECT * FROM `buddha`";
									$sth = $db->prepare($sql);
									$sth->execute();
									while($result = $sth->fetchobject())
									{
										echo "<option value=\"{$result->name}\">{$result->name}</option>";
									}
								?>
							</select>
							<select name="season">
								<?php
									$sql = "SELECT * FROM `season`";
									$sth = $db->prepare($sql);
									$sth->execute();
									while($result = $sth->fetchobject())
									{
										echo "<option value=\"{$result->name}\">{$result->name}</option>";
									}
								?>
							</select>
						</div>
					</td>
					<td align="center"></td>
					<td colspan="4"></td>
					<td align="center">
						<button  class='btn btn-primary' type = "submit" name = "create" value= "1">新增</button>  
					</td> 
				</form>
			</tr>
			
			<?php
				$sql = "SELECT * FROM `notification`";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				while($result = $sth->fetchobject())
				{
					echo "<tr>";
					
				?>
					<form action = "admin_notification.php"  method = "GET">
				<?php
						echo "<td align=\"center\" >" . $result->id . "</td>";	
						
						if($_GET['edit'] == $result->id)
						{
				?>
							<td align="center">
								<input  name="comment"  type="text" size="40" value="<?php echo $result->comment;?>">
							</td> 
							
							<td align="center">
								<input  name="shortcomm"  type="text" size="8" value="<?php echo $result->shortcomm;?>">
							</td> 
							
							<td align="center" colspan="2">
								<select name="month">
									<?php
										for($i = 0 ; $i <= 12 ; $i++)
										{
											if($i == $result->month)
												echo "<option value=\"{$i}\" selected>{$i}</option>";
											else
												echo "<option value=\"{$i}\">{$i}</option>";
										}
									?>
								</select>
								<select name="day">
									<?php
										for($i = 1 ; $i <= 31 ; $i++)
										{
											if($i == $result->day)
												echo "<option value=\"{$i}\" selected>{$i}</option>";
											else
												echo "<option value=\"{$i}\">{$i}</option>";
										}
									?>
								</select>
							</td>
							<td align="center">
								<div align="center">
									<select name="religion">
										<?php
											$sql2 = "SELECT * FROM `religion`";
											$sth2 = $db->prepare($sql2);
											$sth2->execute();
											while($result2 = $sth2->fetchobject())
											{
												if($result2->name == $result->religion) echo "<option value=\"{$result2->name}\" selected>{$result2->name}</option>";
												else									echo "<option value=\"{$result2->name}\">{$result2->name}</option>";
											}
										?>
									</select>
									<select name="buddha">
										<?php
											$sql2 = "SELECT * FROM `buddha`";
											$sth2 = $db->prepare($sql2);
											$sth2->execute();
											while($result2 = $sth2->fetchobject())
											{
												if($result2->name == $result->buddha) echo "<option value=\"{$result2->name}\" selected>{$result2->name}</option>";
												else									echo "<option value=\"{$result2->name}\">{$result2->name}</option>";
											}
										?>
									</select>
									<select name="season">
										<?php
											$sql2 = "SELECT * FROM `season`";
											$sth2 = $db->prepare($sql2);
											$sth2->execute();
											while($result2 = $sth2->fetchobject())
											{
												if($result2->name == $result->season) echo "<option value=\"{$result2->name}\" selected>{$result2->name}</option>";
												else									echo "<option value=\"{$result2->name}\">{$result2->name}</option>";
											}
										?>
									</select>
								</div>
							</td>
							<td></td>
				<?php
						}
						else
						{
							echo "<td align=\"center\" >";
							echo mb_substr($result->comment,0,15,"UTF-8");
							if(mb_strlen($result->comment,"UTF-8") > 15) echo "...";
							echo "</td>";
							echo "<td align=\"center\" >" . $result->shortcomm . "</td>";
							echo "<td align=\"center\"  colspan=\"2\">" . $result->month . "月 ";
							echo $result->day . "日" . "</td>";
							echo "<td align=\"center\" >" . $result->religion . "/" . $result->buddha . "/" . $result->season . "</td>";
							echo "<td align=\"center\" >" . $result->datetime . "</td>";
						}
						
						echo "<td align=\"center\">";
						
						$sql2 = "SELECT * FROM `notification_relative_component`" 
								. " WHERE `notification_id` = ? and article_product = 0";
						$sth2 = $db->prepare($sql2);
						$sth2->execute(array($result->id));
						
						$count = 1;
						while($result2 = $sth2->fetchobject())
						{
							$sql3 = "SELECT * FROM `articles`" 
								. " WHERE `id` = ?";
							$sth3 = $db->prepare($sql3);
							$sth3->execute(array($result2->component_id));
							$result3 = $sth3->fetchobject();
							echo "<div style=\"white-space:nowrap;\">" . $count . ".";
							echo mb_substr($result3->title,0,15,"UTF-8");
							if(mb_strlen($result3->title,"UTF-8") > 15) echo "...";
							$count++;
						}
						
						echo "</td><td>";
						
						if($_GET['addarticle'] == $result->id)
						{
							$sql2 = "SELECT * FROM `articles`";
							$sth2 = $db->prepare($sql2);
							$sth2->execute();
							
							echo "<select name=\"component_id\">";
							while($result2 = $sth2->fetchobject())
							{
								$sql3 = "SELECT * FROM `notification_relative_component`" 
								. " WHERE `notification_id` = ? and `component_id` = ?";
								$sth3 = $db->prepare($sql3);
								$sth3->execute(array($result->id, $result2->id));
								$result3 = $sth3->fetchobject();
								if(!$result3)
								{
									echo "<option value=\"{$result2->id}\">";
									echo mb_substr($result2->title,0,15,"UTF-8");
									if(mb_strlen($result2->title,"UTF-8") > 15) echo "...";
									echo "</option>";
								}
							}
							echo "</select>";
				?>		
							<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "addart" value= "<?php echo $result->id;?>" >新增</button> 
				<?php			
						}
						else if($_GET['removearticle'] == $result->id)
						{
							$sql2 = "SELECT * FROM `notification_relative_component` WHERE `notification_id` = ?";
							$sth2 = $db->prepare($sql2);
							$sth2->execute(array($result->id));
							
							echo "<select name=\"component_id\">";
							while($result2 = $sth2->fetchobject())
							{
								$sql3 = "SELECT * FROM `articles`" 
								. " WHERE `id` = ?";
								$sth3 = $db->prepare($sql3);
								$sth3->execute(array($result2->component_id));
								$result3 = $sth3->fetchobject();
								echo "<option value=\"{$result2->component_id}\">";
								echo mb_substr($result3->title,0,5,"UTF-8");
								if(mb_strlen($result3->title,"UTF-8") > 5) echo "...";
								echo "</option>";
							}
							echo "</select>";
				?>	
							<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "rmart" value= "<?php echo $result->id;?>"  onclick="return confirm('是否確認刪除這筆資料');" >刪除</button> 
				<?php			
						}
						else
						{
				?>			
							<div style="white-space:nowrap;">
								<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "addarticle" value="<?php echo $result->id;?>" >新增</button>  
								<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "removearticle" value="<?php echo $result->id;?>" >刪除</button>  
							</div>
				<?php		
						}
				?>
						</td>
				<?php	
						echo "<td align=\"center\">";
						
						$sql2 = "SELECT * FROM `notification_relative_component`" 
								. " WHERE `notification_id` = ? and article_product = 1";
						$sth2 = $db->prepare($sql2);
						$sth2->execute(array($result->id));
						
						$count = 1;
						while($result2 = $sth2->fetchobject())
						{
							$sql3 = "SELECT * FROM `products`" 
								. " WHERE `id` = ?";
							$sth3 = $db->prepare($sql3);
							$sth3->execute(array($result2->component_id));
							$result3 = $sth3->fetchobject();
							echo "<div >" . $count . ".";
							echo mb_substr($result3->name,0,5,"UTF-8");
							if(mb_strlen($result3->name,"UTF-8") > 5) echo "...";
							$count++;
						}
						
						echo "</td><td>";
						
						if($_GET['addproduct'] == $result->id)
						{
							$sql2 = "SELECT * FROM `products`";
							$sth2 = $db->prepare($sql2);
							$sth2->execute();
							
							echo "<select name=\"component_id\">";
							while($result2 = $sth2->fetchobject())
							{
								$sql3 = "SELECT * FROM `notification_relative_component`" 
								. " WHERE `notification_id` = ? and `component_id` = ?";
								$sth3 = $db->prepare($sql3);
								$sth3->execute(array($result->id, $result2->id));
								$result3 = $sth3->fetchobject();
								if(!$result3)
								{
									echo "<option value=\"{$result2->id}\">";
									echo mb_substr($result2->name,0,5,"UTF-8");
									if(mb_strlen($result2->name,"UTF-8") > 5) echo "...";
									echo "</option>";
								}
							}
							echo "</select>";
				?>		
							<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "addprod" value= "<?php echo $result->id;?>" >新增</button> 
				<?php			
						}
						else if($_GET['removeproduct'] == $result->id)
						{
							$sql2 = "SELECT * FROM `notification_relative_component` WHERE `notification_id` = ?";
							$sth2 = $db->prepare($sql2);
							$sth2->execute(array($result->id));
							
							echo "<select name=\"component_id\">";
							while($result2 = $sth2->fetchobject())
							{
								$sql3 = "SELECT * FROM `products`" 
								. " WHERE `id` = ?";
								$sth3 = $db->prepare($sql3);
								$sth3->execute(array($result2->component_id));
								$result3 = $sth3->fetchobject();
								echo "<option value=\"{$result2->component_id}\">";
								echo mb_substr($result3->name,0,5,"UTF-8");
								if(mb_strlen($result3->name,"UTF-8") > 5) echo "...";
								echo "</option>";
							}
							echo "</select>";
				?>	
							<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "rmprod" value= "<?php echo $result->id;?>"  onclick="return confirm('是否確認刪除這筆資料');" >刪除</button> 
				<?php			
						}
						else
						{
				?>			
							<div style="white-space:nowrap;">
								<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "addproduct" value="<?php echo $result->id;?>" >新增</button>  
								<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "removeproduct" value="<?php echo $result->id;?>" >刪除</button>  
							</div>
				<?php		
						}
				?>
						</td>
						
				<?php
						if($_GET['edit'] == $result->id)
						{
				?>
							<td align="center" style="white-space:nowrap;">
								<button  class='btn btn-primary' type = "submit" name = "update" value= "<?php echo  $result->id ?>">確認修改</button>  
							</td>
				<?php			
						}
						else
						{
				?>
							<td align="center" >
								<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
								<button  class='btn btn-primary' formaction="admin_notification.php" type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
							</td>
				<?php
						}
				?>
					</form>
				<?php
				}
			?>
		</table>  
	</div>  
</body>  
</html>