<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
?>


<?php
	if($_GET['create'])
	{
		if(!$_GET['account'] || !$_GET['password'])
		{
?>
			<script type='text/javascript' charset="UTF-8">
			alert('帳號或密碼不可為空');
			</script>
<?php		
		}
		else
		{
			$sql = "SELECT * FROM users WHERE account = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['account']));
			$result = $sth->fetchobject();
			if($result)
			{
?>
				<script type='text/javascript' charset="UTF-8">
				alert('帳號已被使用');
				</script>
<?php						
			}
			else
			{
				$sql = "INSERT INTO users (id, account, password, name, phone, address, admin, lastlogintime, user_type, prefer ) VALUES ('', ?, ?, ?, ?, ?, ?, '', ?, ?)";
				$sth = $db->prepare($sql);
				$sth->execute(array($_GET['account'], encryptIt($_GET['password']), $_GET['name'], $_GET['phone'], $_GET['address'], $_GET['admin'], $_GET['type']=="personal"?"個人":"公司"), $_GET['prefer']);
			}
		}
	}
	else if($_GET['update'])
	{
		if(!$_GET['account'] || !$_GET['password'])
		{
?>
			<script type='text/javascript' charset="UTF-8">
			alert('帳號或密碼不可為空');
			</script>
<?php		
		}
		else
		{
			$sql = "UPDATE `users` SET account = ?, password = ?, name = ?, phone = ?, address = ?, admin = ?, user_type = ?, prefer = ? " 
				. " WHERE `id` = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['account'], encryptIt($_GET['password']), $_GET['name'], $_GET['phone'], $_GET['address'], $_GET['admin'], $_GET['type']=="personal"?"個人":"公司", $_GET['prefer'],$_GET['update']));
		}
	}
	else if($_GET['remove'])
	{
		$sql = "DELETE FROM `users` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
?>

<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>會員管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include __DIR__.'/../admin_top.php';?>
	<div style="width:80%; margin: 0px auto;">  
		<table width="1200" border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="11" id="color"><div align="center"><strong><h2>會員管理</h2></strong></div></td>  
			</tr>  

			<tr>
			<td id="color2"><div align="center"><strong>id</strong></div></td> 
			<td id="color2"><div align="center"><strong>帳號</strong></div></td> 
			<td id="color2"><div align="center"><strong>密碼</strong></div></td>
			<td id="color2"><div align="center"><strong>稱呼</strong></div></td>
			<td id="color2"><div align="center"><strong>身分</strong></div></td>
			<td id="color2"><div align="center"><strong>偏好設定</strong></div></td>
			<td id="color2"><div align="center"><strong>電話</strong></div></td>
			<td id="color2"><div align="center"><strong>住址</strong></div></td>			
			<td id="color2"><div align="center"><strong>管理權限</strong></div></td>
			<td id="color2"><div align="center"><strong>最後登入</strong></div></td>  
			<td id="color2"><div align="center"><strong>管理</strong></div></td>  

			</tr>  
			
			<tr>
			<td></td>
				<form action = "admin_users.php"  method = "GET">
					<td align="center"><input size="30" type = "text" name = "account" ></td> 
					<td align="center"><input size="12" type = "text" name = "password" ></td>
					<td align="center"><input size="20" type = "text" name = "name"></td> 
					<td align="center">
						<select name="type">
							<option value="personal">個人</option>
							<option value="company">公司</option>
						</select>
					</td>
					<td align="center"><input size="30" type = "text" name = "prefer" ></td> 
					<td align="center"><input size="30" type = "text" name = "phone" ></td> 
					<td align="center"><input size="30" type = "text" name = "address" ></td> 
					<td align="center">
						<select name="admin">
							<option value="1">有</option>
							<option value="0">無</option>
						</select>
					</td>
					<td align="center"> <?php echo $result->lastlogintime; ?></td>
					<td align="center">
						<button  class='btn btn-primary' type = "submit" name = "create" value= "1">新增</button>  
					</td>
				</form>
			</tr>

			<?php
				$sql = "SELECT * FROM `users` ORDER BY admin, lastlogintime DESC";
				$sth = $db->prepare($sql);
				$sth->execute();


				while ($result = $sth->fetchobject())
				{
					echo "<tr>";

					if($_GET['edit'] != $result->id)
					{
						echo "<td align=\"center\">" . $result->id . "</td>";	
						echo "<td align=\"center\">" . $result->account . "</td>";	
						echo "<td align=\"center\">" . decryptIt($result->password) . "</td>";
						echo "<td align=\"center\">" . $result->name . "</td>";	
						echo "<td align=\"center\">" . $result->user_type . "</td>";
						echo "<td align=\"center\">" . $result->prefer . "</td>";						
						echo "<td align=\"center\">" . $result->phone . "</td>";	
						echo "<td align=\"center\">" . $result->address . "</td>";	
						echo "<td align=\"center\">" . ( ($result->admin)?"有":"無" ) . "</td>";
						echo "<td align=\"center\">" . $result->lastlogintime . "</td>";
				?>	
					<td align="center">
						<form action = "admin_users.php"  method = "GET">
							<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
							<button  class='btn btn-primary' type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
						</form>
					</td>  
				<?php
					}
					else
					{
						echo "<td align=\"center\">" . $result->id . "</td>";
				?>
					<form action = "admin_users.php"  method = "GET">
						<td align="center"><input size="30" type = "text" name = "account" value="<?php echo $result->account; ?>"></td> 
						<td align="center"><input size="12" type = "text" name = "password" value="<?php echo decryptIt($result->password); ?>"></td>
						<td align="center"><input size="20" type = "text" name = "name" value="<?php echo $result->name; ?>"></td> 
						<td align="center">
							<select name="type">
								<option value="personal" <?php if($result->user_type=="個人") echo "selected";?>>個人</option>
								<option value="company" <?php if($result->user_type=="公司") echo "selected";?>>公司</option>
							</select>
						</td>
						<td align="center"><input size="30" type = "text" name = "prefer" value="<?php echo $result->prefer; ?>"></td> 
						<td align="center"><input size="30" type = "text" name = "phone" value="<?php echo $result->phone; ?>"></td> 
						<td align="center"><input size="30" type = "text" name = "address" value="<?php echo $result->address; ?>"></td> 
						<td align="center">
							<select name="admin">
								<option value="1" <?php if($result->admin=="1") echo "selected";?>>有</option>
								<option value="0" <?php if($result->admin=="0") echo "selected";?>>無</option>
							</select>
						</td>
						<td align="center"> <?php echo $result->lastlogintime; ?></td>
						<td align="center">
							<button  class='btn btn-primary' type = "submit" name = "update" value= "<?php echo  $result->id ?> ">確認修改</button>  
						</td>
					</form>
				<?php	
					}
				?>
					</tr>
			<?php
				}	
			?>
		</table>  
	</div>  
</body>  
</html>