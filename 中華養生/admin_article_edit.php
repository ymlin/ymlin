<?php
	require_once("dbtools.inc.php"); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>新增文章</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include 'link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include 'admin_top.php';?>
	<div class="container">  
		<form class='form-horizontal' action="upload_file.php" method="post" name="myform">
			<fieldset>
				<legend>新增文章</legend>
				<div class="form-group">
					<label class="col-md-2 control-label" for='account'>標題</label>
					<div class="controls col-md-6">
						<input  name="title"  type="text" size="40" value="">
					</div>
				</div>
					  
				<div class="form-group">
					<label class="col-md-2 control-label">作者</label>
					<div class="controls col-md-6">
					<input  name="editor" type="text" size="20" value="<?php echo $row4['name'] ?>	" />
				</div>

				</div>
				<div class="form-group">
				<label class="col-md-2 control-label" for="input01">分類</label>
					<div class="controls col-md-6">
						<select name="type">
							<?php
								$sql = "SELECT * FROM `user_type`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									echo "<option value=\"{$result->id}\">{$result->type}</option>";
								}
							?>
						</select>
						<select name="religion">
							<?php
								$sql = "SELECT * FROM `religion`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									echo "<option value=\"{$result->id}\">{$result->name}</option>";
								}
							?>
						</select>
						<select name="buddha">
							<?php
								$sql = "SELECT * FROM `buddha`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									echo "<option value=\"{$result->id}\">{$result->name}</option>";
								}
							?>
						</select>
						<select name="season">
							<?php
								$sql = "SELECT * FROM `season`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									echo "<option value=\"{$result->id}\">{$result->name}</option>";
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label">keyword</label>
					<div class="controls col-md-6">
						<input  name="keyword" type="text" size="20" value="" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">標籤</label>
					<div class="controls col-md-6">
						<input  name="tag" type="text" size="20" value="" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="input01">日期</label>
					<div class="controls col-md-6">
						<input name="datetime" id="datetimepicker1" type="text"  value="">
						<script language="JavaScript">
							$(document).ready(function(){ 
								var opt={dateFormat: 'yy-mm-dd',
									showSecond: true,
									timeFormat: 'HH:mm:ss'
									};
								$('#datetimepicker1').datetimepicker(opt);
							});
						</script>
					</div>
				</div>
			</fieldset>
			
			<hr>
			<div class="form-group">
				<label class="col-md-2 control-label" for="input01">內容</label>
				<div class="controls col-md-6">
					<textarea name="comment" cols="85" rows="30"  ></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label">上傳圖片</label>
				<div class="controls col-md-6">
					<input type="hidden" name="MAX_FILE_SIZE" value="1048576">
					<input type="file" name="myfile" size="50"><br><br>
					<input type="submit" value="上傳">
					<input type="reset" value="重新設定">
				</div>
			</div>

		</form>

	</div>  
	
	
	
	<!-- script references -->
	<script src="js/bootstrap.min.js"></script>
</body>  
</html>