<?php
	require_once("dbtools.inc.php"); 

	session_start();
	
	$account = $_SESSION['account'] = $_POST['account'];
	$password = $_SESSION['password'] = $_POST['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($_POST['account']));
	
	$result = $sth->fetchobject();
	
	if($password == decryptIt($result->password))
	{
		if($result->admin == '0')
		{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("尚未運作，敬請期待");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
			//header("Location: user_homepage.php");
			exit;
		}
		else if($result->admin == '1')
		{
			$currenttime = date("Y-m-d H:i:s");
			$sql = "UPDATE `users` SET lastlogintime = ? " 
			. " WHERE `account` = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($currenttime, $account));
			header("Location: UserManagement/admin_users.php");
			exit;
		}
	}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("帳號或密碼錯誤");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
?>
