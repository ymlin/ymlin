<?php
	require_once("dbtools.inc.php"); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($password == decryptIt($result->password))
	{
		//清除 cookie 內容
		session_destroy();

		//將使用者導回主網頁
		header("location:http://bobee.begoodlive.com");
	}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}  
?>