﻿
<?php
session_start();
  if (isset($_SESSION["login_account"]))
	  {
        $login_account = $_SESSION["login_account"];

?>
<!doctype html>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
		
    <title>加入會員</title>
	<!--[if lt IE 9]>
<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/hot-sneaks/jquery-ui.css" rel="stylesheet">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  <link href='../jquery-ui-timepicker-addon.css' rel='stylesheet'>
  <script type="text/javascript" src="../jquery-ui-timepicker-addon.js"></script>
  <script type='text/javascript' src='../jquery-ui-sliderAccess.js'></script>
  <style>
    article,aside,figure,figcaption,footer,header,hgroup,menu,nav,section {display:block;}
    body {font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}
  </style>
	<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
    <meta charset="utf-8">
   <link rel="stylesheet" href="../kindeditor/themes/default/themes/default/default.css" />
<link rel="stylesheet" href="../kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="../kindeditor/plugins/code/prettify.js"></script>
<script>
    KindEditor.ready(function(K) {
        var editor1=K.create('textarea[name="comment"]',{//name=form中textarea的name?性
            cssPath : '../kindeditor/plugins/code/prettify.css',
            uploadJson : '../kindeditor/php/upload_json.php',
            fileManagerJson : '../kindeditor/php/file_manager_json.php',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表?的name?性
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表?的name?性
                });
            }
        });
        prettyPrint();
    });
</script>
  </head>
  <body>
   <?php include 'Admin_tops.php';?>
   <div class="col-md-12 hightline">
<div class="container">
<?php
  require_once("dbtools.inc.php");
    if(isset($_GET["modify_id"])){
 	$modify_id= $_GET["modify_id"];
	  $sql3 = "SELECT * FROM edit_film WHERE id = $modify_id ";
	
      $result2 = mysqli_query($wp_c,$sql3) or die(mysqli_error('error'));
	   $row = mysqli_fetch_assoc($result2);
	
	}
	   if(isset($_GET["update"])){
	   $id3 = $_GET["update"];
      $sql3 = "SELECT * FROM edit_film WHERE id = $id3 ";
      $result2 = mysqli_query($wp_c,$sql3) or die(mysqli_error('error'));
	   $row = mysqli_fetch_assoc($result2);
	   }
	  ?>
<form class='form-horizontal' action="update_edit_film.php" method="post" name="myform"  enctype="multipart/form-data">
    <fieldset>
        <legend>編輯文章</legend>
		<div class="form-group">
            <label class="col-md-2 control-label" >id</label>
            <div class="controls col-md-6">

                <input  name="id"  type="text" size="20" value="<?php echo $row['id'] ?>">  
               
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for='account'>標題</label>
            <div class="controls col-md-6">

                <input  name="title"  type="text" size="40" value="<?php echo $row['title'] ?>">  
               
            </div>
        </div>
                
        <div class="form-group">
            <label class="col-md-2 control-label">作者</label>
            <div class="controls col-md-6">
                <input  name="editor" type="text" size="10" value="<?php echo $row['editor'] ?>" />
                
            </div>
        </div>
		<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">分類</label>
          <div class="controls col-md-6">
            <input name="category"   class="input-xlarge" size="10" value="<?php echo $row['category'] ?>" />
			 
			</div>
			</div>
			 <div class="form-group">
            <label class="col-md-2 control-label">上傳圖片</label>
            <div class="controls col-md-6">
    
        <input type="hidden" name="MAX_FILE_SIZE" value="1048576">
		
        <input type="file" name="myfile" size="50"><br><br>
        
     
    </div>
        </div>
			<div class="form-group">
            <label class="col-md-2 control-label">keyword</label>
            <div class="controls col-md-6">
                <input  name="keyword" type="text" size="20" value="<?php echo $row['keyword'] ?>" />
                
            </div>
        </div>
		 <div class="form-group">
            <label class="col-md-2 control-label">標籤</label>
            <div class="controls col-md-6">
                <input  name="tag" type="text" size="20" value="<?php echo $row['tag'] ?>" />
                
            </div>
        </div>
			
			<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">日期</label>
          <div class="controls col-md-6">
					<input name="datetime" id="datetimepicker1" type="text"  value="<?php echo $row['datetime'] ?>">
				<script language="JavaScript">
					$(document).ready(function(){ 
						var opt={dateFormat: 'yy-mm-dd',
							showSecond: true,
							timeFormat: 'HH:mm:ss'
							};
						$('#datetimepicker1').datetimepicker(opt);
					});
				</script>
				</div>
			</div>
			<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">內容</label>
          <div class="controls col-md-6">
            <textarea name="comment" cols="85" rows="30" ><?php echo $row['comment'] ?></textarea>

			</div>
			</div>
		
    </fieldset>
	
	<hr>
	
    <div class=" form-actions col-md-10 col-md-offset-2" >
	 　
            <input type="submit" value="更新">  
	
	</div>
	
</form>
</div>
</div>
<?php
	  }
	else
	 header("location:index.php");
 ?>
<!-- script references -->
		
		<script src="js/bootstrap.min.js"></script>
	
	</body>
  
</html>