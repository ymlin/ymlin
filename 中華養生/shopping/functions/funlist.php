<?php
if (!isset($_SESSION)) {session_start();}
/*---- uservip DB ----*/;
function sql_vip($SQLdata, $db_na){
           global $wp_c;
    
           $sqlna= "m_" . $db_na;
           $sql_VP = "SELECT " . $sqlna . " as mvp_name FROM uservip WHERE m_id = '{$SQLdata}'";
           $us_VP = mysqli_query($wp_c, $sql_VP) or die(mysqli_error());
           $rows_v = mysqli_fetch_assoc($us_VP);
             return $usevp= $rows_v['mvp_name'];
}
/*---- product_t DB ----*/;
function sql_product_ot($SQLdata, $db_na){
           global $wp_c;
    
           $sqlna= "pro_t" . $db_na;
           $sql_pro_ot = "SELECT " . $sqlna ." AS prodname FROM product_tw WHERE pro_tID = '{$SQLdata}'";
           $ult1 = mysqli_query($wp_c, $sql_pro_ot) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
             return $dbdata= $row['prodname'];
}
/*---- 加購DB odres chk----*/
function odr_oth($oth_proid){
         global $wp_c;

          if($oth_proid > 0){
           for($b=0; $b < count($oth_proid); $b++){
               $proid= $_SESSION['cr_o_proid'][$b];
               $oth_que = $_SESSION['cr_o_que'][$b];
               
             //---- 加訂數量是否為空值 --;
             if($oth_que > 0){
               $sql_pro = "SELECT * FROM product_tw WHERE pro_tID = '{$proid}'";
               $ult2 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
               $row2 = mysqli_fetch_assoc($ult2);

                $oth_na = $row2['pro_tName'];
                $oth_pay = $row2['pro_tPrice2'];
                $oth_tot = number_format($oth_que * $oth_pay);
              echo <<<"oth1"
                   <tr style="background-color: #eaeaea;">
                     <td>{$oth_na}</td>
                     <td>單價 : NT$ {$oth_pay}元</td>
                     <td>數量 : {$oth_que}件</td>
                     <td>小計NT$ : {$oth_tot}元</td>
                   </tr>
oth1;
              }//-- 空值 END;
           }// end for;
          }//end if;
 }
//---- shipping 運費 DB ----;
function sql_outshpping($SQLdata){
         global $wp_c;
    
           $sql_shp = "SELECT * FROM product_freight LEFT JOIN oders_shipping ON oders_shipping.od_sh_id = product_freight.od_sh_id WHERE product_freight.pro_tID = '{$SQLdata}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
          return $row['od_sh_name'];
}
/*---- shipping 運費合計運算 odres chk ----*/
function odr_shipping($total_2, $shpid){
           global $wp_c;
    
           $sql_shp = "SELECT od_sh_price FROM oders_shipping WHERE od_sh_id = '{$shpid}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);

         if($total_2 < 1000){
             $total_shp = $row['od_sh_price'];
         }else{
             $total_shp = 0;
         }
        return $total_shp;
}
/*---- shipping 1 運費合計運算 ----*/
function odr_shipping_1($subtotal, $proid){
         global $wp_c;
    
           $sql_shp = "SELECT pro_fre_price FROM product_freight WHERE pro_tID = '{$proid}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
            //品牌運費上限查詢
           $sql_cat = "SELECT brand_category.brcate_shipping FROM brand_category LEFT JOIN product_tw ON product_tw.brcate_id = brand_category.brcate_id WHERE product_tw.pro_tID = '{$proid}'";
           $cat1 = mysqli_query($wp_c, $sql_cat) or die(mysqli_error());
           $row1 = mysqli_fetch_assoc($cat1);
            $cat_shi = $row1['brcate_shipping'];
            //運費上限比較
         if($subtotal < $cat_shi){
             $total_shp = $row['pro_fre_price'];
         }else{
             $total_shp = 0;
         }
        return $total_shp;
}
/*---- GET subsidy view----*/
function sql_subsidy($sub_id){
         global $wp_c;
    
           $sql_uses = "SELECT * FROM subsidy LEFT JOIN subsidy_data ON subsidy.subda_id = subsidy_data.subda_id WHERE subsidy.sub_id = '{$sub_id}'";
           $uses = mysqli_query($wp_c, $sql_uses) or die(mysqli_error());
           $row_s = mysqli_fetch_assoc($uses);
           $row_num = mysqli_num_rows($uses);
    
         if($row_num !== 0){
             $subid = $row_s['sub_id'];
             $subna = $row_s['subda_name'];
             $subprice = number_format($row_s['sub_price']);

           echo <<<"sub"
                <td colspan="3"><strong>{$subna}</strong></td>
                <td><div class="font8">NT$ : (-{$subprice})元</div></td>
sub;
          }
}
/*---- get subsidy DB----*/
function sql_subsidy_db($dbname, $sub_id){
         global $wp_c;
    
           $sql_uses = "SELECT ". $dbname ." AS sqlname FROM subsidy WHERE sub_id = '{$sub_id}'";
           $uses = mysqli_query($wp_c, $sql_uses) or die(mysqli_error());
           $row_s = mysqli_fetch_assoc($uses);
    
             $sub_na = $row_s['sqlname'];
         return $sub_na;
}
/*----  get brand_category view----*/
function sql_brand_cat($db_id){
         global $wp_c;
    
           $sql_view = "SELECT brcate_name FROM brand_category WHERE brcate_id = '{$db_id}'";
           $views = mysqli_query($wp_c, $sql_view) or die(mysqli_error());
           $row_s = mysqli_fetch_assoc($views);
         return $row_s['brcate_name'];
}
/*----------------------------------------------------------------------------*/
/*---- View orders_use ----*/
              /*---- E-Coupon 折扣 --*/

              if($subid >= 1){
                  $_SESSION['sub_id'] = $_POST['sub_id'];
                  $cart->sub_price = sql_subsidy_db('sub_price', $_SESSION['sub_id']);
              }

              /*---- 合計加購及訂購 ----*/
                $total_2 = $cart->grandtotal2;//扣E-Coupon;不含運;
               // $cart->deliverfee = odr_shipping($total_2, $_SESSION['cr_o_out']); //請設定購物車的運費
/*----------------------------------------------------------------------------*/
/*---- View orders_chk ----*/
if ((isset($_POST['cr_od_use'])) && ($_POST['cr_od_use'] == "cr_od_use")){ 
            /*-- user data --*/
              $m_ids = $_POST['vi_id'];
              $_SESSION['cr_v_id'] = $m_ids;
              $u_name = sql_vip($m_ids, 'name');
              $u_mail = sql_vip($m_ids, 'username');
              //$u_cell = sql_vip($m_ids, 'cellphone');
              $u_noid = sql_vip($m_ids, 'NO');
    
              $_SESSION['cr_f_na'] = $u_name;
              $u_tels = "";
              $u_adds = "";
            if((isset($_POST['vi_phone']))&&($_POST['vi_phone'] !== "")){
              $u_tels = $_POST['vi_phone'];
              $_SESSION['cr_v_ph'] = $u_tels;}
            else{$u_tels = sql_vip($m_ids, 'phone');
                 $_SESSION['cr_v_ph'] =$u_tels;
                }
    
            if((isset($_POST['adds1']))&&($_POST['adds1'] !== "")){
              $u_adds = $_POST['adds1'];
              $_SESSION['cr_v_ad'] = $u_adds;}
            else{$u_adds = sql_vip($m_ids, 'address');
                 $_SESSION['cr_v_ad'] = $u_adds;
                }
    
            /*-- user insert oders address chang--*/
            if((isset($_POST['anre']))&&($_POST['anre'] == "anre")){
                $_SESSION['cr_f_na'] = $u_name;
                $_SESSION['cr_f_ph'] = $u_tels;
                $_SESSION['cr_f_ad'] = $u_adds;
            }else{
                $_SESSION['cr_f_na'] = $_POST['vi_name2'];
                $_SESSION['cr_f_ph'] = $_POST['vi_phone2'];
                $_SESSION['cr_f_ad'] = $_POST['adds2'];
            }
                $_SESSION['cr_f_tx'] = $_POST['vi-text'];
    
            /*-- orders_use 運送shipping and 支付方式 --*/             
                   $paymod = $_POST['pay_mod'];
                   $_SESSION['cr_o_pa'] = $paymod;//paying;
                //$outmod = $_POST['out_mod'];
                //$_SESSION['cr_o_ou'] = $outmod;//shipping;
                //$_SESSION['cr_shi_pri'] = odr_shipping($total_3, $outmod, $proids);//含運費;
                    
             /*-- 合計加購及訂購 + 運費 --*/
             //$_SESSION['cr_total'] = $total_3 + odr_shipping($total_3, $outmod, $proids);//含運費;
                   $_SESSION['sn_id'] = '';
                   $_SESSION['st_name'] = '';
                $outmod = $_SESSION['cr_o_out'];//運費id;
             if($outmod == 2){
                 $order_no = $u_noid;
                 require_once("cvsemap.php");
             }
}//--creat orders_chk end;
/*----------------------------------------------------------------------------*/
/*---- View orders_chk ----*/
if ((isset($_GET['webtemp'])) && ($_GET['webtemp'] == '989897654')){ 
               if((isset($_GET['sn_id']))&&($_GET['sn_id'] !== '00000000')){
                   $sn_id = $_GET['sn_id'];
                   $_SESSION['sn_id'] = $sn_id;
               }//便利到店編號;
               if((isset($_GET['st_name']))&&($_GET['st_name'] !== '')){
                   $st_name = iconv("Big5", "UTF-8",$_GET['st_name']);
                   $_SESSION['st_name'] = $st_name;
               }//便利到店名稱;
}
?>