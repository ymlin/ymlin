﻿<?php
	require_once('dbtools.inc.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
		<title>榜單查詢</title>  
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<?php include 'link.php';?>
		<style>  
		#heig{  
			 height:10px;  
		}  
		#color{  
			 background-color: #666;  
			 color: #FFF;  
		}  
		#color2{  
			 background-color: #999;  
		}  
		</style>
	</head>
	<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px;">
		<form action = "index.php" method="GET">
			<div style="width:80%; margin: 0px auto;">  
				<br>
				<?php
					if(isset($_GET["eid"]))
					{
				?>
				<table border="1" class="table table-bordered table-condensed table-striped table-hover">
				<tr>  
				<td colspan="4" id="color">
					<h2 align="center">榜單</h2>
				</td> 
				</tr>  
				<tr>
				<td id="color2"><div align="center" ><strong>id</strong></div></td> 
				<td id="color2"><div align="center" ><strong>姓名</strong></div></td> 
				<td id="color2"><div align="center" ><strong>學校</strong></div></td>
				<td id="color2"><div align="center" ><strong>系所</strong></div></td>
				</tr>
				<?php
						$eid = $_GET["eid"];
						$pageSize = 8;
						if(isset($_GET['pagenum'])) $pageNo = $_GET['pagenum'];
						else						$pageNo = 1;
						
						$sql = "SELECT eid,name,school,depname FROM Php_enroll,Php_depart";
						if(is_numeric($eid))
							$sql.=" where eid like '$eid%'";
						else
							$sql.=" where name like '%$eid%'";
						$sql.=(" and Php_enroll.depcode = Php_depart.depcode limit ".(($pageNo-1)*$pageSize).",".$pageSize);
						$sth = $db->prepare($sql);
						$sth->execute();
						
						while($result = $sth->fetchobject())
						{
							echo "<tr>";
							echo '<td align="center" >'.$result->eid.'</td>';
							echo '<td align="center" >'.$result->name.'</td>';
							echo '<td align="center" >'.$result->school.'</td>';
							echo '<td align="center" >'.$result->depname.'</td>';
							echo "</tr>";
						}
						
						//count all of the matching data
						$sql = "SELECT eid,name,school,depname FROM Php_enroll,Php_depart";
						if(is_numeric($eid))
							$sql.=" where eid like '$eid%'";
						else
							$sql.=" where name like '%$eid%'";
						$sql.=" and Php_enroll.depcode = Php_depart.depcode";
						$sth = $db->prepare($sql);
						$sth->execute();
						$maxPageNo = 0;
						for($i=0 ; $result = $sth->fetchobject() ; $i++) if($i%8 == 1) $maxPageNo++;
				?>
					<script type="text/javascript">
					function chpagebyno(n){
						if(n) window.location = ("HTTP://www.begoodlive.com/test/uac/index.php?eid=" + <?php echo $eid; ?> + "&pagenum=" + n);
					}
					
					function resetVal(){
						document.getElementById("eid").value = "";
					}
					
					</script>
					
				</table> 
				
				<div align="center">
					<input type="button" class='btn btn-primary' value="第一頁" onclick="chpagebyno(1)">
					<input type="button" class='btn btn-primary' value="上一頁" onclick="chpagebyno(<?php echo ($pageNo > 1)? $pageNo-1:1;?>)">
					第<input id="pagenum" type="text" style="text-align:center;" size="4" value="<?php echo $pageNo;?>" onKeyUp="chpagebyno(this.value)">頁/共&nbsp <?php echo $maxPageNo; ?> &nbsp頁
					<input type="button" class='btn btn-primary' value="下一頁" onclick="chpagebyno(<?php echo ($pageNo < $maxPageNo)? $pageNo+1:$maxPageNo;?>)">
					<input type="button" class='btn btn-primary' value="最末頁" onclick="chpagebyno(<?php echo $maxPageNo;?>)">
				</div>
				
				<?
					}
				?>
				<div align="center">
					<h2 align="center" style="color:red;">請輸入准考證號碼</h2>
					<input type="text" name="eid" id="eid" size="8" value=<?php echo $eid;?> >
					<?php
						if(isset($_GET["eid"])) echo '<button type="button" class="btn btn-primary" onclick="resetVal()" >重設</button>';
						else						echo '<button type="reset" class="btn btn-primary" >重設</button>';
					?>
					<button type="submit" class='btn btn-primary' >送出</button>
				</div>
			</div>
			<hr>
		</form>
	</body>
</html>