<?php @require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php @require_once("../../functions/fundb.php");?>
<?php @require_once("../../functions.php");?>
<?php if(isset($_GET['st_y'])){$st_y=$_GET['st_y'];$st_m=$_GET['st_m'];}?>
<?php @require_once("../functions.php");?>
<?php @require_once("functions_st.php");?>
<?php @require_once("../header_all.php");?>
  <!-- feature -->
  <div id="features-wrapper">
    <div class="container"><!--內容區 -->
       <article class="row clearfix"><!-- 對齊中間 -->
          <div class="mem_list">
             <div class="col-lg-12 column ">
                <div class="col-md-12 top_banner_cont">月訂單檢示</div>
                <div class="col-md-12 hline">
                   <div class="col-sm-12 table-responsive">
                      <table class="table table-bordered">
                         <thead>
                            <tr style="background-color:#e8e6e5">
                              <th class="text-center">商品名稱</th>
                              <th class="text-center">商品單價</th>
                              <th class="text-center">數量</th>
                              <th class="text-right">小計價格</th>
                            </tr>
                         </thead>
                         <tbody>
                            <?php sql_sta_view($st_y, $st_m);?><!-- 訂購商品-->
                         </tbody>
                      </table>
                   </div>
                   <!-- Button -->
                   <div class="col-md-offset-2">
                     <button class="btn button1" type="button" title="回上頁" onclick="location.href='index.php'">回上頁</button>
                   </div>
                </div>
             </div>
          </div>
       </article>      
    </div><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?>
<?php end_sql($end_sqls);?><!--END sql-->