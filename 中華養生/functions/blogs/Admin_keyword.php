﻿

<!DOCTYPE html>  

<html> 
 
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
<title>會員管理</title>  
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">  
<link href="<?=$link;?>css/bootstrap-responsive.min.css" rel="stylesheet">  
<link href="<?=$link;?>css/bootstrap.min.css" rel="stylesheet" media="screen">  
<script src="<?=$link;?>js/jquery.js"></script>  
<script src="<?=$link;?>js/bootstrap.min.js"></script>  
<style>  
#heig{  
     height:10px;  
}  
#color{  
     background-color: #666;  
     color: #FFF;  
}  
#color2{  
     background-color: #999;  
}  
</style>  
</head>  
<body> 
 <div style="margin-bottom:90px">
	  <div id="navbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <span class="navbar-brand"><span class="fa fa-table"></span><a href='index.php'>Home</a></span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
			 <li><a href="Admin.php">管理員專區</a></li>
           <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">講師文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit.php" tabindex="-1">全部講師文章</a></li>					
                        <li><a href="edit.php" tabindex="-1">新增講師文章</a></li>
						  
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">學員文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_student.php" tabindex="-1">全部學員文章</a></li>
						 <li><a href="edit_student.php" tabindex="-1">新增學員文章</a></li>
                       
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">講師影音<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_film.php" tabindex="-1">全部影音</a></li>
						  <li><a href="edit_film.php" tabindex="-1">新增影音</a></li>                       
                     </ul>
                   </li>
				    <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">書院活動<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_htz.php" tabindex="-1">全部書院活動</a></li>
						  <li><a href="edit_htz.php" tabindex="-1">新增書院活動</a></li>                       
                     </ul>
                   </li>
				    <li><a href="Admin_keyword.php">關鍵字管理</a></li>
					 <li><a href="edit_keyword.php">新增關鍵字</a></li>
				 
              <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">帳號<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_mem.php" tabindex="-1">全部帳號</a></li>
                       
                     </ul>
                   </li>
				 <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">圖片<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="add_picture.php" tabindex="-1">新增圖片</a></li>
                       
                     </ul>
                   </li>
              <li><a href="logout.php">登出</a></li>
            </ul>
          </div>
        </div>
    </div>
   </div>
<div class="container">  
 <table width="600" border="1" class="table table-bordered table-condensed table-striped table-hover">  
    <tr>  
      <td colspan="5" id="color"><div align="center"><strong><h2>關鍵字管理</h2></strong></div></td>  
    </tr>  
     
    <tr>  
      <td id="color2"><div align="center"><strong>項次</strong></div></td>  
      <td id="color2"><div align="center"><strong>項次</strong></div></td>  
      <td id="color2"><div align="center"><strong>項次</strong></div></td>  
     
    </tr>  
    
    <tr>
   	<?php
      require_once("dbtools.inc.php");
	 
     
      $sql9 = "SELECT * FROM keyword_table ";
       $result9 = mysqli_query($wp_c,$sql9) or die(mysqli_error('error'));
	  $i=0;
	
		
	while ($row9 = mysqli_fetch_assoc($result9)){
	if( $i<5){
		$key=$row9['keyword'];?>
		   <form action = "edit_keyword_modify.php"  method = "GET">
		<?php   echo "<td>" . $key ;?>
		<div style="text-align:right; ">

      <button  class='btn btn-primary' type = "submit" name = "update" value= "<?php echo  $row9['id']  ?> ">修改</button>  
     
	   
	  <button  class='btn btn-primary' type = "submit" name = "remove" value= "<?php echo  $row9['id'] ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
      </div>
	  </form>
	  
	  <?php
	  echo "</td>";
		$i++;
		}
		if( $i==3){
		$i=0;
		 echo "<tr>";
		?>
	
		
		<?php
		}
	}
	
	  echo "</table>" ;
		?>	
     
    </tr>  
   
  </table>  
</div>  
</body>  
</html>  