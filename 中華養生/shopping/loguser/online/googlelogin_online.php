<?php
header('Content-type: text/html; charset=utf-8');
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require_once './google/Google_Client.php';
require_once './google/contrib/Google_Oauth2Service.php';
// start session
if (!isset($_SESSION)) {session_start();}

$client = new Google_Client();
$client->setApplicationName("Google UserInfo PHP Starter Application");
// Visit https://code.google.com/apis/console?api=plus to generate your
// oauth2_client_id, oauth2_client_secret, and to register your oauth2_redirect_uri.
// $client->setClientId('insert_your_oauth2_client_id');
// $client->setClientSecret('insert_your_oauth2_client_secret');
// $client->setRedirectUri('insert_your_redirect_uri');
// $client->setDeveloperKey('insert_your_developer_key');
// 設定API Key資訊
$client->setClientId('61561074826-r9ojotu2b4s7arhogj7c7nn0ohetmt0u.apps.googleusercontent.com');
$client->setClientSecret('7lZIPVGE15IZL-LgL7wqP5cq');
$client->setRedirectUri('http://www.lasulife.com/group1/loguser/index.php');
$client->setDeveloperKey('AIzaSyBFSIv_Ml3s5cjDfDLcHrhgdS4KLkFexvE');
$oauth2 = new Google_Oauth2Service($client);

if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['token'] = $client->getAccessToken();
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
  return;
}

if (isset($_SESSION['token'])) {
 // 如果Session有Token資料，設定為要使用的token
 $client->setAccessToken($_SESSION['token']);
}

if (isset($_REQUEST['logout'])) {
 // 登出清除taken
  unset($_SESSION['token']);
  $client->revokeToken();
}

if ($client->getAccessToken()) {
  $user = $oauth2->userinfo->get();

  // These fields are currently filtered through the PHP sanitize filters.
  // See http://www.php.net/manual/en/filter.filters.sanitize.php
  $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
  //$img = filter_var($user['picture'], FILTER_VALIDATE_URL);
  $names = filter_var($user['name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
  $uid = filter_var($user['id'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
  //$personMarkup = "$email<div><img src='$img?sz=50'></div>".$names."<br>userid=".$uid."<br>";
  $personMarkup = "<div>$email</div>".$names."<br>userid=".$uid."<br>";

  // The access token may have been updated lazily.儲存access_token
  $_SESSION['token'] = $client->getAccessToken();
} else {
  // 尚未授權
  $authUrl = $client->createAuthUrl();
}
?>
<!--url-->
<?php  
     if(isset($personMarkup)){ 
        $udbemail = base64_encode($email);
        $udbname = base64_encode($names);
        $udbid= base64_encode($uid);
        header("Location:REfresh_other.php?fu=" . $udbemail."&n=" . $udbname."&d=" . $udbid);
        //header ("Location:../index.php");
        exit;
     }
?>
<!--view;-->
<?php //if(isset($personMarkup)): ?>
<?php //print $personMarkup ?>
<?php //endif ?>
<?php
  if(isset($authUrl)) {
    //print "<a class='login' href='$authUrl'>".'<img src="../img/gogl-connent.png"  alt="facebook login"/></a>';
    $gogllg = "<a class='login' href='$authUrl'>".'<img src="../img/gogl-connent.png"  alt="Google login"/></a>';
  } else {
    //print "<a class='logout' href='?logout'>Logout</a>";
    $gogllg = "<a class='logout' href='?logout'>Google Logout</a>";
  }
?>