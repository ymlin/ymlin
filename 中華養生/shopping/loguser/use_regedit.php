<?php @require_once("../conn/wp-c.php");?>
<?php @require_once('../functions/EDcart.php');?>
<?php @require_once("../functions/Getcart.php");?>
<?php @require_once("../functions/fundb.php");?>
<?php @require_once("../functions.php");?>
<?php @require_once("header.php");?>
<!-- pass 在確認 -->
<script type="text/javascript">
   $(function(){
       $("#chkpass").blur(function(){
           if ( $(this).val() != $("#m_passwd").val() ){
               alert("您輸入密碼不同!!");
               
           }
        })
   })
</script><!-- pass END -->
  <!-- feature -->
  <div id="features-wrapper">
     <div class="container"><!--內容區 -->
        <article><!-- 對齊中間 -->
           <div class="row clearfix">
              <div class="col-lg-12 column reg_list">
                 <div class="col-md-12 top_banner_cont">註冊會員</div>
                 <div class="col-md-12 hightline">
                   <form class="form-horizontal" action="fundb.php" method="post" name="commentForm" id="commentForm">
                     <div class="col-md-6">    
                      <!-- infomation訊息處理 -->
                      <?php if(isset($_GET['Msg'])){?>
                      <div class="col-sm-12">
                         <div class="col-sm-4 col-sm-offset-2 hline1">
                            <?php errmess($_GET['Msg']);?>
                         </div>
                      </div>
                      <?php }?>
                      <!-- Text input-->
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="emailid">帳號</label>
                         <div class="col-sm-6">
                            <input id="m_username" name="m_username" placeholder="請輸入Email" class="input25 email" required="" type="text">
                            <h5 class="re_colo">*請輸入您的e-mail，這將會成為您登入的帳號</h5>
                         </div>
                      </div>
                      <!-- Password input-->
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="pass">密碼</label>
                         <div class="col-sm-6">
                            <input id="m_passwd" name="m_passwd" class="input25" placeholder="請輸入密碼" required="" type="password">
                            <h5 class="re_colo">*請輸入6碼以上的英文或數字</h5>
                         </div>
                      </div>
                      <!-- Password input-->
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="chkpass">確認密碼</label>
                         <div class="col-sm-6">
                            <input id="chkpass" name="chkpass" class="input25" placeholder="***請再次輸入您的密碼"  required="" type="password">
                            <h5 class="re_colo">*請再次輸入您的密碼</h5>
                         </div>
                      </div>
                      <!-- name input-->
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="textinput">姓名</label>
                         <div class="col-sm-6">
                            <input id="m_name" name="m_name" class="input25" placeholder="輸入您的姓名" required="" type="text">
                            <h5 class="re_colo">*請輸入您的中文或英文姓名</h5>
                         </div>
                      </div>
                      <!-- callphone -->
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="cellphone">連絡手機</label>
                         <div class="col-sm-6">
                            <input id="cellphone" name="cellphone" class="input25" placeholder="輸入您的連絡手機" required="" type="text">
                            <h5 class="re_colo">*請輸入您的連絡手機號碼</h5>
                         </div>
                      </div>
                      <!-- Text input-->
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="textinput">圖形驗證</label>
                         <div class="col-sm-6">
                            <input id="cas" name="cas" placeholder="請輸入下方驗證碼" class="input25 required" autocomplete="off" type="text"><h5><a href="use_regedit.php" target="_parent" class="ong_colo"><img src="pic.php" width="150"/> 更換一組驗證碼</a></h5>
                         </div>
                      </div>
                      <!-- Multiple Checkboxes -->
                      <div class="form-group">
                         <div class="col-sm-12 col-sm-offset-2">                                 
                            <input name="ck-1" id="ck-1" value="我願意接受來自團購網的各項優惠與團購訊息" checked="checked" type="checkbox"> 我願意接受來自團購網的各項優惠與團購訊息
                         </div>
                         <div class="col-sm-12 col-sm-offset-2">
                            <h5><input name="ck-2" id="ck-2" value="我已經閱讀並同意團購網聲明的使用者條款" checked="checked" type="checkbox"> 我已經閱讀並同意團購網聲明的使用者條款</h5>                                         <!-- 使用者條款 -->
                            <div class="col-sm-offset-2 lines">
                               <?php modal_view(1);?>
                               <?php @require_once("low.php");?>
                               <?php modal_view(2);?>
                            </div>
                         </div>
                      </div>
                      <!-- Button (Double) -->
                      <div class="col-sm-12"><hr>
                         <div class="col-sm-offset-2">
                            <button id="button2id" name="button2id" class="btn button1" type="reset">清除</button>
                            <button id="MM_insert" name="MM_insert" value="commentForm" class="btn">確定送出</button>
                         </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="font2">使用其它帳號登入</div>
                      <div class="spn1"><a href="fblogin.php"><img src="../img/fb-connect.png"  alt="facebook login" /></a></div>
                      <div class="lines hline2"><u><strong>提醒您，若您是以其它帳號登入時：</strong></u>
                          <p>需補填部分個人資料，進行帳號開通流程。</p>
                          <p>其它帳號為「Facebook」等帳號一經開通後，
                             日後即可使用這組帳號來登入並且保障您的隱私。</p></div>
                    </div>
                   </form>
                </div>
             </div>
          </div> 
        </article>
      </div>
    </div><!--內容 結尾-->
  </div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->