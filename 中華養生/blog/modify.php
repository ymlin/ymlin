<?php
 

  require_once("dbtools.inc.php"); 
      
      //取得登入者帳號及名稱
      session_start();
	  if (isset($_SESSION["login_account"]))
	  {
        $login_account = $_SESSION["login_account"];
  
	  
		
    
    //執行 SELECT 陳述式取得使用者資料
    $sql = "SELECT * FROM users Where id = $login_account ";
   $result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
		
    $row = mysqli_fetch_assoc($result);
?>
<!doctype html>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
		
  <title>修改會員資料</title>
    <meta charset="utf-8">
	<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
    <meta charset="utf-8">
    <script type="text/javascript">
      function check_data()
      {
        if (document.myForm.password.value.length == 0)
        {
          alert("「使用者密碼」一定要填寫哦...");
          return false;
        }
        if (document.myForm.password.value.length > 10)
        {
          alert("「使用者密碼」不可以超過 10 個字元哦...");
          return false;
        }
       
        if (document.myForm.name.value.length == 0)
        {
          alert("您一定要留下真實姓名哦！");
          return false;
        }	
      
        myForm.submit();					
      }
    </script>		
  </head>
  <body>
  
<div class="container">
<div class="row">
  <div class="col-sm-12 home_list">
<div class="col-md-12 hline" style=" padding:1.0em">
                <div class="font" style="font-size:1.5em">個人資料</div>
             </div>	
			 </div>	
			  </div>	<div class="row">
			 <div class="col-md-12">
			 
	<div class="content_bar">
<form class='form-horizontal' action="update.php" method="post" name="myForm">
    <fieldset>
     　<input type="hidden" name="id" value="<?php echo $row{"id"} ?>">
		<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">會員姓名</label>
          <div class="controls col-md-3">
            <input type="text" name="name" value="<?php echo $row{"name"} ?>"  >
            
			</div>
			</div>
        <div class="form-group">
            <label class="col-md-2 control-label" for='account'>電子郵件</label>
            <div class="controls col-md-3">

                <input  name="account"  value="<?php echo $row{"account"} ?>" type="text" size="30"  />
              
            </div>
        </div>
		<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">連絡電話</label>
          <div class="controls col-md-3">
            <input type="text" name="telephone" value="<?php echo $row{"telephone"} ?>"  >
           
			</div>
			</div>
           <div class="form-group">
		 <label class="col-md-2 control-label" for="input01">聯絡手機</label>
          <div class="controls col-md-3">
            <input type="text" name="cellphone" value="<?php echo $row{"cellphone"} ?>"  >
          
			</div>
			</div> 
			<div class="form-group">
		 <label class="col-md-2 control-label" >會員地址</label>
          <div class="controls col-md-3">
            <input type="text" name="Address" value="<?php echo $row{"Address"} ?>" >
          
			</div>
			</div>			
        <div class="form-group">
            <label class="col-md-2 control-label" >密碼</label>
            <div class="controls col-md-3">
                <input id='password' name="password" value="<?php echo $row{"password"} ?>" type="password"   />
               
            </div>
        </div>
		
			
			
    </fieldset>
	

	
    <div class=" form-actions  col-md-offset-2" >
	 　
           
	
	<input  type="reset" value="清除"> 
      　<input onClick="check_data()" type="button" value="確定修改"> 
	  
	</div>
	
</form></div>
</div>
</div>
</div>

<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
  </body>
</html>
<?php
	  }
	else
	 header("location:index.php");
 ?>