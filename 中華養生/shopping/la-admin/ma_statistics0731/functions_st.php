<?php
/*----------------------------------------------------------------------------*/
/*---- Limit manger DB ----*/
function sql_limit_man($mandb = '-1'){
         global $wp_c;
            switch($mandb){
              case 1:
                $mandb = '1';
              break;
              case 2:
                $mandb = '2';
              break;
              case 3:
                $mandb = '3';
              break;
              case 4:
                $mandb = '-1';
              break;
            }
             $sql_man = "SELECT * FROM uservip WHERE m_id = '{$mandb}'";
             $ult1 = mysqli_query($wp_c, $sql_man) or die(mysqli_error());
    
         return mysqli_num_rows($ult1);
}
/*----------------------------------------------------------------------------*/
/*---- Year Month view DB ----*/
function sql_ord_view(){
         global $wp_c;
    
             $ourship= '已出貨';
           $sql_year = "SELECT YEAR(od_state_time) AS y_out, MONTH(od_state_time) AS m_out FROM oders WHERE (od_state = '{$ourship}') GROUP BY DATE_FORMAT(od_state_time,'%Y %M')";
           $ult1 = mysqli_query($wp_c, $sql_year) or die(mysqli_error());
           $row1 = mysqli_fetch_assoc($ult1);

         do{
             $ord_year= $row1['y_out'];
             $ord_month= $row1['m_out'];
             
             $sql_num = "SELECT COUNT(oders.od_id) AS m_num, SUM(oders_other.pro_ot_tPrice2) AS m_sum FROM oders LEFT JOIN oders_other ON oders_other.od_id = oders.od_id WHERE (YEAR(od_state_time) = '{$ord_year}' AND MONTH(od_state_time) = '{$ord_month}') AND (od_state = '{$ourship}')";
             $ult2 = mysqli_query($wp_c, $sql_num) or die(mysqli_error());
             $row2 = mysqli_fetch_assoc($ult2);
               $ord_num= $row2['m_num'];
               $ord_sum= number_format($row2['m_sum']);

              echo <<<"tab"
                 <tr>
                  <td>{$ord_year}</td>
                  <td>{$ord_month}</a></td>
                  <td><a href="sta_view.php?st_y={$ord_year}&st_m={$ord_month}" style="color:#313131">+ {$ord_num}</a></td>
                  <td>NT$: {$ord_sum}</td>
                 </tr>
tab;
         } while($row1 = mysqli_fetch_assoc($ult1));
         echo "</tbody>";
         echo "</table>";
}
/*----------------------------------------------------------------------------*/
/*---- order date DB ----*/
function sql_sta_view($years = '-1', $months = '-1'){
        global $wp_c;
    
            $ourship= '已出貨';
          $sql_sta = "SELECT oders_other.pro_tid, COUNT(oders_other.pro_tid) AS pronum, pro_ot_tPrice2, SUM(oders_other.pro_ot_tPrice2) AS prosum FROM oders LEFT JOIN oders_other ON oders_other.od_id = oders.od_id WHERE (YEAR(od_state_time) = '{$years}' AND MONTH(od_state_time) = '{$months}') AND (od_state = '{$ourship}') GROUP BY oders_other.pro_tid"; 
          $ult3 = mysqli_query($wp_c, $sql_sta) or die(mysqli_error());
          $row3 = mysqli_fetch_assoc($ult3);
          $sta_nums = mysqli_num_rows($ult3); //統計總比數

        do{
            $pro_id= $row3['pro_tid'];
            $pro_price= $row3['pro_ot_tPrice2'];
            $pro_num= $row3['pronum'];
            $pro_sum= $row3['prosum'];
            
            $sql_pro = "SELECT * FROM product_tw WHERE pro_tID = '{$pro_id}'";
            $ult4 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
            $row4 = mysqli_fetch_assoc($ult4);
              $pro_name= $row4['pro_tName'];
            
         echo <<<"s1"
            <tr> 
              <td>{$pro_name}</td>
              <td class="text-center">NT$: {$pro_price}</td>
              <td class="text-center">{$pro_num}</td>
              <td class="text-right">NT$: {$pro_sum}</td>
            </tr>
s1;
        }while($row3 = mysqli_fetch_assoc($ult3));
}
?>