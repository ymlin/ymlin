<?php @require_once("../../conn/wp-c.php");?>
<?php
if (!isset($_SESSION)) {session_start();}
/* ---- send DB ----*/
function sqldb($SQLdata){
           global $wp_c;
           $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());         
}
/* ---- oders uservip DB----*/
function use_mvip($m_na, $m_vdb, $m_id){
           global $wp_c;
    
           $SQLdata1 = "UPDATE uservip SET " . $m_na . " = '{$m_vdb}' WHERE m_id = '{$m_id}'";
           $sult1 = mysqli_query($wp_c, $SQLdata1) or die(mysqli_error());
}
/* ---- uservip pass chk -----*/
function use_ps_mvip($use_ps){
         global $wp_c;
    
           $sql_usr = "SELECT m_passwd FROM uservip WHERE m_passwd = '{$use_ps}'";
           $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
           $row_usr = mysqli_num_rows($us_usr);
        return $row_usr;
}
/* ---- ucenter user edit DB----*/
function use_uc_mvip($m_name, $m_oldpass, $m_newpass, $m_mail){
           global $wp_c;
           require_once("../../config.inc.php");
           mysqli_query("SET NAMES 'utf8'"); 
           require_once("../../uc_client/client.php");
    
            //-- ucenter data --;
           $ucresult = uc_user_edit($m_name, $m_oldpass, $m_newpass, $m_mail);
           return $ucresult;
}
/* ---- ucenter user del  DB----*/
function use_uc_delvip($m_id){
           global $wp_c;
           require_once("../../config.inc.php"); 
           require_once("../../uc_client/client.php");
           
           $sql_mvp = "SELECT m_name FROM uservip WHERE m_id = '{$m_id}'";
           $us_mvp = mysqli_query($wp_c, $sql_mvp) or die(mysqli_error());
           $row_mvp = mysqli_fetch_assoc($us_mvp);
             $username = $row_mvp['m_name'];
    
           list($uid, $username, $email)  = uc_get_user($username);
            //-- ucenter data --;
           $ucresult = uc_user_delete($uid);
           return $ucresult;
}
/*----------------------------------------------------------------------------*/
/*---- Edit Member -----*/
if ((isset($_POST['ed_me_send']))&&($_POST['ed_me_send'] == 'edmesend')){
            $use_id = base64_decode($_POST['mem_d']);
            $mid = base64_encode($use_id);
             //-- data --;
            $use_na = $_POST['m_name'];
            $use_mail = $_POST['m_username'];
            $use_tel = $_POST['m_phone'];
            $use_cellph = $_POST['m_cellphone'];
            $use_add = $_POST['m_address'];
            $use_lev = $_POST['m_level'];
    
               //-- 寫入 --;
              if($use_na != ""){use_mvip('m_name', $use_na, $use_id);}
              if($use_mail != ""){use_mvip('m_username', $use_mail, $use_id);}
              if($use_tel != ""){use_mvip('m_phone', $use_tel, $use_id);}
              if($use_tel != ""){use_mvip('m_cellphone', $use_cellph, $use_id);}
              if($use_add != ""){use_mvip('m_address', $use_add, $use_id);}
              if($use_lev != ""){use_mvip('m_level', $use_lev, $use_id);}
             
             $mess = '?m=' . $mid;
             header("Location:mem_edit.php". $mess);
	         exit;
}
/*----------------------------------------------------------------------------*/
/*---- Edit Member -----*/
if ((isset($_POST['ed_pp_send']))&&($_POST['ed_pp_send'] == 'edppsend')){

             //-- data --;
            $use_id= base64_decode($_POST['mem_d']);
               $mid= base64_encode($use_id);
            $use_pass = $_POST['password']; 

             //-- 查 DATA--;
            $sql_src = "SELECT m_name, m_passwd FROM uservip WHERE m_id = '{$use_id}'";
            $us_src = mysqli_query($wp_c, $sql_src) or die(mysqli_error());
            $row_src = mysqli_fetch_assoc($us_src);
               $use_name = $row_src['m_name'];
               $use_oldpass = $row_src['m_passwd'];
    
             //-- 寫入 --;
            if($use_pass != ""){
                use_mvip('m_passwd', $use_pass, $use_id);

               //-- UCenter 寫入 --;
               //$muc = use_uc_mvip($use_name, $use_oldpass, $use_pass, '',1);
        /*-- 1:更新成功 0:沒有做任何修改 -1:舊密碼不正確 -4:Email 格式有誤 -5:Email 不允許註冊 
             -6:該 Email 已經被註冊 -7:沒有做任何修改 -8:該用戶受保護無權限更改 --*/
                
                
                $mess1 = '<h4 class="h4_colo">!! 密碼變更完成' . $muc . '</h4>';
                $mess = '?Msg=' . $mess1 . '&m=' . $mid;
            }
             header("Location:mem_edit.php" . $mess);
	         exit;
}
/*----------------------------------------------------------------------------*/
/*---- Del member ALL -----*/
if ((isset($_POST['de_me_send']))&&($_POST['de_me_send'] == 'deme')){
           //----- Data 第一筆空值-----;
		   if(empty($_POST['d_memd'][0])){
			  header("Location:index.php");
	          exit;}
    
           //-----取得陣列Data-----;
	       for($s=0; $s < count($_POST['d_memd']); $s++){
              $mem_id= $_POST['d_memd'][$s];
              //--del ucenter uid --;
              //$del_uid = use_uc_delvip($mem_id);
               
		      $SQLdata = "DELETE FROM uservip WHERE m_id = '{$mem_id}'";
		      sqldb($SQLdata);//del user;
              $SQLdata2 = "DELETE FROM subsidy WHERE m_id = '{$mem_id}'";
		      sqldb($SQLdata2);//del E-Coupon;
	        } 
	         
              header("Location:index.php");
	          exit;
}
/*----------------------------------------------------------------------------*/
/*---- Creat subsidy E-Coupon -----*/
if ((isset($_POST['sub_send']))&&($_POST['sub_send'] == 'subsend')){

             //-- data --;
              $use_id= base64_decode($_POST['mem_d']);
              $mid= base64_encode($use_id);
              $subda_id = $_POST['subda_id'];
              $sub_pr = $_POST['sub_price'];
              $sub_lm = $_POST['sub_limitdate'];
             //--指定正確的時區;
                date_default_timezone_set('Asia/Taipei');
			  $sub_da = date("Y-m-d");
            $SQLdata = "INSERT INTO subsidy (subda_id, sub_price, sub_date, sub_limitdate, m_id)VALUES('{$subda_id}', '{$sub_pr}', '{$sub_da}', '{$sub_lm}', '{$use_id}')";
              sqldb($SQLdata);
	         
              header("Location:mem_edit.php?m=" . $mid);
	          exit;
}
/*---- Del subsidy E-Coupon ALL -----*/
if ((isset($_POST['sub_dl_send']))&&($_POST['sub_dl_send'] == 'subdel')){
              $midps = $_POST['mem_d'];//未解密;
           //----- Data 第一筆空值-----;
		   if(empty($_POST['d_subd'][0])){
			  header("Location:mem_edit.php?m=" . $midps);
	          exit;}
    
           //-----取得陣列Data-----;
	       for($s=0; $s < count($_POST['d_subd']); $s++){
              $sub_id= $_POST['d_subd'][$s];
                
		      $SQLdata = "DELETE FROM subsidy WHERE sub_id = '{$sub_id}'";
		      sqldb($SQLdata);
	        } 
	         
              header("Location:mem_edit.php?m=" . $midps);
	          exit;
}
/*----------------------------------------------------------------------------*/
/*---- EDIT subsidy E-Coupon ALL -----*/
if ((isset($_POST['sub_edit']))&&($_POST['sub_edit'] == 'subedit')){
              $m_id = $_POST['mem_d'];
              $sub_id = $_POST['sub_d'];
              $subda_id = $_POST['subda_id'];
              $sub_pr = $_POST['sub_price'];
              $sub_lm = $_POST['sub_limitdate'];
    
              $SQLdata = "UPDATE subsidy SET subda_id = '{$subda_id}', sub_price = '{$sub_pr}', sub_limitdate = '{$sub_lm}' WHERE sub_id = '{$sub_id}'";
              sqldb($SQLdata);
	         
              header("Location:mem_ec.php?m=".base64_encode($m_id)."&s=".base64_encode($sub_id));
	          exit;
}
/*----------------------------------------------------------------------------*/
/*---- Creat subsidy_data E-Coupon-----*/
if ((isset($_POST['subda_cc']))&&($_POST['subda_cc'] == 'subdacc')){
              $sub_name = $_POST['su_na'];
    
              $SQLdata = "INSERT INTO subsidy_data (subda_name)VALUES('{$sub_name}')";
	          sqldb($SQLdata);
    
              header("Location:mem_ec_add.php");
	          exit;
}
/*----------------------------------------------------------------------------*/
/*---- Del subsidy_data E-Coupon-----*/
if ((isset($_POST['sub_dd']))&&($_POST['sub_dd'] == 'sub_dd')){
            //----- Data 第一筆空值-----;
		   if(empty($_POST['ecoupon_d'][0])){
			  header("Location:mem_ec_add.php");
	          exit;}
    
           //-----取得陣列Data-----;
	       for($s=0; $s < count($_POST['ecoupon_d']); $s++){
              $subda_id= $_POST['ecoupon_d'][$s];
                
		      $SQLdata = "DELETE FROM subsidy_data WHERE subda_id = '{$subda_id}'";
		      sqldb($SQLdata);
	        } 
    
              header("Location:mem_ec_add.php");
	          exit;
}
/*----------------------------------------------------------------------------*/
/*---- edit subsidy_data E-Coupon-----*/
if ((isset($_POST['subda_ee']))&&($_POST['subda_ee'] == 'subdaee')){
           //-- Data --;
            $subda_d = $_POST['subda_ed'];
            $subda_na = $_POST['su_na_ed'];
	       $SQLdata1 = "UPDATE subsidy_data SET subda_name = '{$subda_na}' WHERE subda_id = '{$subda_d}'";
           sqldb($SQLdata1);
              header("Location:mem_ec_edit.php?s=" . $subda_d);
	          exit;
}
/*----------------------------------------------------------------------------*/
header("Location:index.php");
exit;
?>