<?php
	function mylog($msg)
	{
		$f=fopen("log.txt","a+");
		$time1='[' . date("Y-m-d H:i:s",mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'))) . '] ';
		fwrite($f,$time1);
		fwrite($f,$msg);
		fwrite($f,"\r\n");
		fclose($f);
		
	}

	require_once(__DIR__.'/../dbtools.inc.php');

	//initialize variables
    $jsonData      = array();
    $jsonTempData  = array();
	
	$op 	= urldecode($_POST['op']);
	$id 	= urldecode($_POST['id']);
	$data 	= urldecode($_POST['data']);
	
	mylog($_SERVER['REMOTE_ADDR']."  ".$_SERVER['SERVER_PROTOCOL']."  ".$_SERVER['REQUEST_METHOD']."  ".$_SERVER['REQUEST_URI']."  ".$_SERVER['QUERY_STRING']."  ".$_SERVER['HTTP_USER_AGENT']);
	$p=print_r($_POST,true);
	mylog("\r\n".$p);
	
	if($op == "getarticle")
	{
		$sql = "SELECT * FROM `articles`";
		$sth = $db->prepare($sql);
		$sth->execute();
		
		while($result = $sth->fetchobject())
		{
			$jsonTempData = array();
			$jsonTempData['id'] = $result->id;
			$jsonTempData['title'] = $result->title;
			$jsonTempData['editor'] = $result->editor;
			$jsonTempData['time'] = $result->datetime;
			if($result->keyword == '拜拜大小事') $jsonTempData['type'] = "1";
			else if($result->keyword == '仙佛故事') $jsonTempData['type'] = "2";
			else if($result->keyword == '養生習俗') $jsonTempData['type'] = "3";
			$jsonTempData['imgname'] = $result->photo_name;
			$jsonTempData['comment'] = "";//strip_tags($result->comment);
		
			$jsonData[] = $jsonTempData;
		}
		$jsonTempData['result'] = $result;
	}
	
	$outputArr = array();
	$outputArr['Android'] = $jsonData;
	
	$o1=print_r($outputArr,true);
	mylog("\r\n".$o1);

	print_r( json_encode($outputArr));
?>
		
				