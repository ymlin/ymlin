<?php @require_once("conn/wp-c.php");?>
<?php @require_once('functions/EDcart.php');?>
<?php @require_once("functions/Getcart.php");?>
<?php @require_once("functions/fundb.php");?>
<?php @require_once("functions.php");?>
<?php @require_once("header.php");?>
  <!-- feature -->
  <div id="features-wrapper">
     <article class="container text-center"><!-- 對齊中間 -->
        <div class="row home_list">
          <div class="col-md-3 sider_clr desktop">
             <div class="col-sm-12">
                <div class="spn3"><a href="abouts.php"><img border="0"  src="./img/abouts01.png"></a></div>
             </div>
          </div>
          <div class="col-md-9">
             <div class="row abou">
               <div class="col-md-12 spn2">
                  <img class="img-responsive" border="0" src="./img/abouts02.jpg">
               </div>
               <div class="col-md-12 spn2">
                  <p class="font4">About us</p>
                  <p class="font5">理念的起源</p>
                   <p>&nbsp;</p>
                  <p class="font7">LaSu樂活購是樂蔬活的購物平台，樂蔬活希望帶給人【健康　蔬食　生活樂】。
樂活購秉持樂蔬活的精神，產品開發理念是希望－取法於純淨自然，而可以還自然一個純淨。
                  </p>
                   <p>&nbsp;</p>
                  <p class="font6">對人好，對物好，對環境也好</p>
                  <p class="font7">我們致力建立一個可以帶給人健康生活，生活樂趣的電商教育平台。一個要永續經營的企業就必須肩負起相對應的社會責任，開發的產品除了對人是良善的，還需要對環境也是良善的，讓人可以持續使用而無害，還可以取於自然，還歸於自然。
                  </p>
                   <p>&nbsp;</p>
                  <p class="font6">除了社會責任，希望成為良心企業</p>
                  <p class="font7">社會企業開始受到人們重視，但其實企業本身就是需要承擔社會責任的，樂活購不只是希望成為社會企業，還希望成為良心企業。良善的產品必得要奠基於良心的DNA之上，沒有良心的DNA，產品就不會良善，企業的精神必得跟其目標一致，而樂活購的目標是成為良心企業的表率，開發良善的產品。
                  </p>
                   <p>&nbsp;</p>
                  <p class="font5">品牌的三個任務</p>
                   <p>&nbsp;</p>
                  <p class="font6">教育為本，正確使用</p>
                  <p class="font7">沒有可以無限制使用而能得到好處的產品，只有正確的知識加上有效的使用，才能讓產品發揮最大的效用。讓資源得到有效的利用。我們不希望只是成為一個提供產品的電商，我們希望成為肩負教育使命的品牌。
                  </p>
                   <p>&nbsp;</p>
                  <p class="font6">以人為本，友善環境</p>
                  <p class="font7">要開發對人有益的產品，那個產品必得是要對環境也有益，樂活購致力發揮影響力，讓我們的供應商都可以開發對環境有益，對人類有益的產品。
                  </p>
                   <p>&nbsp;</p>
                  <img class="img-responsive img1" border="0" src="./img/abouts03.png">
                   <p>&nbsp;</p>
                  <p class="font6">公益產品，正義公平</p>
                  <p class="font7">勞者多得是我們的基本概念，所以讓辛苦開發的供應商也可以得到應得的成果，這是樂活購開發概念之一。身為一個良心的企業，樂活購也希望我們的成果，除了可以改善產品，照顧員工之外。還可以幫助社會弱勢，協助改善環境。所以樂活購將從自有產品開始做起，提供3%、5%、8%、10%等不同比例的金額，提供給社會上需要協助的弱勢團體，或是協助我們自然環境的改善等，希望達成一個良心企業的良善循環。
                  </p>
                   <p>&nbsp;</p>
                  <p class="font7">若是您對於我們有任何意見，歡迎您使用網站上的聯絡客服，將您寶貴的意見發給我們，非常感謝您的支持！
                  </p>
               </div>
             </div>      
          </div>
        </div>
     </article>
   </div>
</div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!--END sql-->