<?php
  require_once("dbtools.inc.php");
  if (!isset($_SESSION)) {session_start();}
?>
<!DOCTYPE HTML>
<!--
	ZeroFour by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
        <title>認識我們</title>
        <meta charset='utf-8'>
		<meta name="description" content="認識我們" />
		<meta name="keywords" content="認識我們" />
		 
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
			<script src="js/jquery.min.js"></script>
			<script src="js/jquery.dropotron.min.js"></script>
			<script src="js/skel.min.js"></script>
			<script src="js/skel-layers.min.js"></script>
			<script src="js/init.js"></script>
			
			
			<noscript>
				<link rel="stylesheet" href="css/skel.css" />
				<link rel="stylesheet" href="css/style.css" />
				<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		
	<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">   
   <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <style>  
	
.home_list .hline{padding:0.5em;border:1px solid #E1E1E1; border-radius:6px;}
 .home_list .font{font-size: 1.2em;letter-spacing:0.2em;text-align:left;}
 
 </style> 
<link href="css/bootstrap.min.css" rel="stylesheet">
 
<!-- Optional theme -->
 <link href="css/bootstrap-theme.min.css" rel="stylesheet" >
 <link href="nav.css" rel="stylesheet" >
<!-- Latest compiled and minified JavaScript -->
 <script src="js/bootstrap.min.js"></script>
<link rel="shortcut icon" href="./images/favicon.png"/>
	<link rel="stylesheet" type="text/css" media="all" href="style.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
		<link rel="stylesheet" href="css/responsive-tabs.css">
		 <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
<style>
    #wrap{min-height: 100%;
      height: auto;
      /* negative indent footer by its heigth*/
      margin: 0 auto -60px;
      /* pad bottom by footer height*/
      padding: 0 0 60px;}
	  </style>
	   <style>  
	
.home_list .hline{padding:1.5em;border:1px solid #E1E1E1; border-radius:6px;}
 .home_list .font{font-size: 1.2em;letter-spacing:0.2em;text-align:left;}
 
 </style> 
	</head>
	
	<body>
	<?php
//裁切字串
function cut_content($a,$b){
    $a = strip_tags($a); //去除HTML標籤
    $sub_contents = mb_substr($a, 0, $b, 'UTF-8'); //擷取子字串
    echo $sub_contents;  //顯示處理後的摘要文字
    //顯示 "......"
   
}

//以上程式已經包裝起來,您可存放在例如:function.php網頁
//往後只要使用include("function.php");
//加上 cut_content($a,$b);即可,不需每次撰寫.
//$a代表欲裁切內容.
//$b代表欲裁切字數(字元數)
?>
	  <?php
	   date_default_timezone_set("Asia/Taipei");
			
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time); 


 
?>
	
	<?php include 'Article/nav.php';?>
	<?php include 'Article/header_top.php';?>
		<?php include 'Article/header.php';?>
					
					<div class="container">	
						

		
	</div>
			<div class="container">	
				<div class="row">
				<div class="col-sm-12 home_list">
<div class="col-md-12 hline" style=" padding:1.0em">
                <div class="font" style="font-size:1.5em">認識我們</div>
             </div>	
			 </div>			
			
						<?php include 'R_content.php';?>
						
					
					 
						<?php include 'Article/sidebar.php';?>
						
						</div>
						
						</div>	
						
						<?php include 'footer.php';?>
				</div>
		</div>
		<!-- Features -->
			

		

	</body>
	</html>