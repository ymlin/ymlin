/*
 1.直接使用 jQuery 而不使用 $ 作為 namespace variable
 2.設定 var $j=jQuery.noConflict(); 使用 $j 取代 $
 3.將程式碼放在 jQuery(document).ready(function($){},記得：function()中間要加$
 */
// 解決超連結虛線--------------------------------------------------------------------
jQuery(function($){
  $("a").focus(function(){
    $(this).blur();
  });
});
// 滑動至最上面---------------------------------------------------------------------
$(function(){
    $("#gott").click(function(){
        jQuery("html,body").animate({
            scrollTop:0
        },300);
    });
    $(window).scroll(function() {
        if ( $(this).scrollTop() > 50){
            $('#gott').fadeIn("fast");
        } else {
            $('#gott').stop().fadeOut("fast");
        }
    });
});
// ---------------------------------------------------------------------------------