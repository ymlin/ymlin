<?php require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php require_once("../../functions/fundb.php");?>
<?php require_once("../../functions.php");?>
<?php $nosec= 2; if(isset($_GET['no_d'])){$nosec= $_GET['no_d'];}?><!-- la-amin fun -->
<?php require_once("../functions.php");?>
<?php require_once("functions_bre.php");?>
<?php require_once("../header_all.php");?>
<!--online edit-->
<link rel="stylesheet" href="../kindeditor/themes/default/themes/default/default.css" />
<link rel="stylesheet" href="../kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="../kindeditor/plugins/code/prettify.js"></script>
<script>
    KindEditor.ready(function(K) {
        var editor1=K.create('textarea[name="cr_cat_text[]"]',{//name=form中textarea的name属性
            cssPath : '../kindeditor/plugins/code/prettify.css',
            uploadJson : '../kindeditor/php/upload_json.php',
            fileManagerJson : '../kindeditor/php/file_manager_json.php',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
            }
        });
        prettyPrint();
    });
</script>
  <!-- feature -->
  <div id="features-wrapper">
    <div class="container"><!--內容區 -->
       <article class="row clearfix mem_list">
          <div class="col-md-12 column spn1">
             <div class="col-sm-12 top_banner_cont">廠商新增</div>
             <div class="col-sm-2">
                   <!-- 數量選擇 -->
                   <div class="btn-group">
                         <button type="button" class="btn btn-sm button1">數量選擇</button>
                         <button type="button" class="btn btn-sm button1 dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                         </button>
                      <ul class="dropdown-menu" role="menu">
                            <li><a href="cat_add.php?no_d=2">2 筆</a></li>
                            <li><a href="cat_add.php?no_d=5">5 筆</a></li>
                            <li><a href="cat_add.php?no_d=10">10 筆</a></li>
                            <li><a href="cat_add.php?no_d=15">15 筆</a></li>
                            <li><a href="cat_add.php?no_d=20">20 筆</a></li>
                      </ul>
                     </div>
                 </div>
           </div>
       </article>
       <article class="row clearfix"><!-- 對齊中間 -->
          <div class="col-md-12 column mem_list">
             <div class="col-sm-12 hline inner">
             <form class="form-horizontal" action="fundb.php" method="post" name="commentForm" id="commentForm">
                 <?php for ($i=1; $i<=$nosec; $i++){ ;?>
                    <div class="col-sm-12 inner">
                       <?php catcreat(0);?><!-- 類別 -->
                       <?php catcreat(5);?><!-- 運費 -->
                       <?php catcreat(1);?><!-- 名稱 -->
                       <?php //catcreat(3);?><!-- 圖片 -->
                       <?php catcreat(4);?><!-- 訂購上限額 -->
                       <?php catcreat2();?><!-- 備註 -->
                    </div>
                 <div class="col-sm-3 col-sm-offset-1 spn1">
                    <button id="cr_ca_send" name="cr_ca_send" class="btn">確定送出</button>
                    <button class="btn button1" type="button" title="回上頁" onclick="location.href='cat_view.php'">回上頁</button>
                 </div>
                 <div class="col-sm-12"><hr></div>
                 <?php }?>
             </form>
             </div>
          </div> 
       </article>
     </div><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->