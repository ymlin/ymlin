<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data['titles'];?></title>
    <meta charset="utf-8">
    <meta name="keywords" content="<?php print $data['keyw'];?>"> 
    <meta name="description" content="<?php echo $data['desc'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css" >
    <link rel="stylesheet" href="../../css/bootstrap.css" >
    <!-- <link rel="stylesheet" type="text/css" href="css/style.css" >-->
    <link rel="stylesheet" href="../../css/style1.css" >
    <link rel="stylesheet" href="../../css/style-desktop.css" >
    <link rel="shortcut icon" href="../../img/la-16x16.png"/>
	
	 <!-- 日歷  -->
	 <link href='time/jquery-ui-timepicker-addon.css' rel='stylesheet'>
  <script type="text/javascript" src="time/jquery-ui-timepicker-addon.js"></script>
  <script type='text/javascript' src='time/jquery-ui-sliderAccess.js'></script>
  
  
  <!-- 文字編輯器  -->
	<link rel="stylesheet" href="kindeditor/themes/default/themes/default/default.css" />
<link rel="stylesheet" href="kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="kindeditor/plugins/code/prettify.js"></script>
<script>
    KindEditor.ready(function(K) {
        var editor1=K.create('textarea[name="comment"]',{//name=form中textarea的name属性
            cssPath : '../kindeditor/plugins/code/prettify.css',
            uploadJson : '../kindeditor/php/upload_json.php',
            fileManagerJson : '../kindeditor/php/file_manager_json.php',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
            }
        });
        prettyPrint();
    });
</script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.js"></script>
	<script type="text/javascript" src="../../js/infiniteScroll.js"></script>
</head>
<body>
<div id="wrap"><!--最外層包裝 //-->
  <div id="header-wrapper"></div><!--Logo-->