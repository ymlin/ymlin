<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
    
    <!-- Add libraries -->
    <script type="text/javascript" src="js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/libs/jquery-ui-1.8.5.min.js"></script>
    <script type="text/javascript" src="js/libs/jquery.mobile-1.3.2.min.js"></script>
    
    <!-- Add objects classes -->
    <script src="js/Rect.js" type="text/javascript"></script>
    
    <!-- Add styles -->
    <link href="css/wheelPicker.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript">
        // Hide jQuery Mobile loading message
        $.mobile.loadingMessage = false;
    </script>
</head>

<body style="background-color: <?php echo (isset($_GET['daydiff'])?"#efefef":"transparent");?>; height:100%; margin:0;">

	<style>
	html{
	 -webkit-tap-highlight-color:rgba(0,0,0,0);
	}
	</style>

    <?php
		include 'lunar.class.php';
		
		if(!isset($_GET['daydiff']))
		{
			$_GET['daydiff'] = 0; 
			$nodiff=true;
		}
		else $nodiff=false;
		
		$lunar = new Lunar();
		
		if($_GET['daydiff'] == 0) 		
		{
			$weekday = date('w');
			$year = date('Y');
			$month = date('n');
			$day = date('j');
		}
		else if($_GET['daydiff'] > 0)
		{
			$weekday = date('w',strtotime("+".$_GET['daydiff']." days"));
			$year = date('Y',strtotime("+".$_GET['daydiff']." days"));
			$month = date('n',strtotime("+".$_GET['daydiff']." days"));
			$day = date('j',strtotime("+".$_GET['daydiff']." days"));
		}
		else							
		{
			$weekday = date('w',strtotime("-".(-$_GET['daydiff'])." days"));
			$year = date('Y',strtotime("-".(-$_GET['daydiff'])." days"));
			$month = date('n',strtotime("-".(-$_GET['daydiff'])." days"));
			$day = date('j',strtotime("-".(-$_GET['daydiff'])." days"));
		}
		
		
		$lunarday = $lunar->convertSolarToLunar($year, $month, $day);
		//print_r($lunarday);
	?>
	
	<table style="color:red;  text-align:center; width:100%; background:url(../mickey/top_bar.gif) no-repeat; background-size: cover; background-position: center center; " >
	<tr >
		<td style="width:35%;"></td>
		<td><div style="font-size:1.5em; font-weight: 900;"><?php echo date('Y'); ?></div></td>
		<td><div style="font-size:1em; margin-top:0.2em;"><?php echo "年"; ?></div></td>
		<td><div style="font-size:1.5em; font-weight: 900;"><?php echo date('n'); ?></div></td>
		<td><div style="font-size:1em; margin-top:0.2em;"><?php echo "月"; ?></div></td>
		<td style="width:35%;"></td>
	</tr>
	</table>
	
	<?php
		if(!$nodiff) {
	?>
		<table style="text-align:center; width:100%; height: 11em; 
													background-color: white;
													border-bottom-width: 1px;
													border-bottom-style: solid; 
													border-bottom-color: #ff0000;
													border-collapse: collapse;" >
		<tr >
			<td style="width:22%; height: 12em;">
				<div style="height: 12em; " >
					<div style="height: 3%;"></div>
					<div style="height: 15%; background:url(../mickey/ring.png) no-repeat; background-size: 2em 2em; background-position: center center;"></div>
					<div style="height: 25%; color:red;font-size:100%;">
					<?php 
						for($i = 0 ; $i < mb_strlen($lunarday[2]) ; $i++ )
						{
							echo mb_substr($lunarday[2], $i, 1,'UTF-8')."<br>";
						}
					?>
					</div>
					<?php
					
						$sql3 = "SELECT * FROM `notification` WHERE month = ? and day = ?";
						$sth3 = $db->prepare($sql3);
						$sth3->execute(array($lunarday[4],$lunarday[5]));
						$result3 = $sth3->fetchobject();
					?>
					<div style="height: 20%; color:red;font-size:100%;">
					<?php 
						for($i = 0 ; $i < mb_strlen($result3->shortcomm) ; $i++ )
						{
							echo mb_substr($result3->shortcomm, $i, 1,'UTF-8')."<br>";
						}
					?>
					</div>
				</div>
			</td>
			<td style="width:56%; height: 12em;">
				<div style="
							border-left-width: 1px;
							border-left-style: solid; 
							border-left-color: #ff0000;
							border-right-width: 1px;
							border-right-style: solid; 
							border-right-color: #ff0000;
							height: 12em;">
					<div style="
					font-size:0.4em;">
						<?php echo "<br>"; ?>
					</div>
					<div style="font-size:1.2em; color:red; text-align:right;" >
						<?php
							switch($weekday)
							{
								case 0:echo "星期日&nbsp&nbsp";break;
								case 1:echo "星期一&nbsp&nbsp";break;
								case 2:echo "星期二&nbsp&nbsp";break;
								case 3:echo "星期三&nbsp&nbsp";break;
								case 4:echo "星期四&nbsp&nbsp";break;
								case 5:echo "星期五&nbsp&nbsp";break;
								case 6:echo "星期六&nbsp&nbsp";break;
							}
						?>
					</div>
					<?php
						$day_type = "";
						if($weekday == 0) $day_type = "red";
						if($weekday == 6) $day_type = "green";
					?>
					<div style="
					height: 10em;
					margin:0.2em 0em 0em 0em;
					background:url(day_img/<?php echo $day_type.$day;?>.png) no-repeat; 
					background-size: 10em 10em; 
					background-position: center center;"
					onclick="alert('setdate<?php echo ' '.$year.' '.$month.' '.$day; ?>')"
					>
					</div>
				</div>
			</td>
			<?php
				$sql5 = "SELECT * FROM `holiday` WHERE (month = ? and day = ? and lunar_solar = 0) or (month = ? and day = ? and lunar_solar = 1)";
				$sth5 = $db->prepare($sql5);
				$sth5->execute(array($lunarday[4],$lunarday[5], $month, $day));
				$result5 = $sth5->fetchobject();
				
				if($_GET['weather'] == "clouds") $weather="cloudy";
				else if($_GET['weather'] == "Rain" ) $weather="rainy";
				else if($_GET['weather'] == "Drizzle") $weather="drizzle";
				else if($_GET['weather'] == "Thunderstorm") $weather="thunderstorm";
				else if($_GET['weather'] == "mist") $weather="mist";
				else $weather="sunny";
			?>
			<td style="width:22%; height: 12em;">
				<div style="height: 12em;">
					<div style="height: 2%;"></div>
					<?php
						if($_GET['mintemp']!=""){
					?>
					<div style="height: 30%; background:url('weather_img/<?php echo $weather; ?>.png') no-repeat; background-size: 3.5em 3.5em; background-position: center center;"></div>
					<div style="height: 10%; color:black;font-size:60%;"><?php echo $_GET['mintemp']."°~".$_GET['maxtemp']."°"; ?></div>
					<div style="height: 8%;"></div>
					<?php
						}
						else
						{
					?>
					<div style="height: 30%;"></div>
					<?php
						}
					?>
					<div style="height: 40%; color:red;font-size:120%;">
					<?php 
						for($i = 0 ; $i < mb_strlen($result5->holiday) ; $i++ )
						{
							echo mb_substr($result5->holiday, $i, 1,'UTF-8')."<br>";
						}
					?>
					</div>
				</div>
			</td>
		</tr>
		</table>
		
		<table style="width:100%; border-bottom-width: 1px;
									border-bottom-style: solid; 
									border-bottom-color: #ff0000;
									border-collapse: collapse;" >
		<tr >
			<td style="width:22%; height: 5em;">
				<div style="height: 100%; text-align:center;">
					<div style="height: 22%; "></div>
					<div style="color:red; font-size:120%;"><?php echo "農曆<br>".$lunarday[2];?></div>
				</div>
			</td>
			<?php
				$sql4 = "SELECT * FROM `goodday` WHERE month = ? and day = ?";
				$sth4 = $db->prepare($sql4);
				$sth4->execute(array($lunarday[4],$lunarday[5]));
				$result4 = $sth4->fetchobject();
			?>
			<td style="width:56%; height: 5em;">
				<div style="
					margin: 0;
					border-left-width: 1px;
					border-left-style: solid; 
					border-left-color: #ff0000;
					border-right-width: 1px;
					border-right-style: solid; 
					border-right-color: #ff0000;
					height: 100%;">
					<div style="height:12%; "></div>
					<div style="height:22%; color:red; font-size:0.75em; margin:0% 20% 0%;"><?php echo "【宜】".mb_substr($result4->good, 0, 9, 'utf-8'); ?></div>
					<div style="height:22%; color:red; font-size:0.75em; margin:0% 20% 0%; "><?php echo "　　　".mb_substr($result4->good, 9, 9, 'utf-8'); ?></div>
					<div style="height:22%; color:black; font-size:0.75em; margin:0% 20% 0%;"><?php echo "【忌】".mb_substr($result4->bad, 0, 9, 'utf-8');?></div>
					<div style="height:22%; color:black; font-size:0.75em; margin:0% 20% 0%; "><?php echo "　　　".mb_substr($result4->bad, 9, 9, 'utf-8');?></div>
				</div>
			</td>
			<td style="width:22%; height: 5em;">
				<div style="height: 100%;">
					<div style="height:10%; "></div>
					<div style="height:40%; color:red; font-size:120%; text-align:center;"><?php echo "吉時";?></div>
					<div style="height:50%; color:red; font-size:90%; margin:0% 29% 0%; text-align:center;"><?php echo $result4->goodtime;?></div>
				</div>
			</td>
		</tr>
		</table>
		
		<?php
			$sql = "SELECT prefer FROM `users` WHERE id = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['userid']));
			$result = $sth->fetchobject();
			$prefers = explode(" ", $result->prefer);
			
			$prefer_rel = explode(",", $prefers[0]);
			$qry_prefer = "( top = '1' or (( religion = '不限' ";
			for($i = 0 ; $i < count($prefer_rel) ; $i++ ) $qry_prefer=$qry_prefer." or religion = '".$prefer_rel[$i]."' ";
			$qry_prefer = $qry_prefer.") and ";
			
			$prefer_bud = explode(",", $prefers[1]);
			$qry_prefer = $qry_prefer."( buddha = '無' ";
			for($i = 0 ; $i < count($prefer_bud) ; $i++ ) $qry_prefer=$qry_prefer." or buddha = '".$prefer_bud[$i]."' ";
			$qry_prefer = $qry_prefer.") and ";
			
			$prefer_sea = explode(",", $prefers[1]);
			$qry_prefer = $qry_prefer."( season = '無' ";
			for($i = 0 ; $i < count($prefer_sea) ; $i++ ) $qry_prefer=$qry_prefer." or season = '".$prefer_sea[$i]."' ";
			$qry_prefer = $qry_prefer."))) ";
			
			$sql = "SELECT * FROM `articles` WHERE keyword = '拜拜大小事' and ";
			$sth = $db->prepare($sql.$qry_prefer."ORDER BY top DESC,datetime DESC");
			$sth->execute();
			$result = $sth->fetchobject();
		?>

		<div style="position:relative; left:0px; top:0px; z-index:0;">
			
			<table style="
			width:100%;
			height:6em;
			background:url(img/article_bg.png) no-repeat; 
			background-size: 90% 6em; 
			background-position: center center;"
			>
				<tr>
					<td style="width:40%;" >
						<div style="
							margin: 0px 0px 0px 13%;
							height:6em;
							width:85%;"
							onclick="alert('article <?php echo $result->id;?>')">
							<div style="
							position:relative; left:0px; top:0px; z-index:-1;
							height:100%;
							width:100%;
							background:url('../mickey/<?php echo $result->photo_name; ?>') no-repeat; 
							background-size: cover; 
							background-position: center center;">
							</div>
						</div>
					</td>
					<td style="width:40%;
						font-size:80%;
						position:relative; left:0px; top:0px; z-index:1;">
						<div style="
							height:3em;
							width:100%;"
							onclick="alert('article <?php echo $result->id;?>')">
							<?php 
								echo $result->title."<br>";
							?>
							
						</div>
						
						<div style="
							color:gray;
							font-size: 85%;
							height:3em;
							width:100%;"
							onclick="alert('article <?php echo $result->id;?>')">
							<?php 
								echo mb_substr(strip_tags($result->comment),0,28,"UTF-8");
								if(mb_strlen(strip_tags($result->comment),"UTF-8") > 28) echo "...";
							?>
							
						</div>
					</td>
					<td style="width:6%;
						position:relative; left:0px; top:0px; z-index:1;">
					</td>
				</tr>
			</table>
			
			<?php
				$sql2 = "SELECT * FROM `products` WHERE ";
				$sth2 = $db->prepare($sql2.$qry_prefer."ORDER BY top DESC,datetime DESC");
				$sth2->execute();
				$result2 = $sth2->fetchobject();
			?>
			
			<table style="
			width:100%;
			height:4em;
			background:url(img/product_bg.png) no-repeat; 
			background-size: 90% 4em; 
			background-position: center center;">
				<tr>
					<td style="width:40%;" >
						<div style="
							margin: 0px 0px 0px 13%;
							height:4em;
							width:85%;"
							onclick="alert('product <?php echo $result2->id;?>')">
							<div style="
							position:relative; left:0px; top:0px; z-index:-1;
							height:100%;
							width:100%;
							background:url('../mickey/<?php echo $result2->photo_name; ?>') no-repeat; 
							background-size: cover; 
							background-position: center center;"
							>
							</div>
						</div>
					</td>
					<td style="width:40%;
						height:4em;
						font-size:80%;
						position:relative; left:0px; top:0px; z-index:1;">
						<div style="
							height:2.5em;
							width:100%;
							"
							onclick="alert('product <?php echo $result2->id;?>')"
							>
							<?php
								echo "<br>";
								echo mb_substr(strip_tags($result2->name),0,10,"UTF-8");
								if(mb_strlen(strip_tags($result2->name),"UTF-8") > 10) echo "...";
							?>
						</div>
						
						<div style="
							margin: 0px 0px 0px 85%;
							width:12%;
							height:1.7em;
							background:url('../mickey/into_cart.png') no-repeat; 
							background-size: 100% 1.5em; 
							background-position: center center;
							"
							onclick = "alert('addtocart<?php echo " ".$result2->id." ".mb_substr(strip_tags($result2->name),0,5,"UTF-8").(mb_strlen(strip_tags($result2->name),"UTF-8") > 5? "..." : ""); ?>');"
							>
						</div>
					</td>
					<td style="width:6%;
						position:relative; left:0px; top:0px; z-index:1;">
					</td>
				</tr>
			</table>
		</div>
	<?php
		}
	?>
</div>

</body>

</html>

