<?php @require_once("../conn/wp-c.php");?>
<?php @require_once('../functions/EDcart.php');?>
<?php @require_once("../functions/Getcart.php");?>
<?php @require_once("../functions/fundb.php");?>
<?php @require("../functions.php");?>
<?php @require("header.php");?>
  <!-- feature -->
  <div id="features-wrapper">
     <!--內容區 -->
     <article class="container"><!-- 對齊中間 -->
        <div class="row clearfix reg_list">
               <div class="col-lg-12 column">
                  <div class="col-md-12 top_banner_cont">忘記密碼</div>
                  <div class="col-md-12 hightline">
                    <form class="form-horizontal" action="fundb_fg.php" method="post" name="commentForm" id="commentForm">
                      <div class="form-group">
                         <div class="col-sm-5 col-sm-offset-1">
                            <!-- 訊息處理 --> 
                            <?php if(isset($_GET['Msg'])){errmess($_GET['Msg']);}?>
                         </div>
                      </div>
				      <div class="form-group">
					     <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					     <div class="col-sm-3">
						    <input id="m_username" name="m_username" placeholder="請輸入Email" class="input25 email" required="" type="text">
					     </div>
                         <h5>*請輸入您登入的e-mail帳號</h5>
				      </div>
				      <div class="form-group">
					     <label for="inputPassword3" class="col-sm-2 control-label">圖形驗證
</label>
					     <div class="col-sm-3 has-warning">
						    <input id="cas" name="cas" placeholder="請輸入下方驗證碼" class="input25 required" type="text"><h5><a href="use_forget.php" target="_parent" class="ong_colo"><img src="pic.php" width="150"/>換一組驗證碼</a></h5>
					     </div>
				      </div>
				      <div class="form-group">
					     <div class="col-sm-offset-2 col-sm-10">
						    <button id="MM_forget" name="MM_forget" value="commentForm" class="btn" style="background-color:#676767; color:#fff">我要查</button>
					     </div>
				      </div>
			        </form>
                  </div>
               </div>
            </div> 
        </article>
     </div><!--內容 結尾-->
  </div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->