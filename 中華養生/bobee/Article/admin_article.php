<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['remove'])
	{
		$sql = "DELETE FROM `articles` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
	
	if($_GET['addprod'])
	{
		$sql = "INSERT INTO `relative_product` (id, article_id, product_id) " 
			. " VALUES('', ?, ?) ";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['addprod'], $_GET['newproduct_id']));
	}
	else if($_GET['rmprod'])
	{
		$sql = "DELETE FROM `relative_product`" 
			. " WHERE article_id = ? and product_id = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['rmprod'], $_GET['rmproduct_id']));
	}
	
	if(isset($_GET['change']))
	{
		$sql = "SELECT top FROM `articles`" 
			. " WHERE id = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['change']));
		$result = $sth->fetchobject();
		$top = 1-($result->top);
		
		$sql = "UPDATE `articles` SET top = ? " 
			. " WHERE id = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($top, $_GET['change']));
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>文章管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px;" >
	<?php include __DIR__.'/../admin_top.php';?>
	
	<div style="width:80%; margin: 0px auto;">  
		<script>
			function handleClick(i) {
				window.location.href="admin_article.php?change="+i;
			}
		</script>
		<table border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="20" id="color">
				<form align="center" action = "admin_article_edit.php" align="center">
					<h2 align="center">文章管理</h2>
					<div align="center">
						<button class='btn btn-primary'>新增文章</button>
					</div>
				</form>
			</td> 
			</tr>  

			<tr>
			<td id="color2"><div align="center" ><strong>id</strong></div></td> 
			<td id="color2"><div align="center" ><strong>標題</strong></div></td> 
			<td id="color2"><div align="center" ><strong>作者</strong></div></td>
			<td id="color2"><div align="center" ><strong>分類</strong></div></td>
			<td id="color2"><div align="center" ><strong>標籤</strong></div></td>
			<td id="color2"><div align="center" ><strong>文章</strong></div></td>  
			<td id="color2"><div align="center" ><strong>圖片</strong></div></td>
			<td id="color2"><div align="center" ><strong>影片連結</strong></div></td>
			<td id="color2" colspan="4"><div align="center" ><strong>詳細分類</strong></div></td> 
			<td id="color2"><div align="center" ><strong>置頂</strong></div></td>			
			<td id="color2"><div align="center" ><strong>週點擊率</strong></div></td>
			<td id="color2"><div align="center" ><strong>點擊率</strong></div></td>
			<td id="color2"><div align="center" ><strong>分享人數</strong></div></td>
			<td id="color2" colspan="2"><div align="center" ><strong>關聯商品管理</strong></div></td>
			<td id="color2"><div align="center" ><strong>上架時間</strong></div></td>
			<td id="color2"><div align="center" ><strong>管理</strong></div></td>  			
			</tr>  
			
			<?php
				$sql = "SELECT * FROM `articles` ORDER BY top DESC,datetime DESC";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				while($result = $sth->fetchobject())
				{
					echo "<tr>";
					
				?>
					<form action = "admin_article_update.php"  method = "GET">
				<?php
						echo "<td align=\"center\" >" . $result->id . "</td>";	
						echo "<td align=\"center\" >";
						echo mb_substr($result->title,0,15,"UTF-8");
						if(mb_strlen($result->title,"UTF-8") > 15) echo "...";
						echo "</td>";	
						echo "<td align=\"center\" >" . $result->editor . "</td>";
						echo "<td align=\"center\" >" . $result->keyword . "</td>";	
						echo "<td align=\"center\" >" . $result->tag . "</td>";
						if(!($_GET['preview'] == $result->id))
						{
				?>
							<td align="center">
								<button formaction="admin_article.php"  class='btn btn-primary' type = "submit" name = "preview" value= "<?php echo  $result->id ?>">預覽</button>  
							</td>
				<?php
						}
						else
						{
				?>
							<td align="center" style="word-wrap: break-word; word-break: normal; ">
							<button formaction="admin_article.php"  class='btn btn-primary' type = "submit" name = "preview" value= "">收回</button>
							<br>
							<div class="container">
				<?php			
							echo $result->comment . "</div></td>";
						}
				?>
						<td align="center"><img src="../mickey/<?php echo $result->photo_name; ?>?=<?php echo $photo_name;?>" alt="" height="100" width="100" /></td>
				<?php
						echo "<td align=\"center\" style=\"white-space:nowrap;\">";
						
						if($result->video != "")
						{
							echo '<a href="'.$result->video.'" target="_blank">'.$result->video;
							$pos = strpos($result->video, 'youtu.be');
							$image = "http://img.youtube.com/vi/".substr($result->video, $pos+9)."/maxresdefault.jpg";
				?>
							<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: cover; background-position: center center;" >
								<div style="width:100%; height:100%; margin: 0px auto; background:url('../mickey/play.png') no-repeat; background-size: cover; background-position: center center;"></div>
							</div>
				<?php	
							echo '</a>';
						}
						
						echo "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->user_type . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->religion . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->season . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->buddha . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . "<input type='checkbox' onclick='handleClick(". $result->id .");' ".($result->top=="1"?"checked":"")." />" . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->seven_click_rate . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->total_click_rate . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->share . "</td>";
						echo "<td align=\"center\">";
						
						$sql2 = "SELECT * FROM `relative_product`" 
								. " WHERE `article_id` = ?";
						$sth2 = $db->prepare($sql2);
						$sth2->execute(array($result->id));
						
						$count = 1;
						while($result2 = $sth2->fetchobject())
						{
							$sql3 = "SELECT * FROM `products`" 
								. " WHERE `id` = ?";
							$sth3 = $db->prepare($sql3);
							$sth3->execute(array($result2->product_id));
							$result3 = $sth3->fetchobject();
							echo "<div style=\"white-space:nowrap;\">" . $count . ".";
							echo mb_substr($result3->name,0,3,"UTF-8");
							if(mb_strlen($result3->name,"UTF-8") > 3) echo "...";
							$count++;
						}
						
						echo "</td><td >";
						
						if($_GET['addproduct'] == $result->id)
						{
							$sql2 = "SELECT * FROM `products`";
							$sth2 = $db->prepare($sql2);
							$sth2->execute();
							
							echo "<select name=\"newproduct_id\">";
							while($result2 = $sth2->fetchobject())
							{
								$sql3 = "SELECT * FROM `relative_product`" 
								. " WHERE `article_id` = ? and `product_id` = ?";
								$sth3 = $db->prepare($sql3);
								$sth3->execute(array($result->id, $result2->id));
								$result3 = $sth3->fetchobject();
								if(!$result3)
								{
									echo "<option value=\"{$result2->id}\">";
									echo mb_substr($result2->name,0,5,"UTF-8");
									if(mb_strlen($result2->name,"UTF-8") > 5) echo "...";
									echo "</option>";
								}
							}
							echo "</select>";
				?>		
							<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "addprod" value= "<?php echo $result->id;?>" >新增</button> 
				<?php			
						}
						else if($_GET['removeproduct'] == $result->id)
						{
							$sql2 = "SELECT * FROM `relative_product` WHERE `article_id` = ?";
							$sth2 = $db->prepare($sql2);
							$sth2->execute(array($result->id));
							
							echo "<select name=\"rmproduct_id\">";
							while($result2 = $sth2->fetchobject())
							{
								$sql3 = "SELECT * FROM `products`" 
								. " WHERE `id` = ?";
								$sth3 = $db->prepare($sql3);
								$sth3->execute(array($result2->product_id));
								$result3 = $sth3->fetchobject();
								echo "<option value=\"{$result2->product_id}\">";
								echo mb_substr($result3->name,0,5,"UTF-8");
								if(mb_strlen($result3->name,"UTF-8") > 5) echo "...";
								echo "</option>";
							}
							echo "</select>";
				?>	
							<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "rmprod" value= "<?php echo $result->id;?>"  onclick="return confirm('是否確認刪除這筆資料');" >刪除</button> 
				<?php			
						}
						else
						{
				?>			
							<div style="white-space:nowrap;">
								<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "addproduct" value="<?php echo $result->id;?>" >新增</button>  
								<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "removeproduct" value="<?php echo $result->id;?>" >刪除</button>  
							</div>
				<?php		
						}
				?>
						</td>
				<?php	
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->datetime . "</td>";
				?>	
						<td align="center">
							<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
							<button  class='btn btn-primary' formaction="admin_article.php" type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
						</td>  
					</form>
				<?php
				}
			?>
		</table>  
	</div>  
</body>  
</html>