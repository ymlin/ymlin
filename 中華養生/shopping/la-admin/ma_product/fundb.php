<?php @require_once("../../conn/wp-c.php");?>
<?php
    if (!isset($_SESSION)) {session_start();}
//---- send DB ----;
function sqldb($SQLdata){
           global $wp_c;
           $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());         
    }
//---- 編號產生 DB ----;
function sqldbno(){
           global $wp_c;
    
           $sql_pro = "SELECT Max(pro_tID) as max_id3 FROM product_tw";
           $Result1 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row_pro = mysqli_fetch_assoc($Result1);
           $maxid3 = $row_pro['max_id3'] + 1;
           return $maxid3;
    }
/*----------------------------------------------------------------------------*/
//---- creat product -----;
if (isset($_POST['cr_pr_send']) && ($_POST['cr_pr_send'] == "cr_pro")){ 
            //-----Data 第一筆空值-----;
		    if(empty($_POST['cr_pro_name'][0])){
				header("Location:index.php");
	            exit;}
              
            //-----取得陣列Data-----;
	        for($s=0; $s < count($_POST['cr_pro_name']); $s++){
              //-----Data 第一筆空值-----;
		      if(empty($_POST['cr_pro_name'][$s])){
				header("Location:index.php");
	            exit;} 
               //---- 編號產生 ----;
               $Opro = "BL00" . sqldbno();

              $cr_bid= $_POST['cr_br_id'][$s];
              $cr_cid= $_POST['cr_ca_id'][$s];
              $cr_pna= htmlentities($_POST['cr_pro_name'][$s],ENT_QUOTES,"UTF-8");            
              $cr_ppr1= htmlentities($_POST['cr_pro_price1'][$s],ENT_QUOTES,"UTF-8");
              $cr_ppr2= htmlentities($_POST['cr_pro_price2'][$s],ENT_QUOTES,"UTF-8");
              //$cr_psa= $_POST['cr_pro_saledate'][$s];
              $cr_pqu= htmlentities($_POST['cr_pro_quan'][$s],ENT_QUOTES,"UTF-8");
              $cr_pim= htmlentities($_POST['cr_pro_img'][$s],ENT_QUOTES,"UTF-8");
              $cr_pim2= htmlentities($_POST['cr_pro_img2'][$s],ENT_QUOTES,"UTF-8");
              $cr_pex= $_POST['cr_pro_exch'][$s];
              $cr_pex3= $_POST['cr_pro_exch3'][$s];
              $cr_pex2= $_POST['cr_pro_exch2'][$s];
              //$cr_fre= $_POST['pro_freight'][$s];
    
             $SQLdata = ("INSERT INTO product_tw (sec_ID, brcate_id, pro_tNo, pro_tName, pro_tPrice, pro_tPrice2, pro_Quantity, pro_tIMAGE, pro_tIMAGE2, pro_tExch, pro_tExch3) VALUES ('{$cr_bid}', '{$cr_cid}', '{$Opro}', '{$cr_pna}', '{$cr_ppr1}', '{$cr_ppr2}', '{$cr_pqu}', '{$cr_pim}', '{$cr_pim2}', '{$cr_pex}', '{$cr_pex3}')");
             sqldb($SQLdata);
            }
            header ("Location:index.php");
            exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit product -----;
if (isset($_POST['ed_pr_send']) && ($_POST['ed_pr_send'] == "ed_pro")){ 
              $pro_id= $_POST['pr_d'];
              $cr_id= $_POST['ed_br_id'];
              $cat_id= $_POST['ed_ca_id'];
              $cr_na= htmlentities($_POST['pro_name'],ENT_QUOTES,"UTF-8");
              $cr_pr1= htmlentities($_POST['pro_price1'],ENT_QUOTES,"UTF-8");
              $cr_pr2= htmlentities($_POST['pro_price2'],ENT_QUOTES,"UTF-8");
              $cr_qu= htmlentities($_POST['pro_quan'],ENT_QUOTES,"UTF-8");
              $cr_im= htmlentities($_POST['pro_img'],ENT_QUOTES,"UTF-8");
              $cr_im2= htmlentities($_POST['pro_img2'],ENT_QUOTES,"UTF-8");
              $cr_ex1= $_POST['pro_exch'];
              $cr_ex2= $_POST['pro_exch2'];
              $cr_ex3= $_POST['pro_exch3'];
              //$cr_fr = $_POST['pro_freight'];
              
    	     $SQLdata = ("UPDATE product_tw SET sec_ID = '{$cr_id}', brcate_id = '{$cat_id}', pro_tName = '{$cr_na}', pro_tPrice = '{$cr_pr1}', pro_tPrice2 = '{$cr_pr2}', pro_Quantity = '{$cr_qu}', pro_tIMAGE = '{$cr_im}', pro_tIMAGE2 = '{$cr_im2}', pro_tExch = '{$cr_ex1}', pro_tExch2 = '{$cr_ex2}', pro_tExch3 = '{$cr_ex3}' WHERE pro_tID = '{$pro_id}'");
             sqldb($SQLdata);
	         
             header("Location:pro_edit.php?pr_d=$pro_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Del product -----;
if (isset($_POST['de_pr_send']) && ($_POST['de_pr_send'] == "del_pro")){ 
            //----- Data 第一筆空值-----;
		    if(empty($_POST['d_prod'][0])){
				header("Location:index.php");
	            exit;}
             //-----取得陣列Data-----;
              $num= count($_POST['d_prod']);
	        for($s=0;$s<$num;$s++){
                $pro_id= $_POST['d_prod'][$s];
		        $SQLdata = "DELETE FROM product_tw WHERE pro_tID = '{$pro_id}'";
                $SQLdata1 = "DELETE FROM product_type WHERE pro_tID = '{$pro_id}'";
		        sqldb($SQLdata);			
		        sqldb($SQLdata1);			
	        } 
	         
             header("Location:index.php");
	         exit;
}
//---- Creat category -----;
if (isset($_POST['cr_ca_send']) && ($_POST['cr_ca_send'] == "cr_cat")){ 
            //-----Data 第一筆空值-----;
		    if($_POST['catname'] == ""){
				header("Location:cat_view.php");
	            exit;
            }else{
                $cat_na = $_POST['catname'];
                $cat_shi = $_POST['catshi'];
             $SQLdata = ("INSERT INTO product_category (cat_name, cat_shipping) VALUES ('{$cat_na}', '{$cat_shi}')");
             sqldb($SQLdata);
            }
            header ("Location:cat_view.php");
            exit;
}
/*----------------------------------------------------------------------------*/
//---- Del product_freight db -----;
if (isset($_POST['fre_del']) && ($_POST['fre_del'] == "fredel")){ 
              $pro_id= $_POST['pr_d'];
             //-----取得陣列Data-----;
              $num= count($_POST['pro_fre_d']);
	        for($s=0; $s<$num; $s++){
                $fre_id= $_POST['pro_fre_d'][$s];

               if($fre_id !== ''){     
		          $SQLdata = "DELETE FROM product_freight WHERE pro_fre_id = '{$fre_id}'";
		          sqldb($SQLdata);
               }
	        } 
	         
             header("Location:pro_edit.php?pr_d=$pro_id");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Creat product_freight db-----;
if (isset($_POST['pro_fre']) && ($_POST['pro_fre'] == "profre")){
              $pro_id= $_POST['pr_d'];
             //-----取得陣列Data-----;
              $num= count($_POST['shi_d']);
	        for($s=0; $s<$num; $s++){
                $shi_id= $_POST['shi_d'][$s];
                $frei_pre= $_POST['fre_pre'][$s];
   
              if($frei_pre !== ''){ 
		          $SQLdata = ("INSERT INTO product_freight (pro_tID, od_sh_id, pro_fre_price) VALUES ('{$pro_id}', '{$shi_id}', '{$frei_pre}')");
		          sqldb($SQLdata);
              }
	        } 
            header("Location:pro_edit.php?pr_d=$pro_id");
            exit;
}
/*----------------------------------------------------------------------------*/
//---- creat shipping data -----;
if ((isset($_POST['shp_creat'])) && ($_POST['shp_creat'] == "shpcreat")){
            $pro_id= $_POST['pr_d'];
            if((isset($_POST['shp_na']))&& ($_POST['shp_na'] == '')){
                header("Location:pro_freight.php?pr_d=".$pro_id);
	            exit;
            }
              $cr_shp_na = $_POST['shp_na'];
                
              $SQLdata4 = ("INSERT INTO oders_shipping (od_sh_name)VALUES('{$cr_shp_na}')");
              sqldb($SQLdata4);

             header("Location:pro_freight.php?pr_d=".$pro_id);
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit product type -----;
if (isset($_POST['ca_typ_send']) && ($_POST['ca_typ_send'] == "ca_typ")){ 
              $pro_id= $_POST['pr_d'];
              $ca_type= $_POST['ty_type'];
              $ca_size= $_POST['ty_size'];
              
    	     $SQLdata = ("INSERT INTO product_type (pro_tID, pro_ty_type, pro_ty_size) VALUES ('{$pro_id}', '{$ca_type}', '{$ca_size}')");
             sqldb($SQLdata);
	         
             header("Location:pro_edit.php?pr_d=".$pro_id);
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Del category -----;
if (isset($_POST['de_typ_send']) && ($_POST['de_typ_send'] == "del_typ")){ 
            //----- Data 第一筆空值-----;
		    if(empty($_POST['d_typd'][0])){
				header("Location:index.php");
	            exit;}
             //-----取得陣列Data-----;
              $num= count($_POST['d_typd']);
	        for($s=0;$s<$num;$s++){
                $typ_id= $_POST['d_typd'][$s];
		        $SQLdata = "DELETE FROM product_type WHERE pro_ty_id = '{$typ_id}'";
		        sqldb($SQLdata);			
	        } 
	         
             header("Location:type_view.php");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- creat shipping data -----;
if ((isset($_POST['shp_creat'])) && ($_POST['shp_creat'] == "shpcreat")){
    
              $cr_shp_na = $_POST['shp_na'];   
              $cr_shp_pri = $_POST['shp_pri'];
               
              $SQLdata4 = ("INSERT INTO oders_shipping (od_sh_name, od_sh_price)VALUES('{$cr_shp_na}', '{$cr_shp_pri}')");
              sqldb($SQLdata4);

             header("Location:ord_shipping.php");
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- edit shipping data -----;
if ((isset($_POST['shp_edit'])) && ($_POST['shp_edit'] == "shpedit")){
              $ed_shp_id= $_POST['shp_d'];
              $ed_shp_na = $_POST['shp_na'];   
              $ed_shp_pri = $_POST['shp_pri'];
      
              $SQLdata5 = ("UPDATE oders_shipping SET od_sh_name='{$ed_shp_na}', od_sh_price='{$ed_shp_pri}' WHERE od_sh_id = '{$ed_shp_id}'");
              sqldb($SQLdata5);
             header("Location:ord_shipping_up.php?shd=".$ed_shp_id);
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Del shipping data -----;
if ((isset($_POST['shp_del'])) && ($_POST['shp_del'] == "shpdel")){
           //----- Data 第一筆空值-----;
		   if(empty($_POST['d_shpd'][0])){
			  header("Location: ord_shipping.php");
	          exit;}
           //-----取得陣列Data-----;
	       for($s=0; $s < count($_POST['d_shpd']); $s++){
              $shp_id= $_POST['d_shpd'][$s];
               
		      $SQLdata6 = "DELETE FROM oders_shipping WHERE od_sh_id = '{$shp_id}'";
		      sqldb($SQLdata6);
	        } 
	         
              header("Location:ord_shipping.php");
	          exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit Subsidy product -----;
if (isset($_GET['ed_sub'])&&($_GET['ed_sub'] == "ed_sub")){
            $pro_id= $_REQUEST['pr_d'];
            $subid= $_REQUEST['subda_id'];

          $sql_pro = "SELECT * FROM product_tw WHERE pro_tID = '{$pro_id}'";
          $qu_pro = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
          $totalnum = mysqli_num_rows($qu_pro);
    
          if($totalnum >= 1){
    	    $SQLdata = ("UPDATE product_tw SET subda_id = '{$subid}' WHERE pro_tID = '{$pro_id}'");
            sqldb($SQLdata);
          }
	         
             header("Location:pro_edit.php?pr_d=".$pro_id);
	         exit;
}
/*----------------------------------------------------------------------------*/
//---- Edit meta product -----;
if (isset($_POST['ed_mtp'])&&($_POST['ed_mtp'] == "ed_mtp")){
              $pro_id= $_POST['pr_d'];
              $ed_tit= $_POST['mtp_title'];
              $ed_key= $_POST['mtp_keyw'];
              $ed_des= $_POST['mtp_desc'];
          $sql_mat = "SELECT * FROM meta_product WHERE pro_tID = '{$pro_id}'";
          $qu_mat = mysqli_query($wp_c, $sql_mat) or die(mysqli_error());
          $totalnum = mysqli_num_rows($qu_mat);
    
          if($totalnum < 1){
            $SQLdata = ("INSERT INTO meta_product (pro_tID, mtp_title, mtp_keyw, mtp_desc)VALUES('{$pro_id}', '{$ed_tit}', '{$ed_key}', '{$ed_des}')"); 
          }else{
    	    $SQLdata = ("UPDATE meta_product SET mtp_title = '{$ed_tit}', mtp_keyw = '{$ed_key}', mtp_desc= '{$ed_des}' WHERE pro_tID = '{$pro_id}'");
          }
             sqldb($SQLdata);
	         
             header("Location:meta_edit.php?pr_d=".$pro_id);
	         exit;
}
/*----------------------------------------------------------------------------*/
header("Location:../index.php");
exit;
?>