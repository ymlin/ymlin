<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	$sql = "SELECT * FROM `articles`";
	$sth = $db->prepare($sql);
	$sth->execute(array($id));
	
	$result = $sth->fetchobject();
	
	$article_img = $result->photo_name;
	$article_title = $result->title;
?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
    
    <!-- Add libraries -->
    <script type="text/javascript" src="js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/libs/jquery-ui-1.8.5.min.js"></script>
    <script type="text/javascript" src="js/libs/jquery.mobile-1.3.2.min.js"></script>
    
    <!-- Add objects classes -->
    <script src="js/Rect.js" type="text/javascript"></script>
    
    <!-- Add styles -->
    <link href="css/wheelPicker.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript">
        // Hide jQuery Mobile loading message
        $.mobile.loadingMessage = false;
    </script>
</head>

<body style="background-color: transparent; height:100%; margin:0;">

    <?php
		include 'lunar.class.php';
		
		$lunar = new Lunar();
		
		$day = array();
		$day[0] = $lunar->convertSolarToLunar(date('Y',strtotime("-13 days")), date('m',strtotime("-13 days")), date('d',strtotime("-13 days")));
		$day[1] = $lunar->convertSolarToLunar(date('Y',strtotime("-12 days")), date('m',strtotime("-12 days")), date('d',strtotime("-12 days")));
		$day[2] = $lunar->convertSolarToLunar(date('Y',strtotime("-11 days")), date('m',strtotime("-11 days")), date('d',strtotime("-11 days")));
		$day[3] = $lunar->convertSolarToLunar(date('Y',strtotime("-10 days")), date('m',strtotime("-10 days")), date('d',strtotime("-10 days")));
		$day[4] = $lunar->convertSolarToLunar(date('Y',strtotime("-9 days")), date('m',strtotime("-9 days")), date('d',strtotime("-9 days")));
		$day[5] = $lunar->convertSolarToLunar(date('Y',strtotime("-8 days")), date('m',strtotime("-8 days")), date('d',strtotime("-8 days")));
		$day[6] = $lunar->convertSolarToLunar(date('Y',strtotime("-7 days")), date('m',strtotime("-7 days")), date('d',strtotime("-7 days")));
		$day[7] = $lunar->convertSolarToLunar(date('Y',strtotime("-6 days")), date('m',strtotime("-6 days")), date('d',strtotime("-6 days")));
		$day[8] = $lunar->convertSolarToLunar(date('Y',strtotime("-5 days")), date('m',strtotime("-5 days")), date('d',strtotime("-5 days")));
		$day[9] = $lunar->convertSolarToLunar(date('Y',strtotime("-4 days")), date('m',strtotime("-4 days")), date('d',strtotime("-4 days")));
		$day[10] = $lunar->convertSolarToLunar(date('Y',strtotime("-3 days")), date('m',strtotime("-3 days")), date('d',strtotime("-3 days")));
		$day[11] = $lunar->convertSolarToLunar(date('Y',strtotime("-2 days")), date('m',strtotime("-2 days")), date('d',strtotime("-2 days")));
		$day[12] = $lunar->convertSolarToLunar(date('Y',strtotime("-1 days")), date('m',strtotime("-1 days")), date('d',strtotime("-1 days")));
		$day[13] = $lunar->convertSolarToLunar(date('Y'), date('m'), date('d'));
		$day[14] = $lunar->convertSolarToLunar(date('Y',strtotime("+1 days")), date('m',strtotime("+1 days")), date('d',strtotime("+1 days")));
		$day[15] = $lunar->convertSolarToLunar(date('Y',strtotime("+2 days")), date('m',strtotime("+2 days")), date('d',strtotime("+2 days")));
		$day[16] = $lunar->convertSolarToLunar(date('Y',strtotime("+3 days")), date('m',strtotime("+3 days")), date('d',strtotime("+3 days")));
		$day[17] = $lunar->convertSolarToLunar(date('Y',strtotime("+4 days")), date('m',strtotime("+4 days")), date('d',strtotime("+4 days")));
		$day[18] = $lunar->convertSolarToLunar(date('Y',strtotime("+5 days")), date('m',strtotime("+5 days")), date('d',strtotime("+5 days")));
		$day[19] = $lunar->convertSolarToLunar(date('Y',strtotime("+6 days")), date('m',strtotime("+6 days")), date('d',strtotime("+6 days")));
		$day[20] = $lunar->convertSolarToLunar(date('Y',strtotime("+7 days")), date('m',strtotime("+7 days")), date('d',strtotime("+7 days")));
		$day[21] = $lunar->convertSolarToLunar(date('Y',strtotime("+8 days")), date('m',strtotime("+8 days")), date('d',strtotime("+8 days")));
		$day[22] = $lunar->convertSolarToLunar(date('Y',strtotime("+9 days")), date('m',strtotime("+9 days")), date('d',strtotime("+9 days")));
		$day[23] = $lunar->convertSolarToLunar(date('Y',strtotime("+10 days")), date('m',strtotime("+10 days")), date('d',strtotime("+10 days")));
		$day[24] = $lunar->convertSolarToLunar(date('Y',strtotime("+11 days")), date('m',strtotime("+11 days")), date('d',strtotime("+11 days")));
		$day[25] = $lunar->convertSolarToLunar(date('Y',strtotime("+12 days")), date('m',strtotime("+12 days")), date('d',strtotime("+12 days")));
		$day[26] = $lunar->convertSolarToLunar(date('Y',strtotime("+13 days")), date('m',strtotime("+13 days")), date('d',strtotime("+13 days")));
		$day[27] = $lunar->convertSolarToLunar(date('Y',strtotime("+14 days")), date('m',strtotime("+14 days")), date('d',strtotime("+14 days")));
		$day[28] = $lunar->convertSolarToLunar(date('Y',strtotime("+15 days")), date('m',strtotime("+15 days")), date('d',strtotime("+15 days")));
		$day[29] = $lunar->convertSolarToLunar(date('Y',strtotime("+16 days")), date('m',strtotime("+16 days")), date('d',strtotime("+16 days")));
	?>
	
    <script type="text/javascript">
	var lunar_days = <? echo json_encode($day); ?>;
	var js_img = "<?php echo $article_img; ?>";
	var js_title = "<?php echo $article_title; ?>";	

	//Day of week
	var weekday = new Array(7);
	weekday[0] = "星期日";
	weekday[1] = "星期一";
	weekday[2] = "星期二";
	weekday[3] = "星期三";
	weekday[4] = "星期四";
	weekday[5] = "星期五";
	weekday[6] = "星期六";
	
    
    // Grab screen size
    var scrHeight = $(document).height();
    var scrWidth = $(document).width();
	
    
    // Define pickers sizes
    var wheelX = 0;
    var wheelY = 0;
    var wheelWidth = scrWidth;
    var wheelHeight = scrHeight;
    
    var wheelRect = new Rect(wheelX, wheelY, wheelWidth, wheelHeight);
            
	var label = "";
    var finiteDefVal = 10;
	var cval = 10;		
			
    // Define elements
    var wheelElements = new Array();
	var currentdate = new Date();
	var date, i;
	for(i = 0 ; i < 30 ; i++)
	{
		wheelElements[i] = i;
	}
	
    var context = $(document.body);

    
    
    // Create finite wheel
    var wheelPicker = new WheelPicker('wheelPicker', wheelRect, wheelElements, finiteDefVal, false, false, label, context);
    wheelPicker.add();
    wheelPicker.setOnChangeListener(function(wheel) {
		var nval = wheel.getValue();
		
		if(cval != nval)
		{
			$("#wheel" + (nval+3)).css('height',wheelHeight/3);
			$("#wheel" + (cval+3)).css('height',wheelHeight/12);
			
			var normalHtml, highlightHtml;
			
			var cdate = new Date();
			var ndate = new Date();
			var ccdate = new Date();
			var nndate = new Date();
			cdate.setDate( cdate.getDate() + (cval - finiteDefVal) );
			ndate.setDate( ndate.getDate() + (nval - finiteDefVal) );
			ccdate.setDate( ccdate.getDate() + (cval - finiteDefVal-1) );
			nndate.setDate( nndate.getDate() + (nval - finiteDefVal-1) );
			
			var cmonth = (cdate.getMonth()+1) < 10? ('0'+(cdate.getMonth()+1)) : (''+(cdate.getMonth()+1));
			var cday = cdate.getDate() < 10? ('0'+cdate.getDate()) : (''+cdate.getDate());
			var nmonth = (ndate.getMonth()+1) < 10? ('0'+(ndate.getMonth()+1)) : (''+(ndate.getMonth()+1));
			var nday = ndate.getDate() < 10? ('0'+ndate.getDate()) : (''+ndate.getDate());
			var ccmonth = (ccdate.getMonth()+1) < 10? ('0'+(ccdate.getMonth()+1)) : (''+(ccdate.getMonth()+1));
			var ccday = ccdate.getDate() < 10? ('0'+ccdate.getDate()) : (''+ccdate.getDate());
			var nnmonth = (nndate.getMonth()+1) < 10? ('0'+(nndate.getMonth()+1)) : (''+(nndate.getMonth()+1));
			var nnday = nndate.getDate() < 10? ('0'+nndate.getDate()) : (''+nndate.getDate());
			
			highlightHtml = '<div style = "height: 100%; width:100%; background-image:url(\'../mickey/home_content_lrbg.png\'); background-size:cover; background-position: center center;">'
			highlightHtml += '<div style = "border-radius: 5%; height: 100%; width:93%; margin: 0px auto; position:relative; left:0px; top:0px; z-index:-2;  background-image:url(\'../mickey/home_content_bottombg.png\'); background-size: cover; background-position: center center;">';
			highlightHtml += '<div style = "height:5%;"></div>';
			highlightHtml += '<table style = "width: 100%; height:20%;"><tr>';
			highlightHtml += '<td style = "width: 7.5%;"></td>';
			highlightHtml += '<td style = "width: 23.5%;"><div style = "font-size: 150%; color:red;" >' + nmonth + "/" + nday + '</div></td>';
			highlightHtml += '<td style = "width: 40%;"><div style = "color:red;">農曆' + lunar_days[nval+3][1] + lunar_days[nval+3][2] + '</div></td>';
			highlightHtml += '<td style = "width: 29%;"><div style = "font-size: 140%; color:red;">' + weekday[ndate.getDay()] + '</div></td>';
			highlightHtml += '</tr></table>';
			highlightHtml += '<div style = "height:75%; text-align:center; position:relative; left:0px; top:0px; z-index:0;" >';
			highlightHtml += '<table style = "width: 90%; height:90%; margin: 0 auto;  background-image:url(\'../mickey/home_content_bg.png\'); background-size:cover; background-position: center center;"><tr>';
			highlightHtml += '<td style = "width: 1%;"></td>';
			highlightHtml += '<td style = "width: 38%;"><div style = "height:90%; width: 100%; position:relative; left:0px; top:0px; z-index:-1;   background:url(../mickey/' + js_img + ') center center no-repeat; background-size:contain; "></div>';
			highlightHtml += '<td>';
			highlightHtml += '<td style = "width: 61%;"></td>';
			highlightHtml += '</tr></table>';
			highlightHtml += '</div></div></div>';
			
			normalHtml = '<table style = "width: 100%; position: relative; top: 10%;"><tr>';
			normalHtml += '<td style = "width: 10.7%;"></td>';
			normalHtml += '<td style = "width: 21.8%;"><div style = "height:100%; font-size: 150%; color:black;" >' + cmonth + "/" + cday + '</div></td>';
			normalHtml += '<td style = "width: 37%;"><div style = "color:black;">農曆' + lunar_days[cval+3][1] + lunar_days[cval+3][2] + '</div></td>';
			normalHtml += '<td style = "width: 30.5%;"><div style = "font-size: 140%; color:black;">' + weekday[cdate.getDay()] + '</div></td>';
			normalHtml += '</tr></table>';
			normalHtml += '<hr style = "color: #fff; background-color: #fff; height: 1px; width:85%; border: 0;"/>';

			cnormalHtml = '<table style = "width: 100%; position: relative; top: 10%;"><tr>';
			cnormalHtml += '<td style = "width: 10.7%;"></td>';
			cnormalHtml += '<td style = "width: 21.8%;"><div style = "height:100%; font-size: 150%; color:black;" >' + ccmonth + "/" + ccday + '</div></td>';
			cnormalHtml += '<td style = "width: 37%;"><div style = "color:black;">農曆' + lunar_days[cval+2][1] + lunar_days[cval+2][2] + '</div></td>';
			cnormalHtml += '<td style = "width: 30.5%;"><div style = "font-size: 140%; color:black;">' + weekday[ccdate.getDay()] + '</div></td>';
			cnormalHtml += '</tr></table>';
			cnormalHtml += '<hr style = "color: #fff; background-color: #fff; height: 1px; width:85%; border: 0;"/>';
			
			newnormalHtml = '<table style = "width: 100%; position: relative; top: 10%;"><tr>';
			newnormalHtml += '<td style = "width: 10.7%;"></td>';
			newnormalHtml += '<td style = "width: 21.8%;"><div style = "height:100%; font-size: 150%; color:black;" >' + nnmonth + "/" + nnday + '</div></td>';
			newnormalHtml += '<td style = "width: 37%;"><div style = "color:black;">農曆' + lunar_days[nval+2][1] + lunar_days[nval+2][2] + '</div></td>';
			newnormalHtml += '<td style = "width: 30.5%;"><div style = "font-size: 140%; color:black;">' + weekday[nndate.getDay()] + '</div></td>';
			newnormalHtml += '</tr></table>';
			
			
			document.getElementById("wheel" + (cval+3)).innerHTML = normalHtml;
			document.getElementById("wheel" + (cval+2)).innerHTML = cnormalHtml;
			document.getElementById("wheel" + (nval+3)).innerHTML = highlightHtml;
			document.getElementById("wheel" + (nval+2)).innerHTML = newnormalHtml;
			
			cval = nval;
		}
    });
	
    function WheelPicker(id, size, elements, defPos, isInfinite, isReadOnly, label) {
    
    // Constants
    var SPEED = 0.3;
    var MAX_SCROLL_TIME = 1500;
    var Y_EASE_OUT_RATIO = 0.3;
    var TIME_EASE_OUT_RATIO = 0.3;
    var TOL_POS = 0.4;
    var POS_TOLERANCE = 0.02;
    var MAX_TIME_ACCELERATE = 200;
    var INFINITE_MIN_SEGMENTS= 3;
    var INFINITE_MIN_ELEMENTS = 30;
    
    var min = 0;
    var max = 0;
    
    // Public properties
    this.id = id;
    this.size = size;
    this.elements = elements;
    this.defPos = defPos;
    this.isInfinite = isInfinite;
    this.isReadOnly = isReadOnly;
    this.label = label;
    this.context = $(document.body);;
    
    this.segments = INFINITE_MIN_SEGMENTS;
    
    this.onChange = function(){};
    
    var elementHeight = 0;
    var min = 0;
    var max = 0;
    
    this.setDefault = function() {
        
        this.id = "wheelPicker"
        this.size = new Rect(0, 0, 10, 10)
        this.elements = new Array();
        this.defPos = "";
        this.isInfinite = false;
        this.isReadOnly = false;
        this.context = $(document.body);
        
    }
    
    this.add = function() {
        
        // Get number of segments in infinite case
        if(this.isInfinite) {
            
            this.segments = Math.ceil( INFINITE_MIN_ELEMENTS / this.elements.length );
            
            if(this.segments < INFINITE_MIN_SEGMENTS) {
                this.segments = INFINITE_MIN_SEGMENTS
            }
        }
        
        var imgHtml = this.getHtml();
        $(this.context).append( imgHtml );
        
        // Generate global variables
        this.context = $('#' + this.id);
        
        max =  ((1 + TOL_POS) * elementHeight);
        min = - elementHeight * (this.elements.length - (2 - TOL_POS));
        
        this.scrollTo(this.defPos, false);
        
        this.bindEvents();
        //sthis.init();
        
    }
    
    this.bindEvents = function() {
        
        var wheel = this;
        $( "#" + wheel.id ).off('vmousedown', '.dwwo' ).on('vmousedown', '.dwwo' ,function(e) {
            console.log('Mouse down');
            wheel.mouseDown(wheel, e);
        });
        
    }
    
    this.mouseDown = function(wheel, e){
       
        if(!wheel.isReadOnly) {
            wheel.currentTop = $('ul', this.context).css('top').replace(/[^-\d\.]/g, '');
            
            var date = new Date();
            wheel.initTime = date.getTime(); 
                    
            wheel.initY = e.pageY;
            //this.initY = e.pageY;
            
            // Stop all animations
            $('ul', wheel.context).stop();
            
            $( document.body ).off('vmousemove' ).on('vmousemove' ,function(e) {
                wheel.mouseMove(wheel, e);
            });

            $( document.body ).off('vmouseup' ).on('vmouseup' ,function(e) {
                wheel.mouseUp(wheel, e);
            });
            
        }
       
       
        
    }
    
    this.mouseMove = function(wheel, e){
        
        wheel.currentTop = $('ul', this.context).css('top').replace(/[^-\d\.]/g, '');
        var top =  parseFloat(-(wheel.initY - e.pageY)) + parseFloat(wheel.currentTop);
        wheel.initY = e.pageY;
        
        // Check boundries
        if(!this.isInfinite) {
            if(top > max) {
                top = max;
            }
            if(top < min) {
                top = min;
            }
            
        } else {
            
            wheel.moveToMiddleSegment(top, wheel);
            return;
        }
        
        // Stop all animations
        $('ul', wheel.context).stop();
        $('ul', this.context).css('top', top);
        
    }
    
    this.mouseUp = function(wheel, e){
        
        $( document.body ).off( 'vmouseup' );
        $( document.body ).off( 'vmousemove' );
        
        var date = new Date();
        var timeDiff =  date.getTime() - wheel.initTime; 
        
        var pos = this.getPos();
        
        var lengthMoved = parseFloat(-(wheel.initY - e.pageY));
        
        var addditionalPos = 0;
        
        if(timeDiff < MAX_TIME_ACCELERATE) {
            addditionalPos = -Math.round( (2*lengthMoved)/timeDiff );
        }
        
        this.scrollToNearest(addditionalPos);
        
    }
    
    this.scrollTo = function(pos, animate) {
        
        if(animate) {
            
            if(this.isInfinite) {
                // Set always in the middle segment
                this.animWheelTo(this.elements.length + pos);
            } else {
                this.animWheelTo(pos);
            }
            
        } else {
            // Get current position
            this.setWheelTo(pos);
        }
        
    }
    
    this.scrollBy = function(nmb, animate) {
        
        var wheel = this;
        
        var currentPos = this.getPos();
        var newPos = currentPos + nmb;
        
        if(this.isInfinite) {
           
           //Reduce new position to standard size
           var segmentsToPass = Math.abs( Math.floor(newPos / this.elements.length));
           newPos = Math.abs(newPos);
           newPos -= segmentsToPass * this.elements.length;
           
           
           if(animate) {
                wheel.currentTop = $('ul', wheel.context).css('top').replace(/[^-\d\.]/g, '');
                this.animWheelInfTo(newPos, segmentsToPass, wheel);
           } else {
               this.setWheelTo(newPos);
           }
        
        } else {
            
           if(newPos < 0) {
               newPos = 0;
           } 
           if(newPos > (this.elements.length -1) ) {
               newPos = this.elements.length -1;
           } 
            
           if(animate) {
               this.animWheelTo(newPos);
           } else {
               this.setWheelTo(newPos);
           }
            
        }
        
    }
    
    this.moveToMiddleSegment = function(top, wheel) {
        
        top = parseFloat(top);
        
        // Put user back to center segment while scrolling manually
        var middleSegment = Math.floor(wheel.segments / 2);

        var maxCenter = elementHeight - (middleSegment * wheel.elements.length * elementHeight);
        var minCenter = - elementHeight * (wheel.elements.length - 2) - (middleSegment * wheel.elements.length * elementHeight);

        while(top > maxCenter) {
            top -=  (wheel.elements.length  ) * elementHeight; 
            wheel.currentTop = top;
        }
        while(top < minCenter) {
            top += (wheel.elements.length  ) * elementHeight;
            wheel.currentTop = top;
        }

        console.log("Top set: " + minCenter + ", "+ top + ", " + maxCenter);
        
        // Stop all animations
        $('ul', wheel.context).stop();
        $('ul', wheel.context).css('top', top);
        
    }
    
    /** This widget supports only rotating downwards using fixed number of scrolled elements
     * TODO: Optimize scrolling so it won't generate so many elements, but can reuse it instead + add rotation in oposite direction
     * @param {type} posAbs
     * @param {type} segmentsToPass
     * @param {type} wheel
     * @returns {undefined}
     */
    this.animWheelInfTo = function(posAbs, segmentsToPass, wheel) {
        
        segmentsToPass = Math.abs(segmentsToPass);
        //console.log('Segments to pass: ' + segmentsToPass + ",  posAbs:" + posAbs);
        
        var currentY = wheel.currentTop;
        
        // Move to middle segment
        wheel.moveToMiddleSegment(currentY, wheel);
        

        if(segmentsToPass > 0) {

            // Add all lacking segments and then rotate wheel
            var wheels = "";
            
            // Set final value
            posAbs += this.elements.length * segmentsToPass;
            posAbs = Math.abs(posAbs);
            
            while(segmentsToPass > 0) {
                wheels += this.generateWheelItems('tmpEntries');
                segmentsToPass--;
            }
            
            //Add new wheels
            $('ul', this.context).append( wheels );
            
            wheel.animWheelTo(posAbs, function() {
                // Clean all temporary created elements
               $('.tmpEntries').remove();
            });

            //Rotate wheel by all numbers
        } else {
            // Go to standart position 
            posAbs = Math.abs(posAbs);
            wheel.animWheelTo(posAbs);
        }
            
        
        
        
    }
    
    this.animWheelTo = function(posAbs, onFinish) {
        
        var y = - ((posAbs-1) * elementHeight);
        var scrollToNearest = false;
		
		defPos = y;
        
        if(!this.isInfinite) {
            if(y > max) {
                y = max;
                scrollToNearest = true;
            }
            if(y < min) {
                y = min;
                scrollToNearest = true;
            }
        }
        
        var lengthToMove = y - $('ul', this.context).css('top').replace(/[^-\d\.]/g, '');
        
        var yEaseOut = lengthToMove * Y_EASE_OUT_RATIO;
        var time = Math.abs(lengthToMove / SPEED);
        
        if(time > MAX_SCROLL_TIME) {
            time = MAX_SCROLL_TIME;
        }
        
        var timeEase = time * TIME_EASE_OUT_RATIO;
        
        var wheel  = this;
        
        $('ul', wheel.context).stop().animate({ top : (y - yEaseOut) }, timeEase, 'easeInCirc', function () {

            //animate main body of the animation
            $('ul', wheel.context).animate({ top : y  }, time - timeEase, 'linear', function () {

                //animate the last easing
                $('ul', wheel.context).animate({ top :(y) }, timeEase, 'easeOutCirc', function() {
                    
                    if(scrollToNearest) {
                        wheel.scrollToNearest();
                    } else {
                        wheel.onChange(wheel);
                        
                        if(onFinish != null) {
                            onFinish();
                        }
                        
                        if(wheel.isInfinite) {
                            wheel.moveToMiddleSegment(y, wheel);
                        }
                    }
                });
            });
        });
        
        
    }
    
    this.setWheelTo = function(pos) {
        
        // Always set to center segment
        if(this.isInfinite) {
            var middleSegment = Math.floor(this.segments / 2);
            var pos = (middleSegment * this.elements.length ) + pos;
        }
        
        var y = - ((pos-1) * elementHeight);
		
		defPos = y;
        
        $('ul', this.context).css('top', y);
        
    }
    
    
    
/*
function Scroller(elm, settings) {
        var this = this,
            e = elm,
            elm = $(e),
            theme,
            lang,
            s = $.extend({}, defaults),
            m,
            dw,
            warr = [],
            iv = {},
            input = elm.is('input'),
            visible = false;
*/
        // Private functions


        this.generateWheelItems = function(className) {
            
            var html = '';
            
            console.log(this.size.height + "/" + this.elements);
            
            // Get single item height
            elementHeight = this.size.height / 12;
        
            var segments = 1;
            if(this.isInfinite) {
                segments = this.segments;
            }
            
            if(className == null) {
                className = '';
            } else {
                className = ' ' + className;
            }
            
            while(segments > 0) {
                
                for (var j in this.elements) {
					var date = new Date();
					date.setDate( date.getDate() + (j - defPos - 3) );
				
					var month = (date.getMonth()+1) < 10? ('0'+(date.getMonth()+1)) : (''+(date.getMonth()+1));
					var day = date.getDate() < 10? ('0'+date.getDate()) : (''+date.getDate());
					
					if(j == defPos+3)
					{
						html += '<div ><div id = "wheel' + j + '" style = "height:' + elementHeight*4 + 'px; width:' + this.size.width + 'px;  ">';
						html += '<div style = "height: 100%; width:100%; position:relative; left:0px; top:0px; z-index:-2; background-image:url(\'../mickey/home_content_lrbg.png\'); background-size:cover; background-position: center center;">'
						html += '<div style = "border-radius: 5%; height: 100%; width:93%; margin: 0px auto; background-image:url(\'../mickey/home_content_bottombg.png\'); background-size: cover; background-position: center center;">';
						html += '<div style = "height:5%;"></div>';
						html += '<table style = "width: 100%; height:20%;"><tr>';
						html += '<td style = "width: 7.5%;"></td>';
						html += '<td style = "width: 23.5%;"><div style = "font-size: 150%; color:red;" >' + month + "/" + day + '</div></td>';
						html += '<td style = "width: 40%;"><div style = "color:red;">農曆' + lunar_days[(j - defPos + 10)][1] + lunar_days[(j - defPos + 10)][2] + '</div></td>';
						html += '<td style = "width: 29%;"><div style = "font-size: 140%; color:red;">' + weekday[date.getDay()] + '</div></td>';
						html += '</tr></table>';
						html += '<div style = "height:75%; text-align:center; position:relative; left:0px; top:0px; z-index:0;" >';
						html += '<table style = "width: 90%; height:90%; margin: 0 auto;  background-image:url(\'../mickey/home_content_bg.png\'); background-size:cover; background-position: center center;"><tr>';
						html += '<td style = "width: 1%;"></td>';
						html += '<td style = "width: 38%;"><div style=" height:90%; width: 100%; position:relative; left:0px; top:0px; z-index:-1;  background:url(../mickey/' + js_img + ') center center no-repeat; background-size:contain; "></div>';
						html += '<td>';
						html += '<td style = "width: 61%;"></td>';
						html += '</tr></table>';
						html += '</div></div></div>';
						html += '</div></div>';
					}
					else
					{
						html += '<div ><div id = "wheel' + j + '" style = "height:' + elementHeight + 'px;  width:' + this.size.width + 'px;">'
						html += '<table style = "width: 100%; position: relative; top: 10%;"><tr>';
						html += '<td style = "width: 10.7%;"></td>';
						html += '<td style = "width: 21.8%;"><div style = "height:100%; font-size: 150%; color:black;" >' + month + "/" + day + '</div></td>';
						html += '<td style = "width: 37%;"><div style = "color:black;">農曆' + lunar_days[(j - defPos + 10)][1] + lunar_days[(j - defPos + 10)][2] + '</div></td>';
						html += '<td style = "width: 30.5%;"><div style = "font-size: 140%; color:black;">' + weekday[date.getDay()] + '</div></td>';
						html += '</tr></table>';
						if(j != defPos+2) html += '<hr style = "color: #fff; background-color: #fff; height: 1px; width:85%; border: 0;"/>';
						html += '</div></div>';
					}
                }
                
                segments--;
            }
            
            return html;
        }

       
        /**
        * Enables the scroller and the associated input.
        */
        this.enable = function () {
            this.isReadOnly = false;
        }

        /**
        * Disables the scroller and the associated input.
        */
        this.disable = function () {
            this.isReadOnly = true;
        }

        this.setOnChangeListener = function(onChange) {
            
            this.onChange = onChange;
            
        }



        /**
        * Hides the scroller instance.
        */
        this.hide = function (prevAnim) {
            
            $(this.id).hide();
            
        }

        this.scrollToNearest = function (additionalPos) {
            var y = $('ul', this.context).css('top').replace(/[^-\d\.]/g, '');;
            
            if(isNaN(y)) {
                this.animWheelTo(this.defPos);
                return;
            }
            
            if(additionalPos == null) {
                additionalPos = 0;
            }

            // Find if wheel needs to be set 
            
            var valueModulus = y % elementHeight;
            //if(valueModulus > POS_TOLERANCE) {
                var nearestPos = this.getPos() + additionalPos;
                
                this.animWheelTo(nearestPos);
            //}
        }
        
        this.getPos = function() {
            
            var y = $('ul', this.context ).css('top').replace(/[^-\d\.]/g, '');
            
            if(isNaN(y)) {
                return this.defPos;
            }
            
            // Check boundries
            if(!this.isInfinite) {

                var maxPos = elementHeight;
                var minPos = -elementHeight * (this.elements.length-2 );
                
                if(y < minPos) {
                    y = minPos;
                }
                if(y > maxPos) {
                    y = maxPos;
                }

            }
            
            var value = -Math.round( y / elementHeight );
            value += 1;
            console.log('Pos:' + value);
			
            return value;
        }
        
        this.getValue = function() {
            
            var pos = this.getPos();
            
            if(isInfinite) {
                
                // Include other segments
                var otherSegments = Math.floor( pos / this.elements.length );
                pos -= otherSegments * this.elements.length;
                
            }
            
            var value = this.elements[pos];
            
            return value;
            
        }
        
        this.getHtml = function() {
            
            // Create wheels containers
            var theme = 'dw';
            var wheelNr = 0;
            var html = '<div id ="' + this.id + '"  style="position:absolute; top:' + this.size.y + 'px; left:' + this.size.x + 'px;">';

            html += '<table cellpadding="0" cellspacing="0"><tr>';
            html += '<td>'  + '<div class="dww dwrc" style="height:' + this.size.height + 'px;width:' + this.size.width + 'px;"><ul>';
            
            // Create wheels
                
            // Create wheel values
            html += this.generateWheelItems();
                
            html += '</ul><div class="dwwo"></div></div></td>';
            html += '</tr></table></div>';

            
            return html;
            
        }

      
        /**
        * Scroller initialization.
        */
        this.init = function () {

            //m = Math.floor(s.rows / 2);

            $(document.body).unbind('.dw');

            if ($(document.body).data('dwro') !== undefined)
                this.readOnly = bool(elm.data('dwro'));

            //if (visible)
            //    this.hide();

            this.show();
            
        }

        this.val = null;

        //this.init(settings);
    }

    function testProps(props) {
        for (var i in props)
            if (mod[props[i]] !== undefined)
                return true;
        return false;
    }

    function testPrefix() {
        var prefixes = ['Webkit', 'Moz', 'O', 'ms'];
        for (var p in prefixes) {
            if (testProps([prefixes[p] + 'Transform']))
                return '-' + prefixes[p].toLowerCase();
        }
        return '';
    }

    function getInst(e) {
        return scrollers[e.id];
    }

    function getY(e) {
        return e.changedTouches || (e.originalEvent && e.originalEvent.changedTouches) ? (e.originalEvent ? e.originalEvent.changedTouches[0].pageY : e.changedTouches[0].pageY) : e.pageY;

    }

    function bool(v) {
        return (v === true || v == 'true');
    }

    function calc(t, val, dir, anim, orig) {
        val = val > max ? max : val;
        val = val < min ? min : val;

        var cell = $('li', t).eq(val);

        // Set selected scroller value
        this.elements[index] = cell.attr('data-val');

        // Validate
        this.validate(anim ? (val == orig ? 0.1 : Math.abs((val - orig) * 0.1)) : 0, orig, index, dir);
    }
</script>


    
</body>

</html>

