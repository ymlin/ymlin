<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 
?>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>

	<body style = 'background-color: transparent; height:98vh;'>
	<div  class="col-md-4" style="width:100%; background-color: transparent;" >
		<script>
			var currentonclick = 0;
		
			function change(index) {
				document.getElementById('day'+index).style.backgroundImage="url('../mickey/calelem_bg.png')";
				
				if(currentonclick != 0 && currentonclick != index) document.getElementById('day'+currentonclick).style.backgroundImage="url('../mickey/no_bg.png')";
				currentonclick = index;
			}
		</script>
 
		<table style="width:100%;">
		<tr style="width:100%;">
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:#aa0000; text-align:center; position: relative; top: 15%; font-size:1.5em;">日</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; top: 15%; font-size:1.5em;">一</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; top: 15%; font-size:1.5em;">二</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; top: 15%; font-size:1.5em;">三</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; top: 15%; font-size:1.5em;">四</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; top: 15%; font-size:1.5em;">五</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:#aa0000; text-align:center; position: relative; top: 15%; font-size:1.5em;">六</div></td>
		</tr>
		<tr style="width:100%;">
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:#aa0000; text-align:center; position: relative;  font-size:1.5em;">Su</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; font-size:1.5em;">Mo</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; font-size:1.5em;">Tu</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; font-size:1.5em;">We</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; font-size:1.5em;">Th</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:white; text-align:center; position: relative; font-size:1.5em;">Fr</div></td>
			<td style="width:14.3%; height:2.5em; margin: 0px auto;"><div style = " color:#aa0000; text-align:center; position: relative; font-size:1.5em;">Sa</div></td>
		</tr>
		<?php
			$year = $_GET['year'];
			$month = $_GET['month'];
			
			$cday = date("j");
			$cmonth = date("m");
			$cyear = date("Y");
			
			$dayinmonth = date("t", strtotime($year."/".$month."/1"));
			$dayofweek = date("w", strtotime($year."/".$month."/1"));
			if($dayofweek == 0)
			{
				$tyear = date("Y", strtotime($year."/".$month."/1"));
				$tmonth = date("m", strtotime($year."/".$month."/1"));
				$tday = date("j", strtotime($year."/".$month."/1"));
			}
			else if($dayofweek == 1)
			{
				$tyear = date("Y", strtotime('-'.$dayofweek.' day' , strtotime($year."/".$month."/1")));
				$tmonth = date("m", strtotime('-'.$dayofweek.' day' , strtotime($year."/".$month."/1")));
				$tday = date("j", strtotime('-'.$dayofweek.' day' , strtotime($year."/".$month."/1")));
			}
			else
			{
				$tyear = date("Y", strtotime('-'.$dayofweek.' days' , strtotime($year."/".$month."/1")));
				$tmonth = date("m", strtotime('-'.$dayofweek.' days' , strtotime($year."/".$month."/1")));
				$tday = date("j", strtotime('-'.$dayofweek.' days' , strtotime($year."/".$month."/1")));
			}
			
			if($month == 12)
			{
				$nyear = $year+1;
				$nmonth = 1;
			}
			else
			{
				$nyear = $year;
				$nmonth = $month+1;
			}
			$lastdayofweek = date("w", strtotime($nyear."/".($nmonth)."/1"));
			if($lastdayofweek == 0) $lastdayofweek = 7;
			
			
			for($i = 0 ; $i < $dayinmonth+$dayofweek+7-$lastdayofweek ; $i++)
			{
				if($tmonth == $month) 	$color = "gray";
				else					$color = "#dddddd";
				
				if($tday == $cday && $tmonth == $cmonth && $tyear == $cyear)
				{
					$bgcolor = "gray";
					$color = "#ffffff";
				}
				else
				{
					$bgcolor = "#ffffff";
				}
				
				if($i % 7 == 0) echo '<tr style="width:100%; margin: 0px auto;">';
		?>
		<td style="width:14.3%; height:2.5em; margin: 0px auto;">
			<div id="day<?php echo $i+1;?>" onclick="change(<?php echo $i+1;?>)" style="background: url('../mickey/no_bg.png') center center no-repeat; background-size: contain; background-color:<?php echo $bgcolor;?>; width:100%; height:100%; border-radius: 10%; ">
				<div style = " color:<?php echo $color;?>; text-align:center; position: relative; top: 15%; font-size:1.5em;"><?php echo $tday;?></div>
			</div>
		</td>
		<?php	
				if($i % 7 == 6) echo "</tr>";
				$oyear = $tyear;
				$omonth = $tmonth;
				$oday = $tday;
				$tyear = date("Y", strtotime('+1 day' , strtotime($oyear."/".$omonth."/".$oday)));
				$tmonth = date("m", strtotime('+1 day' , strtotime($oyear."/".$omonth."/".$oday)));
				$tday = date("j", strtotime('+1 day' , strtotime($oyear."/".$omonth."/".$oday)));
			}
		?>
		</table>
	
	</div>	
	</body>
</html>
		
				