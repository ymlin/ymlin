<!DOCTYPE html>
<html lang="en">
<?php
//裁切字串
function cut_content($a,$b){
    $a = strip_tags($a); //去除HTML標籤
    $sub_content = mb_substr($a, 0, $b, 'UTF-8'); //擷取子字串
    echo $sub_content;  //顯示處理後的摘要文字
    //顯示 "......"
   
}

//以上程式已經包裝起來,您可存放在例如:function.php網頁
//往後只要使用include("function.php");
//加上 cut_content($a,$b);即可,不需每次撰寫.
//$a代表欲裁切內容.
//$b代表欲裁切字數(字元數)
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>中華養生活動</title>

    <!-- Bootstrap Core CSS -->
   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php include 'header.php';?>

    <!-- Navigation -->
	 
    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12" style="padding:15px 15px 15px 20px;font-weight:900;font-size:36px;margin-top:20px;margin-right:10px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
			
              活動	
                    
                
            </div>
        </div>
       

      

         <?php
      require_once("dbtools.inc.php");
	  
     
      $sql3 = "SELECT * FROM activity order by s_datetime ASC";
       $result3 = mysqli_query($wp_c,$sql3) or die(mysqli_error('error'));
	 
	   date_default_timezone_set("Asia/Taipei");
			?>
			<div class="row">
			<?php	
			$aa=0;
	while ($row3 = mysqli_fetch_assoc($result3)){
	
		?>
		
										
											<div class="col-md-4 portfolio-item" >
											<div style="margin-top:20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
											<div style="height:260px; ">
                
                    
					<img style=" border-radius: 5px;" class="img-responsive" src="../mickey/<?php echo  $row3["photo_name"] ?>" height="250px"width="100%" alt="" >
               
				</div>
                <h3>
				
				 <div  style="font-weight:600;height:40px;font-size:20px;margin-left:20px">
                    <div class="desktop_1"style="margin-bottom:40px; letter-spacing:3px;line-height:25px" >
				 <?php echo $row3['title'] ?>
					
					</div>
                </h3> 
				 <div style= "color:#696969 ;margin-top:30px;margin-left:20px;margin-bottom:20px;">
						<p>
						<?php $d=$row3['w_datetime'];
						
						echo  $d; ?>
						</p>
				</div>
				<div style="height:120px;font-weight:600;font-size:16px;letter-spacing:3px;line-height:25px;margin-left:20px">
					
					<p><?php cut_content( $row3['comment'],65);  $sub_content = mb_substr($row3['comment'], 0, 65, 'UTF-8');if(strlen($row3['comment']) > strlen($sub_content)) echo '......';?> </p>
														
				</div>
			<div class="col-md-offset-7"style="height:50px;">
				<a style="text-decoration:none;" href="inner_activity.php?id=<?php echo $row3['id'] ?>">
				<div class="readmore"style="font-weight:500;font-size:16px;margin-bottom:20px;padding-top:8px">立即報名➔</div>
				</a>
			</div>
            
			</div>  </div>
										<?php
					 $aa ++; if($aa%3==0){echo '</div><div class="row">';}
					 }?>	
									
									 
									    </div>
       </div>
        <!-- /.row -->

       

      

        <!-- Footer -->
       

    </div>	<?php include '../footer.php';?>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
