<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['remove'])
	{
		$sql = "DELETE FROM `pic_src` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>圖檔管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include __DIR__.'/../admin_top.php';?>
	
	<div style="width:80%; margin: 0px auto;">  
		
	
		<table width="1200" border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="6" id="color">
				<form align="center" action = "admin_pic_edit.php" align="center">
					<h2 align="center">圖檔管理</h2>
					<div align="center">
						<button class='btn btn-primary'>新增圖檔</button>
					</div>
				</form>
			</td> 
			</tr>  

			<tr>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>id</strong></div></td> 
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>英文檔名</strong></div></td> 
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>說明</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>圖片</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>最後修改時間</strong></div></td>
			<td id="color2"><div align="center" style="white-space:nowrap;"><strong>管理</strong></div></td>  			
			</tr>  
			
			<?php
				$sql = "SELECT * FROM `pic_src`";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				while($result = $sth->fetchobject())
				{
					echo "<tr>";
					
				?>
					<form action = "admin_pic_update.php"  method = "GET">
				<?php
						echo "<td align=\"center\">" . $result->id . "</td>";	
						echo "<td align=\"center\">" . $result->eng_name . "</td>";	
			
						echo "<td align=\"center\">" . $result->comment . "</td>";
				?>
						<td align="center"><img src="../mickey/<?php echo $result->photo_name; ?>?=<?php echo $result->photo_name; ?>" alt="" height="42" width="42" /></td>
				<?php
						echo "<td align=\"center\">" . $result->datetime . "</td>";
				?>	
						<td align="center">
							<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
							<button  class='btn btn-primary' formaction="admin_pic.php" type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
						</td>  
					</form>
				<?php
				}
			?>
		</table>  
	</div>  
</body>  
</html>