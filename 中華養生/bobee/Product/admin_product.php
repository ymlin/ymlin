<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT password, admin FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($_GET['remove'])
	{
		$sql = "DELETE FROM `products` " 
			. " WHERE `id` = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
	
	if(isset($_GET['change']))
	{
		$sql = "SELECT top FROM `products`" 
			. " WHERE id = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['change']));
		$result = $sth->fetchobject();
		$top = 1-($result->top);
		
		$sql = "UPDATE `products` SET top = ? " 
			. " WHERE id = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($top, $_GET['change']));
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>產品管理</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include __DIR__.'/../admin_top.php';?>
	
	<script>
		function handleClick(i) {
			window.location.href="admin_product.php?change="+i;
		}
	</script>
	
	<div style="width:80%; margin: 0px auto;">  
		<table width="1200" border="1" class="table table-bordered table-condensed table-striped table-hover">  
			<tr>  
			<td colspan="16" id="color">
				<form align="center" action = "admin_product_edit.php" align="center">
					<h2 align="center">產品管理</h2>
					<div align="center">
						<button class='btn btn-primary'>新增產品</button>
					</div>
				</form>
			</td> 
			</tr>  

			<tr>
			<td id="color2"><div align="center" ><strong>id</strong></div></td> 
			<td id="color2"><div align="center" ><strong>名稱</strong></div></td> 
			<td id="color2"><div align="center" ><strong>分類</strong></div></td>
			<td id="color2"><div align="center" ><strong>介紹</strong></div></td>
			<td id="color2"><div align="center" ><strong>圖片</strong></div></td>
			<td id="color2" colspan="4"><div align="center" ><strong>類型</strong></div></td>  
			<td id="color2"><div align="center" ><strong>置頂</strong></div></td>	
			<td id="color2"><div align="center" ><strong>價錢</strong></div></td>
			<td id="color2"><div align="center" ><strong>點擊率</strong></div></td>
			<td id="color2"><div align="center" ><strong>購買量</strong></div></td>
			<td id="color2"><div align="center" ><strong>上架時間</strong></div></td>
			<td id="color2"><div align="center" ><strong>分享人數</strong></div></td>
			<td id="color2"><div align="center" ><strong>管理</strong></div></td>  			
			</tr>  
			
			<?php
				$sql = "SELECT * FROM `products` ORDER BY top DESC, datetime DESC";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				while($result = $sth->fetchobject())
				{
					echo "<tr>";
					
				?>
					<form action = "admin_product_update.php"  method = "GET">
				<?php
						echo "<td align=\"center\" >" . $result->id . "</td>";	
						echo "<td align=\"center\" >";
						echo mb_substr($result->name,0,5,"UTF-8");
						if(mb_strlen($result->name,"UTF-8") > 5) echo "...";
						echo "</td>";	
						
						echo "<td align=\"center\" >" . $result->category . "</td>";
						
						if(!($_GET['preview'] == $result->id))
						{
				?>
							<td align="center">
								<button formaction="admin_product.php"  class='btn btn-primary' type = "submit" name = "preview" value= "<?php echo  $result->id ?>">預覽</button>  
							</td>
				<?php
						}
						else
						{
				?>
							<td align="center" style="style=word-wrap: break-word; word-break: normal;">
								<button formaction="admin_product.php"  class='btn btn-primary' type = "submit" name = "preview" value= "">收回</button>
							<br>
							<div class="container">
				<?php			
							echo $result->comment . "</div></td>";
						}
				?>
						<td align="center"><img src="../mickey/<?php echo $result->photo_name; ?>?=<?php echo $result->photo_name; ?>" alt="" height="100" width="100" /></td>
				<?php
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->user_type . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->religion . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->season . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->buddha . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . "<input type='checkbox' onclick='handleClick(". $result->id .");' ".($result->top=="1"?"checked":"")." />" . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->price . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->click_rate . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->buy_rate . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->datetime . "</td>";
						echo "<td align=\"center\" style=\"white-space:nowrap;\">" . $result->share . "</td>";
				?>	
						<td align="center">
							<button  class='btn btn-primary' type = "submit" name = "edit" value= "<?php echo  $result->id ?>">修改</button>  
							<button  class='btn btn-primary' formaction="admin_product.php" type = "submit" name = "remove" value= "<?php echo  $result->id ?>"onclick="return confirm('是否確認刪除這筆資料');" >刪除</button>  
						</td>  
					</form>
				<?php
				}
			?>
		</table>  
	</div>  
</body>  
</html>