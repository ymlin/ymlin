<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>登入</title>
		<?php include '../Article/index_head.php';?>
		
	
	</head>
	<body>
	<?php include '../Article/nav.php';?>
	<?php include '../Article/header_top.php';?>
		<?php include '../Article/header.php';?>
<div class="container">

<form class='form-horizontal' action="checkpwd.php" method="post" name="myForm">
    <fieldset>
        <div class="login_bar"><h3>會員登入</h3></div>
	<div class="col-md-12">
	<div class="content_bar">
        <div class="form-group">
            <label class="col-md-2 control-label" for='account'>帳號</label>
            <div class="controls col-md-3">
                <input id='account' name='account' type='text' size="30" placeholder='請輸入帳號' />
                <p class='help-block'>*請輸入您的e-mail，這將會成為您登入的帳號</p>
            </div>
			<div class=" col-md-2" >   
      <a href="register.php">未加入會員?</a>　
	   </div>
        </div>
                
        <div class="form-group">
            <label class="col-md-2 control-label" for='password'>密碼</label>
            <div class="controls col-md-2">
                <input id='password' name='password' type='password' value='' placeholder='請輸入密碼' />
                <p class='help-block'>*請輸入6碼以上的英文或數字</p>
            </div>
			<div class=" col-md-2" >
	   <a href="search_pwd.php">忘記密碼?</a>
	   </div>
        </div>
		
    
    <div class=" form-actions col-md-offset-2" >
      　<button type='submit' class='btn btn-primary' onClick="check_data()">登入</button>
	  　<button type="reset" class='btn btn-primary'>重填</button>
    </div>
</form>

	</div> 
</div>	
 </div>
 <?php include '../footer.php';?>
 </body>


<!-- script references -->
	
	</body>
</html>