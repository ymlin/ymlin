<?php
	require_once(__DIR__.'/../dbtools.inc.php');
	
	function updatedata($name)
	{
		return 1;
	}
?>
		
<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>

	<body style = 'background-color: transparent; height:98vh;'>
	<script>
		function change(index) {
			if(document.getElementById(index).style.backgroundColor == "white")
			{
				document.getElementById(index).style.backgroundColor="gray";
				document.getElementById('inner'+index).style.color="white";
			}
			else
			{
				document.getElementById(index).style.backgroundColor="white";
				document.getElementById('inner'+index).style.color="gray";
			}
		}
		
		function updateprefer()
		{
			window.location.href = "mobile_updateprefer.php?prefer=" + updatedata() + "&id=" + <?php echo $_GET['id'];?>;
		}
	</script>
	<div  class="col-md-4" style="width:100%; background-color: transparent;" >
		<?php
			$sql = "SELECT prefer FROM `users` WHERE id = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['id']));
			$result = $sth->fetchobject();
			$prefers = explode(" ", $result->prefer);
			$prefer_religion = explode(",", $prefers[0]);
			$prefer_buddha = explode(",", $prefers[1]);
			$prefer_season = explode(",", $prefers[2]);
			
			if(!isset($_GET['h'])) $_GET['h'] = 0;
		?>
 
		<table style="width:100%;">
		<tr style="width:100%;">
			<td colspan="4" style="width:14.3%; height:2.5em; margin: 0px auto; "><div style = " color:#aa0000; text-align:center; position: relative; font-size:1.5em;">信仰</div></td>
		</tr>
		
		<?php
			$sql = "SELECT * FROM `religion`";
			$sth = $db->prepare($sql);
			$sth->execute();
		
			$i = 0;
			$j = 0;
			$result = $sth->fetchobject();
			$religiondatas = array();
			for( ; $result = $sth->fetchobject() ; $i++)
			{
				$religiondata = array();
				$religiondata['id'] = $result->id;
				$religiondata['name'] = $result->name;
				$religiondatas[] = $religiondata;
				
				if($prefer_religion[$j] == $result->name)
				{
					$j++;
					$color="gray";
					$innercolor="white";
				}
				else
				{
					$color="white";
					$innercolor="gray";
				}
				
				if($i % 2 == 0) echo '<tr style="width:100%; margin: 0px auto;">';
		?>
		<td style="width:14.3%; height:2.5em; margin: 0px auto;">
			<div id="<?php echo ('r'.($result->id));?>" onclick="change('<?php echo ('r'.($result->id));?>')" style="background-color:<?php echo $color; ?>; width:100%; height:100%; border-radius: 10%; ">
				<div id="<?php echo ('innerr'.($result->id));?>" style = " color:<?php echo $innercolor; ?>; text-align:center; position: relative; top: <?php echo mb_strlen($result->name, 'UTF-8') > 5?"30%":"15%";?>; font-size:<?php echo mb_strlen($result->name, 'UTF-8') > 5?"1em":"1.5em";?>;"><?php echo $result->name;?></div>
			</div>
		</td>
		<?php	
				if($i % 2 == 1) echo "</tr>";
			}
			if($i % 2 != 1) echo "</tr>";
		?>
		<tr style="width:100%;">
			<td colspan="4" style="width:14.3%; height:2.5em; margin: 0px auto; "><div style = " color:#aa0000; text-align:center; position: relative; font-size:1.5em;">神明</div></td>
		</tr>
		<?php		
			$sql = "SELECT * FROM `buddha`";
			$sth = $db->prepare($sql);
			$sth->execute();
		
			$i = 0;
			$j = 0;
			$result = $sth->fetchobject();
			$buddhadatas = array();
			for( ; $result = $sth->fetchobject() ; $i++)
			{
				$buddhadata = array();
				$buddhadata['id'] = $result->id;
				$buddhadata['name'] = $result->name;
				$buddhadatas[] = $buddhadata;
				
				if($prefer_buddha[$j] == $result->name)
				{
					$j++;
					$color="gray";
					$innercolor="white";
				}
				else
				{
					$color="white";
					$innercolor="gray";
				}
				
				if($i % 2 == 0) echo '<tr style="width:100%; margin: 0px auto;">';
		?>
		<td style="width:14.3%; height:2.5em; margin: 0px auto;">
			<div id="<?php echo ('b'.($result->id));?>" onclick="change('<?php echo ('b'.($result->id));?>')" style="background-color:<?php echo $color; ?>; width:100%; height:100%; border-radius: 10%; ">
				<div id="<?php echo ('innerb'.($result->id));?>" style = " color:<?php echo $innercolor; ?>; text-align:center; position: relative; top: <?php echo mb_strlen($result->name, 'UTF-8') > 5?"30%":"15%";?>; font-size:<?php echo mb_strlen($result->name, 'UTF-8') > 5?"1em":"1.5em";?>;"><?php echo $result->name;?></div>
			</div>
		</td>
		<?php
			}
			if($i % 2 != 1) echo "</tr>";
		?>
		<tr style="width:100%;">
			<td colspan="4" style="width:14.3%; height:2.5em; margin: 0px auto; "><div style = " color:#aa0000; text-align:center; position: relative; font-size:1.5em;">節氣</div></td>
		</tr>
		<?php		
			$sql = "SELECT * FROM `season`";
			$sth = $db->prepare($sql);
			$sth->execute();
		
			$i = 0;
			$j = 0;
			$result = $sth->fetchobject();
			$seasondatas = array();
			for( ; $result = $sth->fetchobject() ; $i++)
			{
				$seasondata = array();
				$seasondata['id'] = $result->id;
				$seasondata['name'] = $result->name;
				$seasondatas[] = $seasondata;
				
				if($prefer_season[$j] == $result->name)
				{
					$j++;
					$color="gray";
					$innercolor="white";
				}
				else
				{
					$color="white";
					$innercolor="gray";
				}
				
				if($i % 2 == 0) echo '<tr style="width:100%; margin: 0px auto;">';
		?>
		<td style="width:14.3%; height:2.5em; margin: 0px auto;">
			<div id="<?php echo ('s'.($result->id));?>" onclick="change('<?php echo ('s'.($result->id));?>')" style="background-color:<?php echo $color; ?>; width:100%; height:100%; border-radius: 10%; ">
				<div id="<?php echo ('inners'.($result->id));?>" style = " color:<?php echo $innercolor; ?>; text-align:center; position: relative; top: <?php echo mb_strlen($result->name, 'UTF-8') > 5?"30%":"15%";?>; font-size:<?php echo mb_strlen($result->name, 'UTF-8') > 5?"1em":"1.5em";?>;"><?php echo $result->name;?></div>
			</div>
		</td>
		<?php
			}
			if($i % 2 != 1) echo "</tr>";
		?>
		</table>
		
		<script type="text/javascript" language="javascript">
		function updatedata() {
			var preferdata = "", flag = true;
			var data = new Array();
			data = <?php echo json_encode($religiondatas);?>;
			for(var i = 0; i < data.length ; i++ )
			{
				var id, name;
				id = data[i]['id'];
				name = data[i]['name']; 
				if(document.getElementById('r'+id).style.backgroundColor == "gray")
				{
					if(flag) 				preferdata = name;
					else					preferdata = preferdata+","+name;
					flag = false;
				}
			}
			flag = true;
			data = new Array();
			data = <?php echo json_encode($buddhadatas);?>;
			for(var i = 0; i < data.length ; i++ )
			{
				var id, name;
				id = data[i]['id'];
				name = data[i]['name'];
				if(document.getElementById('b'+id).style.backgroundColor == "gray")
				{
					if(flag) 				preferdata = preferdata+" "+name;
					else					preferdata = preferdata+","+name;
					flag = false;
				}
			}
			flag = true;
			data = new Array();
			data = <?php echo json_encode($seasondatas);?>;
			for(var i = 0; i < data.length ; i++ )
			{
				var id, name;
				id = data[i]['id'];
				name = data[i]['name'];
				if(document.getElementById('s'+id).style.backgroundColor == "gray")
				{
					if(flag) 				preferdata = preferdata+" "+name;
					else					preferdata = preferdata+","+name;
					flag = false;
				}
			}
			
			return preferdata;
		}
	</script>
	</div>	
	</body>
</html>				