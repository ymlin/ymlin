<?php @require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php @require_once("../../functions/fundb.php");?>
<?php @require_once("../../functions.php");?>
<?php @require_once("../functions.php");?>
<?php @require_once("functions_pro.php");?>
<?php @require_once("../header_all.php");?>
  <!-- feature -->
  <div id="features-wrapper">
    <div class="container"><!--內容區 -->
      <article class="row clearfix"><!-- 對齊中間 -->
         <div class="mem_list">
            <div class="col-lg-12 column">
               <div class="col-md-12 top_banner_cont">商品分類檢示</div>
               <div class="col-sm-12 hline">
                  <form class="form-horizontal" action="fundb.php" method="post" name="Form_del" id="Form_del">
                     <div class="col-sm-12 spn1">
                        <button id="de_typ_send" name="de_typ_send" value="del_typ" class="btn">確定刪除</button>
                     </div>
                     <div class="col-md-12">
                        <table class="table table-bordered">
				           <thead>
                              <tr style="background-color:#e8e6e5">
                               <th width="5%">刪除</th>
						       <th width="7%">規格編號</th>
						       <th>商品名稱</th>
                               <th>商品規格</th>
					          </tr>
                           </thead>
                           <tbody>
                               <!-- 商品 category 刪除 -->
                               <tr><?php typeview();?></tr>
                           </tbody>
			            </table>
                     </div>
                  </form>
                </div>
             </div>
          </div> 
       </article>      
    </div><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->