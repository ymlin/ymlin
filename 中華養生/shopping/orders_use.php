<?php @require_once("conn/wp-c.php");?>
<?php @require_once('functions/EDcart.php');?>
<?php @require_once("functions/fundb.php");?>
<?php $dir_s1="1";?><!-- login -->
<?php @require_once("loguser/use_chk.php");?>
<?php 
$cart =& $_SESSION['edCart']; //shopping cart;
if(!is_object($cart)) $cart = new edCart();
if($cart->itemcount == 0){header("Location:index.php");exit;}//Empty cart;
?>
<?php @require_once("functions/funlist.php");?>
<?php @require_once("functions.php");?>
<?php @require_once("header.php");?>
<!-- address get-->
<script src="./js/jquery.min.js"></script>
<script src="./js/jquery_js_ord.js"></script>

<script src="./city/jquery-142.min.js"></script>
<script src="./city/city.js"></script>
<!-- 禁止回上頁的功能 -->
<script> 
	function forward() { 
	    setTimeout("forward()",100); 
	    history.forward(); 
	} 
	if (document.all) 
	    history.forward();  //for ie 
	else     
	    setTimeout("forward()",100);  //for firefox 
	//---禁回此頁End--- 
</script><!-- END -->
 <!-- feature -->
 <div id="features-wrapper">
  <div class="container"><!--內容區 -->
     <article class="ord_list"><!-- 對齊中間 -->
        <div class="row clearfix">
           <div class="col-lg-12 column">
              <form class="form-horizontal" action="orders_chk.php" method="post" name="cdf" id="commentForm">
              <!---- setp 1 ---->
              <div class="col-md-12 hline">
                 <div class="col-sm-6 font3 spn2">Setp 1 <small>購買數量與金額</small></div>
                 <!-- 商品名 -->
                 <div class="col-sm-12 table-responsive">
                       <table class="table">
                          <?php
                               if($cart->itemcount > 0) {
	                           foreach($cart->get_contents() as $item) {
                          ?>
                          <tr>
                            <td><?php echo $item['info'].''.port_pro_type3($item['type']);?></td>
                            <td width="13%"><div class="font5">單價 : NT$ <?php echo number_format($item['price']);?>元</div></td>
                            <td width="10%"><div class="font5">數量 : <?php echo $item['qty'];?> 件</div></td>
                            <td width="10%"><div class="font5">NT$ <?php echo number_format($item['subtotal']);?>元</div></td>
                            <!-- <td width="10%"><div class="font8">運費 : <?php echo $item['freight'];?>元</div></td>-->
                          </tr>
                          <?php }}?> 
                           <!-- ECoupon area -->
                          <tr>
                            <?php sql_subsidy($_SESSION['sub_id']);?> 
                          </tr>
                       </table>
                       <table class="table table-bordered">
                           <!-- bread shopping area -->
                          <?php foreach($cart->get_bread() as $brem) {?>
                          <tr>
                             <td colspan="3">品牌運費: <?php echo sql_brand_cat($brem['brid']);?>  (<?php echo sql_outshpping($brem['proid']);?>-配送)</td>
                             <td width="10%"><div class="font5">NT$ <?php echo odr_shipping_1($brem['total'], $brem['proid']);?>元</div></td>                             
                          </tr>
                          <?php }?>
                       </table>
                          <!-- total area -->
                          <div class="cart_total text-right">合計NT$ : <?php  echo number_format($cart->grandtotal3);?> 元(含運費)</div>
                 </div>
              </div>
              <!---- setp 2 ---->
              <div class="col-md-12 hline cart_inner">
                 <div class="col-sm-12 font3 spn1">Setp 2 <small>請填寫訂購人及收件人</small></div>
                 <div class="col-sm-6">
                    <!-- 訂購姓名-->
                    <div class="col-sm-12 spn1">
                      <div class="col-sm-12 font6 spn1"><u>訂購人資料</u></div>
                      <div class="col-sm-4 text-center font6 spn1"><?php echo $od_us_na;?> <small>先生/小姐</small></div>
                      <!-- 訂購聯絡電話-->
                        <?php oders_use_add('聯絡電話', 'phone', $od_us_id, 'p1');?>
                      <!-- 訂購聯絡地址-->
                        <?php oders_use_add('聯絡地址', 'address', $od_us_id, 'a1');?>
                      <input id="vi_id" name="vi_id" value="<?php echo $od_us_id;?>" type="hidden">
                    </div>
                 </div>
                 <!---- 收件人data -->
                 <div class="col-sm-6">
                    <div class="col-sm-12 font6 spn1"><u>收件人資料</u></div>
                    <label class="radio-inline">
                       <div class="col-sm-12 spn1">
                          <input id="anre" name="anre" value="anre" type="checkbox"> 同訂購人
                       </div>
                    </label>
                    <div id="or">
                      <!-- 收件人姓名-->
                        <?php oders_use_add('收件姓名', 'name', -1, 'n2');?>
                      <!-- 收件人聯絡電話-->
                        <?php oders_use_add('聯絡電話', 'phone', -1, 'p2');?>
                      <!-- 收件人聯絡地址-->
                        <?php oders_use_add('聯絡地址', 'address', -1, 'a2');?>
                      <!-- 備註 area -->
                    </div>
                    <div class="col-sm-12 font6 spn1">
                      <label class="col-sm-3 control-label" for="vi-text">備註</label>
                      <div class="col-sm-9">                     
                        <textarea id="vi-text" name="vi-text" rows="3"> </textarea>
                      </div>
                    </div>
                 </div>
              </div>
              <!---- setp 3 ---->
              <div class="col-md-12 hline">
                 <div class="col-sm-12 font3 spn1">Setp 3 <small>請選擇付款方式,配送方式</small></div>
                 <!-- 付款方式 -->
                 <div class="form-group font7">
                    <div class="col-md-2 control-label"><strong>付款方式 :</strong></div>
                    <div class="col-md-6">
                      <input name="pay_mod" id="pay_mod" value="一般匯款" required=""  checked="checked" type="radio"> 一般匯款／ATM轉帳
                    </div>
                 </div>
                 <div class="col-sm-12">
                    <div class="col-sm-8 col-md-offset-2 font7 hline1">訂單送出後會e-mail『匯款帳號』給您，您也可於團購網上的訂單系統中查詢到我們的匯款帳號，請於三日內至金融機構、郵局或一般ATM轉帳付款。<br>（因金融機構作業時間，付款後約需一個工作天才會入帳）
                    </div>
                 </div>
                 <div class="col-sm-12 hline font7">提醒事項：<br>中華養生無提供批發商功能，亦不會以電話通知您修改付款方式或重新付款，或是要求您提供卡片後方的客服電話，再用該電話告知須操作 ATM 修改設定，以上皆為詐騙集團之惡劣騙術，請千萬不要受騙喔。
                 </div>
              </div>
              <div class="col-md-12">
                 <!-- Button --><hr>
                 <div class="text-right">
                    <form method="post">
                       <button name="cr_od_use" value="cr_od_use" class="btn button1" type="button" onClick="this.form.action='orders.php';this.form.submit();" >回上頁</button>
                    </form>
                    <button id="cr_od_use" name="cr_od_use" value="cr_od_use" class="btn">確認訂購</button>
                 </div>
              </div>
              </form>
             </div> 
         </div>
      </article>
    </div><!--內容 結尾-->
  </div>
<!-- Google Code for &#27138;&#27963;&#36092;1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1033249867;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "9yaWCMOrpV4Qy8jY7AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1033249867/?label=9yaWCMOrpV4Qy8jY7AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->