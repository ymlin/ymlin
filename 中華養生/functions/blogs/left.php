<?php
  //檢查 cookie 中的 passed 變數是否等於 TRUE
  $passed = $_COOKIE["passed"];
	
  /*  如果 cookie 中的 passed 變數不等於 TRUE
      表示尚未登入網站，將使用者導向首頁 index.htm	*/
  if ($passed != "TRUE")
  {
    header("location:index.php");
    exit();
  }
?>
<!doctype html>
<html lang=''>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styless.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>CSS MenuMaker</title>
</head>
<body>

<div id='cssmenu'>
<ul>
   <li><a href='index.php'>Home</a></li>
   <li class='active has-sub'><a href='#'>管理員專區</a>
      <ul>
         <li class='has-sub'><a href='#'>文章</a>
            <ul>
               <li><a href="Admin_edit.php"target="right">全部文章</a></li>
               <li><a href="edit.php"target="right">新增文章</a></li>
			    <li><a href="edit_category.php"target="right">新增文章分類</a></li>
            </ul>
         </li>
          <li class='has-sub'><a href='#'>帳號</a>
            <ul>
               <li><a href="Admin_mem.php"target="right">全部帳號</a></li>
              
            </ul>
         </li>
		 <li class='has-sub'><a href='#'>登出</a>
            <ul>
               <li><a href="logout.php"target="right">登出網站</a></li>
              
            </ul>
         </li>
      </ul>
   </li>
  
</ul>
</div>

</body>
<html>
