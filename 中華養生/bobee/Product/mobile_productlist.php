<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 
?>

<script>
    document.getElementById("body").style.backgroundSize = "cover";
</script>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	</head>
	
	<style>
	html{
	 -webkit-tap-highlight-color:rgba(0,0,0,0);
	}
	</style>

	<body >
	<div class="col-md-4" style="width:100%; background-color: transparent;" >

		<?php
			$sql = "SELECT prefer FROM `users` WHERE id = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($_GET['userid']));
			$result = $sth->fetchobject();
			$prefers = explode(" ", $result->prefer);
			$user_type = $result->user_type;
			
			$prefer_rel = explode(",", $prefers[0]);
			$qry_prefer = "( top = 1 or (( religion = '不限' ";
			for($i = 0 ; $i < count($prefer_rel) ; $i++ ) $qry_prefer=$qry_prefer." or religion = '".$prefer_rel[$i]."' ";
			$qry_prefer = $qry_prefer.") and ";
			
			$prefer_bud = explode(",", $prefers[1]);
			$qry_prefer = $qry_prefer."( buddha = '無' ";
			for($i = 0 ; $i < count($prefer_bud) ; $i++ ) $qry_prefer=$qry_prefer." or buddha = '".$prefer_bud[$i]."' ";
			$qry_prefer = $qry_prefer.") and ";
			
			$prefer_sea = explode(",", $prefers[1]);
			$qry_prefer = $qry_prefer."( season = '無' ";
			for($i = 0 ; $i < count($prefer_sea) ; $i++ ) $qry_prefer=$qry_prefer." or season = '".$prefer_sea[$i]."' ";
			$qry_prefer = $qry_prefer."))) ";
		
			if($_GET['category'] == 'food') 		$sql = "SELECT * FROM `products` WHERE category = '乾貨 蔬果 雜糧 飲料' and (user_type='通用' || user_type='?') and ";
			else if($_GET['category'] == 'money')	$sql = "SELECT * FROM `products` WHERE category = '金紙 佛具 香' and (user_type='通用' || user_type='?') and ";
			else if($_GET['category'] == 'vege')	$sql = "SELECT * FROM `products` WHERE category = '素菜 生鮮' and (user_type='通用' || user_type='?') and ";
			else 									$sql = "SELECT * FROM `products` WHERE (user_type='通用' || user_type='?') and ";
			$sth = $db->prepare($sql.$qry_prefer."ORDER BY top DESC, datetime DESC");
			$sth->execute(array($user_type));
			
			$counter = 0;
			while($result = $sth->fetchobject())
			{
				if($counter % 2 == 0) echo '<table style="width:100%;"><tr>';
		?>
				<td style="width:50%; height:100%; padding: 0.1em 0.2em 0.1em 0.2em; ">
					<div style="background-color:#ffffff; margin: 0px auto;">
						<a href="mobile_product.php?id=<?php echo $result->id; ?>&userid=<?php echo $_GET['userid']; ?>" style="text-decoration:none;">
							<div style="width:100%; height: 8em; margin: 0px auto; background:url(../mickey/<?php echo $result->photo_name; ?>) no-repeat; background-size: cover; background-position: center center;" >
							
							</div>
							<div style="height: 0.5em;"> </div>
							<div style="width:100%; height: 2em; color:black;font-weight:600;line-height: 120%;font-size: 90%;">
							<?php
								echo mb_substr(strip_tags($result->name),0,20,"UTF-8");
								if(mb_strlen(strip_tags($result->name),"UTF-8") > 20) echo "...";
							?>
							</div>
						</a>
						
						<table style="width:100%;">
							<tr>
							<td style="text-align:left; width:70%;">
								<div style = "font-size: 105%; color:orange;"><?php echo "NT$".$result->price; ?></div>
							</td>
							<td style="text-align:right; width:30%;">
								<div style="
									background:url(../mickey/into_cart.png) no-repeat;
									height: 2.5em;
									background-size: contain;
									background-position: center right;"
									onclick = "alert('addtocart<?php echo " ".$result->id." ".mb_substr(strip_tags($result->name),0,5,"UTF-8").(mb_strlen(strip_tags($result->name),"UTF-8") > 5? "..." : ""); ?>');"
								>
								</div>
							</td>
							</tr>
						</table>
					</div>
				</td>
		<?php
				if($counter % 2 == 1) echo "</tr></table>";
				$counter++;
			}
			
			if($counter % 2 == 1) echo '<td style="width:50%;"><div></div></td></tr></table>';
		?>
		
	</div>	
	</body>
</html>
		
				