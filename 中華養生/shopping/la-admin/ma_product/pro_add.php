<?php @require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php @require_once("../../functions/fundb.php");?>
<?php @require_once("../../functions.php");?>
<?php $prono= 2; if(isset($_GET['no_d'])){$prono= $_GET['no_d'];}?><!-- la-amin fun -->
<?php @require_once("../functions.php");?>
<?php @require_once("functions_pro.php");?>
<?php @require_once("../header_all.php");?>
<!--online edit-->
<link rel="stylesheet" href="../kindeditor/themes/default/themes/default/default.css" />
<link rel="stylesheet" href="../kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="../kindeditor/plugins/code/prettify.js"></script>
<script>
    KindEditor.ready(function(K) {
        var editor1=K.create('textarea[name="cr_pro_exch[]"]',{//name=form中textarea的name属性
            cssPath : '../kindeditor/plugins/code/prettify.css',
            uploadJson : '../kindeditor/php/upload_json.php',
            fileManagerJson : '../kindeditor/php/file_manager_json.php',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=myform]')[0].submit(); // name=form表单的name属性
                });
            }
        });
        prettyPrint();
    });
</script>
  <!-- feature -->
  <div id="features-wrapper">
     <div class="container"><!--內容區 -->
       <article class="row clearfix">
          <div class="mem_list">
             <div class="col-lg-12 column">
                <div class="col-sm-12 top_banner_cont">商品新增</div>
                <div class="col-sm-2 spn1">
                   <!-- 數量選擇 -->
                   <div class="btn-group">
                     <button type="button" class="btn btn-sm button1 ">數量選擇</button>
                     <button type="button" class="btn btn-sm dropdown-toggle button1" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                     </button>
                     <ul class="dropdown-menu" role="menu">
                        <li><a href="pro_add.php?no_d=2">2 筆</a></li>
                        <li><a href="pro_add.php?no_d=5">5 筆</a></li>
                        <li><a href="pro_add.php?no_d=10">10 筆</a></li>
                        <li><a href="pro_add.php?no_d=15">15 筆</a></li>
                        <li><a href="pro_add.php?no_d=20">20 筆</a></li>
                     </ul>
                   </div>
                </div>
            </div>
          </div>
       </article>
       <article class="row clearfix"><!-- 對齊中間 -->
          <div class="mem_list">
             <div class="col-lg-12 column">
                <div class="col-md-12 hline">     
                   <form class="form-horizontal" action="fundb.php" method="post" name="myform">
                      <?php for ($i=1; $i<=$prono; $i++){ ;?>
                      <div class="col-sm-12 inner">
                         <!-- Select Basic -->
                         <div class="col-sm-6">
                            <label class="col-sm-3 control-label font5" for="brand">品牌</label>
                            <div class="col-sm-5">
                                <!-- 品牌-->
                               <select id="cr_br_id[]" name="cr_br_id[]" class="input25">
                                   <option value="0" selected="selected">請選擇</option>
                                   <option class="divider"></option>
                                   <?php procreat3();?>
                               </select>
                            </div>
                         </div>
                         <!-- Select Basic -->
                         <div class="col-sm-6">
                            <label class="col-sm-3 control-label font5" for="categ">分類</label>
                            <div class="col-sm-5">
                               <select id="cr_ca_id[]" name="cr_ca_id[]" class="input25">
                                   <option value="0" selected="selected">請選擇</option>
                                   <?php procreat(0, 1);?><!-- 品牌比較-->
                               </select>
                            </div>
                         </div>  
                            <?php procreat(1, 0);?><!-- 商品名稱-->                     
                            <?php procreat(2, 0);?><!-- 商品原價-->   
                            <?php procreat(5, 0);?><!-- 商品數量--> 
                            <?php procreat(3, 0);?><!-- 商品特價-->
                            <?php //procreat(7, 0);?><!-- 商品運費-->                       
                            <?php procreat(6, 0);?><!-- 商品照片-->                       
                            <?php procreat(8, 0);?><!-- 商品首頁圖-->                     
                            <?php procreat2(1);?><!-- 商品簡易說明 -->                       
                            <?php procreat2(2);?><!-- 商品注意事項-->
                            <?php procreat4();?><!-- 商品說明快編器 -->
                      </div>
                      <div class="col-sm-3 col-sm-offset-1 spn1">
                         <button id="cr_pr_send" name="cr_pr_send" value="cr_pro" class="btn">確定送出</button>
                         <button class="btn button1" type="button" title="回上頁" onclick="location.href='index.php'">回上頁</button>
                      </div>
                      <div class="col-sm-12"><hr></div>
                      <?php }?>
                   </form>
                </div>
             </div>
           </div>
         </article>
     </div><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?><!----- END sql ----->
<?php end_sql($end_sqls);?>