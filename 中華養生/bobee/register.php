<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>註冊</title>
		<?php include 'index_head.php';?>
	</head>
	<body>
	<?php include 'nav.php';?>
	<div class="container">
		<form class='form-horizontal' action="add_user.php" method="post" name="myForm">
			<div class="login_bar"><h3>註冊會員</h3></div>
				<div class="col-md-12">
					<div class="content_bar">
						<div class="form-group">
							<label class="col-md-2 control-label" for='account'>帳號</label>
							<div class="controls col-md-4">
								<input id='account' name='account' type='text' size="30" placeholder='請輸入帳號' />
								<p class='help-block'>*請輸入您的e-mail，這將會成為您登入的帳號</p>
							</div>
						</div>
								
						<div class="form-group">
							<label class="col-md-2 control-label" for='password'>密碼</label>
							<div class="controls col-md-3">
								<input id='password' name="password" type="password"  placeholder='請輸入密碼' />
								<p class='help-block'>*建議輸入8碼以上的英文或數字</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for='type'>類型</label>
							<div class="controls col-md-3">
								<select name="type">
									<option value="personal">個人</option>
									<option value="company">公司</option>
								</select>
								<p class='help-block'>*請選擇帳號類型</p>
							<div class="controls col-md-3">
						</div>
						
						<div class=" form-actions  col-md-offset-2" >　
							<button formaction="add_user.php" formmethod="POST" type="submit" class="btn btn-primary" >送出</button>
							<button formaction="index.php" style="margin-left:10px;" formmethod="POST" type="submit" class="btn btn-primary">離開</button>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>

	<!-- script references -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/scripts.js"></script>
	</body>
</html>