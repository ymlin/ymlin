﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body  style="font-family:Microsoft JhengHei;background: #f5f5f5;">

    <!-- Navigation -->
		<?php include 'header.php';?>
	

    <!-- Page Content -->
	  <div class="container"style="margin-bottom:60px;">
	    <div class="row">
		 <div class="col-md-12 ">
		<div class="" style="line-height: 30px;font-size:30px;font-weight:600;letter-spacing:2px;margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
			關於我們
		</div>
		</div>
		</div>
		<div class="row">
			 <div class="col-md-12 ">
				   <div class="" style="line-height: 35px;font-size:20px;font-weight:600;letter-spacing:2px;margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
									<p class="MsoNormal">
							<b>
							<p class="MsoNormal">
								我們是一群熱愛健康養生的人，在這個慢性病肆意橫流，各種良莠不齊的健康資訊充斥的時代，我們發現惟有回歸到我們中華老祖宗養生的智慧上，才能保持我們的一身之健康。<span></span>
							</p>
							<p class="MsoNormal">
								<span>&nbsp;</span>
							</p>
							<p class="MsoNormal">
								自古上醫治未病，從上古開始吾中華先人就提倡了治未病的概念了，這是預防醫學的先驅。養生是人生根本的學問，歷代古聖賢將養生的智慧融入治世當中，使人民可以志閑而少欲，心安而不懼，形勞而不倦，氣從以順，各從其欲。進而可以美其食。任其服。樂其俗。高下不相慕。其民故曰朴。<span></span>
							</p>
							<p class="MsoNormal">
								<span>&nbsp;</span>
							</p>
							<p class="MsoNormal">
								而今科技發達，網路時代，資訊充斥，物慾橫流，我們雖然自以為是科技尖端，極端進步的時代，但是我們的健康卻完全不是這回事，慢性病這幾十年來呈現直線上升，各國投入大量的醫療研究資金，每一年好像都有突破，但是統計的數據卻大大的打了這些研究一個臉，除了癌症人數每年都有突破之外，沒有看到任何一個研究可以抑制這個趨勢。<span></span>
							</p>
							<p class="MsoNormal">
								<span>&nbsp;</span>
							</p>
							<p class="MsoNormal">
								很可惜的是在這個西風東漸的時代，我們自己人卻不重視自己老祖宗的智慧，反而都是國外的人來學習，我們覺得這是非常可惜的一件事，所以我們成立的中華養生來發揚中華古人的養生智慧，歡迎有志一同的人也一起加入。<span></span>
							</p>
						</b>
						</p>
						<p class="MsoNormal">
							<a href="mailto:begood.cht@gmail.com"></a> 
						</p>
													
						
						
					</div>
				</div>
			 
			</div>
		</div>
	 
   	
	<?php include 'footer.php';?>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
