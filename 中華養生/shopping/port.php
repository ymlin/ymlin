<?php @require_once("conn/wp-c.php");?>
<?php @require_once('functions/EDcart.php');?>
<?php @require_once("functions/Getcart.php");?>
<?php @require_once("functions/fundb.php");?>
<?php @require_once("functions.php");?>
<?php @require_once("header.php");?>
<?php $urls = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?><!--Get url-->
<link rel="stylesheet" type="text/css" href="css/tag.css" ><!-- 頁籤 -->
  <!-- Banner touth-->
  <div id="banner-wrapper">
    <div class="container"><!--內容區 -->
      <article class="row"><!-- 對齊中間 -->
         <div class="col-lg-12 category_list">
           <div class="font3"><?php path_page($row_pro_pr['brcate_id'],$row_pro_pr['pro_tName'], 2);?></div>
         </div>
      </article>
    </div>
  </div>
  <!-- feature -->
  <div id="features-wrapper">
   <div class="container"><!--內容區 -->
      <article class="post_list"><!-- 對齊中間 -->
         <div class="row clearfix">
            <div class="col-md-9 column">
               <div class="col-md-12 hline">    
                  <div class="col-md-7 text-center">
                     <div class="inner-q font1"><?php echo $row_pro_pr['pro_tName'];?></div>
                     <div class="inner-q"><img alt="" src="<?php echo $row_pro_pr['pro_tIMAGE'];?>" class="img-responsive text-center"/></div>
                  </div>
                  <div class="col-md-5 text-center">
                  <form class="form-horizontal" action="functions/addtocart.php?A=Add" method="post" name="from1" id="from1">
                    <div class="inner-q bottmli">
                     <p class="font2">(好康價) NT$ <?php echo $row_pro_pr['pro_tPrice2'];?><span class="smal"> 元起</span></p>
                     <p class="font3">原價 : NT$ <?php echo  $row_pro_pr['pro_tPrice'];?>元</p>
                     <p class="font8">編號 : <?php echo  $row_pro_pr['pro_tNo'];?></p>
                     <p class="font8"><?php echo $row_pro_pr['pro_tExch3'];?></p>
                     </div>
                     <div class="inner-q">
                       <p><?php port_pro_type($row_pro_pr['pro_tID']);?></p>
                       <p><?php echo pro_gotopay($row_pro_pr['pro_tID']);?></p>
                          <input name="urls" value="<?php echo $urls;?>" type="hidden">
                          <input name="g" value="gt" type="hidden">
                     </div>
                     <div class="inner-q">
                        <div class="fre">
                           <div class="col-sm-4">配送方式 :</div>
                           <?php port_pro_freight($row_pro_pr['pro_tID'])?>
                        </div>
                     </div>
                   </form>
                   </div>
                </div>
                <!-- Only desktop -->
                <aside class="desktop">
                     <div class="navla_tab">
                       <ul class="navtabs font9">   
                         <li class="active"><a href="#tab1">商品介紹</a></li>
                         <li class=""><a href="#tab2">注意事項</a></li>
                       </ul>
                     </div>
                     <div class="col-sm-12 tab_container">
                        <div style="display: block;" id="tab1" class="tab_content">
                          <div class="row">
                            <div class="font7"><?php echo $row_pro_pr['pro_tExch'];?></div>
                          </div>
                        </div>
                        <div style="display: none;" id="tab2" class="tab_content">
                          <div class="row">
                            <div class="font7"><?php echo $row_pro_pr['pro_tExch2'];?></div>
                          </div>
                        </div>
                     </div>
                </aside>
             </div>
             <div class="col-md-3 sider_clr">
               <div class="col-sm-12 fonts">
                <div class="fbb">
                    <div class="font10">追蹤我們:</div>
                    <div class="col-xs-4" style="padding:0px;width:30px;"><?php home_facebook2();?></div><!--facebook-->
                    <div class="col-xs-4" style="padding:0px;width:30px;"><?php home_line();?></div><!--line-->
                    <div class="col-xs-4" style="padding:0px;width:30px;"><?php home_youtube();?></div><!--youtube-->
                </div>                        
             </div>
            </div>
         </div> 
      </article>
	  <article class="mobile">
         <div class="row clearfix">
		    <div class="col-lg-12 column">
                <!-- Only mobile -->
                <aside>
			       <div class="panel-group" id="accordion">
				      <div class="panel panel-default">
					     <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
						       <a class="collapsed font4" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">商品介紹</a>
                            </h4>
					      </div>
					      <div style="height: 0px;" id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                             <div class="panel-body">
							    <?php echo $row_pro_pr['pro_tExch'];?>
						     </div>
					      </div>
				      </div>
                      <div class="panel panel-default">
					     <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
						       <a class="collapsed font4" data-toggle="collapse" aria-expanded="false" data-parent="#accordion" href="#collapseTwo">注意事項</a>
                            </h4>
					      </div>
					      <div style="height: 0px;" id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                             <div class="panel-body">
							    <?php echo $row_pro_pr['pro_tExch2'];?>
						     </div>
					      </div>
				      </div>
			       </div>
                </aside>
             </div>
         </div>
	  </article>
      <!-- Only desktop button-->
      <div class="col-sm-12 post_list desktop">
        <div class="inner-q">
         <form action="functions/addtocart.php?A=Add" method="post" name="from1" id="from1"> 
           <?php echo pro_gotopay($row_pro_pr['pro_tID']);?>
           <input id="urls" name="urls" value="<?php echo $urls;?>" type="hidden">
          </form>
         </div>
       </div>
   </div>
  </div><!--feature end-->
 </div><!--warp END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->