<?php @require_once("../conn/wp-c.php");?>
<?php @require_once("../functions/EDcart.php");?>
<?php @require_once("../functions/Getcart.php");?>
<?php @require_once("../functions/fundb.php");?>
<?php $dir_s1="2";?><!-- login -->
<?php @require_once("../loguser/use_chk.php");?>
<?php $orid=0; if(isset($_GET['d'])){$orid= base64_decode(htmlentities($_GET['d']));}?>
<?php @require("../functions.php");?>
<?php @require("functions.php");?><!-- la-member fun -->
<?php $anst= use_pro_oth_chk($orid, $uservip)?>
<?php if($anst == "0"){header("Location: index.php");}?>
<?php @require("header.php");?>
  <!-- feature -->
  <div id="features-wrapper">
    <div class="container"><!--內容區 -->
       <article class="row clearfix"><!-- 對齊中間 -->
          <div class="col-lg-12 column">
             <div class="col-md-12 mem_list">
                <div class="col-md-12 top_banner_cont">訂單 / 配送 查詢</div>
                <div class="col-md-12 hline">
                   <div class="table-responsive">
                     <table class="table">
                        <thead>
                           <th>商品</th>
                           <th>商品數量</th>
                           <th width="10%" class="text-right">小計</th>
                        </thead>
                        <tbody>
                           <?php use_pro_oth_view($orid, $uservip);?>
                           <?php use_od_freight($orid);?>
                        </tbody>
                     </table>
                   </div>
                   <!-- E-Coupon -->
                   <div class="table-responsive">
                      <?php use_sub_view_order($orid, $uservip);?>
                   </div>
                   <!-- 配送 -->
                   <div class="table-responsive">
                     <table class="table">
                        <thead>
                           <th width="15%">付款方式</th>
                           <th width="20%">匯款帳號後五碼</th>
                           <th width="20%">配送方式</th>
                           <th width="20%">運送單號</th>
                           <th width="20%">運送狀態</th>
                        </thead>
                        <tbody>
                            <?php use_pay_ship($orid);?><!-- 配送 查詢 -->
                        </tbody>
                     </table>
                    </div>
                    <div class="col-sm-3 hline1 font7">
                           <address>
                              <strong>匯款銀行 : </strong><br>
                              <strong>E-MAIL通知</strong><br>
                           </address>
                    </div>
                    <div class="col-sm-4 hline1 font7">
                           <address>
                              <?php use_shippers($orid);?>
                           </address>
                    </div>
                    <div class="col-sm-12"><hr>
                       <div class="col-sm-offset-2">
                          <button class="btn button1" type="button" title="回上頁" onclick="location.href='index.php'">回上頁</button>
                       </div>
                    </div>
                 </div>
             </div>
          </div> 
       </article>      
    </div><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!--END sql-->