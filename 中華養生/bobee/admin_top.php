<nav class="navbar navbar-inverse">
  <div class="container-fluid">
  
    <ul class="nav navbar-nav">
		<li><a href="http://bobee.begoodlive.com/UserManagement/admin_users.php">會員</a></li>
		<li><a href="http://bobee.begoodlive.com/admin_category.php">分類</a></li>
		<li><a href="http://bobee.begoodlive.com/Article/admin_article.php">文章</a></li>
		<li><a href="http://bobee.begoodlive.com/Product/admin_product.php">產品</a></li>
		<li><a href="http://bobee.begoodlive.com/Pic/admin_pic.php">圖檔</a></li>
		<li><a href="http://bobee.begoodlive.com/Notification/admin_notification.php">提醒</a></li>
		<li><a href="http://bobee.begoodlive.com/Home/admin_goodday.php">宜/忌</a></li>
		<li><a href="http://bobee.begoodlive.com/Home/admin_holiday.php">節日</a></li>
    </ul>

	
	<ul class="nav navbar-nav navbar-right">
		<li><a href="http://bobee.begoodlive.com/admin_info.php">個人資料</a></li>
		<li><a href="http://bobee.begoodlive.com/logout.php">登出</a></li>
    </ul>
  </div>
</nav>