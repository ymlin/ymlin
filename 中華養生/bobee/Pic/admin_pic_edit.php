<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT * FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>新增圖檔</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include __DIR__.'/../admin_top.php';?>
	<div class="container">  
		<form class='form-horizontal' action="admin_pic_upload.php" method="post" enctype="multipart/form-data" name="myform">
			<fieldset>
				<legend>新增圖檔</legend>
				<div class="form-group">
					<label class="col-md-2 control-label" for='account'>英文名稱</label>
					<div class="controls col-md-6">
						<input  name="name"  type="text" size="40" value="">
					</div>
				</div>
			</fieldset>
			
			<hr>
			<div class="form-group">
				<label class="col-md-2 control-label" for="input01">說明</label>
				<div class="controls col-md-6">
					<textarea name="comment" cols="85" rows="30"  ></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label">上傳圖片</label>
				<div class="controls col-md-6">
					<input type="hidden" name="MAX_FILE_SIZE" value="26214400">
					<input type="file" id="imgInp" name="myfile" size="50"><br><br>
					<script language="JavaScript">
						function readURL(input) {
							if (input.files && input.files[0]) {
								var reader = new FileReader();

								reader.onload = function (e) {
									$('#blah').attr('src', e.target.result);
									$('#blah').attr('style', "visibility:visible");
								}

								reader.readAsDataURL(input.files[0]);
							}
						}

						$("#imgInp").change(function(){
							readURL(this);
						});
					</script>
					<img id="blah" style="visibility:hidden" alt="your image" />
				</div>
			</div>
			
			<div align="center">
				<button class='btn btn-primary' type="submit">上傳</button>
			</div>
		</form>

	</div>  
	
	
	
	<!-- script references -->
	<script src="js/bootstrap.min.js"></script>
</body>  
</html>