﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>活動</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<?php
 $id = $_GET["id"];
    require_once("dbtools.inc.php");
  $sql3 = "SELECT * FROM activity WHERE id=$id  ";
	
      $result2 = mysqli_query($wp_c,$sql3) or die(mysqli_error('error'));
	   $row = mysqli_fetch_assoc($result2);
 ?>
<body  style="font-family:Microsoft JhengHei;background: #f5f5f5;">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

     <div class="row">
		<div class="col-md-6 ">
				<a href="#">
                    
					<img style=" border-radius: 5px;" class="img-responsive" src="../mickey/<?php echo  $row["photo_name"] ?>" height="250px"width="100%" alt="" >
                </a>
       
			   <div class="" style="margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
					
					<div style="font-weight:600;font-size:20px">
						活動資訊
					</div>
					<hr>
					開始時間:&nbsp &nbsp<?php echo  $row["s_datetime"] ?>
					<hr>
					結束時間:&nbsp &nbsp<?php echo  $row["f_datetime"] ?>
					<hr>
					E-mail:&nbsp &nbsp begood.cht@gmail.com
					
				</div>
		</div>
       
		<div class="col-md-6 ">
			 <div class="" style="margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
					
					<div style="font-weight:600;font-size:28px">
						<?php echo  $row["title"] ?>
					</div>
					<?php echo  $row["comment"] ?>
				</div>
			<div class="" style="margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
				 <div class="row"style="font-weight:900;font-size:16px">
					<div class="col-md-6 ">
					名稱 / 類型
					
					</div>
					<div class="col-md-3 ">
						報名人數
					</div>
				
				</div>
				 <div class="row"style="margin-top:20px;font-size:14px">
					<div class="col-md-6 ">
					<?php echo  $row["s_datetime"] ?>
					
					</div>
					<div class="col-md-3 ">
						
						  <select class="form-control"style="width: 60%;height: 25px;">
							 <option>1</option>
							 <option>2</option>
							 <option>3</option>
							 <option>4</option>
							 <option>5</option>
						  </select>

					</div>
				
				</div>
				
				<div class="col-md-offset-6 "style="margin-top:20px;">
						<a href="inner_activity.php?id=<?php echo $row3['id'] ?>">
						<div class="readmore"style="font-weight:500;font-size:16px">立即報名➔</div>
				</div>
				</a>		
			</div>		
		</div>
      

        <!-- Footer -->
       

    </div>
	</div>		
	<?php include '../footer.php';?>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
