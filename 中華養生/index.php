

<?php @require_once("index_header.php");?>
   <!-- feature -->
   <div id="features-wrapper" class="home_list">
     <div class="col-xs-12 col-lg-12 spn1"><hr></div>
     <article class="container"><!-- 對齊中間 -->
        <div class="row">
          <section class="art1">
              <div class="col-xs-12 col-md-4">
                <div class="thumbnail font2">
                    <a href="http://begoodlive.com/index2.php">
                    <img src="./img/log1.png" class="img-rounded logo1 hoverbig">
                    <p class="text-center font1">拜拜網</p>
                    <p class="text-center hidden-xs"><span style="font-size:0.8em;"> 解決你的拜拜大小事</span></p>
                    <p class="text-center visible-xs"><span style="font-size:0.5em;">解決你的拜拜大小事</span></p>
                    </a>
                </div>
              </div>
              <div class="col-xs-12 col-md-4">
                <div class="thumbnail font2">
                    <a href="http://begoodlive.com/content.php">
                    <img src="./img/log2.png" class="img-rounded logo2 hoverbig">
                    <p class="text-center font1">日常撇步</p>
                    <p class="text-center hidden-xs"><span style="font-size:0.8em;">一點一滴從生活做起</span></p>
                    <p class="text-center visible-xs"><span style="font-size:0.5em;">一點一滴從生活做起</span></p>
                    </a>
                </div>
              </div>
              <div class="col-xs-12 col-md-4">
                <div class="thumbnail font2">
                    <a href="http://shopping.begoodlive.com/">
                    <img src="./img/log3.png" class="img-rounded logo3 hoverbig">
                    <p class="text-center font1">養生食品</p>
                        <p class="text-center hidden-xs"><span style="font-size:0.8em;">為你把關養生食品</span></p>
                    <p class="text-center visible-xs"><span style="font-size:0.5em;">為你把關養生食品</span></p>
                    </a>
                </div>
              </div>
              
          </section>
        </div>
      </article>
      <div class="col-xs-12 col-lg-12"><hr></div>
      <!-- google Analysis -->
      <div><?php //googleapi();?></div>
   </div>
</div><!--最外層包裝 END-->
<?php @require_once("ha-footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->