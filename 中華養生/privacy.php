<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body  style="font-family:Microsoft JhengHei;background: #f5f5f5;">

    <!-- Navigation -->
		<?php include 'header.php';?>
	

    <!-- Page Content -->
	  <div class="container"style="margin-bottom:60px;">
	    <div class="row">
		 <div class="col-md-12 ">
		<div class="" style="line-height: 30px;font-size:35px;font-weight:600;letter-spacing:2px;margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
			隱私權聲明
		</div>
		</div>
		</div>
		<div class="row">
			 <div class="col-md-12 ">
				   <div class="" style="line-height: 35px;font-size:20px;font-weight:600;letter-spacing:2px;margin-top:20px;padding: 20px 15px 25px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1); background:white;">
																<p class="MsoNormal">
								<b> </b>
							</p>
							<p class="MsoNormal">
								<b>
								
								<p class="MsoNormal">
									為了尊重個人資料之隱私及保護，我們謹以下列聲明，向所有使用者說明「中華養生」在線上搜集使用者個人資料的方式、範圍、利用方法、以及查詢或更正的方式等事項。 <span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									對網友個人資料的收集，謹遵守中華民國「個人資料保護法」之規範。<span></span>
								</p>
								<p class="MsoNormal">
									「中華養生」網站伺服器會自動記錄網友所閱讀的網頁、網友所來自的網域名稱，作為內容與系統改進，及廣告效果評估的依據。此一部分資訊，用來作數據性的統計分析，除供內部參考，並將提供給廣告商，作為廣告效果評估之依據。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									網友加入「中華養生」網站會員或參與部分活動時，本網站可能要求填寫個人資料，如姓名、年齡、性別、電子信箱地址等，其目的是要增加對會員的了解，以提供更好的服務，本公司或關係企業或合作對象，將於本公司業務推展之必要範圍及本公司業務進行之時間及地區內，使用您的個人資料。惟本公司絕不販賣或透露給其他無關之第三人及單位，並且嚴禁內部人員私自使用這些資料。
							如您就個人資料之提供有請求本公司停止蒐集、處理、利用或刪除之需要時，您可以聯絡我們。如您提出前述請求，即視為您同時申請終止使用「中華養生」網站之一切服務。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									網友加入「中華養生」網站會員後，「中華養生」網站得利用會員所填寫的電子信箱地址，寄發「中華養生」網站相關之產品或活動之電子廣告信件。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									讀者在「中華養生」網站的互動服務上（例如：討論區或留言板等），可自由填寫電子郵件信箱的地址，以供互動及溝通之用途。這個資訊可能經由網路傳播被其他讀者利用，此類行為非本網站所能控制，若造成讀者困擾，「中華養生」網站並不對此負責。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									「中華養生」網站為服務網友，在部分網頁中會提供相關網站的連結，網友可藉由這些連結，直接點選到感興趣的網站，但是本網站不負責保護網友在這些經由本網站而連結到訪的網站中的隱私權。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									使用者個人資料有變更、或發現個人資料不正確的時候，可以隨時在「中華養生」相關網站中要求更正，包括要求停止寄發相關訊息等。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									與外站連結：「中華養生」相關網站或網頁都可能包含其他網站或網頁的連結，對於此等不屬於「中華養生」之網站或網頁，「中華養生」網站將會秉持相關法令及隱私權政策進行。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									Cookie程式的使用：為了便利使用者，「中華養生」網站相關網站可能使用<span>cookie</span>技術，以便於提供更適合使用者個人需要的服務；<span>cookie</span>是網站伺候器用來和使用者瀏覽器進行溝通的一種技術，它可能在使用者的電腦中儲存某些資訊，但是使用者可以自由經由瀏覽器的設定，取消、或限制此項功能。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									這份隱私權的保障自本網站成立起即生效，但因應社會環境及法令規定的變遷，若「中華養生」網站為保護網友隱私，認為需要修改這份聲明，會在最短的時間內更新。如果您對「中華養生」網站的隱私權政策或與個人資料有任何疑問，歡迎您聯絡我們。<span></span>
								</p>
								<p class="MsoNormal">
									&nbsp;
								</p>
								<p class="MsoNormal">
									【個人資料蒐集與使用】
							為提供訂購、行銷、客戶管理或其他合於營業登記項目或章程所訂業務需要之目的，中華養生將以電郵、傳真、電話、簡訊或其他通知公告方式利用您所提供之資料。利用對象除中華養生外，亦可能包括相關服務的協力機構。如您有依個資法第三條或其他需要協助之處，請聯絡我們。相關資料如為非必填項目，不提供亦不影響您的權益。<span></span>
								</p>
							</b>
							</p>
							<p class="MsoNormal">
								<b><span></span> </b>
							</p>
							<p>
								<br />
							</p>
							<p class="MsoNormal">
								<a href="mailto:begood.cht@gmail.com"></a> 
							</p>
													
						
						
					</div>
				</div>
			 
			</div>
		</div>
	 
   	
	<?php include 'footer.php';?>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
