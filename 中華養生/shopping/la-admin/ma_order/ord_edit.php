<?php @require_once("../../conn/wp-c.php");?>
<?php @require_once("../../functions/EDcart.php");?>
<?php @require_once("../../functions/Getcart.php");?>
<?php @require_once("../../functions/fundb.php");?>
<?php @require_once("../../functions.php");?>
<?php if (isset($_GET['od_d'])){ $ordids=$_GET['od_d'];}?><!-- la-amin fun -->
<?php @require_once("../functions.php");?>
<?php @require_once("functions_od.php");?>
<?php @require_once("../header_all.php");?>
<!-- date time -->
    <link href="../../css/hot-sneaks/jquery-ui.css" rel="stylesheet">
    <link href='../../css/hot-sneaks/jquery-ui-timepicker-addon.css' rel='stylesheet'>
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
    <script type='text/javascript' src='../../js/jquery-ui-sliderAccess.js'></script>
    <script type="text/javascript" src="../../js/jquery.ui.datepicker-zh-TW.js"></script>
    <style> .ui-datepicker {font-size: 80%!important; }</style>
<!--END date time -->
 <!-- feature -->
 <div id="features-wrapper">
   <article class="container"><!--內容區 -->
     <div class="row column mem_list">
        <div class="col-lg-12 clearfix">
          <div class="col-md-12 hline">
             <div class="col-md-12 title2">訂購商品</div>
             <!---- 訂購商品 Area ---->
             <div class="col-md-12">
               <form class="form-horizontal" action="fundb.php" method="post" name="Form_edit" id="Form_edit">
                 <div class="table-responsive">
                           <table class="table">
                             <thead>
                                <tr style="background-color:#e8e6e5">
                                   <th class="text-center">訂購商品(規格)</th>
                                   <th class="text-center">商品單價</th>
                                   <th class="text-center">訂購數量</th>
                                   <th class="text-right">小計價格</th>
                                </tr>
                              </thead>
                              <tbody>
                                 <tr><!-- 訂購商品-->
                                    <?php ordedit_oth($ordids);?>
                                  </tr>
                                 <tr>
                                   <?php use_od_freight($ordids)?>
                                 </tr>
                              </tbody>
                           </table>
                 </div>
                 <?php //use_od_freight($ordids);?><!-- 運費-->
                 <!-- Ecoupon -->
                 <div class="table-responsive">
                    <?php use_sub_view_order($ordids);?>
                 </div>
                 <!-- id hidden -->
                   <input id="od_d" name="od_d" value="<?php echo $ordids;?>" type="hidden">
                 <!-- Button -->
                 <div class="col-md-offset-2">
                   <button id="ed_od_send" name="ed_od_send" value="edodr" class="btn">確定修改</button>
                   <button class="btn button1" type="button" title="回上頁" onclick="location.href='index.php'">回上頁</button>
                 </div>
                </form>
              </div>
              <!---- 配送放式 Area ---->
              <div class="col-md-12">
                 <div class="col-md-12 title2">配送方式</div>
                 <div class="col-md-12 table-responsive">
                    <table class="table text-center">
                       <thead>
                                <tr style="background-color:#e8e6e5" class="text-center">
                                   <th class="text-center" width="15%">付款方式</th>
                                   <th class="text-center" width="15%">匯款帳號後五碼</th>
                                   <th class="text-center" width="20%">配送方式</th>
                                   <th class="text-center" width="20%">運送單號</th>
                                   <th class="text-center" width="20%">運送狀態</th>
                                </tr>
                       </thead>
                       <tbody>
                                <tr>
                                 <?php use_pay_ship($ordids);?><!-- 配送 查詢 -->
                                </tr>
                       </tbody>
                    </table>
                 </div>
                 <div class="col-md-12 table-responsive">
                    <table class="table text-center">
                       <thead>
                           <tr style="background-color:#e8e6e5" class="text-center">
                              <th class="text-center" width="10%">訂單單號</th>
                              <th class="text-center" width="20%">發票開立</th>
                              <th class="text-center" width="20%">發票號碼</th>
                              <th class="text-center" width="20%">公司統編</th>
                              <th class="text-center" width="20%">公司名稱</th>
                           </tr>
                       </thead>
                       <tbody>
                           <tr>                                 
                             <?php use_invoice($ordids);?><!-- invoice 查詢 -->
                           </tr>
                       </tbody>
                    </table>
                 </div>
              </div>
              <!---- user data Area ---->
              <div class="col-sm-4 hline1">
                 <div class="col-sm-12 title2">訂購者資料</div>
                 <div class="col-sm-12">
                    <address class="font3">
                          <?php ordedit_add('mvp', $ordids);?> <!-- user-->
                          <br>
                          <?php ordedit_add('name', $ordids);?><!-- user-->
                          <?php ordedit_add('add', $ordids);?><!-- address--> 
                          <?php ordedit_add('tel', $ordids);?><!-- tel-->
                          <?php ordedit_add('text', $ordids);?><!-- text-->
                    </address>
                 </div>
		      </div>
              <div class="col-sm-5 hline1">
                 <div class="col-sm-12 title2">配送資料</div>
                 <div class="col-sm-12 inner">
                    <div class="col-sm-4 font5">運送單號</div>
                    <div class="col-sm-8">
                      <form class="form-horizontal" action="fundb.php" method="post" name="Form_edit" id="Form_edit">
                         <input name="shi_no" type="text" class="input15"/>
                         <input id="od_d" name="od_d" value="<?php echo $ordids;?>" type="hidden">
                         <button id="ed_shi" name="ed_shi" value="edshi" class="btn btn-xs">確定</button>
                      </form>
                    </div>
                 </div>
                 <div class="col-sm-12 inner">
                    <div class="col-sm-4 font5">發票號碼</div>
                      <form class="form-horizontal" action="fundb.php" method="post" name="Form_edit" id="Form_edit">
                        <div class="col-sm-8">
                          <input name="invo_no" type="text" class="input15"/>
                          <input id="od_d" name="od_d" value="<?php echo $ordids;?>" type="hidden">
                          <button id="ed_invo" name="ed_invo" value="edinvo" class="btn btn-xs">確定</button>
                        </div>
                      </form>
                 </div>
                 <div class="col-sm-12 inner">
                    <div class="col-sm-4 font5">出貨通知</div>
                    <div class="col-sm-8">
                      <form class="form-horizontal" action="fundb.php" method="post" name="Form3" id="Form3">
                         <input id="od_d" name="od_d" value="<?php echo $ordids;?>" type="hidden">
                         <button id="ed_mess" name="ed_mess" value="edmessage" class="btn btn-xs">Email通知</button>
                      </form>   
                    </div>
                 </div>
                 <!-- limiter date -->
                 <div class="col-sm-12 inner">
                    <form class="form-horizontal" action="fundb.php" method="post" name="Form3" id="Form3">
                    <div class="col-sm-3 font5">出貨狀態</div>
                    <div class="col-sm-3">
                       <select id="sta_name" name="sta_name">
                          <option value="0" selected="selected">請選擇</option>
                          <option value="等待匯款">等待匯款</option>
                          <option value="出貨處理中">出貨處理中</option>
                          <option value="已出貨">已出貨</option>
                       </select>
                    </div>
                    <div class="col-sm-6">
                        <input id="od_d" name="od_d" value="<?php echo $ordids;?>" type="hidden">
                        <input id="datepicker1" name="sta_time" class="input10" placeholder="到期日" type="text">
                        <script language="JavaScript"> 
                               $(document).ready(function(){ 
                                 var opt={dateFormat: 'yy-mm-dd',
	                               showSecond: true,
                                   timeFormat: 'HH:mm:ss'
                                 };
                               $('#datepicker1').datetimepicker(opt);
                               });
                        </script>
                        <button id="ed_state" name="ed_state" value="shpstate" class="btn btn-xs">確定</button>
                     </div>
                     </form>
                 </div>
		      </div>
           </div>
         </div>
      </div>
    </article><!--內容 結尾-->
  </div>
</div><!--最外層包裝 END-->
<?php @require_once("../footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->