﻿ 
<?php
//裁切字串
function cut_content($a,$b){
    $a = strip_tags($a); //去除HTML標籤
    $sub_content = mb_substr($a, 0, $b, 'UTF-8'); //擷取子字串
    echo $sub_content;  //顯示處理後的摘要文字
    //顯示 "......"
   
}

//以上程式已經包裝起來,您可存放在例如:function.php網頁
//往後只要使用include("function.php");
//加上 cut_content($a,$b);即可,不需每次撰寫.
//$a代表欲裁切內容.
//$b代表欲裁切字數(字元數)
?>
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>註冊</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
		<link rel="shortcut icon" href="./images/favicon.png"/>
		
	<?php include 'index_head.php';?>
	</head>
	<body>
 <div style="
			padding: 20px 30px 5px 10px;
			box-shadow: 2px 2px 2px 4px rgba(128,128,128,0.1);
			margin: 0 0 5% 0;">
								
								<div style="margin-left:10px ;font-size:180%;font-weight: 900;">
											<?PHP echo "搜尋結果"; ?>
											
										</div>
											
											<hr>
<?php
if(isset($_GET["search"])){
    $search = $_GET["search"];
	//echo  $search;
	
  require_once("dbtools.inc.php");
    
      $sql = "SELECT * FROM edits where comment like '%$search%' OR tag like '%$search%' OR title like '%$search%' ORDER BY datetime DESC";
	  
	 // echo  $sql;
       $result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	   
	 $i=0;
	 while ($row = mysqli_fetch_assoc($result)){
	   date_default_timezone_set("Asia/Taipei");
	 $date=$row['datetime'];
			$datetime=date("Y-m-d H:i:s");
			$that_time = strtotime($date);
			$diff = ($that_time - $datetime);
			$CheckDay= date("Y-m-d",$that_time);
 if(strtotime($date)<strtotime($datetime)){			
						
	  //cut_content( $row['title'],0); 
	  $test= '\'';
	   $test1=$row['category'];$test2=$test.$test1.$test;
				  $sql3 = "SELECT * FROM categorys where add_category= $test2";
       $result3 = mysqli_query($wp_c,$sql3) or die(mysqli_error('error'));
	   $row3 = mysqli_fetch_assoc($result3);

	 
	 ?>
	
	<div style="border-bottom: solid 1px #dbdbdb;margin: 0 0 2.75em 0;
					padding: 0 0 3em 0;">
												
												<article class="boxes excerpt"> 
											
												<div class="col-sm-12  "style="padding:0px">
												<div class="col-sm-4 ">
													<a href="showpage.php?id=<?php echo $row['id'] ?>" class="image left"><img src="mickey/<?php echo  $row["photo_name"] ?>" alt="" /></a>
													</div>
													<div class="col-sm-8" >
														<header>
															<div style="margin: 0 0 1em 0;"> <?php echo  $row['editor'] ?> | <?php echo  $CheckDay ?> | <?php echo  $row['category'] ?></div>
															<h3><a href="showpage.php?id=<?php echo $row['id'] ?>"><?php echo  $row['title'] ?></a></h3>
														</header>
														<div style="color:#000000;">
														<p><?php cut_content( $row['comment'],95);  $sub_content = mb_substr($row['comment'], 0, 95, 'UTF-8');if(strlen($row['comment']) > strlen($sub_content)) echo '&nbsp;&nbsp;&nbsp;......	<a href="showpage.php?id=' . $row['id'] .' ">閱讀更多</a>';?> </p>
															
													</div>
													
													</div>
												
												</article>
												<div class="col-md-4">
												</div>
												<div class="col-md-8">
									<p>分類   <a href="Acategory.php?id=<?php echo $row3['id'] ?>" ><?php echo  $row['category'] ?> </a>  
									&nbsp;&nbsp;&nbsp;標籤  <?php echo  $row['tag'] ?></p>   
								</div>	
												</div>
	 <?php
	 }
}
}
	?>
	</div>
	</body>


<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>