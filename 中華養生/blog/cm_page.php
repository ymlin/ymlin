<?php @require_once("conn/wp-c.php");?>
<?php @require_once('functions/EDcart.php');?>
<?php @require_once("functions/Getcart.php");?>
<?php @require_once("functions/fundb.php");?>
<?php @require_once("functions.php");?>
<?php @require_once("header.php");?>
<!-- 表單驗證 -->
    <script type="text/javascript" src="./js/jquery.validate_2.js"></script>
    <script type="text/javascript" src="./js/jquery.validate.js"></script>
    <script type="text/javascript">
       $(function(){
	         $("#commentForm").validate();
        });
     </script>
  <!-- feature -->
  <div id="features-wrapper">
     <article class="container"><!-- 對齊中間 -->
        <div class="row clearfix">
           <div class="col-lg-12 column reg_list">
              <div class="top_banner_cont">
                   <h3>聯絡客服</h3>
              </div>
              <div class="col-lg-12 hightline">  
                 <form class="form-horizontal" action="functions/funsend.php" method="post" name="commentForm" id="commentForm">
                    <div class="col-md-9">
                       <div class="col-sm-10 col-sm-offset-1">
                         <!-- 訊息處理 -->  
                         <?php  if(isset($_GET['Msg'])){?>
                         <div class="text-left">
                            <?php echo base64_decode($_GET['Msg']);?>
                         </div>
                         <?php }?>
                       </div>
                       <div class="col-sm-12 font3">Setp 1 <small>請先勾選您的問題類別：(請勾選一項)</small></div>
                       <div class="col-sm-12 font3">
                          <label class="col-sm-2 control-label" for="titel_ml">商品活動</label>
                          <div class="col-sm-10">
                             <div class="col-sm-3"><input  name="problem_ml" value="商品規格洽詢" type="radio">商品規格洽詢</div>
                             <div class="col-sm-3"><input name="problem_ml" value="訂購付款方式" type="radio">訂購付款方式</div>
                             <div class="col-sm-3"><input name="problem_ml" value="行銷活動諮詢" type="radio">行銷活動諮詢</div>
                             <div class="col-sm-3"><input name="problem_ml" value="其他問題" type="radio">其他問題</div>
                          </div>
                       </div>
                       <div class="col-sm-12 font3">
                          <label class="col-sm-2 control-label" for="titel_ml">帳款發票</label>
                          <div class="col-sm-10">
                             <div class="col-sm-3"><input name="problem_ml" value="付款是否成功" type="radio">付款是否成功</div>
                             <div class="col-sm-3"><input name="problem_ml" value="退款問題詢問" type="radio">退款問題詢問</div>
                             <div class="col-sm-3"><input name="problem_ml" value="發票問題" type="radio">發票問題</div>
                             <div class="col-sm-3"><input name="problem_ml" value="其他問題" type="radio">其他問題</div>
                          </div>
                       </div>
                       <div class="col-sm-12 font3">
                          <label class="col-sm-2 control-label" for="titel_ml">團員諮詢</label>
                          <div class="col-sm-10">
                             <div class="col-sm-3"><input name="problem_ml" value="帳號查詢" type="radio">帳號查詢</div>
                             <div class="col-sm-3"><input name="problem_ml" value="關於個資" type="radio">關於個資</div>
                             <div class="col-sm-3"><input name="problem_ml" value="系統網頁問題" type="radio">系統網頁問題</div>
                             <div class="col-sm-3"><input name="problem_ml" value="其他問題" type="radio">其他問題</div>
                          </div>
                       </div>
                       <div class="col-sm-12 font3">
                          <label class="col-sm-2 control-label" for="titel_ml">訂單進度</label>
                          <div class="col-sm-10">
                             <div class="col-sm-3"><input name="problem_ml" value="更改訂單資料" type="radio">更改訂單資料</div>
                             <div class="col-sm-3"><input name="problem_ml" value="我要取消訂單" type="radio">我要取消訂單</div>
                             <div class="col-sm-3"><input name="problem_ml" value="查詢出貨進度" type="radio">查詢出貨進度</div>
                             <div class="col-sm-3"><input name="problem_ml" value="其他問題" type="radio">其他問題</div>
                          </div>
                       </div> 
                       <div class="col-sm-12 font3">
                          <label class="col-sm-2 control-label" for="titel_ml">退換貨問題</label>
                          <div class="col-sm-10">
                             <div class="col-sm-3"><input name="problem_ml" value="猶豫期退貨" type="radio">猶豫期退貨</div>
                             <div class="col-sm-3"><input name="problem_ml" value="新品瑕疵換貨" type="radio">新品瑕疵換貨</div>
                             <div class="col-sm-3"><input name="problem_ml" value="商品缺件不符" type="radio">商品缺件不符</div>
                             <div class="col-sm-3"><input name="problem_ml" value="其他問題" type="radio">其他問題</div>
                          </div>
                       </div>
                       <div class="col-sm-12 font3">
                          <label class="col-sm-2 control-label" for="titel_ml">其它</label>
                          <div class="col-sm-10">
                             <div class="col-sm-3"><input name="problem_ml" value="合作提案" type="radio">合作提案</div>
                             <div class="col-sm-3"><input name="problem_ml" value="講座邀請" type="radio">講座邀請</div>
                             <div class="col-sm-3"><input name="problem_ml" value="海外訂購" type="radio">海外訂購</div>
                             <div class="col-sm-3"><input name="problem_ml" value="批發訂購" type="radio">批發訂購</div>
                          </div>
                       </div>
                       <!-- name input-->
                       <div class="col-sm-12 font3">Setp 2 <small>填寫姓名、問題內容、聯絡E-mail</small></div>
                       <div class="col-sm-12 font3">
                          <label class="col-sm-3 control-label" for="name_ml">姓名</label>                           <div class="col-sm-5">
                             <input name="name_ml" id="name_ml" type="text" class="input25" placeholder="請輸入您的姓名" required=""/>
                          </div>
                       </div>
                       <!-- mail input-->
                       <div class="col-sm-12 font3">
                          <label class="col-sm-3 control-label" for="mail_ml">電子郵件</label>                  <div class="col-sm-5">
                             <input name="mail_ml" id="mail_ml" type="text" class="required email input25" placeholder="請輸入Email" required=""/>
                          </div>
                       </div>
                       <!-- tel input-->
                       <div class="col-sm-12 font3">
                          <label class="col-sm-3 control-label" for="tel_ml">連絡電話</label>                <div class="col-sm-5">
                             <input name="tel_ml" id="tel_ml" type="text" class="input25"placeholder="請輸入連絡電話" required=""/>
                          </div>
                       </div>
                       <!-- name input-->
                       <div class="col-sm-12 font3">
                          <label class="col-sm-3 control-label" for="text_ml">詢問內容</label>                  <div class="col-sm-5">
                             <textarea id="test_ml" name="text_ml" placeholder="請輸入詢問內容" rows="7" cols="30"></textarea>
                          </div>
                       </div>
                       <div class="col-sm-12 col-sm-offset-1">
                          <div class="hline2">
                           ＊客服回覆將直接寄到您的信箱，唯部分免費信箱容易擋信，請您留意垃圾郵件匣；若超過三個「工作天」（不含假日）仍未收到回覆，應為信箱漏信關係，請至 『LaSu樂蔬活粉絲團(點我)』私訊小印度。

＊小印度有空時間：週一至週五 10:00~17:00
                           </div>
                       </div>
                     </div>
                     <div class="col-md-12">
                         <hr>
                         <!-- Button (Double) -->
                         <div class="col-sm-12">
                            <div class="col-sm-3 col-sm-offset-1">
                               <button id="MM_send" name="MM_send" value="serviceck" class="btn" >確定送出</button>
                            </div>
                         </div>
                     </div>
                 </form>
               </div>
             </div>
          </div> 
       </article>
     </div><!--內容 結尾-->
  </div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!--END sql-->