 
<?php
  session_start();
  if (isset($_SESSION["login_account"]))
	  {
        $login_account = $_SESSION["login_account"];
      $accounts=$_SESSION["account"] ;
	   $t="\'";
	     $accountss=$t+$accounts+ $t;
	  
if(isset($_POST["comment"])){
$comment = $_POST["comment"];
}
if(isset($_POST["title"])){
$title = $_POST["title"];
}
if(isset($_POST["editor"])){
$editor = $_POST["editor"];
}
if(isset($_POST["datetime"])){
$date = $_POST["datetime"];
}
if(isset($_POST["category"])){
$category = $_POST["category"];
}
?>
<!doctype html>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
		
    <title>新增文章</title>
	<!--[if lt IE 9]>
<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
	<?php include 'link.php';?>
   
  </head>
  <body>
  
 <div style="margin-bottom:90px">
   <div id="navbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <span class="navbar-brand"><span class="fa fa-table"></span><a href='index.php'>Home</a></span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
           <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit.php" tabindex="-1">全部文章</a></li>
                        <li><a href="edit.php" tabindex="-1">新增文章</a></li>
						  <li><a href="edit_category.php" tabindex="-1">新增文章分類</a></li>
						   <li><a href="classic_edit.php" tabindex="-1">新增經典文章</a></li>
                     </ul>
                   </li>
              <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">帳號<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_mem.php" tabindex="-1">全部帳號</a></li>
                       
                     </ul>
                   </li>
              <li><a href="logout.php">登出</a></li>
            </ul>
          </div>
        </div>
    </div>

 </div>
   <?php
	  }
	else
	 header("location:index.php");
 ?>
<div class="container">
 
<form class='form-horizontal' action="add_edit.php" method="post" name="myform">
    <fieldset>
        <legend>新增經典文章</legend>
        <div class="form-group">
            <label class="col-md-2 control-label" for='account'>標題</label>
            <div class="controls col-md-6">

                <input  name="title"  type="text" size="40" value="">  
               
            </div>
        </div>
            <?php
	 require_once("dbtools.inc.php");  $test=$_SESSION["login_account"];
	 
      $sql4 = "SELECT * FROM users where id=  $login_account ";
	    $result4 = mysqli_query($wp_c,$sql4) or die(mysqli_error('error'));
		$row4 = mysqli_fetch_assoc($result4);
		
?>	  
        <div class="form-group">
            <label class="col-md-2 control-label">作者</label>
            <div class="controls col-md-6">
			
                <input  name="editor" type="text" size="10" value="<?php echo $row4['name'] ?> " />
                
            </div>
        </div>
		
			 <div class="form-group">
		 <label class="col-md-2 control-label" for="input01">經典分類</label>
          <div class="controls col-md-6">
            <select name="category">
			
	  
     <?php
	 require_once("dbtools.inc.php"); 
      $sql = "SELECT * FROM classic ";
       $result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	 
	   date_default_timezone_set("Asia/Taipei");
			
	while ($row = mysqli_fetch_assoc($result)){
		?>
              <option value="<?php echo $row['classic_name'] ?>"><?php echo $row['classic_name'] ?></option>
             
			  <?php
					}?>
            </select>
			 
			</div>
			</div>
			 <div class="form-group">
            <label class="col-md-2 control-label">keyword</label>
            <div class="controls col-md-6">
                <input  name="keyword" type="text" size="20" value="" />
                
            </div>
        </div>
		 <div class="form-group">
            <label class="col-md-2 control-label">標籤</label>
            <div class="controls col-md-6">
                <input  name="tag" type="text" size="20" value="" />
                
            </div>
        </div>
			
			<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">日期</label>
          <div class="controls col-md-6">
					<input name="datetime" id="datetimepicker1" type="text"  value="">
				<script language="JavaScript">
					$(document).ready(function(){ 
						var opt={dateFormat: 'yy-mm-dd',
							showSecond: true,
							timeFormat: 'HH:mm:ss'
							};
						$('#datetimepicker1').datetimepicker(opt);
					});
				</script>
				</div>
			</div>
			
		
    </fieldset>
	
	<hr>
	<div class="form-group">
		 <label class="col-md-2 control-label" for="input01">內容</label>
          <div class="controls col-md-6">
            <textarea name="comment" cols="85" rows="30"  ></textarea>

			</div>
			</div>
    <div class=" form-actions col-md-10 col-md-offset-2" >
	 　
          <input type="submit" value="下一步">  
	
	</div>
	
</form>
</div>

<!-- script references -->
		
		<script src="js/bootstrap.min.js"></script>
	
	</body>
  
</html>