<?php
//header('Content-type: text/html; charset=utf-8');
 
require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookRedirectLoginHelper.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/GraphUser.php');
require_once('Facebook/GraphSessionInfo.php');

require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'Facebook/Entities/AccessToken.php' );
require_once( 'Facebook/Entities/SignedRequest.php' );
 
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;

if (!isset($_SESSION)) {session_start();} 
// init app with app id (APPID) and secret (SECRET)
FacebookSession::setDefaultApplication('1005035829531382','ea6b4ce754a325ebaaa4146718e954f1' );
 
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper('http://www.lasulife.com/shopping/loguser/fblogin.php');
 
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
 
// see if we have a session
if ( isset( $session ) ) {
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
    
  // print data
  //print_r( $graphObject, 1 );echo '<br>';    
    //echo $graphObject->getProperty('id').'<br>';
    $udbemail = base64_encode($graphObject->getProperty('email'));
    $udbname = base64_encode($graphObject->getProperty('name'));
    $udbid= base64_encode($graphObject->getProperty('id'));
    header("Location:REfresh_other.php?fu=".$udbemail."&n=".$udbname."&d=".$udbid);
    //echo '<a href="' . $helper->getLogoutUrl( $session, 'http://www.lasulife.com/shopping/logout.php' ) . '">Facebook Logout</a>';
    exit;
} else {
  //$_SESSION = array(); 
  // show login url
  //'<a href="' . $helper->getLoginUrl() . '">Login</a>';
  $fblg1 = '<a href="' . $helper->getLoginUrl( array( 'email', 'user_friends' ) ) . '"></a>';
  $fblg2 =  $helper->getLoginUrl();
}
?>