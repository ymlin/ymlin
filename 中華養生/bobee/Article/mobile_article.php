<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 
	
	$id = $_GET['id'];
?>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>

	<style>
	html{
	 -webkit-tap-highlight-color:rgba(0,0,0,0);
	}
	</style>
	
	<body>
	<div  class="col-md-4" style="width:100%; background-color: transparent; min-height: 650px;">

		<?php
			$sql = "SELECT * FROM `articles` WHERE id = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($id));
			
			$result = $sth->fetchobject();
			$category = $result->keyword;
			
			$sql3 = "UPDATE `articles` SET total_click_rate = ? WHERE id = ?";
			$sth3 = $db->prepare($sql3);
			$sth3->execute(array($result->total_click_rate + 1, $id));

			if($result->video != "")
			{
		?>
		<script>
		alert('<?php echo $result->video;?>');
		</script>
		<?php
				$pos = strpos($result->video, 'youtu.be');
				$image = "http://img.youtube.com/vi/".substr($result->video, $pos+9)."/maxresdefault.jpg";
			}
			else
			{
				$image = "../mickey/".$result->photo_name;
			}
		?>
			
		
		<div style="width:90%; margin: 0px auto; font-weight:600;line-height: 120%;font-size:150%; text-decoration: none;color:black">
		<?php
			echo $result->title;
		?>
		</div>
		<br>
		<div style=" width:90%; margin: 0px auto;">
				<div style="display:inline-block; width:65%; margin:0px 0px 5%; color:gray;font-size: 110%; position:relative; top:-0.2em;">
				<?php
					echo date("Y-m-d", strtotime($result->datetime)) . " | " . $result->keyword;
				?>
				</div>
				<div style="display:inline-block; width:16%; height:1.5em; margin: 0px auto; background:url('../mickey/share.png') no-repeat; background-size: 100% 90%; background-position: center 0.2em;" onclick="alert('share <?php echo "bobee.begoodlive.com"." ".$result->id; ?>')" ></div>
			</div> 
		<br>
		<?php
			if($result->video != "")
			{
		?>
		<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: cover; background-position: center center;" onclick="alert('video <?php echo ($result->video!="")?($result->video):"";?>')" >
			<div style="width:100%; height:100%; margin: 0px auto; background:url('../mickey/play.png') no-repeat; background-size: cover; background-position: center center;"></div>
		</div>
		<?php
			}
			else
			{
		?>
		<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: contain; background-position: center center;" >
		</div>
		<?php
			}
		?>
		<br>
		<div style="width:90%;margin: 0px auto;color:black;font-weight:400;line-height: 120%;font-size: 120%;">
		<?php
			echo $result->comment;
		?>
		</div>
		<br>
		
		<?php
			$sql2 = "SELECT * FROM `articles` WHERE id != ? and keyword = ? LIMIT 0, 5 ";
			$sth2 = $db->prepare($sql2);
			$sth2->execute(array($id, $category));
		
			while($result2 = $sth2->fetchobject())
			{
				if($result2->video != "")
				{
					$pos = strpos($result2->video, 'youtu.be');
					$image = "http://img.youtube.com/vi/".substr($result2->video, $pos+9)."/maxresdefault.jpg";
				}
				else
				{
					$image = "../mickey/".$result2->photo_name;
				}
			?>
				<hr>
				<br>
				<div style="width:90%; margin: 0px auto; font-weight:600;line-height: 120%;font-size:150%; text-decoration: none;color:black">
				<?php
					echo $result2->title;
				?>
				</div>
				<br>
				<div style=" width:90%; margin: 0px auto;">
						<div style="display:inline-block; width:65%; margin:0px 0px 5%; color:gray;font-size: 110%; position:relative; top:-0.2em;">
						<?php
							echo date("Y-m-d", strtotime($result2->datetime)) . " | " . $result2->keyword;
						?>
						</div>
						<div style="display:inline-block; width:16%; height:1.5em; margin: 0px auto; background:url('../mickey/share.png') no-repeat; background-size: 100% 90%; background-position: center 0.2em;" onclick="alert('share <?php echo "bobee.begoodlive.com"." ".$result2->id; ?>')" ></div>
					</div> 
				<br>
				<?php
					if($result2->video != "")
					{
				?>
				<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: cover; background-position: center center;" onclick="alert('video <?php echo ($result2->video!="")?($result2->video):"";?>')" >
					<div style="width:100%; height:100%; margin: 0px auto; background:url('../mickey/play.png') no-repeat; background-size: cover; background-position: center center;"></div>
				</div>
				<?php
					}
					else
					{
				?>
				<div style="width:90%; height: 10em; margin: 0px auto; background:url(<?php echo $image; ?>) no-repeat; background-size: contain; background-position: center center;" >
				</div>
				<?php
					}
				?>
				<br>
				<div style="width:90%;margin: 0px auto;color:black;font-weight:400;line-height: 120%;font-size: 120%;">
				<?php
					echo $result2->comment;
				?>
				</div>
				<br>
		<?php
			}
		?>
		
		
		<div style="width:90%; margin: 0px auto;">
			<a href="mobile_articlelist.php?userid=<?php echo $_GET['userid']; ?>">
			<div style="
					color: black;
					background-color: white;
					border-style: solid;
					border-color: black;
					border-width: 1px;
					border-radius: 3px;
					text-align: center;
					height: 120%;
					font-size: 150%;
					display:inline-block;">
			 回上一頁</div>
			</a>
		</div>
	</div>	
	
	</body>
</html>
		
				