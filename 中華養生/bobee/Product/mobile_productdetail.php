<?php
	require_once(__DIR__.'/../dbtools.inc.php');

	session_start();
	
	if($_GET['id']) $id = $_SESSION['id'] = $_GET['id'];
	else $id = $_SESSION['id'];
?>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>
	
	<style>
	html{
	 -webkit-tap-highlight-color:rgba(0,0,0,0);
	}
	</style>

	<body>
	<div  class="col-md-4" style="width:100%;" >

		<?php
			$sql = "SELECT * FROM `products` WHERE id = ?";
			$sth = $db->prepare($sql);
			$sth->execute(array($id));
			
			$result = $sth->fetchobject();
		?>
		
		<div style="width:90%; height: 15em; margin: 0px auto; background:url(../mickey/<?php echo $result->photo_name; ?>) no-repeat; background-size: cover; background-position: center center;" >
		
		</div>
		<br>
		<div style="width:90%; margin: 0px auto; font-weight:600;line-height: 120%;font-size:150%; text-decoration: none;color:black">
		<?php
			echo $result->name;
		?>
		</div>
		<br>
		<div style="width:90%; margin: 0px auto; font-weight:600;line-height: 100%;font-size:100%; text-decoration: none;color:black">
		<?php
			echo $result->comment;
		?>
		</div>
		<br>
		<table style="width:90%;margin: 0px auto;">
			<tr>
			<td style="text-align:left; width:60%;">
				<div style = "font-size: 170%; color:orange;"><?php echo "NT$".$result->price; ?></div>
			</td>
			<td style="text-align:right; width:40%;">
				<div style="
					background:url(../mickey/into_cart.png) no-repeat;
					height: 5em;
					background-size: contain;
					background-position: center right;"
					onclick = "alert('addtocart<?php echo " ".$result->id." ".mb_substr(strip_tags($result->name),0,5,"UTF-8").(mb_strlen(strip_tags($result->name),"UTF-8") > 5? "..." : ""); ?>');"
				>
				</div>
			</td>
			</tr>
		</table>
		<br>
		<div style="width:90%; margin: 0px auto;">
			<a href="mobile_shoppingcart.php?id=<?php echo $_GET['userid']; ?>">
			<div style="
					color: black;
					background-color: white;
					border-style: solid;
					border-color: black;
					border-width: 1px;
					border-radius: 3px;
					text-align: center;
					height: 120%;
					font-size: 150%;
					display:inline-block;">
			 回購物車</div>
			</a>
		</div>
	</div>	
	</body>
</html>
		
				