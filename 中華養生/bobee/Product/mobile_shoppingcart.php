<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 
	
	session_start();
	
	if($_GET['id'] != "")
	{
		$id = $_SESSION['id'] = $_GET['id'];
	}
	else
		$id = $_SESSION['id'];
	
	if($_GET['remove'] != "")
	{
		$sql = "DELETE FROM `bills` WHERE id = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($_GET['remove']));
	}
?>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>
	
	<style>
	html{
	 -webkit-tap-highlight-color:rgba(0,0,0,0);
	}
	</style>

	<body>
	<div  class="col-md-4" style="width:100%; background-color: transparent;" >

		<?php
			$sql = "SELECT * FROM `bills` WHERE user_id = ? ORDER BY date DESC ";
			$sth = $db->prepare($sql);
			$sth->execute(array($id));
			
			while($result = $sth->fetchobject())
			{
				$sql2 = "SELECT * FROM `products` WHERE id = ?";
				$sth2 = $db->prepare($sql2);
				$sth2->execute(array($result->product_id));
				$result2 = $sth2->fetchobject();
		?>
		<div style="background-color:#ffffff; width:95%; margin: 0px auto; box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1);">
			<table style="width:100%;margin: 0px auto;">
				<tr>
					<td style="width:25%;">
						<a href="http://bobee.begoodlive.com/Product/mobile_productdetail.php?id=<?php echo $result->product_id; ?>&userid=<?php echo $id; ?>" style="text-decoration:none;">
							<div style="height: 5em; margin: 0px auto; background:url(../mickey/<?php echo $result2->photo_name; ?>) no-repeat; background-size: cover; background-position: center center;" >
							</div>
						</a>
					</td>
					<td style="width:55%;">
						<div style="margin: 0px auto; font-weight:600;line-height: 120%;font-size:80%; text-decoration: none;color:black">
						<?php
							echo "產品:&nbsp";
							echo mb_substr(strip_tags($result2->name),0,15,"UTF-8");
							if(mb_strlen(strip_tags($result2->name),"UTF-8") > 15) echo "...";
						?>
						</div>
						<div style="margin: 0px auto;font-size: 80%;">
						<?php
							echo "數量:&nbsp" . $result->num;
						?>
						</div>
						<div style="margin: 0px auto;font-size: 80%;">
						<?php
							echo "總價:&nbspNT\$" . $result2->price * $result->num;
						?>
						</div>
					</td>
					
					<td style="width:20%;">
						<div style="margin: 0px auto;">
							<a href="http://bobee.begoodlive.com/Product/mobile_shoppingcart.php?remove=<?php echo  $result->id ?>">
							<div style="
								background:url(../mickey/delete.png) no-repeat;
								height: 3em;
								background-size: contain;
								background-position: center center;">
							</div>
							</a>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php			
			}
		?>
	</div>	
	</body>
</html>
		
				