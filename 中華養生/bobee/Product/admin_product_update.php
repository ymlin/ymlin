<?php
	require_once(__DIR__.'/../dbtools.inc.php'); 

	session_start();
	
	$account = $_SESSION['account'];
	$password = $_SESSION['password'];	
	
	
	$sql = "SELECT * FROM `users`" 
			. " WHERE `account` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($account));
	
	$result = $sth->fetchobject();
	
	if($result && $password == decryptIt($result->password))
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("請先登入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	if($result && $result->admin == "1")
	{}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
			alert("非管理員禁止進入");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php
	}
	
	$sql = "SELECT * FROM `products`" 
			. " WHERE `id` = ?";
	$sth = $db->prepare($sql);
	$sth->execute(array($_GET['edit']));
	
	$result = $sth->fetchobject();
	$id = $result->id;
	$name = $result->name;
	$category = $result->category;
	$comment = $result->comment;
	$photo_name = $result->photo_name;
	$user_type = $result->user_type;
	$religion = $result->religion;
	$season = $result->season;
	$buddha = $result->buddha;
	$price = $result->price;
	$click_rate = $result->click_rate;
	$buy_rate = $result->buy_rate;
	$datetime = $result->datetime;
?>


<!DOCTYPE html>  
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<title>修改產品</title>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include __DIR__.'/../link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>  
<body style="font-size:15px;font-family: Microsoft JhengHei;letter-space:3px">
	<?php include __DIR__.'/../admin_top.php';?>
	<div class="container">  
		<form class='form-horizontal' action="admin_product_update_upload.php" method="post" enctype="multipart/form-data" name="myform">
			<fieldset>
				<legend>修改產品</legend>
				<div class="form-group">
					<label class="col-md-2 control-label" for='account'>名稱</label>
					<div class="controls col-md-6">
						<input  name="name"  type="text" size="40" value="<?php echo $name; ?>">
					</div>
				</div>
					  
				<div class="form-group">
					<label class="col-md-2 control-label">價錢</label>
					<div class="controls col-md-6">
						<input  name="price" type="text" size="20" value="<?php echo $price; ?>" />
					</div>
				</div>
				
				<div class="form-group">
				<label class="col-md-2 control-label" for="input01">詳細分類</label>
					<div class="controls col-md-6">
						<select name="type">
							<?php
								$sql = "SELECT * FROM `user_type`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									if($result->type == $user_type)
										echo "<option value=\"{$result->type}\" selected>{$result->type}</option>";
									else
										echo "<option value=\"{$result->type}\">{$result->type}</option>";
								}
							?>
						</select>
						<select name="religion">
							<?php
								$sql = "SELECT * FROM `religion`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									if($result->name == $religion)
										echo "<option value=\"{$result->name}\" selected>{$result->name}</option>";
									else
										echo "<option value=\"{$result->name}\">{$result->name}</option>";
								}
							?>
						</select>
						<select name="buddha">
							<?php
								$sql = "SELECT * FROM `buddha`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									if($result->name == $buddha)
										echo "<option value=\"{$result->name}\" selected>{$result->name}</option>";
									else
										echo "<option value=\"{$result->name}\">{$result->name}</option>";
								}
							?>
						</select>
						<select name="season">
							<?php
								$sql = "SELECT * FROM `season`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									if($result->name == $season)
										echo "<option value=\"{$result->name}\" selected>{$result->name}</option>";
									else
										echo "<option value=\"{$result->name}\">{$result->name}</option>";
								}
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
				<label class="col-md-2 control-label" for="input01">分類</label>
					<div class="controls col-md-6">
						<select name="category">
							<?php
								$sql = "SELECT * FROM `product_category`";
								$sth = $db->prepare($sql);
								$sth->execute();
								while($result = $sth->fetchobject())
								{
									if($result->category == $category)
										echo "<option value=\"{$result->category}\" selected>{$result->category}</option>";
									else
										echo "<option value=\"{$result->category}\">{$result->category}</option>";
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="input01">日期</label>
					<div class="controls col-md-6">
						<input name="datetime" id="datetimepicker1" type="text"  value="<?php echo $datetime; ?>" >
						<script language="JavaScript">
							$(document).ready(function(){ 
								var opt={dateFormat: 'yy-mm-dd',
									showSecond: true,
									timeFormat: 'HH:mm:ss'
									};
								$('#datetimepicker1').datetimepicker(opt);
							});
						</script>
					</div>
				</div>
			</fieldset>
			
			<hr>
			<div class="form-group">
				<label class="col-md-2 control-label" for="input01">介紹</label>
				<div class="controls col-md-6">
					<textarea name="comment" cols="85" rows="30"  ><?php echo $comment; ?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label">上傳圖片</label>
				<div class="controls col-md-6">
					<input type="hidden" name="MAX_FILE_SIZE" value="26214400">
					<input type="file" id="imgInp" name="myfile" size="50"><br><br>
					<script language="JavaScript">
						function readURL(input) {
							if (input.files && input.files[0]) {
								var reader = new FileReader();

								reader.onload = function (e) {
									$('#blah').attr('src', e.target.result);
									$('#blah').attr('style', "visibility:visible");
								}

								reader.readAsDataURL(input.files[0]);
							}
						}

						$("#imgInp").change(function(){
							readURL(this);
						});
					</script>
					<img id="blah" src="../mickey/<?php echo $photo_name;?>?=<?php echo $result->photo_name; ?>" alt="your image" />
				</div>
			</div>
			
			<div align="center">
				<button class='btn btn-primary' type="submit" name="id" value="<?php echo $id; ?>">上傳</button>
			</div>
		</form>

	</div>  
	
	
	
	<!-- script references -->
	<script src="js/bootstrap.min.js"></script>
</body>  
</html>