﻿<!doctype html>
<html>
	<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<?php
	require_once("dbtools.inc.php");
	session_start();
	if(isset( $_GET["room_num"])){
		$_SESSION["room_num"] = $_GET["room_num"];
	}
	$room_num=$_SESSION["room_num"];
	$sql = "SELECT * FROM vividpet_position order by id ASC";
	$result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	while ($row = mysqli_fetch_assoc($result))
	{
		
		$building_there = ""; 
		if($row['building']!='')
		{
			$buildings = explode ("-", $row['building']);  
			for($i = 0 ; $i < count($buildings) ; $i++)
			{
				$room_datas = explode ("_", $buildings[$i]);
				if($room_datas[0] - $room_num == 0)
				{
					if(count($room_datas) == 2) $building_there = $room_datas[1];
					else						$building_there = $room_datas[1]."_".$room_datas[2];
				}
			}
		}
		
		$owner_of_building = 0; 
		if($row['owner_id']!='')
		{
			$owner_ids = explode ("-", $row['owner_id']);  
			for($k = 0 ; $k < count($owner_ids) ; $k++)
			{
				$id_datas = explode ("_", $owner_ids[$k]);
				if($id_datas[0] ==  sprintf("%d", $room_num))
				{
					$owner_of_building = sprintf("%d", $id_datas[1]);
				}
			}
		}
	
		if($building_there == "bank+1")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/bank_1_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "bank+2")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/bank_2_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "bank+3")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/bank_3_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "company+1")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/company_1_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "company+2")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/company_2_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "company+3")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/company_3_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "market+1")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/market_1_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "market+2")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/market_2_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "market+3")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/market_3_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "hotel+1")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/hotel_1_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "hotel+2")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/hotel_2_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "hotel+3")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/hotel_3_<?php echo $owner_of_building ?>.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "bomb")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/bomb.png) ; padding:50px 50px;position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "cardmarket")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/cardmarket.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "hospital")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/hospital.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "buildbank_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/buildbank_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "bomb_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/bomb_card.png) ; padding:50px 50px;position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "buildcompany_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/buildcompany_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "buildmarket_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/buildmarket_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "buildhotel_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/buildhotel_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "missile_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/missile_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "change_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/change_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "rob_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/rob_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "destroy_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/destroy_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else if($building_there == "construct_card")
		{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(card/construct_card.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		else{
?>
		<div style="left:<?php echo $row['x_position'] ?>px;top:<?php echo $row['y_position'] ?>px;background: url(tool/green.png) ; padding:50px 50px; position: absolute;border-radius: 3px;">
		</div>
<?php
		}
		
	}
?>

<?php
	//Display all the players
	require_once("dbtools.inc.php");
	$sql = "SELECT * FROM vividpet_playerlist where room_num='$room_num'";
	$result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	
	while($row = mysqli_fetch_assoc($result))
	{
		$player_id = $row['id'];
		/*player type*/
		$sql2 = "SELECT * FROM vividpet_petlist WHERE id = '$player_id' ";
		$result2 = mysqli_query($wp_c,$sql2) or die(mysqli_error('error'));
		$rows2 = mysqli_fetch_assoc($result2);
		$player_type = $rows2['type'];
		/*player type*/
		
		$player_pos = $row['position'];
		/*player position*/
		$sql2 = "SELECT * FROM vividpet_position WHERE number = '$player_pos' ";
		$result2 = mysqli_query($wp_c,$sql2) or die(mysqli_error('error'));
		$rows2 = mysqli_fetch_assoc($result2);
		$player_position = $rows2;
		/*player position*/
		
		if($row['seq'] == 1)
		{
			$player1_x = $player_position['x_position'];
			$player1_y = $player_position['y_position'];
			$player1_type = $player_type;
		}
		else if($row['seq'] == 2)
		{
			$player2_x = $player_position['x_position'];
			$player2_y = $player_position['y_position'];
			$player2_type = $player_type;
		}
		else if($row['seq'] == 3)
		{
			$player3_x = $player_position['x_position'];
			$player3_y = $player_position['y_position'];
			$player3_type = $player_type;
		}
		else if($row['seq'] == 4)
		{
			$player4_x = $player_position['x_position'];
			$player4_y = $player_position['y_position'];
			$player4_type = $player_type;
		}
	}
	
	$sql = "SELECT * FROM vividpet_roomlist where room_num='$room_num'";
	$result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	$row = mysqli_fetch_assoc($result);
	$seq=$row['seq'];
	$room_state = $row['state'];
?>

	<style>	
	.player1
	{
		visibility: <?php echo $seq == 1 && $room_state == "Refresh"? "hidden":"visible"?>;
		left:<?php echo ($player1_x+25) ?>px;
		top:<?php echo ($player1_y+5) ?>px;
		width:63px;
		height:90px;
		background: url(<?php echo $player1_type?>.png) ;
		position:absolute;
	}
	
	.player2
	{
		visibility: <?php echo $seq == 2 && $room_state == "Refresh"? "hidden":"visible"?>;
		left:<?php echo ($player2_x+25) ?>px;
		top:<?php echo ($player2_y+5) ?>px;
		width:63px;
		height:90px;
		background: url(<?php echo $player2_type?>.png) ;
		position:absolute;
	}
	
	.player3
	{
		visibility: <?php echo $seq == 3 && $room_state == "Refresh"? "hidden":"visible"?>;
		left:<?php echo ($player3_x+25) ?>px;
		top:<?php echo ($player3_y+5) ?>px;
		width:63px;
		height:90px;
		background: url(<?php echo $player3_type?>.png) ;
		position:absolute;
	}
	
	.player4
	{
		visibility: <?php echo $seq == 4 && $room_state == "Refresh"? "hidden":"visible"?>;
		left:<?php echo ($player4_x+25) ?>px;
		top:<?php echo ($player4_y+5) ?>px;
		width:63px;
		height:90px;
		background: url(<?php echo $player4_type?>.png) ;
		position:absolute;
	}
	</style>	

	</head>
	
	<body style="background: url(back4.jpg); background-size:1360px 1260px;">	
		<div class="player1"> </div>
		<div class="player2"> </div>
		<div class="player3"> </div>
		<div class="player4"> </div>
			
		
	</body>
</html>