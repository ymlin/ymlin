<?php
/*----------------------------------------------------------------------------*/
/*---- bread view ----*/
function breview(){
       global $wp_c;
    
        $sql_bre = "SELECT * FROM brand_sec ORDER BY sec_ID ASC";
        $qu_bre = mysqli_query($wp_c, $sql_bre) or die(mysqli_error());
        $row_bre = mysqli_fetch_assoc($qu_bre);
             $cnc =0;
         do {
             $sec_id= $row_bre['sec_ID'];
             $sec_na= $row_bre['sec_Name'];

             echo <<<"tab"
                  <td>
                     <input name="d_bred[]" id="{d_bred[]}" value="{$sec_id}" type="checkbox">
                  </td>
                  <td>{$sec_id}</td>
                  <td><a href="bre_edit.php?br_d={$sec_id}" style="color:#313131">+ {$sec_na}</a></td>
tab;
             $cnc ++;if($cnc%1==0){echo '</tr><tr>';}     
         } while ($row_bre = mysqli_fetch_assoc($qu_bre));
}
/*----------------------------------------------------------------------------*/
/*---- bread Creat ----*/
function brecreat($msg){
    
          if($msg == "1"){
                 /* 名稱 */
	           $ed_bre= "cr_bre_name[]";
               $ed_bre1= "類別名稱";
              
             echo <<<"ts1"
              <div class="form-group">
                 <label class="col-sm-3 control-label font5" for="{$ed_bre}">{$ed_bre1}</label>
                 <div class="col-sm-5">
                    <input id="{$ed_bre}" name="{$ed_bre}" placeholder="{$ed_bre1}" class="input25" type="text">
                 </div>
              </div>
ts1;
          }elseif($msg == "2"){
               /* 備註 */
	           $ed_bre= "cr_bre_text[]";
               $ed_bre1= "類別備註";
              
             echo <<<"tsl"
                 <div class="form-group">
                 <label class="col-sm-3 control-label font5" for="{$ed_bre}">{$ed_bre1}</label>
                 <div class="col-sm-9">
                    <textarea id="{$ed_bre}" name="{$ed_bre}" placeholder="{$ed_bre1}" wrap="physical" rows="4" cols="50"></textarea>
                 </div>
                 </div>
tsl;
          }
}
/*----------------------------------------------------------------------------*/
/*----bread edit---- */
function breedit($breid, $msg){
       global $wp_c;
    
        $sql_edit = "SELECT * FROM brand_sec WHERE sec_ID = '{$breid}'";
        $qu_edit = mysqli_query($wp_c, $sql_edit) or die(mysqli_error());
        $row_edit = mysqli_fetch_assoc($qu_edit);
              
        switch ($msg){
          case "1":
            /* 類別名稱 */
	        $ed_bre= "sec_Name";
            $ed_bre_na= "類別名稱";
            $sql_na= $row_edit["sec_Name"];
            
            echo <<<"s1"
              <div class="form-group">
                 <label class="col-sm-3 control-label font5" for="{$ed_bre}">{$ed_bre_na}</label>
                 <div class="col-sm-9">
                    <input id="{$ed_bre}" name="{$ed_bre}" value="{$sql_na}" placeholder="{$sql_na}" class="input25" type="text">
                 </div>
              </div>
s1;
	      break;
          case "2":
	        /* 類別備註 */
	        $ed_bre= "sec_text";
            $ed_bre_na= "類別備註";
            $sql_na= $row_edit["sec_text"];
            
            echo <<<"tsl"
                 <div class="form-group">
                 <label class="col-sm-3 control-label font5" for="{$ed_bre}">{$ed_bre_na}</label>
                 <div class="col-sm-9">
                    <textarea id="{$ed_bre}" name="{$ed_bre}" placeholder="{$sql_na}" wrap="physical" rows="4" cols="50">$sql_na</textarea>
                 </div>
                 </div>
tsl;
	      break;
        }
}
/*----------------------------------------------------------------------------*/
/*---- bread category view ----*/
function bre_cat_view($papg1){
       global $wp_c;
    
        $sql = "SELECT * FROM brand_category LEFT JOIN brand_sec ON brand_sec.sec_ID = brand_category.sec_ID ORDER BY brand_category.sec_ID ASC";
        $qu_cat = mysqli_query($wp_c, $sql) or die(mysqli_error());
        $data_nums = mysqli_num_rows($qu_cat); //統計總比數
    
          $per = 10; //每頁顯示項目數量
          $pages = ceil($data_nums/$per); //取得不小於值的下一個整數
          if (isset($papg1) == 0){ //假如$_GET["page"]未設置
             $page=1; //則在此設定起始頁數
          }else{$page = intval($papg1);} //確認頁數只能夠是數值資料
          $start = ($page-1)*$per; //每一頁開始的資料序號
          $result = mysqli_query($wp_c, $sql.' LIMIT '.$start.', '.$per) or die(mysqli_error());
    
             $cnc =0;
          while ($row_cat = mysqli_fetch_assoc($result)) {
             $cat_id= $row_cat['brcate_id'];
             $cat_na= $row_cat['brcate_name'];
             $sec_na= $row_cat['sec_Name'];
             $cat_pri= $row_cat['brcate_shipping'];

             echo <<<"tab"
                  <td>
                     <input name="d_catd[]" id="d_catd[]" value="{$cat_id}" type="checkbox">
                  </td>
                  <td>{$sec_na}</td>
                  <td>{$cat_id}</td>
                  <td><a href="cat_edit.php?ca_d={$cat_id}">+ {$cat_na}</a></td>
                  <td>$ {$cat_pri}</td>
tab;
             $cnc ++;if($cnc%1==0){echo '</tr><tr>';}    
         }
          echo "</tbody>";
          echo "</table>";
          echo '<div class="pagenav">';
          echo '共 '.$data_nums.' 筆-在 '.$page.' 頁';
          echo "<br /><a href=?page=1>[第一頁]</a> ";
          for( $i=1 ; $i<=$pages ; $i++ ) {
             if ( $page-3 < $i && $i < $page+3 ) {
                echo "<a href=?page=".$i.">[".$i."]</a> ";
              }
          } 
          echo " <a href=?page=".$pages."> >>[最後一頁]</a>";
          echo "</div>";
}
/*----------------------------------------------------------------------------*/
/*---- bread category Creat ----*/
/*----------------------------------------------------------------------------*/
function catcreat($msg){
         global $wp_c;
         switch ($msg){
             case "0":
                 /* 類別 */
                 $sql_bre = "SELECT * FROM brand_sec ORDER BY sec_ID ASC";
                 $qu_bre = mysqli_query($wp_c, $sql_bre) or die(mysqli_error());
                 echo '<div class="col-sm-3 text-center font5">類別名稱</div>';
                 echo '<div class="col-sm-9 spn1">';
                 echo '<select id="cr_br_id[]" name="cr_br_id[]" class="input15">';
                 echo '<option value="0" selected="selected">請選擇</option>';
               while ($row_bre = mysqli_fetch_assoc($qu_bre)){
                 $bra_id=$row_bre['sec_ID'];
                 $bra_name=$row_bre['sec_Name'];
                 echo <<<"ts1"
                    <option value="{$bra_id}">{$bra_name}</option>
ts1;
               }
                 echo '</select></div>';
          break;
             case "1":
                 /* 名稱 */
	           $ed_cat= "cr_cat_name[]";
               $ed_cat1= "廠商名稱";
              
             echo <<<"ts1"
                 <div class="col-sm-3 text-center font5">{$ed_cat1}</div>
                 <div class="col-sm-9 spn1">
                    <input id="{$ed_cat}" name="{$ed_cat}" placeholder="{$ed_cat1}" class="input25" type="text">
                 </div>
ts1;
          break;
          case "2":
               /* 備註 */
	           $ed_cat= "cr_cat_text[]";
               $ed_cat1= "廠商備註";
              
             echo <<<"tsl"
                 <div class="col-sm-3 text-center font5">{$ed_cat1}</div>
                 <div class="col-sm-9 spn1">
                    <textarea id="{$ed_cat}" name="{$ed_cat}" placeholder="{$ed_cat1}" wrap="physical" rows="4" cols="30"></textarea>
                 </div>
tsl;
           break;
           case "3":
               /* 圖片 */
	           $ed_cat= "cr_cat_img[]";
               $ed_cat1= "廠商圖片";
              
             echo <<<"tsl"
                 <div class="col-sm-3 text-center font5">{$ed_cat1}</div>
                 <div class="col-sm-9 spn1">
                    <input id="{$ed_cat}" name="{$ed_cat}" placeholder="{$ed_cat1}" class="input25" type="text">
                 </div>
tsl;
           break;
           case "4":
               /* 圖片 */
	           $ed_cat= "cr_cat_pri[]";
               $ed_cat1= "訂購上限額";
              
             echo <<<"tsl"
                 <div class="col-sm-3 text-center font5">{$ed_cat1}</div>
                 <div class="col-sm-9 spn1">
                    <input id="{$ed_cat}" name="{$ed_cat}" placeholder="$ {$ed_cat1}" class="input25" type="text">
                 </div>
tsl;
           break;
           case "5":
                 /* 運費 */
                 $sql_shp = "SELECT * FROM oders_shipping ORDER BY od_sh_id ASC";
                 $qu_shp = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
                 echo '<div class="col-sm-3 text-center font5">運費選擇</div>';
                 echo '<div class="col-sm-9 spn1">';
                 echo '<select id="od_shid[]" name="od_shid[]" class="input15">';
                 echo '<option value="0" selected="selected">請選擇</option>';
               while ($row_shp = mysqli_fetch_assoc($qu_shp)){
                 $shp_id=$row_shp['od_sh_id'];
                 $shp_name=$row_shp['od_sh_name'];
                 $shp_pri=$row_shp['od_sh_price'];
                 echo <<<"ts1"
                    <option value="{$shp_id}">{$shp_name}'$'{$shp_pri}</option>
ts1;
               }
                 echo '</select></div>';
          break;
       }/* switch end */
}
/*----------------------------------------------------------------------------*/
/*---- bread category Creat 2---- */
function catcreat2(){
            /* 備註 */
	        $ed_cat= "cr_cat_text[]";
            $ed_cat1= "廠商備註";
            $ed_red= '<code>貼圖語法:&lt;img src="http://shopping.begoodlive.com/mall/p1/圖檔名稱.jpg" alt="" class="img-responsive"><br> 文字左右對齊:stely="text-align:justify"例&lt;p stely="text-align:justify">xxx&lt;/p></code>';

           echo <<<"s55"
                 <div class="col-sm-12">
                    <div class="font5" style="padding-left:6em;margin-bottom:1em;">{$ed_cat1}</div>
                    <div style="padding-left:10em;margin-bottom:1em;">
                       <textarea name="{$ed_cat}"></textarea>
                       <br>$ed_red
                    </div>
                 </div>
s55;
}
/*----------------------------------------------------------------------------*/
/*---- bread category Edit ----*/
function catedit($msg, $catid){
         global $wp_c;
                $sql_cat = "SELECT * FROM brand_category WHERE brcate_id = '{$catid}'";
                $qu_cat = mysqli_query($wp_c, $sql_cat) or die(mysqli_error());
                $row_cat = mysqli_fetch_assoc($qu_cat);
                
                  $brnid=$row_cat['sec_ID'];
                  $shpid=$row_cat['od_sh_id'];
                  $cat_name=$row_cat['brcate_name'];
                  $cat_img=$row_cat['brcate_img'];
                  $cat_pri=$row_cat['brcate_shipping'];
                  $cat_text=$row_cat['brcate_text'];
                  $ed_red= '<code>貼圖語法:&lt;img src="http://shopping.begoodlive.com/mall/p1/圖檔名稱.jpg" alt="" class="img-responsive"></code>';
         switch ($msg){
             case "0":
                 /* 類別 */
                $sql_bre = "SELECT * FROM brand_sec";
                $qu_bre = mysqli_query($wp_c, $sql_bre) or die(mysqli_error());
             
                  echo '<div class="col-sm-3 text-center font3">類別名稱</div>';
                  echo '<div class="col-sm-9 spn1">';
                  echo '<select id="ed_br_id" name="ed_br_id" class="input15">';
               while ($row_bre = mysqli_fetch_assoc($qu_bre)){
                  $bra_id=$row_bre['sec_ID'];
                  $bra_name=$row_bre['sec_Name'];
                  $sel = '';
                  if($brnid == $bra_id){$sel= 'selected="selected"';}
                  echo <<<"ts1"
                    <option value="{$bra_id}" {$sel}>{$bra_name}</option>
ts1;
               }
                  echo '</select></div>';
             break;
             case "1":
                  $ed_cat= "edcat_name";
                  $ed_cat1= "廠商名稱";
                  echo <<<"tsl"
                    <div class="col-sm-3 text-center font5">廠商名稱</div>
                    <div class="col-sm-9 spn1">
                      <input id="{$ed_cat}" name="{$ed_cat}" value="{$cat_name}" placeholder="{$cat_name}" class="input25" type="text">
                    </div>
tsl;
             break;
             case "2":
                  $ed_cat= "edcat_text";
                  $ed_cat1= "廠商備註";
                  echo <<<"tsl"
                    <div class="col-sm-3 text-center font5">廠商備註</div>
                    <div class="col-sm-9 spn1">
                      <textarea id="{$ed_cat}" name="{$ed_cat}" value="{$cat_text}" placeholder="{$ed_cat1}" wrap="physical" rows="4" cols="30"></textarea>
                 </div>
                    </div>
tsl;
             break;
             case "3":
                  $ed_cat= "edcat_img";
                  $ed_cat1= "廠商圖片";
                  echo <<<"tsl"
                    <div class="col-sm-3 text-center font5">廠商圖片</div>
                    <div class="col-sm-9 spn1">
                      <input id="{$ed_cat}" name="{$ed_cat}" value="{$cat_img}" placeholder="{$ed_cat1}" class="input25" type="text"><br>{$ed_red}
                    </div>
tsl;
             break;
             case "4":
                  $ed_cat= "edcat_pri";
                  $ed_cat1= "訂購上限額";
                  echo <<<"tsl"
                    <div class="col-sm-3 text-center font5">訂購上限額</div>
                    <div class="col-sm-9 spn1">
                      <input id="{$ed_cat}" name="{$ed_cat}" value="{$cat_pri}" placeholder="{$ed_cat1}" class="input25" type="text">
                    </div>
tsl;
             break;
             case "5":
                 /* 運費 */
                $sql_shp = "SELECT * FROM oders_shipping";
                $qu_shp = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
             
                  echo '<div class="col-sm-3 text-center font3">運費選擇</div>';
                  echo '<div class="col-sm-9 spn1">';
                  echo '<select name="ed_shid" class="input15">';
               while ($row_shp = mysqli_fetch_assoc($qu_shp)){
                  $shp_id=$row_shp['od_sh_id'];
                  $shp_name=$row_shp['od_sh_name'];
                  $shp_pri=$row_shp['od_sh_price'];
                  $sel = '';
                  if($shpid == $shp_id){$sel= 'selected="selected"';}
                  echo <<<"ts1"
                    <option value="{$shp_id}" {$sel}>{$shp_name}'$'{$shp_pri}</option>
ts1;
               }
                  echo '</select></div>';
             break;
         }
}
/*----------------------------------------------------------------------------*/
/*---- bread category Edit 2---- */
function catedit2($catid){
         global $wp_c;
           $sql_cat = "SELECT * FROM brand_category WHERE brcate_id = '{$catid}'";
           $qu_cat = mysqli_query($wp_c, $sql_cat) or die(mysqli_error());
           $row_cat = mysqli_fetch_assoc($qu_cat);
    
            /* 品牌備註 */
	        $ed_cat= "edcat_text";
            $ed_cat1= "廠商備註";
            $cat_text=$row_cat['brcate_text'];
            $ed_red= '<code>貼圖語法:&lt;img src="http://shopping.begoodlive.com/mall/p1/圖檔名稱.jpg" alt="" class="img-responsive"><br> 文字左右對齊:stely="text-align:justify"例&lt;p stely="text-align:justify">xxx&lt;/p></code>';

           echo <<<"s55"
                 <div class="col-sm-12">
                    <div class="font5" style="padding-left:2em;margin-bottom:1em;">{$ed_cat1}</div>
                    <div style="padding-left:10em;margin-bottom:1em;">
                       <textarea name="{$ed_cat}">{$cat_text}</textarea>
                       <br>$ed_red
                    </div>
                 </div>
s55;
}
?>