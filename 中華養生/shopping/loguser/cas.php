<?php
// 自己的密碼對照表與演算法
function num2text($num) {
    $text = "";
   
    for ($i = 0; $i < strlen($num) - 1; $i++) {
        $shift = substr($num, $i, 1);
        $char = substr($num, $i + 1, 1);
        $key = $char + $shift;
       
        switch ($key) {
            case '0': $text .= "a"; break;
            case '1': $text .= "B"; break;
            case '2': $text .= "3"; break;
            case '3': $text .= "D"; break;
            case '4': $text .= "E"; break;
            case '5': $text .= "F"; break;
            case '6': $text .= "G"; break;
            case '7': $text .= "H"; break;
            case '8': $text .= "i"; break;
            case '9': $text .= "J"; break;
            case '10': $text .= "K"; break;
            case '11': $text .= "Z"; break;
            case '12': $text .= "7"; break;
            case '13': $text .= "N"; break;
            case '14': $text .= "4"; break;
            case '15': $text .= "5"; break;
            case '16': $text .= "Q"; break;
            case '17': $text .= "R"; break;
            case '18': $text .= "8"; break;
            case '19': $text .= "T"; break;
        }
    }
   
    return $text;
}

    // 檔案類型
    Header("Content-type: image/PNG");

    // 產生種子, 作圖形干擾用
    srand((double)microtime()*1000000);

    // 產生圖檔, 及定義顏色
    $im = imageCreate(70, 28);
    $back = ImageColorAllocate($im, 255, 255, 204);
    $font = ImageColorAllocate($im, 10, 10, 10);
    $point = ImageColorAllocate($im, 204, 0, 51);

    // 自己的密碼對照表與演算法
    $authText = num2text($HTTP_GET_VARS['authnum']); // 轉換
   
    // 填入圖檔底色, 及寫入驗證文字至圖檔中
    imageFill($im, 0, 0, $back);
    imageString($im, 5, 10, 8, $authText, $font);

    // 插入圖形干擾點共 50 點, 可插入更多, 但可能會使圖形太過混雜
    for($i = 0; $i < 50; $i++) {
        imagesetpixel($im, rand() % 70 , rand() % 28 , $point);
    }

    // 定義圖檔類型並輸入, 最後刪除記憶體
    ImagePNG($im);
    ImageDestroy($im);
?>