<html>
<head>
	<title>卡拉OK點唱機</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php include 'link.php';?>
	<style>  
	#heig{  
		 height:10px;  
	}  
	#color{  
		 background-color: #666;  
		 color: #FFF;  
	}  
	#color2{  
		 background-color: #999;  
	}  
	</style>
</head>

<body>
	<script>
	function okPlay(mp3filename) {
		au = document.getElementById("mp3play");
		au.src = mp3filename;
		au.volume = .5;
		au.play();
	}
	</script>
	<style>
	table {
		border-collapse: collapse;
	}

	table, th, td {
		border: 1px solid black;
	}
	</style>
	
	<div class="container">  
		<table align="center" width="1200" border="1" class="table table-bordered table-condensed table-striped table-hover" >
			<tr><th colspan="4"><h2 align="center">卡拉OK點唱機</h2></th></tr>
			<tr>
				<td align="center">順序</td>
				<td align="center">歌名</td>
				<td align="center">檔名</td>
				<td align="center">action</td>
			</tr>
			
			<form method="post">
			<?php 
				require_once("dbtools.inc.php");
				
				$sql = "SELECT * FROM `song` ORDER BY id";
				$sth = $db->prepare($sql);
				$sth->execute();
				
				for($counter = 1 ; $result = $sth->fetchobject() ; $counter++)
				{
					echo '<tr>';
					echo  '<td align="center">'.$counter.'</td>'
						, '<td align="center">'.$result->name.'</td>'
						, '<td align="center">'.$result->filename.'</td>'
						, '<td align="center">';
			?>
				<input type="button" value="播放" class='btn btn-primary' onclick="okPlay('mickey/<?php echo  $result->filename ?>')">
				<input type="button" value="刪除" class='btn btn-primary' onclick="location.href='songplayer_delete.php?remove=<?php echo  $result->id ?>'">
			<?php
					echo '</td>';
					echo '</tr>';
					$count++;
				}
			?>
			</form>
			
			<tr>
			<form action="songplayer_add.php" method="post" enctype="multipart/form-data">
				<td align="center">加入歌曲</td>
				<td align="center"><input  name="name"  type="text" size="20" value="" align="center"></td>
				<td align="center">
					<input type="hidden" name="MAX_FILE_SIZE" value="26214400">
					<input type="file" id="songInp" name="myfile" size="50">
				</td>
				<td align="center"><input type="submit" value="新增歌曲" class='btn btn-primary'></td>
			</form>
			</tr>
		</table>
		<div align="center"><audio id="mp3play" src="" controls="controls" autoplay="autoplay"></audio></div>
	</div>
</body>
</html>
