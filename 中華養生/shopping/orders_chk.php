<?php @require_once("conn/wp-c.php");?>
<?php @require_once('functions/EDcart.php');?>
<?php @require_once("functions/fundb.php");?>
<?php $dir_s1="1";?><!-- login -->
<?php @require_once("loguser/use_chk.php");?>
<?php $cart =& $_SESSION['edCart'];//shopping cart;
if(!is_object($cart)) $cart = new edCart();
if($cart->itemcount == 0){header("Location:index.php");exit;}//Empty cart;
?><!-- END -->
<?php @require_once("functions/funlist.php");?>
<?php @require_once("functions.php");?>
<?php @require_once("header.php");?>
<!-- 禁止回上頁的功能 -->
<script> 
	function forward() { 
	    setTimeout("forward()",100); 
	    history.forward(); 
	} 
	if (document.all) 
	    history.forward();  //for ie 
	else     
	    setTimeout("forward()",100);  //for firefox 
	//---禁回此頁End--- 
</script>
<script type="text/javascript">
   $(function(){
      $("#cr_od_send").click(function(){
      var name = $("#pro_ty2").val();
      if( name==''||name==0){
         alert('提醒: 商品的規格請選取，並點選~更新購物車~!');
         return false;
       }
       });
   });
</script><!-- pass END -->
 <!-- feature -->
 <div id="features-wrapper">
   <div class="container"><!--內容區 -->
     <article class="row clearfix"><!-- 對齊中間 -->
        <div class="ord_list">
           <div class="col-lg-12 column"> 
              <form class="form-horizontal" action="./functions/funorder_u.php" method="post" name="commentForm" id="commentForm">
              <!---- setp 1 ---->
              <div class="col-md-12 hline">
                 <div class="col-sm-6 font3 spn2">Setp 1 <small>購買數量與金額</small></div>
                 <!-- 商品名 -->
                 <div class="col-sm-12 table-responsive">
                    <table class="table">
                      <?php
                         if($cart->itemcount > 0) {
	                     foreach($cart->get_contents() as $item) {
                      ?>
                      <tr>
                            <td><?php echo $item['info'].''.port_pro_type3($item['type']);?></td>
                            <td width="13%"><div class="font5">單價 : NT$ <?php echo number_format($item['price']);?>元</div></td>
                            <td width="10%"><div class="font5">數量 : <?php echo $item['qty'];?> 件</div></td>
                            <td width="10%"><div class="font5">NT$ <?php echo number_format($item['subtotal']);?>元</div></td>
                            <!-- <td width="10%"><div class="font8">運費 : <?php echo $item['freight'];?>元</div></td>-->
                      </tr>
                      <?php }}?>
                         <!-- ECoupon area -->
                      <tr><?php sql_subsidy($_SESSION['sub_id']);?></tr>
                    </table>
                    <table class="table table-bordered">
                           <!-- bread shopping area -->
                      <?php foreach($cart->get_bread() as $brem) {?>
                      <tr>
                         <td colspan="3">品牌運費: <?php echo sql_brand_cat($brem['brid']);?> (<?php echo sql_outshpping($brem['proid']);?>-配送)</td>
                         <td width="10%"><div class="font5">NT$ <?php echo odr_shipping_1($brem['total'], $brem['proid']);?>元</div></td>                             
                      </tr>
                      <?php }?>
                    </table>
                         <!-- total area -->
                         <div class="cart_total text-right">合計NT$ : <?php  echo number_format($cart->grandtotal3);?>元</div>
                 </div>
              </div>
              <!---- setp 2 ---->
              <div class="col-md-12 hline">
                 <div class="col-sm-12 font3 spn2">Setp 2 <small>訂購人及收件人</small></div>
                 <!-- 訂購姓名-->
                 <div class="col-sm-6">
                    <div class="col-sm-12 font6 spn1"><u>訂購人資料</u></div>
                    <div class="col-sm-5 text-center font6 spn1">
                       姓   名 : <?php echo $_SESSION['cr_f_na'];?> <small>先生/小姐</small></div>
                    <!-- 訂購聯絡電話-->
                    <div class="col-sm-12 font6 spn1">
                       聯絡電話 : <?php echo $_SESSION['cr_v_ph'];?>
                    </div>
                    <!-- 訂購聯絡地址-->
                    <div class="col-sm-12 font6 spn1">
                       聯絡地址 : <?php echo $_SESSION['cr_v_ad'];?>
                    </div>
                 </div>
                 <!---- 收件人data -->
                 <div class="col-sm-6">
                    <div class="col-sm-12 font6 spn1"><u>收件人資料</u></div>
                    <div class="col-sm-5 text-center font6 spn1">
                       姓   名 : <?php echo $_SESSION['cr_f_na'];?> <small>先生/小姐</small></div>
                    <!-- 收件人電話-->
                    <div class="col-sm-12 font6 spn1">
                       聯絡電話 : <?php echo $_SESSION['cr_f_ph'];?>
                    </div>
                    <!-- 收件人地址-->
                    <div class="col-sm-12 font6 spn1">
                       聯絡地址 : <?php echo $_SESSION['cr_f_ad'];?>
                    </div>
                    <!-- 備註-->
                    <div class="col-sm-12 font6 spn1">
                       備   註 : <?php echo $_SESSION['cr_f_tx'];?>
                    </div>
                 </div>
              </div>
              <!---- setp 3 ---->
              <div class="col-md-12 hline">
                 <div class="col-sm-6 font3 spn2">Setp 3 <small>付款方式</small></div> 
                    <!-- 付款方式-->
                    <div class="col-sm-12 font6 spn1">
                       付款方式 : <?php echo $_SESSION['cr_o_pa'];?>
                    </div>
                    <!-- 運送方式 -->
                    <!--<div class="col-sm-12 font6 spn1">
                       主要運送方式 : <?php echo sql_outshpping(3);?>
                    </div> -->
                    <!-- Button -->                             
                    <div class="col-md-12">
                       <div class="text-right">
                          <form method="post">
                            <button name="cr_od_use" value="cr_od_use"class="btn button1" type="button" onClick="this.form.action='orders_use.php';this.form.submit();" >回上頁</button>
                           </form>
                           <button name="cr_od_send" value="cr_od_send" class="btn">確認訂購</button>
                       </div>
                    </div>
              </div>
              </form>
            </div>
          </div> 
       </article>
    </div><!--內容 end-->
  </div><!-- feature end-->
</div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->