<?php
	require_once('dbtools.inc.php'); 
?>

<!DOCTYPE html>  
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	</head>

	<body>
	<div  class="col-md-4" >
		
		<header>
		<div style="margin: 0px auto; font-size:180%;font-weight: 900; text-align: center;">
			最新文章
		</div>	
		<hr>
		</header>

		<?php
					$sql = "SELECT * FROM `articles`";
					$sth = $db->prepare($sql);
					$sth->execute();
					
					while($result = $sth->fetchobject())
					{
		?>
		<div style="width:80%; margin: 0px auto; padding: 20px 20px 20px 20px;box-shadow: 0px 0px 1px 2px rgba(128,128,128,0.1);">
			<div class="row" style="color:#000000;font-weight:400;line-height: 25px;">
			<?php
				strip_tags($result->comment, '<br>');
				echo mb_substr($result->comment,0,100,"UTF-8");
				if(mb_strlen($result->comment,"UTF-8") > 100) echo "...";
			?>
			</div>
		</div>
		<?php			
					}
		?>
	</div>	
	</body>
</html>
		
				