<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>檔案更名</title>
</head>
<body>
<?php
if (isset($_GET["File"]))  // 取得URL參數
	$file=$_GET["File"];
else
	$file = "demo.bak";
// 檢查檔案是否存在
if ( file_exists($file)) {
   rename($file, "demo.txt");  // 檔案更名
   print "檔案: $file 更名成 demo.txt<br>";
}
?>
</body>
</html>