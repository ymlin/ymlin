﻿<link rel="shortcut icon" href="./images/favicon.png"/>
	    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, minimal-ui">
      <meta name="format-detection" content="telephone=no">
      <meta name="description" content="An awesome solution for responsive tables with complex data.">
		  <link rel="shortcut icon" href="./images/favicon.png"/>
      <!-- Font Awesome -->
      <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

		  <!-- Latest compiled and minified Bootstrap CSS -->
      <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">