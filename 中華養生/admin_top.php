<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="http://begoodlive.com/">Home</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Admin.php">管理員頁面</a></li>
      <li><a href="Admin_edit.php">全部文章</a></li>
      <li><a href="edit.php">新增文章</a></li>
	   <li><a href="Admin_activity.php">全部活動</a></li>
	   <li><a href="edit_activity.php">新增活動</a></li>
      <li><a href="edit_keyword.php">新增關鍵字</a></li>
	<li><a href="Admin_mem.php">帳號</a></li>
	<li><a href="logout.php">登出</a></li>
    </ul>
  </div>
</nav>