﻿<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
	 
	<head>
      <meta charset="utf-8" />
		 
      <title>Responsive tables</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, minimal-ui">
      <meta name="format-detection" content="telephone=no">
      <meta name="description" content="An awesome solution for responsive tables with complex data.">
		  <link rel="shortcut icon" href="./images/favicon.png"/>
      <!-- Font Awesome -->
      <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

		  <!-- Latest compiled and minified Bootstrap CSS -->
      <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

     
		<style>
    #wrap{min-height: 100%;
      height: auto;
      /* negative indent footer by its heigth*/
      margin: 0 auto -60px;
      /* pad bottom by footer height*/
      padding: 0 0 60px;}
	  </style>
	</head>

   <body data-spy="scroll" data-target="#navbar" data-offset="50">
      <div style="margin-bottom:90px">
	  <div id="navbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <span class="navbar-brand"><span class="fa fa-table"></span><a href='index.php'>Home</a></span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
			 <li><a href="Admin.php">管理員專區</a></li>
			 
			  <li class="dropdown">
                     <a href="edit_keyword.php" class="dropdown-toggle" data-toggle="dropdown">經典分類<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_keyword.php" tabindex="-1">全部經典</a></li>
                         <li><a href="edit_keyword.php" tabindex="-1">新增經典分類</a></li>
                     </ul>
                   </li>
			 <?php
  require_once("dbtools.inc.php");
    $sql = "SELECT * FROM radio_set order by id ASC";
       $result= mysqli_query($wp_c,$sql) or die(mysqli_error('error'));
	   	while ($row = mysqli_fetch_assoc($result)){
  ?>
           <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $row['name'] ?><strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_<?php echo $row['id'] ?>.php" tabindex="-1">全部<?php echo $row['name'] ?></a></li>					
                        <li><a href="edit_<?php echo $row['id'] ?>.php" tabindex="-1">新增<?php echo $row['name'] ?>章節</a></li>
						  
                     </ul>
                   </li>
				   <?php
				   }
				 ?>
				 <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">圖片<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="add_picture.php" tabindex="-1">新增圖片</a></li>
                       
                     </ul>
                   </li>
				  
             
              <li><a href="logout.php">登出</a></li>
            </ul>
          </div>
        </div>
    </div>
   </div>
  
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

      <!-- Latest compiled and minified Bootstrap JavaScript -->
      <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

      <script src="js/rwd-table.min.js?v=5.0.3"></script>
   
		
	</body>
	
</html>
