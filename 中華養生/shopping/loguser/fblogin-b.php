<?php
//header('Content-type: text/html; charset=utf-8');
 
require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookRedirectLoginHelper.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/GraphUser.php');
require_once('Facebook/GraphSessionInfo.php');

require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'Facebook/Entities/AccessToken.php' );
require_once( 'Facebook/Entities/SignedRequest.php' );
 
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;

if (!isset($_SESSION)) {session_start();}
// init app with app id (APPID) and secret (SECRET)
FacebookSession::setDefaultApplication('857203747680238','add0747a89b351d70a4c482475404d32' );
 
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper( 'http://localhost/shopping/loguser/fblogin.php' );
 
// see if a existing session exists
if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
  // create new session from saved access_token
  $session = new FacebookSession( $_SESSION['fb_token'] );
  
  // validate the access_token to make sure it's still valid
  try {
    if ( !$session->validate() ) {
      $session = null;
    }
  } catch ( Exception $e ) {
    // catch any exceptions
    $session = null;
  }
}

if ( !isset( $session ) || $session === null ) {
  // no session exists
  
  try {
    $session = $helper->getSessionFromRedirect();
  } catch( FacebookRequestException $ex ) {
    // When Facebook returns an error
    // handle this better in production code
    print_r( $ex );
  } catch( Exception $ex ) {
    // When validation fails or other local issues
    // handle this better in production code
    print_r( $ex );
  }
  
}

// see if we have a session
if (isset($session)) {
  // save the session
  $_SESSION['fb_token'] = $session->getToken();
  // create a session using saved token or the new one we generated at login
  $session = new FacebookSession( $session->getToken() );
  
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject()->asArray();
  
  // print profile data
  echo '<pre>' . print_r( $graphObject, 1 ) . '</pre>';
    
    //echo $graphObject->getProperty('id').'<br>';
    $udbemail = base64_encode($graphObject->getProperty('email'));
    $udbname = base64_encode($graphObject->getProperty('name'));
    $udbid= base64_encode($graphObject->getProperty('id'));
    //header("Location:REfresh_other.php?fu=" . $udbemail."&n=" . $udbname."&d=" . $udbid);
    echo '<a href="' . $helper->getLogoutUrl( $session, 'http://localhost/shopping/logout.php' ) . '">Facebook Logout</a>';
    exit;
} else {
  //$_SESSION = array(); 
  // show login url
  //'<a href="' . $helper->getLoginUrl() . '">Login</a>';
  $fblg1 = '<a href="' . $helper->getLoginUrl( array( 'email', 'user_friends' ) ) . '">in</a>';
  $fblg2 =  $helper->getLoginUrl();
}
?>