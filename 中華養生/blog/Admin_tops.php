﻿ <div style="margin-bottom:90px">
	  <div id="navbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <span class="navbar-brand"><span class="fa fa-table"></span><a href='index.php'>Home</a></span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
			 <li><a href="Admin.php">管理員專區</a></li>
           <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">講師文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit.php" tabindex="-1">全部講師文章</a></li>					
                        <li><a href="edit.php" tabindex="-1">新增講師文章</a></li>
						  
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">學員文章<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_student.php" tabindex="-1">全部學員文章</a></li>
						 <li><a href="edit_student.php" tabindex="-1">新增學員文章</a></li>
                       
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">講師影音<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_film.php" tabindex="-1">全部影音</a></li>
						  <li><a href="edit_film.php" tabindex="-1">新增影音</a></li>                       
                     </ul>
                   </li>
				    <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">書院活動<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_edit_htz.php" tabindex="-1">全部書院活動</a></li>
						  <li><a href="edit_htz.php" tabindex="-1">新增書院活動</a></li>                       
                     </ul>
                   </li>
				   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">關鍵字<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_keyword.php" tabindex="-1">全部關鍵字</a></li>
                         <li><a href="edit_keyword.php" tabindex="-1">新增關鍵字</a></li>
                     </ul>
                   </li>
              <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">帳號<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="Admin_mem.php" tabindex="-1">全部帳號</a></li>
                       
                     </ul>
                   </li>
				 <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">圖片<strong class="caret"></strong></a>
                     <ul class="dropdown-menu">
                        <li><a href="add_picture.php" tabindex="-1">新增圖片</a></li>
                       
                     </ul>
                   </li>
              <li><a href="logout.php">登出</a></li>
            </ul>
          </div>
        </div>
    </div>
   </div>