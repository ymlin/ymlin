<?php
	require_once("dbtools.inc.php"); 
	
	session_start();

	
	$account = $_SESSION['account'] = $_POST['account'];
	$password = $_SESSION['password'] = $_POST['password'];
	$type = $_POST['type'] == "personal"? "個人" : "公司";

	if($account && $password)
	{
		$sql = "SELECT * FROM users WHERE account = ?";
		$sth = $db->prepare($sql);
		$sth->execute(array($account));
		$result = $sth->fetchobject();
	}
	else
	{
?>
		<script type='text/javascript' charset="UTF-8">
		alert('帳號或密碼不可為空');
		window.location.assign("HTTP://bobee.begoodlive.com/register.php");
		</script>
<?php
	}
	
	if ($result)
	{
?>
		<script type='text/javascript' charset="UTF-8">
		alert('帳號已被使用');
		window.location.assign("HTTP://bobee.begoodlive.com/register.php");
		</script>
<?php	
	}
	else
	{
		$hash = encryptIt($password);
		$currenttime = date("Y-m-d H:i:s");
		$sql = "INSERT INTO users (id, account, password, name, admin, lastlogintime, user_type ) VALUES ('', ?, ?, ?, '0', ?, ?)";
		$sth = $db->prepare($sql);
		$sth->execute(array($account,$hash, $account,$currenttime,$type));
		
?>
		<script type='text/javascript' charset="UTF-8">
			alert("申請成功！");
			window.location.assign("HTTP://bobee.begoodlive.com");
		</script>
<?php	
	}
?>