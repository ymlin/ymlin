<?php
      require_once("dbtools.inc.php");
			
      //指定每頁顯示幾筆記錄
      $records_per_page = 3;

      //取得要顯示第幾頁的記錄
      if (isset($_GET["page"]))
        $page = $_GET["page"];
      else
        $page = 1;

      
			
      //執行 SQL 命令
      $sql = "SELECT * FROM message  ORDER BY date DESC";	
      $result = mysqli_query($wp_c,$sql) or die(mysqli_error('error'));

      //取得記錄數
      $total_records = mysqli_num_rows($result);

      //計算總頁數
      $total_pages = ceil($total_records / $records_per_page);

      //計算本頁第一筆記錄的序號
      $started_record = $records_per_page * ($page - 1);

      //將記錄指標移至本頁第一筆記錄的序號
      mysqli_data_seek($result, $started_record);

      //使用 $bg 陣列來儲存表格背景色彩
      ?>
<div style="border: 1px solid #E1E1E8;padding: 10px 14px;border-radius:15px;">
<?php
      echo "<table width='800' align='center' cellspacing='3'>";

      //顯示記錄
      $j = 1;
      while ($row = mysqli_fetch_assoc($result) and $j <= $records_per_page)
      {
        echo "<tr bgcolor='" . $bg[$j - 1] . "'>";
        echo "<td width='12' align='center'>
             ";
        echo "<td>您的姓名：" .  $row["author"] . "<br>";
        echo "您的email：" . $row["subject"] . "<br>";
        echo "時間：" . $row["date"] . "<hr>";
        echo "<h3>留言內容：</h3>".$row["content"] . "<hr></td></tr>";
        $j++;
      }
      echo "</table>" ;

      //產生導覽列
      echo "<p align='center'>";

      if ($page > 1)
        echo "<a href='index.php?page=". ($page - 1) . "'>上一頁</a> ";

      for ($i = 1; $i <= $total_pages; $i++)
      {
        if ($i == $page)
          echo "$i ";
        else
          echo "<a href='index.php?page=$i'>$i</a> ";
      }

      if ($page < $total_pages)
        echo "<a href='index.php?page=". ($page + 1) . "'>下一頁</a> ";
      echo "</p>";

     
    ?>
    <form name="myForm" method="post" action="post.php">
      <table border="0" width="800" align="center" cellspacing="0">
        <tr  align="center">
          <td colspan="2">
            <font color="#FFFFFF">請在此輸入新的留言</font></td>
        </tr>
        <tr >
          <td width="15%">您的姓名</td>
          <td width="85%"><input name="author" type="text" size="50"></td>
        </tr>
        <tr >
          <td width="15%">您的email</td>
          <td width="85%"><input name="subject" type="text" size="50">
		  
		  <input type="hidden" name="category" value="1"></td>
        </tr>
        <tr >
          <td width="15%">留言內容</td>
          <td width="85%"><textarea name="content" cols="50" rows="5"></textarea></td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" value="張貼留言" >　
            <input type="reset" value="重新輸入">
          </td>
        </tr>
      </table>
    </form>
	</div>