<!DOCTYPE HTML>
<!--
	Prologue by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Prologue by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		 <noscript>
				<link rel="stylesheet" href="css/skel.css" />
				<link rel="stylesheet" href="css/style.css" />
				<link rel="stylesheet" href="css/style-desktop.css" />
		   </noscript>
		<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/bootstrap-theme.min.css" rel="stylesheet" >
			<?php include 'index_head.php';?>
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	  <style>  
	
.home_list .hline{padding:0.5em;border:1px solid #E1E1E1; border-radius:6px;}
.home_list .hline2{padding:0.5em;border:1px solid #E1E1E1; border-radius:6px;margin-right:45px;margin-left:45px}
 .home_list .font{font-size: 1.2em;letter-spacing:0.2em;text-align:left;}
 .home_list .font2{letter-spacing:0.2em;text-align:center;}
 </style>  
 <script src="js/scrollfix-home.js"></script>
<script type="text/javascript">
        $('.navbar').scrollFix();
</script>
	</head>
	<body>
<hr>

   <div id="footer-wrapper" class="footer">
  <div class="container">
    <div class="row">
        <div class="col-md-12 column text-center">
           <a href="login.php" target="_parent">登入</a>
<link rel="shortcut icon" href="./images/favicon.png"/>
        </div>
    </div>
  </div>
</div> 
<div class="container">
		<div class="row">
			<div class="col-md-offset-3"style="text-align:right;position:fixed ">
			

					<!-- Logo -->
				
						<nav class="navbar navbar-default nav_menu" id="nav" role="navigation">
				     <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
				     </div> 
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav">
                            <!--  GET NAV MENU // -->
                 
								<li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="">講師文章</span></a></li>
								<li><a href="#portfolio" id="portfolio-link" class="skel-layers-ignoreHref"><span class="">文章分類</span></a></li>
								<li><a href="#about" id="about-link" class="skel-layers-ignoreHref"><span class=""></span>講師影音</a></li>
								<li><a href="#contact" id="contact-link" class="skel-layers-ignoreHref"><span class="">學員心得</span></a></li>
									   
				  					     
				   </ul>
                         				
			    </div></nav>
					<!-- Nav -->
						

				
				</div>
				</div>

			</div>
  <div id="top_test"></div>

 <div class="desktops" id="htz_right"></div>
  <div class="desktops" id="htz_left"></div>

</body>