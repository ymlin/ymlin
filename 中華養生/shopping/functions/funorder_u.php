<?php @require_once("../conn/wp-c.php");?>
<?php @require_once("EDcart.php");?>
<?php @require_once("Getcart.php");?>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script> //<!-- 禁止回上頁的功能 -->
	function forward() { 
	    setTimeout("forward()",100); 
	    history.forward(); 
	} 
	if (document.all) 
	    history.forward();  //for ie 
	else     
	    setTimeout("forward()",100);  //for firefox 
</script>
<?php
/*----------------------------------------------------------------------------*/
header("Content-Type:text/html; charset=UTF-8");
    if (!isset($_SESSION)) {session_start();}
/*---- send DB ----*/
function sqldb($SQLdata){
         global $wp_c;
           $Result1 = mysqli_query($wp_c, $SQLdata) or die(mysqli_error());
    }
/*----------------------------------------------------------------------------*/
/*---- product_tw DB ID ----*/
function sqldbno(){
         global $wp_c;
           $sql_pro = "SELECT Max(od_id) as max_id3 FROM oders";
           $Result2 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row_pro = mysqli_fetch_assoc($Result2);
           $maxid3 = $row_pro['max_id3'] + 1;
             return $maxid3;
           mysqli_close($Result2);
}
/*----------------------------------------------------------------------------*/
/*---- product_t DB ----*/
function sql_product($SQLdata){
         global $wp_c;
           $sql_pro = "SELECT * FROM product_tw WHERE pro_tID = '{$SQLdata}'";
           $Result1 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row = mysqli_fetch_assoc($Result1);
           $prices= $row['pro_tPrice2'];
             return $prices;
           mysqli_close($Result1);
}
/*----------------------------------------------------------------------------*/
/*---- oders DB ID ----*/
function oder_other(){
         global $wp_c;
           $sql_pro = "SELECT Max(od_id) as max_id1 FROM oders";
           $sult3 = mysqli_query($wp_c, $sql_pro) or die(mysqli_error());
           $row = mysqli_fetch_assoc($sult3);
           $maxid1 = $row['max_id1'];
             return $maxid1;
           mysqli_close($sult3);
}
/*----------------------------------------------------------------------------*/
/*---- oders uservip DB ----*/
function oder_mvip($m_na, $m_vdb, $m_id){
           global $wp_c;
           $SQLdata1 = "UPDATE uservip SET " . $m_na . " = '{$m_vdb}' WHERE m_id = '{$m_id}'";
           $sult1 = mysqli_query($wp_c, $SQLdata1) or die(mysqli_error());
}
/*----------------------------------------------------------------------------*/
/*---- oders oders_address DB ----*/
function oder_add($odid, $mid){
           global $wp_c;
           $SQLdata2 = "INSERT INTO oders_address (od_id, m_id) VALUES ('{$odid}', '{$mid}')";
           $sult2 = mysqli_query($wp_c, $SQLdata2) or die(mysqli_error());
}
/*----------------------------------------------------------------------------*/
/*---- oders oders_address updata DB ----*/
function oder_add_up($od_na, $od_db, $odid, $mid){
         global $wp_c;
           $SQLdata3 = "UPDATE oders_address SET " . $od_na . " = '{$od_db}' WHERE (od_id = '{$odid}' and m_id = '{$mid}')";
           $sult1 = mysqli_query($wp_c, $SQLdata3) or die(mysqli_error());
}
/*----------------------------------------------------------------------------*/
/*---- get subsidy DB----*/
function sql_subsidy_db($dbname, $sub_id){
         global $wp_c;
    
           $sql_uses = "SELECT ". $dbname ." AS sqlname FROM subsidy WHERE sub_id = '{$sub_id}'";
           $uses = mysqli_query($wp_c, $sql_uses) or die(mysqli_error());
           $row_s = mysqli_fetch_assoc($uses);
             $sub_na = $row_s['sqlname'];
         return $sub_na;
}
/*----------------------------------------------------------------------------*/
/*---- updata subsidy ----*/
function subsidy_updata($sub_na, $odr_db, $sub_id){
         global $wp_c;
    
           $SQLdata4 = "UPDATE subsidy SET " . $sub_na . " = '{$odr_db}' WHERE sub_id = '{$sub_id}'";
           $sult4 = mysqli_query($wp_c, $SQLdata4) or die(mysqli_error());
         return $sult4;
}
/*----------------------------------------------------------------------------*/
/*---- DB shipping ----*/
function odr_shipping($dbname, $shpid){
           global $wp_c;
            
             $sqlna = 'od_sh_' . $dbname;
           $sql_shp = "SELECT ". $sqlna ." AS shpname FROM oders_shipping WHERE od_sh_id = '{$shpid}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
    
        return $row['shpname'];
}
/*----------------------------------------------------------------------------*/
function odr_shipping_1($subtotal, $proid){
         global $wp_c;
    
           $sql_shp = "SELECT pro_fre_price FROM product_freight WHERE pro_tID = '{$proid}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
            //品牌運費上限查詢
           $sql_cat = "SELECT brand_category.brcate_shipping FROM brand_category LEFT JOIN product_tw ON product_tw.brcate_id = brand_category.brcate_id WHERE product_tw.pro_tID = '{$proid}'";
           $cat1 = mysqli_query($wp_c, $sql_cat) or die(mysqli_error());
           $row1 = mysqli_fetch_assoc($cat1);
            $cat_shi = $row1['brcate_shipping'];
            //運費上限比較
         if($subtotal < $cat_shi){
             $total_shp = $row['pro_fre_price'];
         }else{
             $total_shp = 0;
         }
        return $total_shp;
}
/*----------------------------------------------------------------------------*/
/*---- DB shipping ----*/
function odr_shipping_3($proid){
         global $wp_c;

           $sql_vie = "SELECT * FROM product_tw LEFT JOIN product_freight ON product_freight.pro_tID = product_tw.pro_tID WHERE product_tw.pro_tID = '{$proid}'";
           $view = mysqli_query($wp_c, $sql_vie) or die(mysqli_error());
           $row = mysqli_fetch_assoc($view);
    
           return $row['od_sh_id'];
}
/*----------------------------------------------------------------------------*/
//---- shipping 運費 DB ----;
function sql_outshpping2($SQLdata){
         global $wp_c;
    
           $sql_shp = "SELECT * FROM product_freight WHERE pro_tID = '{$SQLdata}'";
           $ult1 = mysqli_query($wp_c, $sql_shp) or die(mysqli_error());
           $row = mysqli_fetch_assoc($ult1);
          return $row['od_sh_id'];
}
/*----------------------------------------------------------------------------*/
//---- creat oders -----;
/*----------------------------------------------------------------------------*/
if ((isset($_POST['cr_od_send'])) && ($_POST['cr_od_send'] == "cr_od_send")){
               //--指定正確的時區;
                  date_default_timezone_set('Asia/Taipei');
                $od_date = date("Y-m-d H:i:s");
               //---- 訂單編號產生 ----; 
                $Opro = "BGL0".date("md").sqldbno();
                $_SESSION['Oderp'] = $Opro;
               
    
              //---- GET user data --;
               if((isset($_SESSION['cr_v_id']))&&($_SESSION['cr_v_id'] !== ""))
                   {$cr_v_id = $_SESSION['cr_v_id'];}else{$cr_v_id = 0;}
               if((isset($_SESSION['cr_v_ph']))&&($_SESSION['cr_v_ph'] !== ""))
                   {$cr_v_ph = $_SESSION['cr_v_ph'];}else{$cr_v_ph = 0;}
               if((isset($_SESSION['cr_v_ad']))&&($_SESSION['cr_v_ad'] !== ""))
                   {$cr_v_ad = $_SESSION['cr_v_ad'];}else{$cr_v_ad = 0;}
    
             //---- updata uservip--; 
               if($cr_v_ph > 0){oder_mvip('m_phone', $cr_v_ph, $cr_v_id);}
               if($cr_v_ad > 0){oder_mvip('m_address', $cr_v_ad, $cr_v_id);}
    
              //---- E-Coupon ----;
               if((isset($_SESSION['sub_id']))&&($_SESSION['sub_id'] >= 1))
                   {$sub_id = $_SESSION['sub_id'];}
               else{$sub_id = 0;}
                   $total_2 = $cart->grandtotal2;//扣E-Coupon;不含運;

               //---- shipping 運送方式, 支付方式 and shipping 便利店號suid, st_name----;
                 $cr_pa = 0;
                 $cr_total = 0;
                 if(isset($_SESSION['cr_o_pa']))
                   {$cr_pa = $_SESSION['cr_o_pa'];}//paying;

                 if($cart->itemcount > 0) {
	               foreach($cart->get_contents() as $item) {
                     $od_shiid = odr_shipping_3($item['proid']);
                     $ship_na = odr_shipping('name', $od_shiid);//運費名稱;
                    if($od_shiid == '4'){
                       break;//跳開迴圈;
                     } 
                   }
                 }         
                 $cr_total = $cart->grandtotal3;//合計訂購 扣除價金 (含運:yes or no);
    
                 $SQLdata3 = ("INSERT INTO oders (od_no, od_total, od_paying, od_shippers, od_date) VALUES ('{$Opro}', '{$cr_total}', '{$cr_pa}', '{$ship_na}', '{$od_date}')");
                 sqldb($SQLdata3);

                 $cr_oid= oder_other();//oder_id to oders_other and order_address;
               //-----------  寫入訂購 order product other -----------;
               if($cart->itemcount > 0) {
	             foreach($cart->get_contents() as $item) {
                     $cr_prid = $item['proid'];
                     $cr_que = $item['qty'];
                     $cr_price2= $item['price'];
                     $cr_sum1= $item['subtotal'];
                     $cr_freg= $item['freight'];
                     
                   $SQLdata4 = ("INSERT INTO oders_other (od_id, pro_tid, pro_ot_tPrice2, od_ot_que, od_ot_sum) VALUES ('{$cr_oid}', '{$cr_prid}', '{$cr_price2}', '{$cr_que}', '{$cr_sum1}')");
                   sqldb($SQLdata4);
                 }
/*----------------------------------------------------------------------------*/
               //-----------  寫入訂購 order other 2 shipping運費 -----------;
	             foreach($cart->get_bread() as $brem){
                   $ot_brcatid = $brem['brid'];
                   $ot_proid = $brem['proid'];
                   $ot_freg= odr_shipping_1($brem['total'],$ot_proid);
                   $ot_shpid= sql_outshpping2($ot_proid);
                     
                   $SQLdata5 = ("INSERT INTO oders_other_2 (od_id, brcate_id, od_sh_id, od_fr_freight) VALUES ('{$cr_oid}', '{$ot_brcatid}', '{$ot_shpid}', '{$ot_freg}')");
                   sqldb($SQLdata5);
                 } 
               }  
    
             //----------- insert oders address order 編號, user_id -----------; 
                oder_add($cr_oid, $cr_v_id);//寫入;
             //---- updata oders address--;
             if(isset($_SESSION['cr_f_na'])){
                $cr_f_na = $_SESSION['cr_f_na'];
                $cr_f_ph = $_SESSION['cr_f_ph'];
                $cr_f_adds = $_SESSION['cr_f_ad'];
                $cr_f_tx = $_SESSION['cr_f_tx'];
                 
               if($cr_f_na != ""){oder_add_up('od_ad_rec_name', $cr_f_na, $cr_oid, $cr_v_id);}
               if($cr_f_ph != ""){oder_add_up('od_ad_rec_tel', $cr_f_ph, $cr_oid, $cr_v_id);}
               if($cr_f_adds != ""){oder_add_up('od_ad_rec_add', $cr_f_adds, $cr_oid, $cr_v_id);}
               if($cr_f_tx != ""){oder_add_up('od_ad_rec_text', $cr_f_tx, $cr_oid, $cr_v_id);}
             }
            
             //---- E-Coupone recode ----;
             if($sub_id > 0){
                subsidy_updata('od_id',$cr_oid , $sub_id);
                subsidy_updata('od_no',$Opro , $sub_id);
                subsidy_updata('sub_to_date',$od_date , $sub_id);
             }
             //-- send email--;
              $em = base64_encode('emailsend');
             //-- 帶入訂單資料 --;
              $od = $cr_oid;//訂單id;
              require_once("funorder_mail.php");
             //--清空cart all;
              unset($_SESSION['edCart']);
}
header("Location:./funsend.php?EM=" . $em);
exit;
?>