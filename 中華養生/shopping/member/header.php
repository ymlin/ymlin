<?php
if (!isset($_SESSION)) { session_start();}
    //---- 會員指向 ----;
    if (isset($_GET['accesscheck'])) {$_SESSION['PrevUrl'] = $_GET['accesscheck'];} 
$dir_s= "../";
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data['titles'];?></title>
    <meta charset="utf-8">
    <meta name="keywords" content="<?php echo $data['keyw'];?>"> 
    <meta name="description" content="<?php echo $data['desc'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="../css/style1.css" >
    <link rel="stylesheet" type="text/css" href="../css/style-desktop.css" >
    <link rel="shortcut icon" href="../img/la-16x16.png"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- jQuery 往上面 -->
    <script src="../js/jquery.easing.1.3.js"></script>
    <!-- jQuery 表單驗證 -->
    <script src="../js/jquery.validate.js"></script>
</head>
<body>
<div id="wrap"> <!--最外層包裝 //-->
  <div id="header-nav"><!--頁首 bar//--> 
     <div class="container">
       <div class="row">
        <div class="col-lg-12 top_bar2">
           <?php nav_menu_sider(1,$uservip);?>
        </div>
       </div>
     </div>
  </div>
  <div id="header-wrapper"><!--頁首 //--> 
     <div class="container">
       <div class="row column">
        <header id="header">
          <div class="mobile">
             <div id="logo_mob" class="col-md-12">
               <a href="<?php info_url('../');?>"><img alt="" src="../img/logo_color2.png" class="img-polaroid"/></a>
             </div>
          </div>
          <div class="desktop">
		    <div id="logo">
               <a href="<?php info_url('../');?>"><img alt="" src="../img/logo_color3.png" class="img-polaroid" /></a>
             </div>
             <!-- Only desktop -->
             <nav id="nav">
                <?php nav_menu_cart(1,$cart->total,count($cart->get_contents()));?>
             </nav>
          </div>
        </header>
       </div>
     </div>
  </div>
  <div id="banner-wrapper" class="headnav" style="z-index:9999;">
     <!---- new bar //----> 
     <div class="container headnav2">
        <div class="row clearfix">
		   <div class="col-md-12 column">
              <nav class="navbar navbar-default nav_menu" role="navigation">
                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav nav_font2">
                          <?php t_bar1('./');?><!--  GET NAV MENU // -->
					</ul>
                    <ul class="nav navbar-nav navbar-right nav_font2">
					  <?php usevip(1, $uservip);?>
				    </ul>
				 </div>
			  </nav>
		   </div>
	    </div>
     </div>
  </div><!--頁首 結尾 //-->