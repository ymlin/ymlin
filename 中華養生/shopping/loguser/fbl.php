<?php
header('Content-type: text/html; charset=utf-8');
 
if (!isset($_SESSION)) {session_start();}

require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookRedirectLoginHelper.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/GraphUser.php');
require_once('Facebook/GraphSessionInfo.php');

require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'Facebook/Entities/AccessToken.php' );
require_once( 'Facebook/Entities/SignedRequest.php' );
 
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
 
// init app with app id (APPID) and secret (SECRET)
FacebookSession::setDefaultApplication('1558321007742053','b4c251be1a3be9a526884cf34647c9e7' );
 
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper( 'http://localhost/shopping/loguser/fbl.php' );
 
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
 
// see if we have a session
if ( isset( $session ) ) {
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();

    
  // print data
  echo  print_r( $graphObject, 1 );
    echo '<br>';
    echo $graphObject->getProperty('name').'<br>';
    echo $graphObject->getProperty('email').'<br>';
    echo $graphObject->getProperty('id').'<br>';
    echo $udbemail = base64_encode($graphObject->getProperty('email'));
    $udbname = base64_encode($graphObject->getProperty('name'));
    $udbid= base64_encode($graphObject->getProperty('id'));
    //header("Location:REfresh_other.php?fu=" . $udbemail."&n=" . $udbname."&d=" . $udbid);
    //exit;
     echo $fblg = '<a href="' . $helper->getLogoutUrl( $session, 'http://localhost/shopping/logout.php' ) . '">Facebook Logout</a>';
} else {
  // show login url
  //'<a href="' . $helper->getLoginUrl() . '">Login</a>';
  echo $fblg = '<a href="' . $helper->getLoginUrl( array( 'email', 'user_friends' ) ) . '"><img src="../img/fb-connect.png"  alt="facebook login" /></a>';
}
?>