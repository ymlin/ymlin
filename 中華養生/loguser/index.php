<?php @require_once("../con_inc/dbtools.inc.php");?>
<?php @require_once("../functions/fundb.php");?>
<?php @require("../functions/functions.php");?>
<?php @require_once("googlelogin.php");?>
<?php @require("header.php");?>
  <!-- feature -->
  <div id="features-wrapper">
    <article class="container">
       <div class="row">
        <div class="col-lg-12">
           <div class="reg_list">
              <div class="col-md-12 top_banner_cont">會員登入</div>
              <div class="col-md-12 hightline">
                <form class="form-horizontal" action="REfresh.php" method="post" name="commentForm" id="from1">
                 <div class="col-md-6">
                     <div class="col-sm-offset-1 font2">使用LaSulife帳號登入</div>
                     <div class="col-sm-10 col-sm-offset-1">
                       <!-- 訊息處理 --> 
                       <?php if(isset($_GET['Msg'])){errmess($_GET['Msg']);}?>
                     </div>
				     <div class="col-sm-12">
					    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
					    <div class="col-sm-9">
						   <input id="mm_username" name="mm_username" placeholder="請輸入Email" class="input25" required="" type="text" value="<?php if(isset($_COOKIE['username'])) echo $_COOKIE['username'];//帳號; ?>">
					    
                           <h5>首次加入的朋友，<a href="use_regedit.php" class="ong_colo">請先註冊新團員。</a></h5>
                        </div>
				     </div>
				     <div class="col-sm-12">
					    <label for="inputPassword3" class="col-sm-3 control-label">密碼</label>
					    <div class="col-sm-9">
						   <input id="mm_passwd" name="mm_passwd" placeholder="請輸入密碼" class="input25" required="" type="password" value="<?php if(isset($_COOKIE['password'])) echo $_COOKIE['password'];//密碼; ?>">
                        <h5>忘記密碼？<a href="use_forget.php" class="ong_colo">請點我</a></h5>
					    </div>
				     </div>
                     <div class="col-sm-12">
					    <label for="inputPassword3" class="col-sm-3 control-label">圖形驗證
</label>
					    <div class="col-sm-9">
						   <input id="cas" name="cas" placeholder="請輸入下方驗證碼" class="input25 required" autocomplete="off" type="text"><a href="index.php" target="_parent" class="ong_colo"><h5><img src="pic.php" width="150"/> 更換一組驗證碼</h5></a>
					    </div>
				     </div>
				     <div class="col-sm-12">
                         <div class="col-sm-12 col-sm-offset-2">
				            <input name="rememberMe" id="rememberMe" type="checkbox"/> 記住我
				         </div>
				     </div>
                     <!-- submit -->
				     <div class="col-sm-12">
					    <div class=" col-sm-10 col-sm-offset-2">
						   <button name="MM_log" id="MM_log" value="MM_log" type="submit" class="btn">登入</button>
					    </div>
				     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="font2">使用其它帳號登入</div>
                     <div class="spn1"><a href="fblogin.php"><img src="../img/fb-connect.png"  alt="facebook login" /></a></div>
                     <div class="spn1"><?php print $gogllg;?></div>
                     <div class="lines hline2"><u><strong>提醒您，若您是以其它帳號登入時：</strong></u>
                          <p>需補填部分個人資料，進行帳號開通流程。</p>
                          <p>其它帳號為「Facebook,Google」等帳號一經開通後，
                             日後即可使用這組帳號來登入並且保障您的隱私。</p></div>
                  </div>
			    </form>
              </div>
            </div>
         </div>
       </div>
    </article>
  </div><!--內容 結尾-->
</div><!--最外層包裝 END-->
<?php @require_once("footer.php");?>
<?php end_sql($end_sqls);?><!----- END sql ----->