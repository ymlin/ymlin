<?php
/*---- user bar 管理 ----*/
function t_bar1($msg){
        echo <<<"s1"
                     <li><a href="{$msg}index.php"><div class="mread">您的訂單頁面 |</div></a></li>
         		     <li><a href="{$msg}use_personal.php"><div class="mread">個人資料 |</div></a></li>
                     <li><a href="{$msg}use_page.php"><div class="mread">您的E-Coupon |</div></a></li>
s1;
}
/*----------------------------------------------------------------------------*/
/*---- orders view ----*/
function use_ordview($use_id, $papg1){ 
        global $wp_c;
    
          $sql = "SELECT * FROM oders LEFT JOIN oders_address ON oders.od_id = oders_address.od_id WHERE oders_address.m_id = '{$use_id}' ORDER BY oders.od_id DESC"; //修改成你要的 SQL 語法
          $result = mysqli_query($wp_c, $sql) or die(mysqli_error());
          $data_nums = mysqli_num_rows($result); //統計總比數
    
          $per = 10; //每頁顯示項目數量
          $pages = ceil($data_nums/$per); //取得不小於值的下一個整數
          if (isset($papg1) == 1){ //假如$_GET["page"]未設置
             $page=1; //則在此設定起始頁數
          }else{$page = intval($papg1);} //確認頁數只能夠是數值資料
          $start = ($page-1)*$per; //每一頁開始的資料序號
          $result = mysqli_query($wp_c, $sql.' LIMIT '.$start.', '.$per) or die(mysqli_error());
    
         while ($row_viw = mysqli_fetch_array ($result)){
             $viw_id= $row_viw['od_id'];
              $vid= base64_encode($viw_id);//加密;
             $viw_date= $row_viw['od_date'];
             $viw_total= number_format($row_viw['od_total']);
              /* 將數字每隔三位加上逗號http://php.net/manual/zh/function.number-format.php */
             $viw_no= $row_viw['od_no'];
             $viw_sh_no= $row_viw['od_shi_no'];
             $viw_payno = $row_viw['od_pay_no'];
             $viw_payno = $row_viw['od_pay_no'];
             $viw_payno = $row_viw['od_pay_no'];
             $viw_state= $row_viw['od_state'];
             $viw_stat_ti= $row_viw['od_state_time'];
          echo <<<"tab"
                  <table class="table table-bordered table-condensed text-center">
                  <thead>
                     <tr style="background-color:#e8e6e5">
                        <th width="10%" class="text-center">訂單編號</th>
						<th width="20%" class="text-center">訂單日期</th>
						<th width="10%" class="text-center">訂單總金額</th>
                        <th width="20%" class="text-center">匯款帳號後五碼</th>
                        <th width="20%" class="text-center">貨運單號</th>
                        <th width="20%" class="text-center">運送狀態</th>
					 </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td><a href="use_view.php?d={$vid}">+ {$viw_no}</a></td>
                        <td>{$viw_date}</td>
                        <td><asy class="asy5_2">NT$ {$viw_total} 元</asy></td>
                        <td>
                           <input id="od_d[]" name="od_d[]" value="{$vid}" type="hidden">
                           <input name="pay_no[]" placeholder="{$viw_payno}" type="text" class="input5" />
                           <button id="pay_send" name="pay_send" value="uspayshi" class="btn btn-xs">確定帳號</button>
                        </td>
                        <td>{$viw_sh_no}</td>
                        <td>{$viw_state}<br>{$viw_stat_ti}</td
                     </tr>
                  </tbody>
                  </table>
tab;
         }
          echo '<div class="pagenav">';
          echo '共 '.$data_nums.' 筆-在 '.$page.' 頁';
          echo "<br /><a href=?page=1>[第一頁]</a> ";
          for( $i=1 ; $i<=$pages ; $i++ ) {
             if ( $page-3 < $i && $i < $page+3 ) {
                echo "<a href=?page=".$i.">[".$i."]</a> ";
              }
          } 
          echo " <a href=?page=".$pages."> >>[最後一頁]</a><br /><br />";
          echo "</div>";
}
/*----------------------------------------------------------------------------*/
/*---- 查無資料 back ----*/
function use_pro_oth_chk($viw_id, $use_id){
        global $wp_c;
             $sql_sch3 = "SELECT COUNT(*) FROM oders_address WHERE (od_id = '{$viw_id}' AND m_id = '{$use_id}')";
             $qu_sch3 = mysqli_query($wp_c, $sql_sch3) or die(mysqli_error());
             $row_sch3 = mysqli_fetch_assoc($qu_sch3);
               $conu= $row_sch3['COUNT(*)'];
         return $conu;
}
/*----------------------------------------------------------------------------*/
/*---- Member pass data ----*/
function use_mem_ps(){
         echo <<<"mem"
              <div class="form-group">
                 <label class="col-sm-3 bg-danger text-center font5">登入密碼</label>
                 <div class="col-sm-8">
                    <input id="password" name="password" class="input15" type="password">
                    <spen style="color:#ff4500;">*必要輸入</spen>
                 </div>
              </div>
mem;
}
/*----------------------------------------------------------------------------*/
/*---- Member data ----*/
function use_mem_viw($titel, $db_na, $use_id){
        global $wp_c;

            $dbname= "m_" . $db_na;
          $sql_usr = "SELECT ". $dbname ." AS sqlna FROM uservip WHERE m_id = '{$use_id}'";
          $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
          $row_usr = mysqli_fetch_assoc($us_usr);
            $us_name = $row_usr['sqlna'];
    
          //-- 分類 view --;
          switch($db_na){
            case "NO":
              echo <<<"mem"
               <div class="form-group">
                  <label class="col-sm-2 text-center font5">{$titel}</label>
                  <div class="col-sm-9">$us_name</div>
                </div>
mem;
            break;
            case "address":
                //---- GET address --;
                  $sql_usr = "SELECT m_address FROM uservip WHERE m_id = '{$use_id}'";
                  $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
                  $row_usr = mysqli_fetch_assoc($us_usr);
                     $us_add = $row_usr['m_address'];
                     
              echo <<<"mem"
               <div class="form-group">
                  <label class="col-sm-2 text-center font5">{$titel}</label>
                  <div class="col-sm-7">
                     <span id="select_add1"></span><input id="addr" name="addr" placeholder="{$us_add}" value="{$us_add}" class="input45" type="text">
                  </div>
                </div>
mem;
            break;
            default:
              echo <<<"mem"
               <div class="form-group">
                  <label class="col-sm-2 text-center font5">{$titel}</label>
                  <div class="col-sm-9">
                     <input id="{$dbname}" name="{$dbname}" placeholder="{$us_name}" value="{$us_name}" class="input25" type="text">
                  </div>
                </div>
mem;
           }
}
/*----------------------------------------------------------------------------*/
/*---- 查product, order_other data ----*/
function use_pro_oth_view($viw_id, $use_id){
        global $wp_c;
              
             $sql_sch2 = "SELECT * FROM oders_other LEFT JOIN product_tw ON oders_other.pro_tid = product_tw.pro_tid LEFT JOIN oders_address ON oders_other.od_id = oders_address.od_id WHERE (oders_other.od_id = '{$viw_id}' AND oders_address.m_id = '{$use_id}')";
             $qu_sch2 = mysqli_query($wp_c, $sql_sch2) or die(mysqli_error());
             $row_sch2 = mysqli_fetch_assoc($qu_sch2);

             do{
                $pro_name= $row_sch2['pro_tName'];
                $ord_price2= $row_sch2['pro_ot_tPrice2'];
                $ord_que= $row_sch2['od_ot_que'];
                 $tota= number_format($ord_que*$ord_price2);
                $ord_freg= $row_sch2['od_ot_freight'];
                
              echo <<<"vitab"
                   <tr>
                      <td>{$pro_name}</td>
                      <td>{$ord_que}件</td>
                      <td class="text-right"><asy class="asy5_2">NT$ {$tota}元</asy></td>
                    </tr>
vitab;
             } while ($row_sch2 = mysqli_fetch_assoc($qu_sch2));
}
/*----------------------------------------------------------------------------*/
/*-- GET 運費 --*/
function use_od_freight($odrid){
        global $wp_c;
           
             $sql_fre = "SELECT * FROM oders_other_2 LEFT JOIN brand_category ON oders_other_2.brcate_id = brand_category.brcate_id LEFT JOIN oders_shipping ON oders_shipping.od_sh_id = oders_other_2.od_sh_id WHERE oders_other_2.od_id = '{$odrid}'";
             $us_fre = mysqli_query($wp_c, $sql_fre) or die(mysqli_error());
             $row_fre = mysqli_fetch_assoc($us_fre);

         do {
             $barcat_na = $row_fre['brcate_name'];
             $barcat_preice = $row_fre['od_fr_freight'];
             $barcat_shp = $row_fre['od_sh_name'];
           echo <<<"vfre"
               <tr>
               <td colspan="2">品牌運費: {$barcat_na}  ({$barcat_shp}-配送)</td>
               <td class="text-right">NT$ {$barcat_preice}元</td>
               </tr>
vfre;
         } while($row_fre = mysqli_fetch_assoc($us_fre)); 
}
/*----------------------------------------------------------------------------*/
/*---- GET order pay shiper data ----*/
function use_pay_ship($odrid){
         global $wp_c;
           
             $sql_usr = "SELECT * FROM oders WHERE od_id = '{$odrid}'";
             $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
             $row_usr = mysqli_fetch_assoc($us_usr);
               
               $od_pay = $row_usr['od_paying'];
               $od_payno = $row_usr['od_pay_no'];
               $od_shi = $row_usr['od_shippers'];
               $od_shino = $row_usr['od_shi_no'];
               $od_shino = $row_usr['od_shi_no'];
               $od_state= $row_usr['od_state'];
               $od_stat_ti= $row_usr['od_state_time'];

             echo <<<"pashi"
                <tr>
                  <td>{$od_pay}</td>
                  <td>{$od_payno}</td>
                  <td>{$od_shi}</td>
                  <td>{$od_shino}</td>
                  <td>{$od_state}<br>{$od_stat_ti}</td>
                </tr>
pashi;
}
/*----------------------------------------------------------------------------*/
/*---- GET order edit---- */
function use_invoice($odrid){
         global $wp_c;
           
            $sql_usr = "SELECT * FROM oders WHERE od_id = '{$odrid}'";
            $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
            $row_usr = mysqli_fetch_assoc($us_usr);
               
               $od_invo = $row_usr['od_invoice'];
               $od_invono = $row_usr['od_invoice_no'];
               $od_com = $row_usr['od_company'];
               $od_comna = $row_usr['od_company_na'];
    
            echo <<<"pashi"
                  <td>{$od_invo}</td>
                  <td>{$od_invono}</td>
                  <td>{$od_com}</td>
                  <td>{$od_comna}</td>
pashi;
}
/*---- Member subsidy data ----*/
function use_sub_page($use_id){
        global $wp_c;
    
             $sql_usr = "SELECT * FROM subsidy LEFT JOIN subsidy_data ON subsidy.subda_id = subsidy_data.subda_id WHERE subsidy.m_id = '{$use_id}'";
             $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
             $row_usr = mysqli_fetch_assoc($us_usr);

             $cnc =0;
         do {
              $su_id = $row_usr['sub_id'];
              $su_name = $row_usr['subda_name'];
              $su_pri = number_format($row_usr['sub_price']);
              $vid= base64_encode($row_usr['od_id']);//加密;
              $su_odno = $row_usr['od_no'];
              $su_date = $row_usr['sub_date'];
              $su_todate = $row_usr['sub_to_date'];
              $su_limitdate = $row_usr['sub_limitdate'];
             
                //--指定正確的時區;
                date_default_timezone_set('Asia/Taipei');
              $sql_Date = date("Y-m-d");
            if($su_limitdate < $sql_Date){$su_limitdate = '<h5 class="re_colo">'.$su_limitdate.'</h4>';}
         echo <<<"sub"
                 <td>{$su_date}</td>
                 <td>{$su_limitdate}</td>
                 <td>{$su_name}</td>
                 <td><asy class="asy5_2">NT$ {$su_pri}元</td>
                 <td><a href="use_view.php?d={$vid}" style="color:#313131">{$su_odno}</a></td>
                 <td>{$su_todate}</td>
sub;
             $cnc ++;if($cnc%1==0){echo '</tr><tr>';}     
         } while ($row_usr = mysqli_fetch_assoc($us_usr)); 
}
/*----------------------------------------------------------------------------*/
/*---- Member subsidy order data ----*/
function use_sub_view_order($odrid, $use_id){
        global $wp_c;
    
             $sql_usr = "SELECT * FROM subsidy LEFT JOIN subsidy_data ON subsidy.subda_id = subsidy_data.subda_id WHERE (subsidy.m_id = '{$use_id}' AND subsidy.od_id = '{$odrid}')";
             $us_usr = mysqli_query($wp_c, $sql_usr) or die(mysqli_error());
             $row_usr = mysqli_fetch_assoc($us_usr);
             $num_usr = mysqli_num_rows($us_usr);
    
        if($num_usr > 0){    
          echo <<<"subth"
              <table class="table">
                 <thead>
                    <th>抵扣日期</th>
                    <th>抵扣訂單號</th>
                    <th>E-Coupon</th>    
                    <th>E-Coupon/元</th>
                 </thead>
                 <tbody>
subth;
                $cnc =0;
           do {
              $su_id = $row_usr['sub_id'];
              $su_name = $row_usr['subda_name'];
              $su_pri = number_format($row_usr['sub_price']);
              $vid= base64_encode($row_usr['od_id']);//加密;
              $su_no = $row_usr['od_no'];
              $su_date = $row_usr['sub_date'];
              $su_todate = $row_usr['sub_to_date'];
             
           echo <<<"sub"
                 <td>{$su_todate}</td>
                 <td>{$su_no}</td>
                 <td>{$su_name}</td>
                 <td width="10%"><asy class="asy5_2">NT$ ({$su_pri})元</asy></td>              
sub;
             $cnc ++;if($cnc%1==0){echo '</tr><tr>';}     
           } while ($row_usr = mysqli_fetch_assoc($us_usr)); 
           echo '</tbody></table><hr>';
        }//IF END;
}
/*----------------------------------------------------------------------------*/
function use_shippers($ordid){
        global $wp_c;
    
             $sql_ord = "SELECT * FROM oders_address WHERE od_id = '{$ordid}'";
             $us_ord = mysqli_query($wp_c, $sql_ord) or die(mysqli_error());
             $row_ord = mysqli_fetch_assoc($us_ord);
                $add_name = $row_ord ['od_ad_rec_name'];
                $add_tel = $row_ord ['od_ad_rec_tel'];
                $add_adr = $row_ord ['od_ad_rec_add'];
                $add_text = $row_ord ['od_ad_rec_text'];
        
           echo <<<"sub"
                <strong>收件人 : {$add_name}</strong><br>
                <strong>連絡電話 : {$add_tel}</strong><br>
                <p>收件人地址 : {$add_adr}</p>
                <p>備註 : {$add_text}</p>            
sub;
}
?>